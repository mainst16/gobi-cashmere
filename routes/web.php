<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::post('logout', function(){
	Auth::logout();
	return redirect()->action('HomeController@index');
});
Route::get('/', function(){
	return view('nuur');
});
Route::get('newspaper', 'AboutController@newspaper');
Route::get('mn', 'HomeController@index');

Route::get('account', 'HomeController@alogin');
Route::get('account/register', 'HomeController@areg');
Route::post('account/aregister', 'HomeController@createreg');

//Route::get('products/{id}/{pt_id}', 'ProController@index');
Route::get('products/{brand}/{gender}/{pt}', 'HomeController@products');

Route::get('atrum', 'HomeController@atrum');

Route::get('yama', 'HomeController@yama');


Route::get('organic', 'HomeController@organic');

Route::get('kids', 'HomeController@kids');

Route::get('now', 'HomeController@now');
Route::get('gift','HomeController@gift');
Route::get('now/{nm_id}', 'HomeController@nowmenu');
Route::get('now/post/{id}', 'HomeController@nowpost');

Route::get('details/{brand}/{product}', 'HomeController@items');

Route::get('brand/{brand}/{gender}', 'HomeController@allpros');

Route::get('collections/{brand}', 'HomeController@coll');
Route::get('collection/{brand}/{coll_id}', 'HomeController@collitems');

// Bidnii tuhai
Route::get('about', 'AboutController@index');
Route::get('about/jobs/ажлын-байр', 'AboutController@jobs');
Route::get('about/view-jobs/{id}/ажлын-байр', 'AboutController@vjobs');
Route::get('about/нийгмийн/хариуцлага', 'AboutController@nh');
Route::get('about/тендер', 'AboutController@tender');
Route::get('about/brands', 'AboutController@brand');
Route::get('about/branches', 'AboutController@branch');
// Uilchilgee
Route::get('services', 'AboutController@services');
Route::get('services/care', 'AboutController@servicescare');
// tuslaltsaa
Route::get('help/contact', 'AboutController@contact');
Route::get('help/returns', 'AboutController@returns');
Route::get('help/care', 'AboutController@care');

//anket
Route::get('anket', function(){
	return view('contents.other.anket');
});
Route::post('anket_send', 'AboutController@anket_send');

Route::get('хувьцаа-эзэмшигчдэд/{id}','AboutController@huwi');
Route::get('хувьцаа-эзэмшигчдэд/компаний-засаглал/{id}', 'AboutController@zasaglal');
Route::get('хувьцаа-эзэмшигчдэд/хувьцаа-эзэмшигч/{id}', 'AboutController@hezemshigch');
Route::get('хувьцаа-эзэмшигчдэд/төлөөлөн-удирдах-зөвлөл/{id}', 'AboutController@zuwlul');
Route::get('хувьцаа-эзэмшигчдэд/хөрөнгийн-зах-зээл/{id}', 'AboutController@zahzeel');
Route::get('хувьцаа-эзэмшигчдэд/гүйцэтгэх-удирдлага/{id}', 'AboutController@udirdlaga');
Route::get('хувьцаа-эзэмшигчдэд/мэдээ/{id}', 'AboutController@medeelel');

Route::post('registermail', 'HomeController@mail');
Route::post('sendmail', 'HomeController@sendmail');
Route::get('search', 'HomeController@search');

Route::get('sale', 'HomeController@sale');

Route::get('/uploadfile', 'UploadController@index');
Route::post('/uploadfile', 'UploadController@upload');

Route::get('anket-view', 'AboutController@view_anket');
Route::get('anketview/{id}', 'AboutController@viewanket');
// Route::get('products', function () {
//     return view('contents.products');
// });

// Route::get('atrum', function () {
//     return view('contents.atrum');
// });

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});


