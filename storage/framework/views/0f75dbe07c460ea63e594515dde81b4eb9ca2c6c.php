<?php $__env->startSection('style'); ?>
<style type="text/css">
  img {
    -webkit-filter: grayscale(100%); /* Safari 6.0 - 9.0 */
    filter: grayscale(100%);
</style>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<div id="rn_PageTitle" class="rn_AnswerDetail">
  <!-- <div id="rn_SearchControls" class="tb_Mobile_Show">
      <h1 class="tb_Mobile_Title">Frequently Asked Questions</h1>
      <h1 class="rn_ScreenReaderOnly">Search</h1>
      <form onsubmit="return false;">
          <div class="rn_SearchInput">
              <rn:widget path="search/AdvancedSearchDialog"/>
              <rn:widget path="search/KeywordText" label_text="" placeholder_text="Search a frequently asked question" label_text="Find the answer to your question" initial_focus="false"/>
          </div>
          <div class="">
              <rn:widget path="search/SearchButton" icon_path="images/tb_search_icon.png" report_page_url="/app/answers/list"/>
          </div>
      </form>
      <rn:widget path="search/DisplaySearchFilters"/>
  </div> -->
  
</div>
<div class="aboutSubContent" style="font-size: 14px">
  <?php $__currentLoopData = $tuzs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $tuz): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
  <h2><?php echo e($tuz->title); ?></h2>
    <?php if(empty($tuz->pics)): ?>
    
    <?php else: ?>
    <img src="<?php echo e(asset('storage/'.$tuz->pics.'')); ?>" class="img-thumbnail" style="width: 717px">
    <?php endif; ?>
    <?php echo $tuz->desc; ?>

    <hr> 
  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('contents.other.huwitsaa.huwicaa', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>