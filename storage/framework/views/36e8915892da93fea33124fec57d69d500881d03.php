<?php $__env->startSection('css'); ?>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
<?php $__env->stopSection(); ?>

<?php $__env->startSection('content'); ?>
    <div class="page-content container-fluid">
        <div class="row">
            <div class="col-md-12">
            	<table id="example" class="display" style="width:100%">
	              <tr>
	                <th>Ажлын байр</th>
	                <th>Овог</th>
	                <th>Нэр</th>
	                <th>Зураг</th>
	                <th>Actions</th>
	              </tr>
	              <?php $__currentLoopData = $anket1; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $ank1): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
	              <tr>
	                <td><?php echo e($ank1->wish_albantushaal); ?></td>
	                <td><?php echo e($ank1->ovog); ?></td>
	                <td><?php echo e($ank1->ner); ?></td>
	                <td><img style="width: 100px" src="<?php echo e(asset('/imgs/anketpics/'.$ank1->zurag.'')); ?>"></td>
	                <td><a href="<?php echo e(url('anketview/'.$ank1->id.'')); ?>" class="btn btn-sm btn-primary view"><i class="voyager-eye"></i><span class="hidden-xs hidden-sm">View</span></a></td>
	              </tr>
	              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
	            </table>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('javascript'); ?>
<script type="text/javascript" src="<?php echo e(asset('js/jquery.dataTables.min.js')); ?>"></script>
<script>
var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var panel = this.nextElementSibling;
    if (panel.style.maxHeight){
      panel.style.maxHeight = null;
    } else {
      panel.style.maxHeight = panel.scrollHeight + "px";
    } 
  });
}
</script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#example').DataTable({
      "searching": false,
      "lengthChange" : false,
      "language": {
            "info": " Нийт _TOTAL_ үр дүн. Энэ хуудсанд _START_  - _END_",
            "paginate": {
              "previous": "Өмнөх",
              "next": "Дараах"
            }
        }
    });
} );
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('voyager::master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>