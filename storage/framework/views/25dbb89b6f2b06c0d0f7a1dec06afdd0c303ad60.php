<?php $__env->startSection('styles'); ?>
<link href="<?php echo e(asset('css/aboutus.css')); ?>" type="text/css" rel="stylesheet" />
<style type="text/css">
    #form {
  position: relative;
  width: 100%;
}

input {
  font-family: 'SamsungSans', sans-serif;
  font-size: 0.875em;
  width: 99%;
  height: 50px;
  
  background: transparent;
  outline: none;
  color: #726659;
  
  border: solid 1px #b3aca7;
  border-bottom: none;
  
  transition: all 0.3s ease-in-out;
  -webkit-transition: all 0.3s ease-in-out;
  -moz-transition: all 0.3s ease-in-out;
  -ms-transition: all 0.3s ease-in-out;
}

input:hover {
  background: #b3aca7;
  color: #e2dedb;
}

textarea {
  width: 95%;
  height: 110px;
  max-height: 110px;
  padding: 15px;
  
  background: transparent;
  outline: none;
  
  color: #726659;
  font-family: 'SamsungSans', sans-serif;
  font-size: 0.875em;
  
  border: solid 1px #b3aca7;
  
  transition: all 0.3s ease-in-out;
  -webkit-transition: all 0.3s ease-in-out;
  -moz-transition: all 0.3s ease-in-out;
  -ms-transition: all 0.3s ease-in-out;
}

textarea:hover {
  background: #b3aca7;
  color: #e2dedb;
}

#submit {
  width: 100%;
  
  padding: 0;
  margin: 0px 0px 0px 0px;
  
  font-family: 'Lato', sans-serif;
  font-size: 0.875em;
  color: #b3aca7;
  
  outline:none;
  cursor: pointer;
  
  border: solid 1px #b3aca7;
  border-top: none;
  border-left: none;
  border-right: none;
  border-bottom: solid 1px;
}

#submit:hover {
  color: #e2dedb;
}
</style>
    <?php echo $__env->yieldContent('style'); ?>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('pro'); ?>
<div id="main">
    <div id="mainCtnt">
        <div id="leftcolumn" class="nobackground">

            <ul class="mainCtntMenu">
                <li><a class="headerlink" href="<?php echo e(url('help/contact')); ?>">Туслалцаа</a></li>
                <li><a class="navlink" href="<?php echo e(url('help/contact')); ?>">Хобоо барих</a></li>
                <li><a class="navlink" href="<?php echo e(url('help/returns')); ?>">Буцаах солих журам</a></li>
                <li class="lastlink" style="border-bottom:none;"><a class="navlink" href="<?php echo e(url('help/care')); ?>">Ноолуур арчилгаа</a></li>

            </ul>
            <br>
            <ul class="mainCtntMenu">
                <li style="text-align: justify; text-transform: none; padding-left: 15px;">ХОЛБОО БАРИХ<br><br> +(976) 70139977<br><br> Үйлдвэрийн гудамж, 3-р хороо, Хан-Уул дүүрэг, Улаанбаатар - 17062, 
Монгол Улс, Ш/Х 36/434<br><br>marketing@gobi.mn</li>

            </ul>
            <script type="text/javascript" language="javascript">
                jQuery(document).ready( function() {
                            
                    var addr = $(location).attr('href');
                    
                    var names = addr.split("=");
                    names = names[names.length-1];
                    
                    jQuery('.navgroup a[href$='+names[0]+']').css('color', '#D8D8D8');
                    
                    jQuery('.navgroup h3:last').addClass('borderLastNav');
                });
            </script>
            <div class="clear"></div>
        </div>
    <div id="content">
        <div class="breadcrumb" itemprop="breadcrumb">
            <a href="<?php echo e(url('/')); ?>" title="Home">Нүүр</a>
            <span class="divider">/</span><a href="<?php echo e(url('about')); ?>">Бидний тухай</a>
        </div>
        <script language="javascript" type="text/javascript">
            app.currentPage.breadcrumbs = [];
            app.currentPage.breadcrumbs.push("About Us"); 
              
              
        </script>
        <div class="contentasset mobile-available">
            <style type="text/css">.aboutContentWrapper p {font-family:SamsungSans; color:#606061; font-size:12px !important}
            .aboutContentWrapper .thirdColumns a {font-size: 12px;}
            </style>
            <div class="aboutContentWrapper nonBordered">
                <?php if(Request::is('help/contact')): ?>
                
                <div class="pageHeader">&nbsp; <h2 style="font-size: 14px">Холбоо барих</h2>
                    <form action="<?php echo e(url('sendmail')); ?>" id="form" class="topBefore" method="post">
                      <?php echo csrf_field(); ?>

                      <input id="name" type="text" placeholder="Нэр" name="name">
                      <input id="email" type="text" placeholder="Цахим хаяг" name="mail">
                      <textarea id="message" type="text" placeholder="" name="message"></textarea>
                      <input id="submit" type="submit" value="ИЛГЭЭХ">
                      
                    </form>
                </div>
                <?php else: ?>
                <?php echo $__env->yieldContent('content'); ?>
                <?php endif; ?>
            </div>
        </div>
        <div class="mobilecontentasset">
            <style type="text/css">.aboutContentWrapper p {font-family:SamsungSans; text-transform: uppercase;
            </style>
            <div class="aboutContentWrapper nonBordered">
                <div class="pageHeader">&nbsp;
                    <form id="form" class="topBefore">
        
                      <input id="name" type="text" placeholder="Нэр">
                      <input id="email" type="text" placeholder="Цахим хаяг">
                      <textarea id="message" type="text" placeholder=""></textarea>
                      <input id="submit" type="submit" value="ИЛГЭЭХ!">
                      
                    </form>
                </div>
            </div>
        </div>
        <div class="clear"></div>
    </div>
    <div class="clear"></div>
    </div>
</div>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('customscripts'); ?>
    <?php echo $__env->yieldContent('scripts'); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('welcome', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>