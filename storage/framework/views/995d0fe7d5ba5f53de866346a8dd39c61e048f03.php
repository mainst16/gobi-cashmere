<footer>
      <div class="contentasset mobile-not-available"><!-- dwMarker="content" dwContentID="bcJ0aiaag388QaaacQNNUvRbQH" -->
                                    <div class="select" id="select-footer-country">
                                          <ul class="select__list">
                                          </ul>
                                    </div>
                                    <hr>
 
            <div class="footer--links">
                <div class="footer--links-column">
                    <ul class="">
                        <li class="column-header">Туслалцаа</li>
                        <li><a href="<?php echo e(url('help/contact')); ?>">Холбоо барих</a></li>
                        <li><a href="<?php echo e(url('help/returns')); ?>">Буцаах солих журам</a></li>
                        <li><a href="<?php echo e(url('help/care')); ?>">Ноолуур арчилгаа</a></li><!-- 
                        <li id="footerChatLinkContainer" style="display:none;">
                            <div id="footerChatLink">
                                Live Chat
                                <div id="footerChatLinkInfo"></div>
                            </div>
                        </li> -->
                        <li><a href="#"></a></li>
                        <li><a href="#"></a></li>
                    </ul>
                </div>
                <div class="footer--links-column">
                    <ul>
                        <li class="column-header">Үйлчилгээ</li> 
                        <li><a href="<?php echo e(url('services')); ?>">Манай байршил</a></li>
                        <li><a href="<?php echo e(url('services/care')); ?>">Хими цэвэрлэгээ</a></li>
                    </ul>
                    <!-- <ul class="v-offset-top-l">
                                    <li class="column-header">Download Our App</li> 
                        <li class="app-icon-li">
                            <a class="app-icon apple" href="https://itunes.com/apps/torydaily" target="_blank" title="Apple Store" ></a>
                        </li>
                        <li class="app-icon-li">
                               <a class="app-icon android" href="https://play.google.com/store/apps/details?id=com.toryburch.torydaily" target="_blank" title="Android Shop"></a>
                        </li>
                    </ul> -->
                </div>
                <div class="footer--links-column">
                    <ul>
                        <li class="column-header">Бидний тухай</li>    
                        <li><a href="<?php echo e(url('about')); ?>">Говь ХК</a></li>
                        <li><a href="<?php echo e(url('about/brands')); ?>">Брэндүүд</a></li>
                        <li><a href="<?php echo e(url('about/branches')); ?>">Салбар дэлгүүр</a></li>
                        <li><a href="<?php echo e(url('about/jobs/ажлын-байр')); ?>">Ажлын байр</a></li>
                        <li><a href="<?php echo e(url('about/нийгмийн/хариуцлага')); ?>">Нийгмийн хариуцлага</a></li>
                        <li><a href="<?php echo e(url('about/тендер')); ?>">Тендерийн материал</a></li>
                        <li><a href="<?php echo e(url('хувьцаа-эзэмшигчдэд/7')); ?>">Хувьцаа эзэмшигчдэд</a></li>
                    </ul>
                </div>
                <div class="footer--links-column">
                    <ul>
                        <li class="column-header">Сошиал медиа</li>
                        <li>
                            <a href="https://www.facebook.com/GobiCashmere/" target="blank" title="Follow Us on Facebook">Facebook</a>
                        </li>
                        <li>
                            <a href="https://instagram.com/gobi_cashmere" target="_blank" title="Follow Us on Instagram">Instagram</a>
                        </li>
                        <li>
                            <a href="https://twitter.com/GobiCashmere" target="_blank" title="Follow Us on Twitter">Twitter</a>
                        </li>
                        <li>
                            <a href="https://www.youtube.com/channel/UCzUZQnPw4XdIw3_IHq6xywA" target="_blank" title="Follow Us on YouTube">YouTube</a>
                        </li>
                        <li>
                            <a href="https://www.gobicashmere.com" target="_blank">OnlineShop</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="footer--mobile-links">
                <div class="footer--links-column">
                    <div class="column-header js-footer-show-social">Биднийг дага</div>               
                    <ul class="js-footer-social-list">
                        <li>
                            <a href="https://www.facebook.com/GobiCashmere/" target="blank" title="Follow Us on Facebook">Facebook</a>
                        </li>
                        <li>
                            <a href="https://www.instagram.com/gobi_cashmere_mongolia/" target="_blank" title="Follow Us on Instagram">Instagram</a>
                        </li>
                        <li>
                            <a href="https://www.youtube.com/channel/UCzUZQnPw4XdIw3_IHq6xywA" target="_blank" title="Follow Us on YouTube">YouTube</a>
                        </li>
                        <li>
                            <a href="https://www.gobicashmere.com" target="_blank">OnlineShop</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="clear"></div>
            <div class="footer--bottom">
                <div class="footer--bottom-left">
                    <div>&copy; 1981-2018 Gobi Cashmere &nbsp;</div>
                </div>
               <!--  <div class="footer--bottom-right">
                    <div><a href="#">Privacy &amp; Usage</a></div>
                    <div class="middle"><a href="#" title="California Transparency Act">Corporate Governance</a></div>
                    <div><a href="#">Site Map</a></div>
                </div> -->
            </div>
      </div><!-- End contentasset mobile-not-available -->
		
		
	



</footer>