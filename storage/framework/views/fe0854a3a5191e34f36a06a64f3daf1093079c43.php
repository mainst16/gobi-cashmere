<!doctype html>
<html> 
<?php echo $__env->make('layouts.htmlhead', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
<?php echo $__env->yieldContent('styles'); ?>

<body class="homepage">
<!-- Start: components/googletagmanager/datalayer -->
<script>
    var gtmData = {
            "siteId": app.resources["SITE_ID"],
            "isProduction": true,
            "custoraEnabled": true,
            "pageTitle": app.metrics.getPageTitle(false),
            "pageName": app.metrics.pageName,
            "pageType": app.metrics.pageType,
            "event": app.metrics.event
        };

</script>

<!-- Start: components/googletagmanager/customer -->
    

<script>
    var currentCustomer = {"authenticated":false,"deviceType":"desktop","isMobile":false,"visitorLoginState":"Not Logged In","customerGroups":["Everyone","Unregistered","isNotMobilecustomer"],"sourceCode":null,"sourceCodeGroup":null,"visitorOrderHistoryCount":0,"geoCountry":"MN"};
    jQuery.extend(gtmData, currentCustomer);
</script>   

<!-- End: components/googletagmanager/customer -->  
<script>

    if (typeof app.customer != 'undefined') {
        jQuery.extend(gtmData, {
            "hashedCustomerEmail": app.customer.hashedEmail,
            "hashedCustomerNo": app.customer.hashedCno,
            "registeredCustomer" : app.customer.registered
        });
    }


    if (typeof orderConfirmation != 'undefined')
    {
        jQuery.extend(gtmData, {
            "event": "purchase",
            "checkoutState": "null",
            "transactionType": "Purchase",
            "visitorID": orderConfirmation.customerNo,
            "transactionId": orderConfirmation.orderNo,
            "transactionCurrency": orderConfirmation.currencyCode,
            "transactionTotal": orderConfirmation.gross,
            "transactionShipping": orderConfirmation.adjShippingNet,
            "transactionTax": orderConfirmation.tax,
            "transactionProducts": orderConfirmation.productLineItems,
            "billToCity": orderConfirmation.billingAddress.city,
            "billToStateCode": orderConfirmation.billingAddress.stateCode,
            "billToPostalCode": orderConfirmation.billingAddress.postalCode,
            "billToCountryCode": orderConfirmation.billingAddress.countryCode,
            "shipToCity": jQuery.map(orderConfirmation.shipments, function(val, i) { return val.shipAddress.city; }).join('|'),
            "shipToStateCode": jQuery.map(orderConfirmation.shipments, function(val, i) { return val.shipAddress.stateCode; }).join('|'),
            "shipToPostalCode": jQuery.map(orderConfirmation.shipments, function(val, i) { return val.shipAddress.postalCode; }).join('|'),
            "shipToCountryCode": jQuery.map(orderConfirmation.shipments, function(val, i) { return val.shipAddress.countryCode; }).join('|'),
            "transactionProductIds": jQuery.map(orderConfirmation.productLineItems, function(val, i) { return val.id; }).join('|'),
            "transactionProductSkus": jQuery.map(orderConfirmation.productLineItems, function(val, i) { return val.sku; }).join('|'),
            "transactionProductNames": jQuery.map(orderConfirmation.productLineItems, function(val, i) { return val.name; }).join('|'),
            "transactionProductQtys": jQuery.map(orderConfirmation.productLineItems, function(val, i) { return val.quantity; }).join('|'),
            "transactionProductPrices": jQuery.map(orderConfirmation.productLineItems, function(val, i) { return val.adjustedGrossPrice; }).join('|'),
            "transactionProductListPrices" : jQuery.map(orderConfirmation.productLineItems, function(val, i) { return val.listPrice; }).join('|'),
            "transactionCoupon" : orderConfirmation,
            "transactionSubtotal" : orderConfirmation.merchandizeTotalNet,
            "transactionDiscountAmount" : orderConfirmation.orderDiscount,
            "transactionShipmentMethod": orderConfirmation.shippingMethods.join('|'),
            "transactionPaymentMethod": orderConfirmation.paymentMethods[0]
        });
        //console.log('gtmData',gtmData);
        if (orderConfirmation.orderContainsSmartGift) {
            jQuery.extend(gtmData, {
                "transactionPaymentMethod" : "Smartgift"
            });
        }
    }


    var gtmDataLayer = [ gtmData ];

    if (gtmData.authenticated) {
        gtmDataLayer.push({'event': 'userIsLoggedIn', 'uid':gtmDataLayer[0].visitorEmail});
    }


    if (typeof basketConfirmation != 'undefined')
    {
        jQuery.extend(gtmData, {
            "transactionProducts": basketConfirmation.productLineItems
        });
    }

    if (typeof basketConfirmation != 'undefined')
    {
        gtmDataLayer.push({
            "event" : "e_dataLayerReady"
         });

    }

    if (typeof basketCheckoutConfirmation != 'undefined')
    {
        jQuery.extend(gtmData, {
            "checkoutState": "null",
            "transactionProducts": basketCheckoutConfirmation.productLineItems
        });
    }
    if (typeof basketCheckoutConfirmation != 'undefined')
    {
        gtmDataLayer.push({
            "event" : "e_dataLayerReady"
         });
    }

    

</script>
<!-- End: components/googletagmanager/datalayer -->



    <!-- Google Tag Manager --> 
    <noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-V5JJ"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <script id="GoogleTagManager">(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    '//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','gtmDataLayer','GTM-V5JJ');</script>
    <!-- End Google Tag Manager -->

    
    

    
        <!-- load Custora JS library -->
        <script type="text/javascript">
            window.custoraTrack=window.custoraTrack||[],custoraTrack.load=function(t,a){var r,c,o;window.custoraTrack.appid=t,
            window.custoraTrack.config=a,r=document.createElement("script"),r.type="text/javascript",r.async=!0,
            r.src=("https:"===document.location.protocol?"https:":"http:")+"//track.custora.com/pixel.js",
            c=document.getElementsByTagName("script")[0],c.parentNode.insertBefore(r,c),
            o=function(t){return function(){custoraTrack.push([t].concat(Array.prototype.slice.call(arguments,0)))}},
            custoraTrack.track=o("track"),custoraTrack.identify=o("identify"),custoraTrack.addToCart=o("addToCart"),
            custoraTrack.removeFromCart=o("removeFromCart"),custoraTrack.completeTransaction=o("completeTransaction"),
            custoraTrack.productView=o("productView"),custoraTrack.categoryView=o("categoryView"),
            custoraTrack.searchView=o("searchView"),custoraTrack.cartView=o("cartView"),
            custoraTrack.addToWishList=o("addToWishList"),custoraTrack.removeFromWishList=o("removeFromWishList"),
            custoraTrack.wishListView=o("wishListView"),custoraTrack.createAccount=o("createAccount")},
            custoraTrack.load("59a2e787c1d20ea");
        </script>
    


    <div id="container" class="pt_storefront pt_wideformat">

        <?php echo $__env->make('layouts.headerYama', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>





<noscript>
<div class="disabledcontainer">
    <div class="disabledmessage">
        <p>Your browser's Javascript functionality is turned off. Please turn it on so that you can experience the full capabilities of this site.</p>
    </div>          
</div><!-- END: disabledcontainer -->
</noscript>


<div class="disabledcontainer" style="display:none" id="cookiesdisabled">
    <div class="disabledmessage">
        <p></p>
    </div>          
</div><!-- END: disabledcontainer -->
<script type="text/javascript">
if (document.cookie.length===0) {
    jQuery(function () {
        jQuery("#cookiesdisabled").show();
    });
}
</script>
        
        <div id="main" role="main">

            <?php echo $__env->yieldContent('content'); ?>

            <div id="rightcolumn">
                <div class="clear"></div>
            </div><!-- END: rightcolumn -->
            <div class="clear"></div>
        </div><!-- END: main -->
        <?php echo $__env->yieldContent('pro'); ?>



<div id="footer" class="footer" role="contentinfo">

    <?php echo $__env->make('layouts.footernew', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

</div>

    <?php echo $__env->make('layouts.scripts', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php echo $__env->yieldContent('customscripts'); ?>

</body>
</html>