<?php $__env->startSection('styles'); ?>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('pro'); ?>
<div id="main">

	<div id="content_co" class="account_new">
		<div class="accountlogin">

			<div class="logincustomers">
				<h3> </h3>
				<div class="returningcustomers">
					<form action="<?php echo e(route('voyager.login')); ?>" method="POST">
                    	<?php echo e(csrf_field()); ?>

						<legend>Нэвтрэх</legend>
						<div class="formfield username">
							<label class="formLabel" for="dwfrm_login_username">Цахим хаяг</label>
							<div class="value">

							<input class="textinput required" id="dwfrm_login_username" type="email" name="email" value="" maxlength="2147483647" placeholder="Цахим хаяг" aria-required="true" data-rule-email="true" data-msg-email="Please enter a valid Email Address" />
							</div>
							<div class="clear"></div>
						</div>
						<label class="formLabel" for="dwfrm_login_password">Нууц үг</label>
						<div class="value">
							<input class="textinputpw required" id="dwfrm_login_password" type="password" name="password" value="" maxlength="2147483647" placeholder="Нууц үг" aria-required="true" data-msg-required="Please type a password" />
						</div>
						<!-- <div class="formfield rememberme">
						<div class="checkbox">
						<input class="checkinput" type="checkbox" name="dwfrm_login_rememberme" id="dwfrm_login_rememberme" value="true" maxlength="2147483647" />
						<label for="dwfrm_login_rememberme">Remember Me</label>
						</div>
						</div>
						<div class="forgotpassword">
						<a id="PasswordReset" title="Forgot your password?">Forgot your password?</a>
						</div> -->
						<button class="button button--large button ladda-btn"type="submit" value="Submit">Нэвтрэх
						</button>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('customscripts'); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('welcome', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>