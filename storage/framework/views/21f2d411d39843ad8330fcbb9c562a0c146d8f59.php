<?php $__env->startSection('styles'); ?>
<link href="<?php echo e(asset('css/aboutus.css')); ?>" type="text/css" rel="stylesheet" />

    <?php echo $__env->yieldContent('style'); ?>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('pro'); ?>
<div id="main">
    <div id="mainCtnt">
        <div id="leftcolumn" class="nobackground">

            <ul class="mainCtntMenu">
                <li><a class="headerlink" href="<?php echo e(url('services')); ?>">Үйлчилгээ</a></li>
                <li><a class="navlink" href="<?php echo e(url('services')); ?>">Манай байршил</a></li>
                <li class="lastlink" style="border-bottom:none;"><a class="navlink" href="<?php echo e(url('services/care')); ?>">Хими цэвэрлэгээ</a></li>

            </ul>
            <script type="text/javascript" language="javascript">
                jQuery(document).ready( function() {
                            
                    var addr = $(location).attr('href');
                    
                    var names = addr.split("=");
                    names = names[names.length-1];
                    
                    jQuery('.navgroup a[href$='+names[0]+']').css('color', '#D8D8D8');
                    
                    jQuery('.navgroup h3:last').addClass('borderLastNav');
                });
            </script>
            <div class="clear"></div>
        </div>
    <div id="content">
        <div class="breadcrumb" itemprop="breadcrumb">
            <a href="<?php echo e(url('/')); ?>" title="Home">Нүүр</a>
            <span class="divider">/</span><a href="<?php echo e(url('about')); ?>">Бидний тухай</a>
        </div>
        <script language="javascript" type="text/javascript">
            app.currentPage.breadcrumbs = [];
            app.currentPage.breadcrumbs.push("About Us"); 
              
              
        </script>
        <div class="contentasset mobile-available">
            <style type="text/css">.aboutContentWrapper p {font-family:SamsungSans; color:#606061; font-size:12px !important}
            .aboutContentWrapper .thirdColumns a {font-size: 12px;}
            </style>
            <div class="aboutContentWrapper nonBordered">
                <?php if(Request::is('services')): ?>
                <div class="pageHeader">&nbsp;
                    <iframe src="https://www.google.com/maps/d/embed?mid=1AXFDWEBepdstYK5pH4jXBCywqc_N5xCV" width="100%" height="700px" style="margin-top: -63px"></iframe>
                </div>
                <?php else: ?>
                <?php echo $__env->yieldContent('content'); ?>
                <?php endif; ?>
            </div>
        </div>
        <div class="mobilecontentasset">
            <style type="text/css">.aboutContentWrapper p {font-family:SamsungSans; text-transform: uppercase;}
            </style>
            <div class="aboutContentWrapper nonBordered">
                <a href="#/about-tory/about-tory.html">
                    <img alt="Explore Tory's Story" height="408" src="<?php echo e(asset('imgs/GobiaboutIMG.jpg')); ?>" width="717" /> 
                </a>
                <div class="pageHeader">&nbsp;
                    <iframe src="https://www.google.com/maps/d/embed?mid=1LLdaTCHj2mH8OJTejevn4k2eO3WzTp19" width="100%"  height="480"></iframe>
                </div>
            </div>
        </div>
        <div class="clear"></div>
    </div>
    <div class="clear"></div>
    </div>
</div>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('customscripts'); ?>
    <?php echo $__env->yieldContent('scripts'); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('welcome', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>