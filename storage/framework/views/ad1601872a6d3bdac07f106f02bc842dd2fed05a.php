<?php $__env->startSection('pro'); ?>


		<div id="content">
		
			<script type="text/javascript" src="<?php echo e(asset('js/product2.js')); ?>"></script>
			<link type="text/css" rel="stylesheet" href="<?php echo e(asset('css/pdp.css')); ?>" />
			<link rel="stylesheet" href="<?php echo e(asset('css/animate.min.css')); ?>">
			

			<style>
			    .custom-cursor--zoom-in {
			        cursor: url(""), pointer;
			        cursor: -webkit-image-set(
			                url(<?php echo e(asset('imgs/icons/tb-cursor-l-zoom-in.png')); ?>) 1x,
			                url(<?php echo e(asset('imgs/icons/tb-cursor-l-zoom-in@2x.png')); ?>) 2x),
			                pointer;
			    }
			    .custom-cursor--zoom-out {
			        cursor: url(""), pointer;
			        cursor: -webkit-image-set(
			                url(<?php echo e(asset('imgs/icons/tb-cursor-l-zoom-out.png')); ?>) 1x,
			                url(<?php echo e(asset('imgs/icons/tb-cursor-l-zoom-out@2x.png')); ?>),
			                pointer;
			    }

			    .custom-cursor--zoom-out:active {
			        cursor: move;
			    }
			</style>
			

			<div class="pdp-page__product-image-background"></div>
			<div class="page-container" >

			

			

			<div id="pdpMain_new" class="pdp-page" >
				<!-- CQuotient Activity Tracking (viewProduct-cquotient.js) -->
				
				<div class="pdp-page__product-image">

					<div class="product-image">


					    <div class="product-image__header v-offset-top-xs v-offset-bottom-xs">
					        <div class="product-image__name">
					            
								<div itemprop="name" class="product-name">
									<h1 class="title title--l">
										<?php echo e($products->name); ?>

									</h1>
								</div>
					        </div>
					        <div class="product-image__price v-offset-top-xxxs">	
						
								<div id="pricing-190041832972" class="pricing" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
									<span itemprop="priceCurrency" class="posoffpage">MNT</span>									
									<div class="price pricing--l" id="price-45435">											
											<span class="price--standard notonsale" data-number-price="348.0">
												
													₮<?php echo e($products->price); ?>

												
											</span>	
												
									
									</div>			
								</div>            
					        </div>
					    </div>

					    <div class="product-image__image-container product-image__thumb-container" >
					        <div class="product-image__image custom-cursor--zoom-in js-show-image-viewer js-main-image"
					             data-s7-images="[&quot;TB_45435_278_A&quot;,&quot;TB_45435_278_D&quot;]"
					             style="background-image: url(<?php echo e(asset('storage/'.$pic.'')); ?>)" data-maincategory="clothing" data-special="null">
					        </div>
					        <div class="product-image__arrow product-image__arrow-left product-tile__arrow product-tile__arrow--left js-change-main-image" data-direction="left"></div>
					        <div class="product-image__arrow product-image__arrow-right product-tile__arrow product-tile__arrow--right js-change-main-image" data-direction="right"></div>

					        <div class="product-image__video-container"></div>

					    </div>

				        <div class="product-image__image-mobile js-embedded-image-viewer js-show-image-viewer">
				        	<?php $__currentLoopData = $pics; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $pic): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
				            <div class="mobile-image">                
				                <img src="<?php echo e(asset('storage/'.str_replace('\\', '/', $pic->pics).'')); ?>"></img>                
				            </div>
				            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
				    	</div>

    					<div class="product-image__gutter"></div>
					</div>
					<div class="image-viewer-popup" role="dialog" aria-label="Displays the product gallery. Use the left and right arrow keys to switch between images. Press on the ESC key to close the image viewer." style="display:none">
    					<div class="image-viewer-popup__wrapper">
        					<div class="image-viewer-popup__close js-close-image-viewer" aria-label="Close" role="button"></div>
		        			<div class="image-viewer-popup__arrow image-viewer-popup__arrow--left js-show-prev-image" aria-label="Previous Image" role="button">
					            <div class="image-viewer-popup__tooltip js-tooltip-text title--m"></div>
					        </div>
					        <div class="image-viewer-popup__arrow image-viewer-popup__arrow--right js-show-next-image" aria-label="Next Image" role="button">
					            <div class="image-viewer-popup__tooltip js-tooltip-text title--m"></div>
					        </div>
					        <div class="image-viewer-popup__images">
					            <div class="image-viewer">

					                <!-- <div class="js-video-wrapper image-viewer__current-video js-swipe-elem" data-video-exists="true"></div> -->
					                <div class="image-viewer__current-image js-current-image custom-cursor--zoom-in" style=""></div>
					                <div class="image-viewer__current-image-wrapper js-image-toggle">
					                    <img class="image-viewer__current-image-mobile js-current-image-mobile js-swipe-elem" src="" />
					                </div>

					                <div class="image-viewer__image-list">
					                    <div class="image-list js-viewer-image-container">
					                        
					                            
					                                <?php $__currentLoopData = $pics; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $pic): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
					                                    <div class="0 image-list__item js-show-current-image" data-image-index="<?php echo e($pic->dd); ?>">
					                                        <img src="<?php echo e(asset('storage/'.$pic->pics.'')); ?>" data-imgrl="<?php echo e(asset('storage/'.str_replace('\\', '/', $pic->pics).'')); ?>"></img>
					                                    </div>
					                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
					                                
					                            
					                                
					                                    <!-- <div id="show-video-item" data-video-id="TB_45435_278_VID-AVS"  class="2 image-list__item js-show-current-image" data-image-index="3">
					                                        <div class="video-wrapper">
					                                           <div class="play-video-btn video-s-btn"></div>
					                                           <img src="https://s7.toryburch.com/is/image/ToryBurchNA/TB_45435_278_E?$mainline_pdp_thumbnail$"></img>
					                                        </div>
					                                    </div>
 -->					                                
					                            
					                        
					                    </div>
					                </div>
					            </div>
					        </div>
					    </div>
					</div>
				</div>
				<div class="pdp-page__flexible-column v-offset-bottom-xxl">
					
					
						<!-- <div class="product-image-gallery ">
							<div class="product-image-gallery__column">
									<div class="product-image-gallery__item custom-cursor--zoom-in js-show-image-viewer js-product-gallery-image 1"
										data-gallery-index="1"
										data-name="ToryBurchNA/TB_45435_278_D"
										data-type="image"
										data-index="1">
										

										<img src="<?php echo e(asset('imgs/products/coats/_MG_4625.png')); ?>" />

										<div class="product-image-gallery__item-badge body-copy body-copy--s">2											
											<span class="tooltip-index-slash">/</span>
											<span class="js-tooltip-img-sum">2</span>
										</div>
									</div>

									<div class="product-image-gallery__item custom-cursor--zoom-in js-show-image-viewer js-product-gallery-image 3"
										data-gallery-index="3"
										data-name=""
										data-type=""
										data-index="">
										

										<img src="" />

										<div class="product-image-gallery__item-badge body-copy body-copy--s">

											
												4
											
											<span class="tooltip-index-slash">/</span>
											<span class="js-tooltip-img-sum">3</span>
										</div>
									</div>
								
							</div>
						</div> -->
						<div class="btn-play-video">play</div>
							
						</div>
						<div class="pdp-page__fixed-column" >
							<div class="product-info-panel__wrap js-product-info-wrapper">
								<div class="product-info-panel product-info-panel--centred">
									<div class="product-description v-offset-top-xxl">
										<div class="product-description__header">
											<h6 class="header header--xs">details</h6>
										</div>
										<div class="v-offset-top-m body-copy--s body-copy product-description__content">		
											<p>
											    <?php echo $products->desc; ?>

											</p>		
											<div class="panelContent v-offset-top-m" id="longDescription">
												<ul>
													<li> 100% ноолуур</li>
													<!--<li> Front zipper closure</li>-->
													<!--<li> Folds up into a pocket pouch</li>-->
													<li style="text-transform:uppercase;"> Style Number: <?php echo e($products->style_no); ?></li>
												</ul>				
											</div>
										</div>
									</div>
								</div>
								<!-- END: productdetailcolumn -->
							</div>
						</div >
						<div class="pdp-page__additional-descriptions">
							
						</div >
					</div >
				</div >


		<script type="text/javascript" src="<?php echo e(asset('js/imageViewer.js')); ?>"></script>
	
		</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('customscripts'); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make((Request::is('details/4/*')) ? 'Kids' : ((Request::is('details/3/*')) ? 'welcome': ((Request::is('details/1/*')) ? 'Yama' : 'Organic')), array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>