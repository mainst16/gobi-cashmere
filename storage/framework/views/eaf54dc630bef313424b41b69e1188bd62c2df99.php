<?php $__env->startSection('styles'); ?>
<link href="<?php echo e(asset('css/aboutus.css')); ?>" type="text/css" rel="stylesheet" />
    <?php echo $__env->yieldContent('style'); ?>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('pro'); ?>
<div id="main">
    <div id="mainCtnt">
        <div id="leftcolumn" class="nobackground">

            <ul class="mainCtntMenu">
                <li><a class="headerlink" href="<?php echo e(url('хувьцаа-эзэмшигчдэд/7')); ?>">ХУВЬЦАА ЭЗЭМШИГЧДЭД</a></li>
                <li><a class="navlink" href="<?php echo e(url('хувьцаа-эзэмшигчдэд/компаний-засаглал/1')); ?>">Компанийн засаглал</a></li>
                <li><a class="navlink" href="<?php echo e(url('хувьцаа-эзэмшигчдэд/хувьцаа-эзэмшигч/2')); ?>">Хувьцаа эзэмшигчид</a></li>
                <li><a class="navlink" href="<?php echo e(url('хувьцаа-эзэмшигчдэд/төлөөлөн-удирдах-зөвлөл/3')); ?>">Төлөөлөн удирдах зөвлөл</a></li>
                <li><a class="navlink" href="<?php echo e(url('хувьцаа-эзэмшигчдэд/гүйцэтгэх-удирдлага/4')); ?>">Гүйцэтгэх удирдлага болон эрх бүхий албан тушаалтан</a></li>
                <li><a class="navlink" href="<?php echo e(url('хувьцаа-эзэмшигчдэд/хөрөнгийн-зах-зээл/5')); ?>">Хөрөнгийн зах зээл ба компанийн хувьцаа</a></li>
                <li><a class="navlink" href="<?php echo e(url('хувьцаа-эзэмшигчдэд/хөрөнгийн-зах-зээл/8')); ?>">Ногдол ашиг</a></li>
                <li class="lastlink" style="border-bottom:none;"><a class="navlink" href="<?php echo e(url('хувьцаа-эзэмшигчдэд/мэдээ/6')); ?>">Мэдээ мэдээлэл</a></li>

            </ul>
            <script type="text/javascript" language="javascript">
                jQuery(document).ready( function() {
                            
                    var addr = $(location).attr('href');
                    
                    var names = addr.split("=");
                    names = names[names.length-1];
                    
                    jQuery('.navgroup a[href$='+names[0]+']').css('color', '#D8D8D8');
                    
                    jQuery('.navgroup h3:last').addClass('borderLastNav');
                });
            </script>
            <div class="clear"></div>
        </div>
    <div id="content">
        <div class="breadcrumb" itemprop="breadcrumb">
            <a href="<?php echo e(url('/')); ?>" title="Home">Нүүр</a>
            <span class="divider">/</span><a href="<?php echo e(url('about')); ?>">Бидний тухай</a>
        </div>
        <script language="javascript" type="text/javascript">
            app.currentPage.breadcrumbs = [];
            app.currentPage.breadcrumbs.push("About Us"); 
              
              
        </script>
        <div class="contentasset mobile-available">
            <style type="text/css">.aboutContentWrapper p {font-family:SamsungSans; color:#8c8a8a; font-size:12px !important}
            .aboutContentWrapper .thirdColumns a {font-size: 12px;}
            </style>
            <div class="aboutContentWrapper nonBordered">
                <?php if(Request::is('хувьцаа-эзэмшигчдэд/7')): ?>
                <?php $__currentLoopData = $tuzs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $tuz): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <div class="pageHeader">&nbsp;
                    <h2><?php echo e($tuz->title); ?></h2>
                       <?php echo $tuz->desc; ?>

                </div>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                <?php else: ?>
                <?php echo $__env->yieldContent('content'); ?>
                <?php endif; ?>
            </div>
        </div>
        <div class="mobilecontentasset">
            <style type="text/css">.aboutContentWrapper p {font-family:SamsungSans; text-transform: uppercase;
            </style>
            <div class="aboutContentWrapper nonBordered">
                <?php if(Request::is('хувьцаа-эзэмшигчдэд/7')): ?>
                <?php $__currentLoopData = $tuzs; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $tuz): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <div class="pageHeader">&nbsp;
                    <h2><?php echo e($tuz->title); ?></h2>
                       <?php echo $tuz->desc; ?>

                </div>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                <?php else: ?>
                <?php echo $__env->yieldContent('content'); ?>
                <?php endif; ?>
            </div>
        </div>
        <div class="clear"></div>
    </div>
    <div class="clear"></div>
    </div>
</div>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('customscripts'); ?>
    <?php echo $__env->yieldContent('scripts'); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('welcome', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>