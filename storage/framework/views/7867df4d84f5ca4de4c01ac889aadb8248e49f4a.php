<meta name="viewport" content="width=device-width, initial-scale=1">
<?php $__env->startSection('content'); ?>
<div class="blogWrapper">
			<div class="stickyPosts">

				<ul>
				 	<?php $__currentLoopData = $limit; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $slide): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
					<li id="post-403854">
						<a href="<?php echo e(url('now/post/'.$slide->now_id.'')); ?>" rel="bookmark"><img src="<?php echo e(asset('storage/'.$slide->vpics.'')); ?>" border="0"  width="500" height="500" /></a>
						<div class="slideBody">
							<h2 style="text-transform: uppercase;"><a href="<?php echo e(url('now/post/'.$slide->now_id.'')); ?>" rel="bookmark" ><?php echo e($slide->title); ?></a></h2>
							<div class="slideText"></div>
							<a class="readmore" href="<?php echo e(url('now/post/'.$slide->now_id.'')); ?>">Цааш унших</a>	
						</div>
					</li>
					<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
				</ul>
				<script type="text/javascript" language="javascript">
					jQuery(function () {
						//jQuery(".blogThumbnails li .postTitle").blogTitleStyling();
					});
				</script>
			</div><!-- END: sticky-posts -->

			<div class="blogPostsWrapper">
				<?php if(Request::is('now/*')): ?>
					<?php $__currentLoopData = $nowm; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $now): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
					<div class="blogPostOuter" id="post-404234">
						<div class="postImage">
						<!-- Opening Page Image -->
							<a href="<?php echo e(url('now/post/'.$now->now_id.'')); ?>"><img src="<?php echo e(asset('storage/'.$now->pics.'')); ?>" border="0" /></a>
						</div>			
						<div class="postMeta">
							<h2><a href="<?php echo e(url('now/post/'.$now->now_id.'')); ?>"><?php echo e($now->title); ?></a></h2>
						</div>
						<div class="postBody">
							<div class="postDetail">							
								<h2><a href="<?php echo e(url('now/post/'.$now->now_id.'')); ?>"></a></h2>
								<?php echo $now->desc; ?>												
							</div>										
							<a href="3" class="readmore">Цааш унших</a>
						</div>
						<div class="clear"></div>
					</div>
					<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
				<?php else: ?>
					<?php $__currentLoopData = $nows; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $now): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
					<div class="blogPostOuter" id="post-404234">
						<div class="postImage">
						<!-- Opening Page Image -->
							<a href="<?php echo e(url('now/post/'.$now->now_id.'')); ?>"><img src="<?php echo e(asset('storage/'.$now->pics.'')); ?>" border="0" /></a>
						</div>			
						<div class="postMeta">
							<h2><a href="<?php echo e(url('now/post/'.$now->now_id.'')); ?>"><?php echo e($now->title); ?></a></h2>
						</div>
						<div class="postBody">
							<div class="postDetail">							
								<h2><a href="<?php echo e(url('now/post/'.$now->now_id.'')); ?>"></a></h2>
								<?php echo $now->desc; ?>												
							</div>										
							<a href="3" class="readmore">Цааш унших</a>
						</div>
						<div class="clear"></div>
					</div>
					<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
				<?php endif; ?>
							
				<div class="blogPaging">
					<div class="nextPage floatleft"><a href="#" >Өмнөх постууд</a></div>
					<div class="previousPage floatright"></div>
					<div class="clear"></div>
				</div>
	

			</div><!-- END: blogPostsWrapper -->
			
			<div class="blogFooter">		
				
				<div class="contentasset mobile-not-available"><!-- dwMarker="content" dwContentID="bcLnQiaagnJ8Aaaadgs7cqMwnT" -->
					<div class="blog-followus">
						<h5>Сошиал медиа</h5>
						<div style="padding-left: 30%;">
							<!------
						INSTAGRAM: do not change id=instagram-data
						------>
						<p><a href="http://instagram.com/gobi_cashmere" id="instagram-data" target="_blank" title="Follow Us on Instagram"><img alt="" src="<?php echo e(asset('/imgs/icons/if_instagram.png')); ?>" /><b></b></a></p>

						<!------
						FACEBOOK 
						------>
						<p><a href="http://www.facebook.com/GobiCashmere" target="_blank" title="Follow Us On Facebook"><img alt="" src="<?php echo e(asset('/imgs/icons/if_facebook.png')); ?>" /><b></b></a></p>

						<!------
						PINTEREST 
						------>
						<p><a href="http://twitter.com/GobiCashmere" target="_blank" title="Follow Us on Pinterest"><img alt="" src="<?php echo e(asset('/imgs/icons/if_twitter.png')); ?>" /><b></b></a></p>


						<!------
						TUMBLR 
						------>
						<p><a href="https://www.youtube.com/channel/UCzUZQnPw4XdIw3_IHq6xywA" target="_blank" title="Follow Us on Tumblr"><img alt="" src="<?php echo e(asset('/imgs/icons/if_youtube.png')); ?>"  /><b></b></a></p>
						</div>

						
					</div>
					<!-- <div style="font-size: 14px; color: #A2A2A2; text-align: center; padding-top: 80px; font-family: SamsungSans;">
						#ToryPartner is a paid program in which influencers and<br />bloggers receive compensation for featuring our products.
					</div> -->
				</div><!-- End contentasset mobile-not-available -->	

			</div><!-- END: blogFooter -->
		</div><!-- END: blogWrapper -->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('contents.now', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>