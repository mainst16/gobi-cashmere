<?php $__env->startSection('style'); ?>

<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<div id="rn_PageTitle" class="rn_AnswerDetail">
  <!-- <div id="rn_SearchControls" class="tb_Mobile_Show">
      <h1 class="tb_Mobile_Title">Frequently Asked Questions</h1>
      <h1 class="rn_ScreenReaderOnly">Search</h1>
      <form onsubmit="return false;">
          <div class="rn_SearchInput">
              <rn:widget path="search/AdvancedSearchDialog"/>
              <rn:widget path="search/KeywordText" label_text="" placeholder_text="Search a frequently asked question" label_text="Find the answer to your question" initial_focus="false"/>
          </div>
          <div class="">
              <rn:widget path="search/SearchButton" icon_path="images/tb_search_icon.png" report_page_url="/app/answers/list"/>
          </div>
      </form>
      <rn:widget path="search/DisplaySearchFilters"/>
  </div> -->
  
</div>
<div class="aboutSubContent" style="font-size: 14px">
  <div class="main-content career">
    <h2 style="text-transform: uppercase;">Брэндүүд</h2> 
    <hr> 
    <p style="text-align: justify;">Эх орны хосгүй үнэт баялаг болсон Монгол ноолуурыг дэлхийд таниулахын тулд бүтээгдэхүүнээ илүү загварлаг, тансаг, эко болгохыг зорьж хийсний нэгэн бодит жишээ бол ORGANIC CASHMERE /элдэв төрлийн будаггүй, байгалийн гаралтай/, YAMA /тансаг зэрэглэл/, GOBI KIDS брэндүүд юм.</p>  
    <br>
    <h2 style="text-transform: uppercase; font-size: 14px">Говь брэнд</h2>
    <p style="text-align: justify;">Гэр бүлийн гишүүн бүрт зориулсан Говь брэндийн үндсэн концепц аз жаргал, халуун дулаан уур амьсгалыг илэрхийлдэг. Монгол улсад албан ёсны 14 салбартай Говь брэнд гадаадын зах зээлд хэдийнээ нэрээ түгээж, 13 орны 34 хотод 50 франчайз дэлгүүрээ нээгээд байна.</p>
<br>
    <h2 style="text-transform: uppercase; font-size: 14px">Говь Органик</h2>
    <p style="text-align: justify;">Ноолуурын байгалийн үндсэн 4 өнгө болох цайвар, цайвар шаргал, хөх саарал, бор өнгүүдийг ашигласан, ямар ч будаг, цайруулалт ороогүй органик бүтээгдэхүүн гэдгээрээ бусдаас ялгардаг. Хэрэглэгчдийн дунд өндрөөр үнэлэгддэг Говь органик брэнд нь классик хэв шинжийг хадгалсан, трэнд хөөдөггүй, натурал төрхийг хадгалсан.</p>
<br>
    <h2 style="text-transform: uppercase; font-size: 14px">Yama</h2>
    <p style="text-align: justify;">Трэнд дагасан мэт классик хэлбэрээр дүр төрхөө тодорхойлсон YAMA нь ноолууран бүтээгдэхүүний зах зээлд богино хугацаанд амжилтаа ахиулж яваа брэнд юм. Монголоор “Ямаа” буюу тансаг, дулаан ноолуурыг бий болгодог таван хошуу малын нэгээр нэрлэсэн бол японоор Yama гэх үг нь “Уулын оргил” гэсэн утгыг илэрхийлсэн байна.</p>
<br>
    <h2 style="text-transform: uppercase; font-size: 14px">Gobi kids</h2>
    <p style="text-align: justify;">Хүүхдийн арьс маш эмзэг, булбарай учраас харшил өгдөггүй, зөөлөн бүтэцтэй материал илүү тохиромжтой байдаг. Энэ хэрэгцээг хангаж чадах цорын ганц бүтээгдэхүүн бол ноолуур юм. Тиймдээ ч хүүхдийн биед эвтэйхэн, загварлаг гэсэн дүрмийг хадгалж, Gobi kids дэд брэндийг амжилттайгаар явуулж байна.</p>

    
  </div>
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('contents.other.about1', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>