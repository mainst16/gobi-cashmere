<?php $__env->startSection('pro'); ?>
<div id="container" class="pt_productsearchresult" data-cgid="clothing--outerwear">

	<div id="main_new" role="main" class="main_grid">
		<div id='content' class="clearfix">
			<div class="producthits">
				<div class="category-landing-search-banner">
					<div class="htmlslotcontainer mobile-not-available">
						<div class="asset-template asset-template--header-1">
						    <div class="asset-template__wrapper">
						        <span class="header header--m"><?php echo e(\Request::get('q')); ?></span>
						        <br/>
						        <span class="body-copy body-copy--m" style="font-family:'SamsungSans', sans-serif;">
						        	<?php if($sale->isEmpty()): ?>
						 				Тун удахгүй
						 			<?php else: ?>
						 			<?php endif; ?>
						        </span>
						    </div>
						</div>
					</div>
				</div>

				<div id="search" class="search">
					<div class="productresultareaheader">
						<h1 id="results"></h1>
					</div>		

					<script type="text/javascript">
						var omnRefinementType = 'product';
						var omnRefinementName, omnRefinementValue;
					</script>
					
					<div class="productresultarea ">

						<div class="searchresults" data-gtm-search-list="searchresults">
							<div class="product-grid__group ">

								<?php $__currentLoopData = $sale; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
								<div class="col-xs-6 col-sm-6 col-md-4 col-lg-3 grid-tile-container js-grid-tile-container ">
											<!-- CQuotient Activity Tracking (viewCategory-cquotient.js) -->
									<script type="text/javascript">//<!--
									/* <![CDATA[ */
									(function(){
									try {
									    if(window.CQuotient) {
										var cq_params = {};
										
										cq_params.cookieId = window.CQuotient.getCQCookieId();
										cq_params.userId = window.CQuotient.getCQUserId();
										cq_params.accumulate = true;
										cq_params.products = [{
										    id: '46054',
										    sku: ''
										}];
										cq_params.categoryId = 'clothing--outerwear';
										cq_params.refinements = '[{\"name\":\"Category\",\"value\":\"clothing--outerwear\"}]';
										cq_params.personalized = 'false';
										cq_params.sortingRule = 'merchant-picks-and-price';
										
										if(window.CQuotient.sendActivity)
										    window.CQuotient.sendActivity(CQuotient.clientId, 'viewCategory', cq_params);
										else
										    window.CQuotient.activities.push({
										    	activityType: 'viewCategory',
										    	parameters: cq_params
										    });
									  	}
										} catch(err) {}
										})();
										/* ]]> */
										// -->
										</script>
										<script type="text/javascript">//<!--
										/* <![CDATA[ (viewProduct-active_data.js) */
										dw.ac.capture({id: "46054", type: "searchhit"});
										/* ]]> */
										// -->
										</script>
												

									

									<div
										class="product-tile  product-list__item js-product-tile js-product-list-item"
										data-product-variant="190041828272"
										data-product-colors="001"
										data-product-id="46054"
										data-product-imageset="{&quot;001&quot;:{&quot;imageset&quot;:[&quot;TB_46054_001&quot;,&quot;TB_46054_001_A&quot;,&quot;TB_46054_001_B&quot;,&quot;TB_46054_001_C&quot;],&quot;video&quot;:&quot;TB_46054_001_VID-AVS&quot;,&quot;poster&quot;:&quot;TB_46054_001_E&quot;}}">

										
										<div class="product-tile__thumb-container js-product-tile-thumb" data-default-color="001">

											<!-- <div class="product-tile__qs-heart like-btn wishlist-heart-btn qs-like-button"></div>

											<div class="product-tile__arrow product-tile__arrow--left js-product-tile-arrow" data-direction="left"></div>
											<div class="product-tile__arrow product-tile__arrow--right js-product-tile-arrow" data-direction="right"></div> -->

											<a href="<?php echo e(url('details/'.$product->brand_id.'/'.$product->product_id.'')); ?>" class="product-tile__thumb">
													
												<!-- isproductimage -->	
												<img src="<?php echo e(asset('storage/'.$product->pics.'')); ?>" />


											</a>
										</div>
										<div class="product-tile__info-container">
											<a href="<?php echo e(url('details/'.$product->brand_id.'/'.$product->product_id.'')); ?>" class="product-tile__name title title--xs">
												<?php echo e($product->name); ?> 
											</a>
											<div alt="TRISTAN " class="product-tile__price pricing pricing--s">		
												<!-- <span class="price--standard notonsale js-product-price" data-number-price="1298.0">
													$1,298				
												</span>	 -->			
											</div>

											<div class="product-tile__colors swatches v-offset-top-xxxs">

												
											</div>
											
										</div>
									</div>

									<script>
									var sftileCount = 1;
									jQuery(".js-product-list-item").each(function(){
										var sfprodprice = jQuery(this).find(".js-product-price").attr("data-number-price");

										jQuery(this).attr("data-product-price",sfprodprice);
										jQuery(this).attr("data-product-position",sftileCount);
										sftileCount++;
									});
									</script>
								</div>
								<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>	

							<!--  end: row -->
								
							</div>
						
						</div><!-- END: productresultarea -->
					
					</div><!-- END: search -->
				<div class="category-landing-search-banner"></div>
			</div><!-- END: producthits -->
		
			
			<div class="htmlslotcontainer mobile-not-available">
					
				<script>

					jQuery(document).ready(function() {
					function getQSValue(name) {
						var i;
						var qs = window.location.search.replace("?","");;
						//console.log(qs);
						var pairs = qs.split('&');
						//console.log(pairs);
						var pairsLength = pairs.length;
						if(pairsLength){
							for(i = 0; i<pairsLength; i++){
								var currentPair = pairs[i].split("=");
								var currentPairName = currentPair[0];
								if(currentPairName == name){
									return currentPair[1];
								}
							}
						}
					}
					app.log.write(getQSValue('searchterm'));
					var qs = window.location.search.replace("?","");
					if(qs.length){
						var pairs = qs.split("&");
						if(pairs.length){
						}
					}
					if(qs.indexOf('searchmodal=yes') != -1){
						app.createDialog({
							id: 'search-modal-popup',
							options: {
								height: 404,
								width: 587,
								dialogClass: 'ui-no-theme ui-no-close ui-no-title',
							   	resizable: false,
							   	draggable: false
							   	//,position:top
							}
						});
						jQuery('#search-modal-popup').css({width:587,height:404});
						jQuery('#search-modal-popup').dialog('open');
						app.ajax.load({
							selector: '#search-modal-popup',
							url: app.URLs.searchPopupUrl,
							callback: function(responseText, textStatus){
								app.dialog.moveToMiddle('#search-modal-popup');
								jQuery('#searchRedirectString').html((getQSValue('searchterm'))?getQSValue('searchterm'):'');
								jQuery('.search-modal-close').click(function(){
									jQuery("#search-modal-popup").dialog("close");
								});
							}
						});
					}
				});
				</script>	
			</div>
		</div><!-- END: content -->
		<div class="clear"></div>
	</div><!-- END: main -->
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('customscripts'); ?>
    <script type="text/javascript" src="<?php echo e(asset('js/metrics_new.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(asset('js/searchresults2.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(asset('js/in-grid-video.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(asset('js/new-in-grid-video.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(asset('js/templates.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(asset('js/modals.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(asset('js/productTileCarousel.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(asset('js/quickshopWidget.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(asset('js/productsetWidget.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(asset('js/CustomVideoViewer.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(asset('js/shopTheLook.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(asset('js/breakersParallax.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(asset('js/product2.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(asset('js/breakersVideo.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(asset('js/dwanalytics-18.3.js')); ?>"></script>
    <script type="text/javascript" src="<?php echo e(asset('js/gretel.min.js')); ?>"></script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('welcome', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>