<?php $__env->startSection('customstyles'); ?>
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>

{
  box-sizing: border-box;
}

.row > .column {
  padding: 8px 8px;
}

.row:after {
  content: "";
  display: table;
  clear: both;
}

.column {
  float: left;
  width: 19%;
}

/* The Modal (background) */
.modal {
  display: none;
  position: fixed;
  z-index: 102;
  padding-top: 100px;
  /*margin-top: 156px;*/
  left: 0;
  top: 0;
  width: 100%;
  height: 100%;
  overflow: auto;
  background-color: #fff;
}

/* Modal Content */
.modal-content {
  position: relative;
  /*background-color: #fefefe;*/
  margin: auto;
  padding: 0;
  width: 100%;
  max-width: 1200px;
}

/* The Close Button */
.close {
  color: #999;
  position: absolute;
  top: 10px;
  right: 25px;
  font-size: 35px;
  font-weight: bold;
}

.close:hover,
.close:focus {
  color: #999;
  text-decoration: none;
  cursor: pointer;
}

.mySlides {
  display: none;
}

.cursor {
  cursor: pointer
}

/* Next & previous buttons */
.prev,
.next {
  cursor: pointer;
  position: absolute;
  top: 50%;
  width: auto;
  padding: 16px;
  margin-top: -50px;
  color: #999;
  font-weight: bold;
  font-size: 20px;
  transition: 0.6s ease;
  border-radius: 0 3px 3px 0;
  user-select: none;
  -webkit-user-select: none;
}

/* Position the "next button" to the right */
.next {
  right: 0;
  border-radius: 3px 0 0 3px;
}

/* On hover, add a black background color with a little bit see-through */
.prev:hover,
.next:hover {
  background-color: rgba(0, 0, 0, 0.8);
}

/* Number text (1/3 etc) */
.numbertext {
  color: #f2f2f2;
  font-size: 12px;
  padding: 8px 12px;
  position: absolute;
  top: 0;
}

img {
  margin-bottom: -4px;
}

.caption-container {
  text-align: center;
  background-color: white;
  padding: 2px 16px;
  color: white;
}

.demo {
  opacity: 0.6;
}

.active,
.demo:hover {
  opacity: 1;
}

img.hover-shadow {
  transition: 0.3s;
}

.hover-shadow:hover {
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19)
}
</style>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<div class="blogWrapper">
	<div class="blogPostDetail blogWideContentWrapper">
		<div id="post-main-408564" class="blogPostOuter">
			<div class="postTemplate7">
				<div class="postMeta">
					<h2 style="font-family: SamsungSans_th"><?php echo e($readmore->title); ?></h2>
				</div>
				<div class="postImage" align="center">
					<img style="width: 950px" src="<?php echo e(asset('storage/'.$readmore->pics.'')); ?>"/></p>
					<!-- <div class="wp-caption-text" align="center">Ээж охин</div> -->
				</div>
				<div class="postBody" style="padding: 25px  25% 0 25%;">
					<h3 style="text-align: center;"><?php echo e($readmore->longtitle); ?></h3>
					<?php echo $readmore->desc; ?>

				</div>
				<div>
					<?php if( !empty ($pic) ): ?>
					<div class="row" style="    padding: 0px 20% 0% 28%;">
						<?php $__currentLoopData = json_decode($pic, true); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $key=>$images): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
					  <div class="column">
					  	<div class="product-tile__thumb-container js-product-tile-thumb" data-default-color="001">
							<a  class="product-tile__thumb" onclick="openModal();currentSlide(<?php echo e($key); ?>+1)" style="background-image: url(<?php echo e(asset('storage/'.$images)); ?>); background-size: auto 541px; background-repeat: no-repeat;background-position: center;">
					    		<!-- <img src="<?php echo e(asset('storage/'.$images)); ?>" style="height: 100%" onclick="openModal();currentSlide(1)" class="hover-shadow cursor"> -->
					    	</a>
					    </div>
					  </div>
					  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
					</div>
					
					
					<div id="myModal" class="modal">
					  <span class="close cursor" onclick="closeModal()">&times;</span>
					  <div class="modal-content">
					  	<?php $__currentLoopData = json_decode($pic, true); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $images): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
					    <div class="mySlides">
					      <div class="numbertext"></div>
					      <img src="<?php echo e(asset('storage/'.$images)); ?>" style="width:auto; height: 800px;margin: auto; display: block;">
					    </div>
						<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>	    
					    <a class="prev" onclick="plusSlides(-1)">&#10094;</a>
					    <a class="next" onclick="plusSlides(1)">&#10095;</a>

					    <div class="caption-container">
					      <p id="caption"></p>
					    </div>
					  </div>
					</div>
					<?php else: ?>
					<div></div>
					<?php endif; ?>
				</div>
				<!-- <div class="postLabels test">
					<label>Labels:</label>
					<a href="#">ХЭЛХГЭР ПИДЖАК</a>
				</div>
				<div class="clear"></div> -->
			</div>
		</div>
	</div>
	<div class="clear"></div>
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('customscripts'); ?>
<script>
function openModal() {
  document.getElementById('myModal').style.display = "block";
}

function closeModal() {
  document.getElementById('myModal').style.display = "none";
}

var slideIndex = 1;
showSlides(slideIndex);

function plusSlides(n) {
  showSlides(slideIndex += n);
}

function currentSlide(n) {
  showSlides(slideIndex = n);
}

function showSlides(n) {
  var i;
  var slides = document.getElementsByClassName("mySlides");
  var dots = document.getElementsByClassName("demo");
  var captionText = document.getElementById("caption");
  if (n > slides.length) {slideIndex = 1}
  if (n < 1) {slideIndex = slides.length}
  for (i = 0; i < slides.length; i++) {
      slides[i].style.display = "none";
  }
  for (i = 0; i < dots.length; i++) {
      dots[i].className = dots[i].className.replace(" active", "");
  }
  slides[slideIndex-1].style.display = "block";
  dots[slideIndex-1].className += " active";
  captionText.innerHTML = dots[slideIndex-1].alt;
}
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('contents.now', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>