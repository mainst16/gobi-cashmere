<?php $__env->startSection('style'); ?>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">

<style>
.accordion {
    background-color: #eee;
    color: #444;
    cursor: pointer;
    padding: 18px;
    width: 100%;
    border: none;
    text-align: left;
    outline: none;
    font-size: 15px;
    transition: 0.4s;
}

.active, .accordion:hover {
    background-color: #ccc;
}

.accordion:after {
    content: '\002B';
    color: #777;
    font-weight: bold;
    float: right;
    margin-left: 5px;
}

.active:after {
    content: "\2212";
}

.panel {
    padding: 0 18px;
    background-color: white;
    max-height: 0;
    overflow: hidden;
    transition: max-height 0.2s ease-out;
    font-size: 13px;
}
</style>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<div id="rn_PageTitle" class="rn_AnswerDetail">
  <!-- <div id="rn_SearchControls" class="tb_Mobile_Show">
      <h1 class="tb_Mobile_Title">Frequently Asked Questions</h1>
      <h1 class="rn_ScreenReaderOnly">Search</h1>
      <form onsubmit="return false;">
          <div class="rn_SearchInput">
              <rn:widget path="search/AdvancedSearchDialog"/>
              <rn:widget path="search/KeywordText" label_text="" placeholder_text="Search a frequently asked question" label_text="Find the answer to your question" initial_focus="false"/>
          </div>
          <div class="">
              <rn:widget path="search/SearchButton" icon_path="images/tb_search_icon.png" report_page_url="/app/answers/list"/>
          </div>
      </form>
      <rn:widget path="search/DisplaySearchFilters"/>
  </div> -->
  
</div>
<div class="aboutSubContent" style="font-size: 14px">
  <img src="<?php echo e(asset('imgs/abouts/job.jpg')); ?>" style="width: 800px">
    <h4 style="text-transform: uppercase;">Нээлттэй ажлын байр</h4>
    <p><strong></strong> <strong></strong> Та нээлттэй ажлын байранд шаардлага хангаж байна гэж үзвэл ажилд орох хүсэлт буюу анкет бөглөн hr@gobi.mn рүү илгээнэ үү. <a href="<?php echo e(url('../../ckfinder/userfiles/files/gobi-anket.docx')); ?>"><strong>Анкет татах</strong></a></p>
    <?php $__currentLoopData = $deps; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $dep): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
    
    <button class="accordion"><?php echo e($dep->name); ?> ( <?php echo e($dep->jobss->count()); ?> )</button>
    <div class="panel">
      <table id="example" class="display" style="width:100%">
              <tr>
                <th>Ажлын байр</th>
                <th>Нээгдсэн огноо</th>
                <th>Хаагдах огноо</th>
              </tr>
              <?php $__currentLoopData = $dep->jobss; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $job): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
              <tr>
                <td><a href="<?php echo e(url('about/view-jobs/'.$job->job_id.'/ажлын-байр')); ?>"><?php echo e($job->name); ?></a></td>
                <td><?php echo e($job->begin_date); ?></td>
                <td><?php echo e($job->end_date); ?></td>
              </tr>
              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </table>
    </div>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>
<script type="text/javascript" src="<?php echo e(asset('js/jquery.dataTables.min.js')); ?>"></script>
<script>
var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var panel = this.nextElementSibling;
    if (panel.style.maxHeight){
      panel.style.maxHeight = null;
    } else {
      panel.style.maxHeight = panel.scrollHeight + "px";
    } 
  });
}
</script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#example').DataTable({
      "searching": false,
      "lengthChange" : false,
      "language": {
            "info": " Нийт _TOTAL_ үр дүн. Энэ хуудсанд _START_  - _END_",
            "paginate": {
              "previous": "Өмнөх",
              "next": "Дараах"
            }
        }
    });
} );
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('contents.other.about1', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>