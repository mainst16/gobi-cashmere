<!doctype html>
<html>
<head>

		
		<title>Gobi &amp; Now</title>

		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="shortcut icon" href="imgs/favicon.png"/>


		<link href="<?php echo e(asset('css/jquery.ui.all.css')); ?>" type="text/css" rel="stylesheet" />

		<link href="<?php echo e(asset('css/SweetSansPro.css')); ?>" type="text/css" rel="stylesheet" />
		<link href="<?php echo e(asset('css/footericonfont.css')); ?>" type="text/css" rel="stylesheet" />
		    


		<link href="<?php echo e(asset('css/default.css')); ?>" type="text/css" rel="stylesheet" />
		<link href="<?php echo e(asset('css/index.css')); ?>" type="text/css" rel="stylesheet" />
		<link href="<?php echo e(asset('css/common.css')); ?>" type="text/css" rel="stylesheet" />
		<link rel="stylesheet" href="<?php echo e(asset('css/animate.min.css')); ?>">


			<!-- Additional Site CSS -->
		<style type="text/css">

			/*body {background-image: none; background-color: #FFF; }

			body.homepage {background-image: none; background-color: #FFF; } */

			body.homepage .home-page-hide{display:none;}
			body.blog .home-page-hide{display:none;}

			body.blog { background-image: none; background-color: #FFF; }
			body.blog .postComments { display: none; }

			body.privatesale {background-image: none; }

			#srd_pd {padding: 10px 0px 5px !important;}

			.beautylandingouter .headerbanner .htmlslotcontainer {background: #fff;}

			.blog .headerbanner .htmlslotcontainer, .blog .headerbanner .mobilehtmlslotcontainer {background: none;}

			.categorymenu ul.menu-category.mega-menu > li .blog-sub .row-wrapper {}

			.categorymenu .menu-background {height:450px;}

			@media  only screen and (min-device-width : 768px) and (max-device-width : 1024px) {
			.blog .headerbanner .promo-message {display:none; }
			.categorymenu ul.mega-menu li .blog-sub .categorymenu-slot {}
			.categorymenu ul.menu-category { }
			}

			@media  only screen and (min-device-width : 768px) and (max-device-width : 1024px) and (orientation : Landscape) {
			.categorymenu ul.mega-menu .swim .sub .categorymenu-slot {  }
			.watches .image .productimage a img {  }
			}

			@media  only screen and (min-device-width : 768px) and (max-device-width : 1024px) and (orientation : Portrait) {
			#navigation.categorymenu {min-width: 100%;}
			.mega-menu li.accessories .sub .row {}
			}


			@media  screen and (max-width:767px){
			.product.bannertile.twocolumn-banner-parent .bannertileitem { width: 324px!important; }
			.menu-category .sale > span {}
			}
			@media  screen and (min-width:768px){
			.pt_cart .cart .item-actions-cell .stockstate { padding: 0;}
			}
			.producttile .product-banner-bcrf-pink{color:#c67f83;}

			.text-menu .sale .row-container {  }
			.text-menu .sale.open { }

			.menu-category .clothing-sale a, .menu-category .shoes-sale a, .menu-category .handbags-sale a, .menu-category .accessories-sale a {}

			/* Mega menu styles */
			@media  all and (-ms-high-contrast: none), (-ms-high-contrast: active) {
			    /* IE10+ CSS styles go here */
			}


			@media  screen and (-webkit-min-device-pixel-ratio: 0) {
			}

			@media  only screen and (min-device-width : 768px) and (max-device-width : 1024px) and (orientation : Landscape) {
			}

			@media  only screen and (min-device-width : 768px) and (max-device-width : 1024px) and (orientation : Portrait) {
				.categorymenu ul.menu-category>li>a {  }
			        #navigationsport ul.menu-category > li.categorymenudivider { width: 22.5px !important; }
			}

			/* iPad Pro portrait */
			@media  only screen and (min-device-width : 1024px) and (max-device-width : 1024px) and (min-device-height : 1366px) and (max-device-height : 1366px) and (min-width: 1024px) and (max-width: 1024px) {
			    #navigationsport ul.menu-category > li.categorymenudivider { width: 69px !important; }
			}
			/* iPad Pro landscape */
			@media  only screen and (min-device-width : 1024px) and (max-device-width : 1024px) and (min-device-height : 1366px) and (max-device-height : 1366px) and (min-width: 1366px) and (max-width: 1366px) {
			    #navigationsport ul.menu-category > li.categorymenudivider { width: 30px !important; }
			    div#navigationsport.categorymenu { width: 1000px !important; }
			}
			/* Microsoft Edge */
			@supports (-ms-ime-align:auto) {
			   #navigationsport ul.menu-category > li.categorymenudivider { width: 30px !important; }
			}
			.mega-menu li .sub .row-wrapper {width:100%;}
			@-moz-document url-prefix() { 
			  .categorymenu .menu-background { height: 470px; }
			}

			@media  only screen and (min-width : 816px) {
			#navigation {min-width:816px;}
			}

			#home-navigation {display:none;}
		</style>


			<script type="text/javascript" src="<?php echo e(asset('js/jquery-1.11.1.min.js')); ?>" ></script>
			



		<script type="text/javascript" src="<?php echo e(asset('js/jquery-1.11.1.min.js')); ?>" ></script>
		
		<link href="<?php echo e(asset('css/videojs-min-css-5.0.css')); ?>" type="text/css" rel="stylesheet" />

		<script>var app={};</script>

		    
		<script src="<?php echo e(asset('js/1371544684.js')); ?>"></script>
		    

		<script type="text/javascript" src="<?php echo e(asset('js/app.js')); ?>"></script>
		<script type="text/javascript" src="<?php echo e(asset('js/app.sharing.js')); ?>"></script>
		<script type="text/javascript" src="<?php echo e(asset('js/app.wishlist.js')); ?>"></script>

		<script type="text/javascript" src="<?php echo e(asset('js/share-manager.js')); ?>"></script>


		



		

		<!-- End: components/omniture/headervars -->


		<script type="text/javascript" src="<?php echo e(asset('js/metrics.js')); ?>"></script>
	    <script type="text/javascript" src="<?php echo e(asset('js/product.js')); ?>"></script>
	    <script type="text/javascript" src="<?php echo e(asset('js/searchsuggest.js')); ?>"></script>



			

		<script>
			var pageContext = {};
			if (app)
				if (app.metrics)
					app.metrics.setContext(pageContext);
		</script>
		<link rel="stylesheet" href="<?php echo e(asset('css/responsive.css')); ?>" />
		<script src="<?php echo e(asset('js/responsive.js')); ?>" type="text/javascript"></script>

			<script type="text/javascript">
			
				app.device = "desktop";
				app.bandwidth = "";
				//alert('device='+app.device+'\r\nsetDeviceViewport=true\r\nloadResponsive=true');
			
				var hideUrlBar = function() {
					if (window.pageYOffset <= 0)
						window.scrollTo(0,1);
				};
				
				if (app.device == 'mobile') {
					window.addEventListener('load',function(){
						window.setTimeout(hideUrlBar, 0); 
						window.setTimeout(hideUrlBar, 200);
					}, false);			
				}

				jQuery(document).ready(function() {
					jQuery('html').addClass('device-' + app.device);
					if (screen.width < 768) {
						jQuery('#footer .mobile-site-link').show();
					} else {
						jQuery('#footer .full-site-link').hide();
					}
				});
				
			</script>
			<!-- Start: components/adobetagmanager/adobetagmanagerheader -->
			
			
				<!-- No ADTM ID for object -->
			 
		<!-- End: components/adobetagmanager/adobetagmanagerheader -->






		<script type="text/javascript">//<!--
		/* <![CDATA[ (head-active_data.js) */
		var dw = (window.dw || {});
		dw.ac = {
		    _analytics: null,
		    _events: [],
		    _category: "",
		    _capture: function(configs) {
		        if (Object.prototype.toString.call(configs) === "[object Array]") {
		            configs.forEach(captureObject);
		            return;
		        }
		        dw.ac._events.push(configs);
		    },
		    capture: function() { dw.ac._capture(arguments); },
		    EV_PRD_SEARCHHIT: "searchhit",
		    EV_PRD_DETAIL: "detail",
		    EV_PRD_RECOMMENDATION: "recommendation",
		    EV_PRD_SETPRODUCT: "setproduct",
		    applyContext: function(context) {
		        if (typeof context === "object" && context.hasOwnProperty("category")) {
		        	dw.ac._category = context.category;
		        }
		    },
		    setDWAnalytics: function(analytics) {
		        dw.ac._analytics = analytics;
		    }
		};
		/* ]]> */
		// -->
		</script><script type="text/javascript">//<!--
		/* <![CDATA[ (head-cquotient.js) */
		var CQuotient = window.CQuotient = {};
		CQuotient.clientId = 'aafh-ToryBurch_US';
		CQuotient.activities = [];
		CQuotient.cqcid='';
		CQuotient.cquid='';
		CQuotient.initFromCookies = function () {
			var ca = document.cookie.split(';');
			for(var i=0;i < ca.length;i++) {
			  var c = ca[i];
			  while (c.charAt(0)==' ') c = c.substring(1,c.length);
			  if (c.indexOf('cqcid=') == 0) {
				  CQuotient.cqcid=c.substring('cqcid='.length,c.length);
			  } else if (c.indexOf('cquid=') == 0) {
				  CQuotient.cquid=c.substring('cquid='.length,c.length);
				  break;
			  }
			}
		}
		CQuotient.getCQCookieId = function () {
			if(window.CQuotient.cqcid == '')
				window.CQuotient.initFromCookies();
			return window.CQuotient.cqcid;
		};
		CQuotient.getCQUserId = function () {
			if(window.CQuotient.cquid == '')
				window.CQuotient.initFromCookies();
			return window.CQuotient.cquid;
		};
		/* ]]> */
		// -->
		</script>
		<!-- Demandware Apple Pay -->

		<style type="text/css">ISAPPLEPAY{display:inline;}.dw-apple-pay-button,.dw-apple-pay-button:hover,.dw-apple-pay-button:active{background-color:black;background-image:-webkit-named-image(apple-pay-logo-white);background-position:50% 50%;background-repeat:no-repeat;background-size:75% 60%;border-radius:5px;border:1px solid black;box-sizing:border-box;margin:5px auto;min-height:30px;min-width:100px;padding:0;}
		.dw-apple-pay-button:after{content:'Apple Pay';visibility:hidden;}.dw-apple-pay-button.dw-apple-pay-logo-white{background-color:white;border-color:white;background-image:-webkit-named-image(apple-pay-logo-black);color:black;}.dw-apple-pay-button.dw-apple-pay-logo-white.dw-apple-pay-border{border-color:black;}</style>

				
		<!--[if lt IE 9]>	
			<link href="https://static.toryburch.com/static/on/demandware.static/Sites-ToryBurch_US-Site/-/default/v1523230596852/fonts/sweetsanspro/SweetSansProFull-ie8.css" type="text/css" rel="stylesheet" />
		<![endif]-->


		<!--[if gte IE 9]><!-->
			<link href="https://static.toryburch.com/static/on/demandware.static/Sites-ToryBurch_US-Site/-/default/v1523230596852/fonts/sweetsanspro/SweetSansProFull.css" type="text/css" rel="stylesheet" />  
		<!--<![endif]-->

		    
		    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('css/divine-slideshows.css')); ?>" media="screen"/>
			<script src="<?php echo e(asset('js/easySlider1.7.js')); ?>" type="text/javascript"></script>
				
			<link type="text/css" rel="stylesheet" href="<?php echo e(asset('css/toryblog2.css')); ?>" />
			<script type="text/javascript" language="javascript" src="<?php echo e(asset('js/toryblog.js')); ?>"></script>
			 	
				<script>jQuery(function () { app.scrollToTop.init(".blogPostsWrapper", 15); });</script>
				<?php echo $__env->yieldContent('customstyles'); ?>
</head>
<body class="blog shrinkyHeader blogLP">
<script language="javascript" type="text/javascript">
jQuery(document).ready(function () {
if (blogTheLatest) blogTheLatest.init({ imagePath: "#" });
});
</script>
<div id="container">
    <div id="header" role="banner">
		<header>
			<div class="topheadernav-wrapper">
				<div class="sticky-wrapper">
					<div class="topheadernav alternative">
					<div id="sidebarbtn" title="Menu">
						<div class="inner-sidebar">
							<span></span>
		  					<span></span>
		  					<span></span>
		  				</div>
		  			</div>
					<ul class="topheadernav-left">
						<li class="yama">
							<a href="<?php echo e(url('yama')); ?>"><img src="<?php echo e(asset('imgs/icons/no.png')); ?>"></a>
						</li>
						<li class="organic">
							<div class="tbf"><a href="<?php echo e(url('organic')); ?>"><img src="<?php echo e(asset('imgs/icons/no.png')); ?>"></a></div>
						</li>
						<li class="kids">
							<div class="logo-blog"><a href="<?php echo e(url('kids')); ?>"><img src="<?php echo e(asset('imgs/icons/no.png')); ?>"></a></div>
						</li>
					</ul>
					<ul class="topheadernav-middle">
						<li class="main-logo">
							<a href="<?php echo e(url('/')); ?>">
							</a>
						</li>
					</ul>
					<ul class="topheadernav-right">

						<li class="search">
							<div id="searchbtn" title="Search"></div>
							
							<!-- <script type="text/javascript">
							var searchSuggestURL = "/on/demandware.store/Sites-ToryBurch_US-Site/default/Search-GetSuggestions";

							jQuery(document).ready(function() {
								
							});
							</script> -->

							<div class="sitesearch mainsearch">
								<span class="arrowtip"></span>
								<span class="close"></span>
								
								
								<form action="#" method="get" id="SimpleSearchForm" class="simplesearch">
									<fieldset>
								
										<label>Search</label>
										<input type="search" name="q" value="" class="simplesearchinput" id="searchinput" title="Search" placeholder="Search" />
										
										
											<button type="submit" value="Go" name="simplesearch"><span>Go</span></button>
										
									</fieldset>
								</form>
							</div>
							<!-- END: sitesearch -->
						</li>

						<!-- <li class="topnav-shipping">
							<div class="shipping-dropdown">
								
									<a href="/global/international-shipping.html"><div class="flag-icon us"></div></a>
								
							</div>
						</li> -->

						<li class="myaccount-item">					 
			
							<a class="header-myaccount" href="#/account">My Account</a>
							<div class="header-myaccount-dialog">
								<div class="topnav-underline"></div>	
								<div class="myaccount-container">
									<div class="header-myaccount-links">	
										<a class="header-signin" href="#/account" title="Sign In">
											Нэвтрэх
										</a>
										<a class="header-createanaccount" href="#/on/demandware.store/Sites-ToryBurch_US-Site/default/Account-Register" title="Create An Account">
											Бүртгүүлэх
										</a>
									</div>
									<div class="header-myaccount-image">				
									</div>
								</div>
							</div>
						</li>
						

						<li id="wishlist-nav" class="wishlist-nav wishlist-qty-zero">
							<a href="#/wishlist" title="View Wish List">
								0
							</a>
						</li>
					</ul>
				</div>
				</div>
			</div>
		</header>    
	</div><!-- END: header -->


	<div id="main" role="main" class="contentasset mobile-not-available"><!-- dwMarker="content" dwContentID="bcKukiaagnZZEaaac6zf28pPyg" -->
			<div class="blogHeader clearfix">
				<div class="blogLogo">

					<button class="blogMenuToggle open-menu-btn"></button>
					<button class="blogMenuToggle close-menu-btn"></button>

					<a href="<?php echo e(url('now')); ?>">
						<img style="width: 150px" src=" <?php echo e(asset('imgs/icons/now1.png')); ?> " />
					</a>
			        <div class="blog-search-button blogSearchBox mobile-search-box"></div>
			    </div><!-- END: blogLogo -->
    
			    <div class="blog-search-button blogSearchBox desktop-search-box"></div>
			    
			    <!-- Search Dialog -->
			    <div id="blog-search-dialog" hidden="hidden">
			        <div id="searhDialog">
			            <div id="modalOpen">
			                <div class="searchTheBlog new-search-button">
			                    <form action="#" class="searchform" method="get">
			                        <fieldset>
			                            <input class="searchInput new-search-input" id="s" name="s" title="SEARCH THE BLOG" type="text" value="" placeholder="Хайлт" />
			                            <button class="pop-up-search-btn" type="submit" value="Search">
			                                <span style="display: none;">Search</span>
			                            </button>
			                        </fieldset>
			                    </form>
			                </div>
			            </div>
			        </div>
			    </div>

				<div class="blogNav clearfix">
				    <ul class="clearfix">
				    	<?php $__currentLoopData = $menus; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $menu): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
				        <li><a href="<?php echo e(url('now/'.$menu->nm_id.'')); ?>" title="<?php echo e($menu->name); ?>"><?php echo e($menu->name); ?></a> </li>
				        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
				    </ul>
				</div><!-- END: blogNav -->
			</div><!-- END: blogHeader -->

			<div class="blogMobileMenu">
				<?php $__currentLoopData = $menus; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $menu): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
			        <a href="<?php echo e(url('now/'.$menu->nm_id.'')); ?>" title="<?php echo e($menu->name); ?>"><?php echo e($menu->name); ?></a>
		        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
			</div><!-- END: blogMobileMenu -->
		</div><!-- End contentasset mobile-not-available -->

		<?php echo $__env->yieldContent('content'); ?>
</div><!-- END: main -->
    



<div id="footer" class="footer" role="contentinfo">
	<?php echo $__env->make('layouts.footernew', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
</div>

	<?php echo $__env->make('layouts.scripts', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php echo $__env->yieldContent('customscripts'); ?>
</body>
</html>