
<!DOCTYPE html>
<head>
<title>GOBI MONGOLIAN CASHMERE</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="shortcut icon" href="<?php echo e(asset('imgs/favicon.png')); ?>"/>



	<style>
	body  {
	    background-image: url("<?php echo e(asset('imgs/nuur.jpg')); ?>");
	    background-repeat: no-repeat;
	    background-size: 100% auto;
	}
	.center {
		margin: auto;
		margin-left: 10%;
		padding-top: 25%;
	}

	@media  only screen and (max-width: 600px) {
	    body {
	    	background-image: url("<?php echo e(asset('imgs/nuurs.jpg')); ?>");
		    background-repeat: no-repeat;
		    background-size: 100% auto;
	    }

	    .center {
			margin: auto;
			margin-left: 10%;
			padding-top: 65%;
		}
	}

	.logo {
		width: 150px; padding: 25px 25px;
	}

	.styled-select {
	  border: 1px solid #ccc;
	  box-sizing: border-box;
	  border-radius: 3px;
	  
	  overflow: hidden;
	  position: relative;
	  background-color: #fff;
	}
	.styled-select, .styled-select select { width: 240px;}
	select:focus { outline: none; }
	.styled-select select {
	  height: 34px;
	  padding: 5px 40px 5px 5px;
	  background: transparent;
	  border: none;
	  
	  /*hide default down arrow in webkit */
	  
	}

	@-moz-document url-prefix(){
	  .styled-select select { width: 110%; }
	}

	.fa-sort-desc {
	  position: absolute;
	  top: 0;
	  right: 12px;
	  font-size: 24px;
	}

	 select::-ms-expand { display: none; } /* hide default down arrow in IE10*/

	/* hack to fall back in opera */
	_:-o-prefocus, .selector {
	  .styled-select { background: none; }
	  }
	</style>
</head>
<body>
	<img src="<?php echo e(asset('imgs/logos/logo.png')); ?>" class="logo">
	<div class="center">
		<div class="styled-select">
			<row>
				<select onchange="if (this.value) window.location.href=this.value" style="text-align: center;">
					<option value="">Select country</option>
				  	<option value="http://www.gobi.mn/mn">Монгол</option>
				  	<option value="https://www.gobicashmere.com">International</option>
				</select>
			</row>
		</div>
	</div>

</body>
</html>