<?php $__env->startSection('style'); ?>
<style type="text/css">
    /*common styles !!!YOU DON'T NEED THEM */
.container {
  width: 1100px;
  margin: 50px auto 0;
}
.container h1 {
  font-size: 25px;
  text-align: center;
}
.container h1 span {
  position: relative;
  font-family: 'SamsungSans', sans-serif;
  color: #CA9CE1;
}
.container h1 span:after {
  position: absolute;
  left: 0;
  bottom: -10px;
  content: "~";
  width: 100%;
  text-align: center;
}
.container .subheader {
  text-align: center;
  margin-top: 15px;
}

.link {
  display: flex;
  justify-content: center;
  width: 800px;
  margin: 30px auto 0;
}
.link a {
  transition-property: all;
  transition-duration: 0.2s;
  transition-timing-function: linear;
  transition-delay: 0s;
  display: flex;
  align-items: center;
  flex-shrink: 0;
  margin-right: 40px;
  color: inherit;
  font-size: inherit;
  text-decoration: none;
}
.link a:hover {
  color: #CA9CE1;
}
.link a:last-child {
  margin-right: 0;
}
.link a i {
  color: #CA9CE1;
  margin-right: 9px;
  font-size: 30px;
}

.slider-containers {
  width: 100%;
  margin-top: 60px;
}

.slider-container {
  margin-bottom: 60px;
}
.slider-container h2 {
  text-align: center;
}

.flexbox-slider {
  margin-top: 50px;
}

/*slide styles !!!YOU NEED THEM */
.flexbox-slider {
  display: flex;
  width: 100%;
  height: 500px;
  visibility: hidden;
}
.flexbox-slider .flexbox-slide {
  transition-property: all;
  transition-duration: 0.3s;
  transition-timing-function: linear;
  transition-delay: 0s;
  width: 25%;
  height: 100%;
  position: relative;
  overflow: hidden;
  cursor: pointer;
  visibility: visible;
}
.flexbox-slider .flexbox-slide:after {
  position: absolute;
  top: 0;
  left: 0;
  content: "";
  display: block;
  width: 100%;
  height: 100%;
  background-color: rgba(255, 255, 255, 0.6);
  z-index: 2;
  opacity: 0;
}
.flexbox-slider .flexbox-slide img {
  position: absolute;
  top: 50%;
  left: 50%;
  height: auto;
  width: auto;
  min-width: 100%;
  min-height: 100%;
  -webkit-transform: translate(-50%, -50%);
          transform: translate(-50%, -50%);
  z-index: 1;
}
.flexbox-slider .flexbox-slide .text-block {
  position: absolute;
  left: 30px;
  bottom: 30px;
  max-width: 500px;
  padding: 20px;
  border-radius: 5px;
  background-color: rgba(0, 0, 0, 0.6);
  color: #fff;
  z-index: 4;
  visibility: hidden;
  opacity: 0;
}
.flexbox-slider .flexbox-slide .text-block h3 {
  font-size: 20px;
  font-weight: 700;
}
.flexbox-slider:hover .flexbox-slide:hover {
  flex-shrink: 0;
  width: 80%;
}

/* effect 1 styles */
.flexbox-slider.flexbox-slider-1 .flexbox-slide .text-block {
  bottom: 60px;
}
.flexbox-slider.flexbox-slider-1:hover .flexbox-slide:hover .text-block {
  transition-property: all;
  transition-duration: 0.2s;
  transition-timing-function: linear;
  transition-delay: 0.5s;
  bottom: 30px;
  opacity: 1;
  visibility: visible;
}

/* effect 2 styles */
.flexbox-slider.flexbox-slider-2 .flexbox-slide .text-block {
  left: -60px;
}
.flexbox-slider.flexbox-slider-2:hover .flexbox-slide:not(:hover):after {
  transition-property: all;
  transition-duration: 0.2s;
  transition-timing-function: linear;
  transition-delay: 0s;
  opacity: 1;
}
.flexbox-slider.flexbox-slider-2:hover .flexbox-slide:hover .text-block {
  transition-property: all;
  transition-duration: 0.3s;
  transition-timing-function: cubic-bezier(0, 0, 0, 1.82);
  transition-delay: 0.5s;
  left: 30px;
  opacity: 1;
  visibility: visible;
}

/* effect 3 styles */
.flexbox-slider.flexbox-slider-3 .flexbox-slide:after {
  -webkit-transform: scale(0.5);
          transform: scale(0.5);
  -webkit-transform-origin: center center;
          transform-origin: center center;
}
.flexbox-slider.flexbox-slider-3 .flexbox-slide .text-block {
  -webkit-transform: rotate(-20deg);
          transform: rotate(-20deg);
}
.flexbox-slider.flexbox-slider-3:hover .flexbox-slide:hover:after {
  transition-property: all;
  transition-duration: 0.2s;
  transition-timing-function: linear;
  transition-delay: 0.4s;
  -webkit-transform: scale(1);
          transform: scale(1);
  opacity: 1;
  visibility: visible;
}
.flexbox-slider.flexbox-slider-3:hover .flexbox-slide:hover .text-block {
  transition-property: all;
  transition-duration: 0.2s;
  transition-timing-function: linear;
  transition-delay: 0.6s;
  -webkit-transform: rotate(0deg);
          transform: rotate(0deg);
  opacity: 1;
  visibility: visible;
}

/* effect 4 styles */
.flexbox-slider.flexbox-slider-4 .flexbox-slide:after {
  top: -100%;
}
.flexbox-slider.flexbox-slider-4 .flexbox-slide .text-block {
  -webkit-transform: scale(1.2);
          transform: scale(1.2);
}
.flexbox-slider.flexbox-slider-4:hover .flexbox-slide:not(:hover):after {
  transition-property: all;
  transition-duration: 0.2s;
  transition-timing-function: linear;
  transition-delay: 0s;
  top: 0;
  opacity: 1;
  visibility: visible;
}
.flexbox-slider.flexbox-slider-4:hover .flexbox-slide:hover .text-block {
  transition-property: all;
  transition-duration: 0.2s;
  transition-timing-function: linear;
  transition-delay: 0.5s;
  -webkit-transform: scale(1);
          transform: scale(1);
  opacity: 1;
  visibility: visible;
}

/* effect 5 styles */
.flexbox-slider.flexbox-slider-5 .flexbox-slide .text-block {
  -webkit-transform: scale(0.85);
          transform: scale(0.85);
}
.flexbox-slider.flexbox-slider-5:hover .flexbox-slide:hover .text-block {
  transition-property: all;
  transition-duration: 0.2s;
  transition-timing-function: linear;
  transition-delay: 0.7s;
  -webkit-transform: scale(1);
          transform: scale(1);
  opacity: 1;
  visibility: visible;
}
.flexbox-slider.flexbox-slider-5:hover .flexbox-slide:hover:after {
  transition-property: all;
  transition-duration: 0.2s;
  transition-timing-function: linear;
  transition-delay: 0.4s;
  opacity: 1;
  visibility: visible;
}

</style>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<div id="rn_PageTitle" class="rn_AnswerDetail">
                <!-- <div id="rn_SearchControls" class="tb_Mobile_Show">
                    <h1 class="tb_Mobile_Title">Frequently Asked Questions</h1>
                    <h1 class="rn_ScreenReaderOnly">Search</h1>
                    <form onsubmit="return false;">
                        <div class="rn_SearchInput">
                            <rn:widget path="search/AdvancedSearchDialog"/>
                            <rn:widget path="search/KeywordText" label_text="" placeholder_text="Search a frequently asked question" label_text="Find the answer to your question" initial_focus="false"/>
                        </div>
                        <div class="">
                            <rn:widget path="search/SearchButton" icon_path="images/tb_search_icon.png" report_page_url="/app/answers/list"/>
                        </div>
                    </form>
                    <rn:widget path="search/DisplaySearchFilters"/>
                </div> -->
                <h1 id="rn_Summary">Говь ХК</h1>
            </div>
            <div id="rn_PageContent" class="rn_AnswerDetail">
                <div id="rn_AnswerText">
                    <div class="rn_AnswerPage rn_StyledFromCSS">  
                        <div class='section'>
                            <img src="<?php echo e(asset('imgs/GobiaboutIMG.jpg')); ?>" style="width: 100%">
                            <p style="text-align: justify;">Монголын ноос, ноолуурын салбарын анхдагч “Говь” үйлдвэр нь 1981 онд Япон улсын хөрөнгө оруулалттайгаар байгуулагдаж эдүгээ 30 гаруй жил тасралтгүй үйл ажиллагаагаа амжилттай явуулж байгаа үндэсний томоохон үйлдвэрлэгч юм. “Говь” ХК-ийн төрийн мэдлийн хувьцааг 2007 оны 7 сард “ЭФ СИ АЙ” ХХК Олон улсын нээлттэй дуудлага худалдаагаар худалдан авснаар “Говь” ХК өнөөг хүртэл хувийн хэвшлийн бүтэц зохион байгуулалт, менежменттэйгээр үйл ажиллагаагаа явуулж байна.</p>
                            <p style="text-align: justify;">Монгол ямааны 100% ноолуур болон тэмээний ноосыг ашиглан дэлхийн зэрэглэлийн хамгийн сүүлийн үеийн техник, технологиор үйлдвэрлэл явуулдаг “Говь” ХК нь үйлдвэрлэл, хүчин чадал, чанар, үйлчилгээ зэрэг бүх талын үзүүлэлтээрээ дэлхийн топ 5 ноолууран бүтээгдэхүүн үйлдвэрлэгчийн тоонд зүй ёсоор тооцогддог билээ. 1600 гаруй чадварлаг боловсон хүчнийг эгнээндээ нэгтгэсэн “Говь” ХК жилдээ 1100 тн түүхий ноос, ноолуур 30 тн бөмбөгөн ноос, ноолуур 210 тн самнасан ноолуур боловсруулах хүчин чадалтай ба түүхий эд бэлтгэхээс эхлээд бэлэн бүтээгдэхүүн үйлдвэрлэх хүртэлх бүхий л шатны үйлдвэрлэл, дамжлагыг 100% Монголдоо боловсруулдаг цорын ганц үйлдвэр юм.</p>
                        </div>
                    </div>
                </div>
            </div>
            <h2>Ноолуурын тухай</h2>

            <!--effect #1 -->
            <div class="slider-container">
              <div class="flexbox-slider flexbox-slider-1">
                <div class="flexbox-slide">
                  <img src="<?php echo e(asset('imgs/white.jpg')); ?>">
                  <div class="text-block">
                    <h3>ЦАГААН НООЛУУР</h3>
                    <div class="text">
                      <p style="text-align: justify;">Цагаан өнгө нийт Монгол ямааны 20 хүртэлх хувийг эзэлдэг ажээ. Тэдний ихэнх нь Монголын өмнөд бүс болох Өмнөговь, Баянхонгор, Говь-Алтай аймгуудад байдаг. Монголын говьд эрс тэс уур амьсгал ноёрхдог бөгөөд өвөл -40oC, хэм хүрч зун +50oC хэм хүртэл халдаг байна. “Говь” ХК онцгойлон Баянхонгор аймгийн Шинэжинст сумаас цагаан ноолуурыг авдаг.</p>
                    </div>
                  </div>
                </div>
                <div class="flexbox-slide">
                  <img src="<?php echo e(asset('imgs/brown.jpg')); ?>">
                  <div class="text-block">
                    <h3>БОР НООЛУУР</h3>
                    <div class="text">
                      <p style="text-align: justify;">Монголын баруун хэсгээр Алтайн нуруунд оршдог. Баян-Өлгий, Ховд, Завхан, Хөвсгөл аймгууд бор өнгийн ноолуурыг гаргадаг хар зүсмийн ямааны нутаг юм. Алтай Хангайн бүс далайгаас 2,000 – 4,000м өндөрлөгт оршдог бөгөөд асар хүйтэн болдог. Гэвч үслэг зөөлөн ноолуур энэ тэсгэм хүйтэн өвлөөр ч ямааг хүйтнээс бүрэн хамгаалж байдаг юм. Жил бүрийн 3-р сард “Говь” ХК Хөвсгөл аймгийн Төмөрбулаг сумаар айлчилж, бор өнгийн ноолуурыг шилэн авдаг билээ.</p>
                    </div>
                  </div>
                </div>
                <div class="flexbox-slide">
                  <img src="<?php echo e(asset('imgs/grey.jpg')); ?>">
                  <div class="text-block">
                    <h3>ХӨХ СААРАЛ НООЛУУР</h3>
                    <div class="text">
                      <p style="text-align: justify;">Дэлхийн хамгийн ховор ноолуурын өнгө бол хөх саарал. Хөх саарал өнгийн ямаа зөвхөн Монголд байдаг бөгөөд Монгол ямааны ердөө 2 хувийг эзэлдэг. Энэ нь дэлхий дээрх 2,000 ямаа тутмын нэг л хөх саарал өнгөтэй байдаг гэсэн үг юм. “Говь” ХК-ийн байгалийн хамгийн ховор бөгөөд онцгой ноолуурыг үйлчлүүлэгчиддээ түгээх зорилго, энэхүү содон ноолуурыг хайж олоход хүргэсэн билээ. Хөх саарал өнгийн энэ ноолуурыг зөвхөн “Говь” ХК боловсруулан, бүтээгдэхүүн болгон үйлдвэрлэдэг бөгөөд Ховд аймгаас тусгайлан хөх саарал өнгийн ноолуурыг авдаг.</p>
                    </div>
                  </div>
                </div>
                <div class="flexbox-slide">
                  <img src="<?php echo e(asset('imgs/beige.jpg')); ?>">
                  <div class="text-block">
                    <h3>ЦАЙВАР ШАРГАЛ НООЛУУР</h3>
                    <div class="text">
                      <p style="text-align: justify;">Цайвар шаргал өнгө хамгийн элбэг байдаг. Монголын ямааны бараг 60 хувийг эзлэх энэ өнгө мөн бүх аймгаар тархсан байдаг. Дулаан, тансаг харагдах цайвар шаргал өнгө органик ноолуурын хамгийн тохиромжтой төлөөлөл юм. Хэнтий аймгийн Галшар сумын малчидтай “Говь” ХК тусгайлан харилцан ажиллаж, цайвар шаргал ноолуурыг боловсруулдаг байна.</p>
                    </div>
                  </div>
                </div>
              </div>
            </div><!-- effect #1 end -->
<?php $__env->stopSection(); ?>
<?php echo $__env->make('contents.other.about1', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>