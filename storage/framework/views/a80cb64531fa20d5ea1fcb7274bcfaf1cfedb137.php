<?php $__env->startSection('style'); ?>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
<?php $__env->stopSection(); ?>
<?php $__env->startSection('content'); ?>
<div id="rn_PageTitle" class="rn_AnswerDetail">
  <!-- <div id="rn_SearchControls" class="tb_Mobile_Show">
      <h1 class="tb_Mobile_Title">Frequently Asked Questions</h1>
      <h1 class="rn_ScreenReaderOnly">Search</h1>
      <form onsubmit="return false;">
          <div class="rn_SearchInput">
              <rn:widget path="search/AdvancedSearchDialog"/>
              <rn:widget path="search/KeywordText" label_text="" placeholder_text="Search a frequently asked question" label_text="Find the answer to your question" initial_focus="false"/>
          </div>
          <div class="">
              <rn:widget path="search/SearchButton" icon_path="images/tb_search_icon.png" report_page_url="/app/answers/list"/>
          </div>
      </form>
      <rn:widget path="search/DisplaySearchFilters"/>
  </div> -->
  
</div>
<h2>БУЦААЛТЫН ЖУРАМ</h2>
<div class="aboutSubContent" style="font-size: 14px">
  <p style="text-align: justify;">Та хэрвээ худалдаж авсан бараандаа сэтгэл хангалуун бус байвал, бараа худалдаж авсан өдрөө буцаалт хийж болно эсвэл 3 хоног /72цаг/ дотор өнгө, размер, загварын хувьд солих боломжтой. 

Жич: Загварын төрлөөр сольж буй тохиолдолд тухайн бүтээгдэхүүний үнэ борлогдсон барааны үнийн дүнгээс бага байж болохгүй.
  
Буцаах болон солих бараа нь анх худалдаж авсан байдлаар, өмсөөгүй, цэвэр, шошго, гар талон,кассын баримтын хамт байх бөгөөд дээрх шаардлага хангаагүй тохиолдолд буцаалт болон солилт хийхгүй болно.</p>
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startSection('scripts'); ?>
<script type="text/javascript" src="<?php echo e(asset('js/jquery.dataTables.min.js')); ?>"></script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#example').DataTable({
      "searching": false,
      "lengthChange" : false,
      "pageLength": 1000,
      "paging": false,
      "info": false,
      "language": {
            "info": " Нийт _TOTAL_ үр дүн. Энэ хуудсанд _START_  - _END_",
            "paginate": {
              "previous": "Өмнөх",
              "next": "Дараах"
            }
        }
    });
} );
</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('contents.other.about.contact', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>