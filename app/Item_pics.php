<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item_pics extends Model
{
    protected $table = 'item_pics';
    protected $primaryKey = 'ip_id';
}
