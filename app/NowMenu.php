<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class NowMenu extends Model
{
    protected $table = 'now_menu';
    protected $primaryKey = 'nm_id';
}
