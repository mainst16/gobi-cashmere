<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Now extends Model
{
    protected $table = 'now';
    protected $primaryKey = 'now_id';
}
