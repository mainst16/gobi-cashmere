<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jobs extends Model
{
    protected $table = 'jobs';
    protected $primaryKey = 'job_id';

    public function depss(){
    	return $this->belongsTo('App\Department', 'dep_id', 'dep_id');
    }
}
