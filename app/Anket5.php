<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Anket5 extends Model
{
    protected $table = 'anket5';
    protected $primaryKey = 'id';
}
