<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Brands extends Model
{
    protected $table = 'brands';
    protected $primaryKey = 'brand_id';

    public function colls(){
    	return $this->hasMany('App\Collection', 'brand_id', 'brand_id');
    }
}

