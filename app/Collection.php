<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Collection extends Model
{
    public function brands(){
    	return $this->belongsTo('App\Brands', 'brand_id', 'brand_id');
    }
}
