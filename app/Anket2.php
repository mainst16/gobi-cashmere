<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Anket2 extends Model
{
    protected $table = 'anket2';
    protected $primaryKey = 'id';
}
