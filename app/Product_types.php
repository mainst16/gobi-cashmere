<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product_types extends Model
{
    protected $table = 'product_types';

    protected $primaryKey = 'pt_id';

    public function prods(){
    	return $this->hasMany('App\Products', 'pt_id', 'pt_id');
    }
}
