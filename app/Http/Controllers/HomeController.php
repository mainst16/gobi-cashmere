<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Brands;
use App\Product_types;
use App\Genders;
use App\Products;
use App\Item_pics;
use DB;
use View;
use App\Mails;
use App\Now;
use App\NowMenu;
use App\User;
use App\Mail\Mailtest;
use Mail;
use App\Collection;
use App\Sale;


class HomeController extends Controller
{
	public function alogin()
    {
        $male = Product_types::where('gender_id','=', 1)->get();
        $fmale = Product_types::where('gender_id','=', 2)->get();
        $home = Product_types::where('gender_id','=', 3)->get();
        $acc = Product_types::where('gender_id','=', 4)->get();
        $brands = Brands::where('brand_id', '=', 3)->first();
        $urlbrand = $brands->brand_id;
        // return $brand;
        return view('account.alogin',['male' => $male, 'fmale' => $fmale, 'home' => $home, 'acc' => $acc, 'urlbrand' => $urlbrand]);
    }
    public function areg()
    {
        $male = Product_types::where('gender_id','=', 1)->get();
        $fmale = Product_types::where('gender_id','=', 2)->get();
        $home = Product_types::where('gender_id','=', 3)->get();
        $acc = Product_types::where('gender_id','=', 4)->get();
        $brands = Brands::where('brand_id', '=', 3)->first();
        $urlbrand = $brands->brand_id;
        // return $brand;
        return view('account.aregister',['male' => $male, 'fmale' => $fmale, 'home' => $home, 'acc' => $acc, 'urlbrand' => $urlbrand]);
    }

    public function createreg(Request $request)
    {
        $male = Product_types::where('gender_id','=', 1)->get();
        $fmale = Product_types::where('gender_id','=', 2)->get();
        $home = Product_types::where('gender_id','=', 3)->get();
        $acc = Product_types::where('gender_id','=', 4)->get();
        $brands = Brands::where('brand_id', '=', 3)->first();
        $urlbrand = $brands->brand_id;


        
        // dd($request->all());
        // return "result";

        $new_user = new User;
        $new_user->role_id = 2; 
        $new_user->fname = $request->fname;
        $new_user->name = $request->name;
        $new_user->password = bcrypt($request->password);
        $new_user->email = $request->email;
        $new_user->country = $request->country;
        $new_user->gender = $request->gender;
        $new_user->save();
        // return $brand;
        return redirect()->action('HomeController@index', ['alert' => 'Aмжилттай бүртгэгдлээ.']);
    }

    public function index(Request $request)
    {
        $male = Product_types::where('gender_id','=', 1)->get();
        $fmale = Product_types::where('gender_id','=', 2)->get();
        $home = Product_types::where('gender_id','=', 3)->get();
        $acc = Product_types::where('gender_id','=', 4)->get();
        $brands = Brands::where('brand_id', '=', 3)->first();
        $urlbrand = $brands->brand_id;
        // return $brand;
        return view('contents.home',['male' => $male, 'fmale' => $fmale, 'home' => $home, 'acc' => $acc, 'urlbrand' => $urlbrand]);
    }

    public function atrum()
    {
        $male = Product_types::where('gender_id','=', 1)->get();
        $fmale = Product_types::where('gender_id','=', 2)->get();
        $home = Product_types::where('gender_id','=', 3)->get();
        $acc = Product_types::where('gender_id','=', 4)->get();
        // return $male;
        return view('contents.atrum',['male' => $male, 'fmale' => $fmale, 'home' => $home, 'acc' => $acc]);
    }

    public function yama()
    {
        $male = Product_types::where('gender_id','=', 1)->get();
        $fmale = Product_types::where('gender_id','=', 2)->get();
        $home = Product_types::where('gender_id','=', 3)->get();
        $acc = Product_types::where('gender_id','=', 4)->get();
         $brands = Brands::where('brand_id', '=', 1)->first();
         $urlbrand = $brands->brand_id;
        // return $male;
        return view('contents.yamaHome',['male' => $male, 'fmale' => $fmale, 'home' => $home, 'acc' => $acc, 'urlbrand' => $urlbrand]);
    }

    public function organic()
    {
        $male = Product_types::where('gender_id','=', 1)->get();
        $fmale = Product_types::where('gender_id','=', 2)->get();
        $home = Product_types::where('gender_id','=', 3)->get();
        $acc = Product_types::where('gender_id','=', 4)->get();
         $brands = Brands::where('brand_id', '=', 2)->first();
         $urlbrand = $brands->brand_id;
        // return $male;
        return view('contents.organicHome',['male' => $male, 'fmale' => $fmale, 'home' => $home, 'acc' => $acc, 'urlbrand' => $urlbrand]);
    }
    public function kids()
    {
        $male = Product_types::where('gender_id','=', 1)->get();
        $fmale = Product_types::where('gender_id','=', 2)->get();
        $home = Product_types::where('gender_id','=', 3)->get();
        $acc = Product_types::where('gender_id','=', 4)->get();
         $brands = Brands::where('brand_id', '=', 4)->first();
         $urlbrand = $brands->brand_id;
        // return $male;
        return view('contents.kidsHome',['male' => $male, 'fmale' => $fmale, 'home' => $home, 'acc' => $acc, 'urlbrand' => $urlbrand]);
    }

    public function products(Request $request, $brand, $gender, $pt)
    {
        $male = Product_types::where('gender_id','=', 1)->get();
        $fmale = Product_types::where('gender_id','=', 2)->get();
        $home = Product_types::where('gender_id','=', 3)->get();
        $acc = Product_types::where('gender_id','=', 4)->get();
        $products = Products::where('brand_id', '=', $brand)
        					->where('pt_id', '=', $pt)
        					->get();
        // dd($request->is('products/3/*'));
        $url = $request->path();
        $types = Product_types::where('pt_id', '=', $pt)->first();
        $brands = Brands::where('brand_id', '=', $brand)->first();
        $urlbrand = $brands->brand_id;

        // return $products;
        return view('contents.products',['male' => $male, 'fmale' => $fmale, 'home' => $home, 'acc' => $acc, 'products' => $products, 'url' => $url, 'types' => $types, 'urlbrand' => $urlbrand]);
    }

    public function items($brand, $product)
    {
        $male = Product_types::where('gender_id','=', 1)->get();
        $fmale = Product_types::where('gender_id','=', 2)->get();
        $home = Product_types::where('gender_id','=', 3)->get();
        $acc = Product_types::where('gender_id','=', 4)->get();
        $products = Products::where('product_id', '=', $product)->first();
        $pics = Item_pics::where('product_id', '=', $product)->get();
        $brands = Brands::where('brand_id', '=', $brand)->first();
        $urlbrand = $brands->brand_id;
        // $products->pics
        $pic = str_replace('\\', '/', $products->pics);
        // return $data;
        return view('contents.items',['male' => $male, 'fmale' => $fmale, 'home' => $home, 'acc' => $acc, 'products' => $products, 'pics' => $pics, 'urlbrand' => $urlbrand, 'pic' => $pic]);
    }

    public function allpros($brand, $gender)
    {
        $male = Product_types::where('gender_id','=', 1)->get();
        $fmale = Product_types::where('gender_id','=', 2)->get();
        $home = Product_types::where('gender_id','=', 3)->get();
        $acc = Product_types::where('gender_id','=', 4)->get();
        $products = Products::leftJoin('product_types', 'products.pt_id', '=', 'product_types.pt_id')
        						->leftJoin('genders', 'product_types.gender_id', '=', 'genders.gender_id')
        						->where('products.brand_id', '=', $brand)
        						->where('product_types.gender_id', '=', $gender)
        						->select('products.name', 'products.brand_id', 'products.product_id', 'products.pt_id', 'products.pics')
        						->get();
        // dd($request->is('products/3/*'));
        // $url = $request->path();
        $types = Genders::where('gender_id', '=', $gender)->first();
        // $ptypes = Product_types::where('gender_id', '=', $types->gender_id)->get();
        $brands = Brands::where('brand_id', '=', $brand)->first();
        $urlbrand = $brands->brand_id;

        $ptypes = Product_types::leftJoin('products', 'products.pt_id', '=', 'product_types.pt_id')
        						->leftJoin('genders', 'product_types.gender_id', '=', 'genders.gender_id')
        						->where('products.brand_id', '=', $brand)
        						->where('product_types.gender_id', '=', $gender)
        						->groupBy('product_types.name')
        						->select('product_types.name', 'product_types.pt_id')
                                ->orderBy('product_types.pt_id', 'asc')
        						->get();
        // return $brand ;
        // return $productstypes;
        return view('contents.allpros',['male' => $male, 'fmale' => $fmale, 'home' => $home, 'acc' => $acc, 'products' => $products, 'types' => $types, 'urlbrand' =>$urlbrand, 'ptypes' =>$ptypes, 'brand' =>$brand]);
    }

    public function coll($brand)
    {
        $male = Product_types::where('gender_id','=', 1)->get();
        $fmale = Product_types::where('gender_id','=', 2)->get();
        $home = Product_types::where('gender_id','=', 3)->get();
        $acc = Product_types::where('gender_id','=', 4)->get();
        $collect = DB::table('collections')
                                ->where('brand_id', '=', $brand)
                                ->limit(4)
                                ->orderBy('created_at', 'asc')
                                ->get();

        $brands = Brands::where('brand_id', '=', $brand)->first();
        $urlbrand = $brands->brand_id;
        

        // return $collect;
        return view('contents.collections',['male' => $male, 'fmale' => $fmale, 'home' => $home, 'acc' => $acc, 'collect' => $collect, 'urlbrand' =>$urlbrand]);
    }

    public function collitems($brand, $coll_id)
    {
        $male = Product_types::where('gender_id','=', 1)->get();
        $fmale = Product_types::where('gender_id','=', 2)->get();
        $home = Product_types::where('gender_id','=', 3)->get();
        $acc = Product_types::where('gender_id','=', 4)->get();
        $collection = Collection::where('id', '=', $coll_id)->first();
        $pics = Item_pics::where('product_id', '=', $coll_id)->get();
        $brands = Brands::where('brand_id', '=', $brand)->first();
        $urlbrand = $brands->brand_id;
        // $products->pics
        $pic = str_replace('\\\\', '/', $collection->multi_pics);
        // return $collection;
        return view('contents.collitems',['male' => $male, 'fmale' => $fmale, 'home' => $home, 'acc' => $acc, 'collection' => $collection, 'pics' => $pics, 'urlbrand' => $urlbrand, 'pic' => $pic]);
    }

    public function now()
    {
        $menus = NowMenu::all();
        $nows = Now::orderBy('now_id', 'desc')->get();
        $limit = Now::whereNotNull('vpics')->limit(5)->orderBy('created_at', 'desc')->get();
    	return view('contents.nows.contents', ['nows' => $nows, 'limit'=>$limit, 'menus' => $menus]);
    }
    public function gift()
    {
        $menus = NowMenu::all();
        $nows = Now::orderBy('now_id', 'desc')->get();
        $limit = Now::whereNotNull('vpics')->limit(5)->orderBy('created_at', 'desc')->get();
    	return view('contents.gift', ['nows' => $nows, 'limit'=>$limit, 'menus' => $menus]);
    }
    public function nowmenu($nm_id)
    {
        $menus = NowMenu::all();
        $nows = Now::all();
        $limit = Now::whereNotNull('vpics')->limit(5)->orderBy('created_at', 'desc')->get();
        $nowm = Now::where('nm_id', '=', $nm_id)->orderBy('created_at', 'desc')->get();
        // return $nowm;
        return view('contents.nows.contents', ['nows' => $nows, 'limit'=>$limit, 'menus' => $menus, 'nowm' => $nowm]);
    }
    public function nowpost(Request $request, $id)
    {
        $menus = NowMenu::all();
        $readmore = Now::where('now_id' , '=', $id)->first();
        $pic = str_replace('\\\\', '/', $readmore->multi_img);
    	return view('contents.nows.det', ['readmore' => $readmore, 'menus' => $menus, 'pic' => $pic]);
    }

    public function mail(Request $request)
    {
    	$new_mails = new Mails;
        $new_mails->mail = $request->email;
        $new_mails->country = $request->country;
        $new_mails->save();
    	// var_dump($request->all());
    	// return $request->all();
        return redirect()->action('HomeController@index')->with('alert', 'Таны email амжилттай бүртгэгдлээ.');
    }

    public function sendmail(Request $request)
    {
        $data = array('name' => $request->name, 'mail' => $request->mail, 'message' => $request->message);

        Mail::to('info@gobi.mn')->send(new Mailtest);

        // Mail::send('emails.gobimail', $data, function ($message) use($request)
        //                 {
        //                     $message->from($request->mail, $name = null);
        //                     $message->to('munkhsuld.b@gobi.mn', $name = null);
        //                     $message->subject('hello');
        //                 });
        // Mail::send('emails.gobimail', ['name' => $request->name, 'mail' => $request->mail, 'message' => $request->message], function ($message) use ($request)
        //                 {
        //                     $message->from($request->mail, $request->name);
        //                     $message->to('munkhsuld.b@gobi.mn');
        //                     $message->subject('hello');
        //                     // dd($message);
        //                 });


        return redirect()->action('HomeController@index')->with('alert', 'Таны email амжилттай илгээгдлээ.');
    }
    public function search(Request $request)
    {
        $male = Product_types::where('gender_id','=', 1)->get();
        $fmale = Product_types::where('gender_id','=', 2)->get();
        $home = Product_types::where('gender_id','=', 3)->get();
        $acc = Product_types::where('gender_id','=', 4)->get();
        // $products = DB::table('products')
        //                         ->leftJoin('product_types', 'products.pt_id', '=', 'product_types.pt_id')
        //                         ->leftJoin('genders', 'product_types.gender_id', '=', 'genders.gender_id')
        //                         ->where('products.brand_id', '=', $brand)
        //                         ->where('product_types.gender_id', '=', $gender)
        //                         ->select('products.name', 'products.brand_id', 'products.product_id', 'products.pt_id', 'products.pics')
        //                         ->get();
        // // dd($request->is('products/3/*'));
        // // $url = $request->path();
        // $types = Genders::where('gender_id', '=', $gender)->first();
        // // $ptypes = Product_types::where('gender_id', '=', $types->gender_id)->get();
        $brands = Brands::where('brand_id', '=', 3)->first();
        $urlbrand = $brands->brand_id;
        $search = $request->q;
        // dd($request->all());
        // return $request->q;
        // $ptypes = DB::table('products')
        //                         ->leftJoin('product_types', 'products.pt_id', '=', 'product_types.pt_id')
        //                         ->leftJoin('genders', 'product_types.gender_id', '=', 'genders.gender_id')
        //                         ->where('products.brand_id', '=', $brand)
        //                         ->where('product_types.gender_id', '=', $gender)
        //                         ->groupBy('product_types.name')
        //                         ->select('product_types.name')
        //                         ->get();
        $products = Products::orWhere('name', 'like', '%'.$request->q.'%')->get();

        // return $products;
        return view('search.search',['male' => $male, 'fmale' => $fmale, 'home' => $home, 'acc' => $acc, 'products' => $products, 'urlbrand' => $urlbrand]);
    }

    public function sale(Request $request)
    {
        $male = Product_types::where('gender_id','=', 1)->get();
        $fmale = Product_types::where('gender_id','=', 2)->get();
        $home = Product_types::where('gender_id','=', 3)->get();
        $acc = Product_types::where('gender_id','=', 4)->get();
        
        $brands = Brands::where('brand_id', '=', 3)->first();
        $urlbrand = $brands->brand_id;
        $search = $request->q;
        
        $sale = Sale::all();

        // return $products;
        return view('sale.sale',['male' => $male, 'fmale' => $fmale, 'home' => $home, 'acc' => $acc, 'sale' => $sale, 'urlbrand' => $urlbrand]);
    }
}
