<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Brands;
use App\Product_types;
use App\Genders;
use App\Products;
use App\Item_pics;
use App\HR;
use App\Jobs;
use App\Department;
use DB;
use App\TuzContents;
use App\Heh;
use App\Report;
use App\Tender;
use App\Nh;
use App\Anket;
use App\Anket2;
use App\Anket3;
use App\Anket4;
use App\Anket5;
use TCG\Voyager\Facades\Voyager;

class AboutController extends Controller
{
    public function index()
    {
        $male = Product_types::where('gender_id','=', 1)->get();
        $fmale = Product_types::where('gender_id','=', 2)->get();
        $home = Product_types::where('gender_id','=', 3)->get();
        $acc = Product_types::where('gender_id','=', 4)->get();
        $brands = Brands::where('brand_id', '=', 3)->first();
        $urlbrand = $brands->brand_id;
        // return $brand;
        return view('contents.other.info',['male' => $male, 'fmale' => $fmale, 'home' => $home, 'acc' => $acc, 'urlbrand' => $urlbrand]);
    }

    public function newspaper()
    {
        $male = Product_types::where('gender_id','=', 1)->get();
        $fmale = Product_types::where('gender_id','=', 2)->get();
        $home = Product_types::where('gender_id','=', 3)->get();
        $acc = Product_types::where('gender_id','=', 4)->get();
        $brands = Brands::where('brand_id', '=', 3)->first();
        $urlbrand = $brands->brand_id;
        // return $brand;
        return view('contents.other.pdfs',['male' => $male, 'fmale' => $fmale, 'home' => $home, 'acc' => $acc, 'urlbrand' => $urlbrand]);
    }

    public function jobs()
    {
        $desc = HR::first();
        $jobs = Jobs::all();
        $deps = Department::all();
    	$male = Product_types::where('gender_id','=', 1)->get();
        $fmale = Product_types::where('gender_id','=', 2)->get();
        $home = Product_types::where('gender_id','=', 3)->get();
        $acc = Product_types::where('gender_id','=', 4)->get();
        $brands = Brands::where('brand_id', '=', 3)->first();
        $urlbrand = $brands->brand_id;
        // return $desc;
        return view('contents.other.job',['male' => $male, 'fmale' => $fmale, 'home' => $home, 'acc' => $acc, 'jobs' => $jobs, 'deps' => $deps, 'desc' => $desc, 'urlbrand' => $urlbrand]);
    	// return view('contents.job');
    }

    public function vjobs($id)
    {
        $desc = HR::first();
        $jobs = Jobs::all();
        $deps = Department::all();
    	$male = Product_types::where('gender_id','=', 1)->get();
        $fmale = Product_types::where('gender_id','=', 2)->get();
        $home = Product_types::where('gender_id','=', 3)->get();
        $acc = Product_types::where('gender_id','=', 4)->get();
        $brands = Brands::where('brand_id', '=', 3)->first();
        $urlbrand = $brands->brand_id;
        $view = Jobs::where('job_id', '=', $id)->first();
        // return $desc;
        return view('contents.other.about.view_jobs',['male' => $male, 'fmale' => $fmale, 'home' => $home, 'acc' => $acc, 'jobs' => $jobs, 'deps' => $deps, 'desc' => $desc, 'urlbrand' => $urlbrand, 'view' => $view]);
    	// return view('contents.job');
    }
    public function nh()
    {
    	$male = Product_types::where('gender_id','=', 1)->get();
        $fmale = Product_types::where('gender_id','=', 2)->get();
        $home = Product_types::where('gender_id','=', 3)->get();
        $acc = Product_types::where('gender_id','=', 4)->get();
        $brands = Brands::where('brand_id', '=', 3)->first();
        $urlbrand = $brands->brand_id;
        $nhs = Nh::all();
        // return $brand;
        return view('contents.other.about.niigem',['male' => $male, 'fmale' => $fmale, 'home' => $home, 'acc' => $acc, 'urlbrand' => $urlbrand, 'nhs' => $nhs]);
    }
    public function tender()
    {
        $male = Product_types::where('gender_id','=', 1)->get();
        $fmale = Product_types::where('gender_id','=', 2)->get();
        $home = Product_types::where('gender_id','=', 3)->get();
        $acc = Product_types::where('gender_id','=', 4)->get();
        $brands = Brands::where('brand_id', '=', 3)->first();
        $urlbrand = $brands->brand_id;
        $tenders = Tender::all();
        // return $brand;
        return view('contents.other.about.tender',['male' => $male, 'fmale' => $fmale, 'home' => $home, 'acc' => $acc, 'urlbrand' => $urlbrand, 'tenders' => $tenders]);
    }

    public function huwi($id)
    {
    	$male = Product_types::where('gender_id','=', 1)->get();
        $fmale = Product_types::where('gender_id','=', 2)->get();
        $home = Product_types::where('gender_id','=', 3)->get();
        $acc = Product_types::where('gender_id','=', 4)->get();
        $brands = Brands::where('brand_id', '=', 3)->first();
        $tuzs = TuzContents::where('tm_id', '=', $id)->get();
        $urlbrand = $brands->brand_id;
        // return $brand;
        return view('contents.other.huwitsaa.huwicaa',['male' => $male, 'fmale' => $fmale, 'home' => $home, 'acc' => $acc, 'urlbrand' => $urlbrand, 'tuzs' => $tuzs]);
    }

    public function brand()
    {
    	$male = Product_types::where('gender_id','=', 1)->get();
        $fmale = Product_types::where('gender_id','=', 2)->get();
        $home = Product_types::where('gender_id','=', 3)->get();
        $acc = Product_types::where('gender_id','=', 4)->get();
        $brands = Brands::where('brand_id', '=', 3)->first();
        $urlbrand = $brands->brand_id;
        // return $brand;
        return view('contents.other.about.brands',['male' => $male, 'fmale' => $fmale, 'home' => $home, 'acc' => $acc, 'urlbrand' => $urlbrand]);
    }

    public function branch()
    {
    	$male = Product_types::where('gender_id','=', 1)->get();
        $fmale = Product_types::where('gender_id','=', 2)->get();
        $home = Product_types::where('gender_id','=', 3)->get();
        $acc = Product_types::where('gender_id','=', 4)->get();
        $brands = Brands::where('brand_id', '=', 3)->first();
        $urlbrand = $brands->brand_id;
        // return $brand;
        return view('contents.other.about.branches',['male' => $male, 'fmale' => $fmale, 'home' => $home, 'acc' => $acc, 'urlbrand' => $urlbrand]);
    }

    public function zasaglal($id)
    {
    	$male = Product_types::where('gender_id','=', 1)->get();
        $fmale = Product_types::where('gender_id','=', 2)->get();
        $home = Product_types::where('gender_id','=', 3)->get();
        $acc = Product_types::where('gender_id','=', 4)->get();
        $brands = Brands::where('brand_id', '=', 3)->first();
        $tuzs = TuzContents::where('tm_id', '=', $id)->get();
        $urlbrand = $brands->brand_id;
        // return $brand;
        return view('contents.other.huwitsaa.zasaglal',['male' => $male, 'fmale' => $fmale, 'home' => $home, 'acc' => $acc, 'urlbrand' => $urlbrand, 'tuzs' => $tuzs]);
    }

    public function hezemshigch($id)
    {
    	$male = Product_types::where('gender_id','=', 1)->get();
        $fmale = Product_types::where('gender_id','=', 2)->get();
        $home = Product_types::where('gender_id','=', 3)->get();
        $acc = Product_types::where('gender_id','=', 4)->get();
        $brands = Brands::where('brand_id', '=', 3)->first();
        $tuzs = TuzContents::where('tm_id', '=', $id)->get();
        $hehs = Heh::all();
        $urlbrand = $brands->brand_id;
        // return $brand;
        return view('contents.other.huwitsaa.hezemshigch',['male' => $male, 'fmale' => $fmale, 'home' => $home, 'acc' => $acc, 'urlbrand' => $urlbrand, 'tuzs' => $tuzs, 'hehs' => $hehs]);
    }

    public function zuwlul($id)
    {
    	$male = Product_types::where('gender_id','=', 1)->get();
        $fmale = Product_types::where('gender_id','=', 2)->get();
        $home = Product_types::where('gender_id','=', 3)->get();
        $acc = Product_types::where('gender_id','=', 4)->get();
        $brands = Brands::where('brand_id', '=', 3)->first();
        $tuzs = TuzContents::where('tm_id', '=', $id)->get();
        $urlbrand = $brands->brand_id;
        // return $brand;
        return view('contents.other.huwitsaa.zuwlul',['male' => $male, 'fmale' => $fmale, 'home' => $home, 'acc' => $acc, 'urlbrand' => $urlbrand, 'tuzs' => $tuzs]);
    }
    public function zahzeel($id)
    {
    	$male = Product_types::where('gender_id','=', 1)->get();
        $fmale = Product_types::where('gender_id','=', 2)->get();
        $home = Product_types::where('gender_id','=', 3)->get();
        $acc = Product_types::where('gender_id','=', 4)->get();
        $brands = Brands::where('brand_id', '=', 3)->first();
        $tuzs = TuzContents::where('tm_id', '=', $id)->get();
        $reports = Report::all();
        $urlbrand = $brands->brand_id;
        // return $brand;
        return view('contents.other.huwitsaa.zahzeel',['male' => $male, 'fmale' => $fmale, 'home' => $home, 'acc' => $acc, 'urlbrand' => $urlbrand, 'tuzs' => $tuzs, 'reports' => $reports]);
    }
    public function udirdlaga($id)
    {
        $male = Product_types::where('gender_id','=', 1)->get();
        $fmale = Product_types::where('gender_id','=', 2)->get();
        $home = Product_types::where('gender_id','=', 3)->get();
        $acc = Product_types::where('gender_id','=', 4)->get();
        $brands = Brands::where('brand_id', '=', 3)->first();
        $tuzs = TuzContents::where('tm_id', '=', $id)->get();
        $reports = Report::all();
        $urlbrand = $brands->brand_id;
        // return $brand;
        return view('contents.other.huwitsaa.udirdlaga',['male' => $male, 'fmale' => $fmale, 'home' => $home, 'acc' => $acc, 'urlbrand' => $urlbrand, 'tuzs' => $tuzs, 'reports' => $reports]);
    }
    public function medeelel($id)
    {
        $male = Product_types::where('gender_id','=', 1)->get();
        $fmale = Product_types::where('gender_id','=', 2)->get();
        $home = Product_types::where('gender_id','=', 3)->get();
        $acc = Product_types::where('gender_id','=', 4)->get();
        $brands = Brands::where('brand_id', '=', 3)->first();
        $tuzs = TuzContents::where('tm_id', '=', $id)->get();
        $reports = Report::all();
        $urlbrand = $brands->brand_id;
        // return $brand;
        return view('contents.other.huwitsaa.medeelel',['male' => $male, 'fmale' => $fmale, 'home' => $home, 'acc' => $acc, 'urlbrand' => $urlbrand, 'tuzs' => $tuzs, 'reports' => $reports]);
    }

    public function services(Request $request)
    {
    	$male = Product_types::where('gender_id','=', 1)->get();
        $fmale = Product_types::where('gender_id','=', 2)->get();
        $home = Product_types::where('gender_id','=', 3)->get();
        $acc = Product_types::where('gender_id','=', 4)->get();
        $brands = Brands::where('brand_id', '=', 3)->first();
        $urlbrand = $brands->brand_id;
        // return $brand;
       
        return view('contents.other.about.services',['male' => $male, 'fmale' => $fmale, 'home' => $home, 'acc' => $acc, 'urlbrand' => $urlbrand]);
        
    }
    public function servicescare(Request $request)
    {
    	$male = Product_types::where('gender_id','=', 1)->get();
        $fmale = Product_types::where('gender_id','=', 2)->get();
        $home = Product_types::where('gender_id','=', 3)->get();
        $acc = Product_types::where('gender_id','=', 4)->get();
        $brands = Brands::where('brand_id', '=', 3)->first();
        $urlbrand = $brands->brand_id;
        // return $brand;
        
		return view('contents.other.about.himi',['male' => $male, 'fmale' => $fmale, 'home' => $home, 'acc' => $acc, 'urlbrand' => $urlbrand]);
        
    }

    public function contact()
    {
    	$male = Product_types::where('gender_id','=', 1)->get();
        $fmale = Product_types::where('gender_id','=', 2)->get();
        $home = Product_types::where('gender_id','=', 3)->get();
        $acc = Product_types::where('gender_id','=', 4)->get();
        $brands = Brands::where('brand_id', '=', 3)->first();
        $urlbrand = $brands->brand_id;
        // return $brand;
        return view('contents.other.about.contact',['male' => $male, 'fmale' => $fmale, 'home' => $home, 'acc' => $acc, 'urlbrand' => $urlbrand]);
    }

    public function returns(Request $request)
    {
    	$male = Product_types::where('gender_id','=', 1)->get();
        $fmale = Product_types::where('gender_id','=', 2)->get();
        $home = Product_types::where('gender_id','=', 3)->get();
        $acc = Product_types::where('gender_id','=', 4)->get();
        $brands = Brands::where('brand_id', '=', 3)->first();
        $urlbrand = $brands->brand_id;
        // return $brand;
       
        return view('contents.other.about.returns',['male' => $male, 'fmale' => $fmale, 'home' => $home, 'acc' => $acc, 'urlbrand' => $urlbrand]);
        
    }
    public function care(Request $request)
    {
    	$male = Product_types::where('gender_id','=', 1)->get();
        $fmale = Product_types::where('gender_id','=', 2)->get();
        $home = Product_types::where('gender_id','=', 3)->get();
        $acc = Product_types::where('gender_id','=', 4)->get();
        $brands = Brands::where('brand_id', '=', 3)->first();
        $urlbrand = $brands->brand_id;
        // return $brand;
        
		return view('contents.other.about.care',['male' => $male, 'fmale' => $fmale, 'home' => $home, 'acc' => $acc, 'urlbrand' => $urlbrand]);
        
    }
    public function anket_send(Request $request)
    {
        // Tseej zuragnii upload heseg
        $this->validate($request, [
          'select_file'  => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:20048'
         ]);

         $image = $request->file('select_file');

         $new_name = rand() . '.' . $image->getClientOriginalExtension();

         $image->move(public_path('imgs/anketpics'), $new_name);

        //diplomnii zuragnii uploud heseg
        
         $this->validate($request, [
          'select_file1'  => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:20048'
         ]);

         $image1 = $request->file('select_file1');

         $new_name1 = rand() . '.' . $image1->getClientOriginalExtension();

         $image1->move(public_path('imgs/diplompics'), $new_name1);

        
        // dd($request->all());
        $anket = new Anket;
        $anket->wish_albantushaal = $request->wish_albantushaal;
        $anket->wish_sonirhol1 = $request->wish_sonirhol1;
        $anket->wish_sonirhol2 = $request->wish_sonirhol2;
        $anket->wish_tsalin = $request->tsalin;
        $anket->zurag = $new_name;
        $anket->uovog = $request->uovog;
        $anket->ovog = $request->ovog;
        $anket->ner = $request->ner;
        $anket->homephone = $request->homephone;
        $anket->undes = $request->undes;

        $anket->blood = $request->blood;
        $anket->kg = $request->kg;
        $anket->cm = $request->cm;
        $anket->regno = $request->regno;
        $anket->emdno = $request->emdno;
        $anket->nddno = $request->nddno;
        $anket->fpno = $request->fpno;
        $anket->driveno = $request->driveno;
        $anket->angilal = $request->angilal;
        $anket->barisanjil = $request->barisanjil;

        $anket->mobile = $request->mobile;
        $anket->musthavephoneno = $request->musthavephoneno;
        $anket->email = $request->email;
        $anket->workemail = $request->workemail;
        $anket->bolowsrol = $request->bolowsrol;
        $anket->job = $request->job;
        $anket->jobrank = $request->jobrank;
        $anket->marredornot = $request->marredornot;
        $anket->borncnty = $request->borncnty;
        $anket->borncity = $request->borncity;

        $anket->bornaimag = $request->bornaimag;
        $anket->bornduureg = $request->bornduureg;
        $anket->birth = $request->birth;
        $anket->sex = $request->sex;
        $anket->tsereg = $request->tsereg;
        $anket->alba_batlah_no = $request->alba_batlah_no;
        $anket->niigmiingaral = $request->niigmiingaral;
        $anket->jil = $request->jil;
        $anket->ord = $request->ord;
        $anket->fbaddress = $request->fbaddress;

        $anket->twitaddress = $request->twitaddress;
        $anket->old_work1 = $request->old_work1;
        $anket->old_work2 = $request->old_work2;
        $anket->old_work3 = $request->old_work3;
        $anket->old_work4 = $request->old_work4;
        $anket->old_work5 = $request->old_work5;
        $anket->old_albantushaal1 = $request->old_albantushaal1;
        $anket->old_albantushaal2 = $request->old_albantushaal2;
        $anket->old_albantushaal3 = $request->old_albantushaal3;
        $anket->old_albantushaal4 = $request->old_albantushaal4;

        $anket->old_albantushaal5 = $request->old_albantushaal5;
        $anket->old_work_enter1 = $request->old_work_enter1;
        $anket->old_work_enter2 = $request->old_work_enter2;
        $anket->old_work_enter3 = $request->old_work_enter3;
        $anket->old_work_enter4 = $request->old_work_enter4;
        $anket->old_work_enter5 = $request->old_work_enter5;
        $anket->old_work_out1 = $request->old_work_out1;
        $anket->old_work_out2 = $request->old_work_out2;
        $anket->old_work_out3 = $request->old_work_out3;
        $anket->old_work_out4 = $request->old_work_out4;

        $anket->old_work_out5 = $request->old_work_out5;
        $anket->old_work_out_shaltgaan1 = $request->old_work_out_shaltgaan1;
        $anket->old_work_out_shaltgaan2 = $request->old_work_out_shaltgaan2;
        $anket->old_work_out_shaltgaan3 = $request->old_work_out_shaltgaan3;
        $anket->old_work_out_shaltgaan4 = $request->old_work_out_shaltgaan4;
        $anket->old_work_out_shaltgaan5 = $request->old_work_out_shaltgaan5;
        $anket->old_done_work1 = $request->old_done_work1;
        $anket->old_done_work2 = $request->old_done_work2;
        $anket->old_done_work3 = $request->old_done_work3;
        $anket->old_done_work4 = $request->old_done_work4;

        $anket->old_done_work5 = $request->old_done_work5;
        $anket->uunes = $request->uunes;
        $anket->udaan_workout = $request->udaan_workout;
        $anket->uls_jil = $request->uls_jil;
        $anket->uls_sar = $request->uls_sar;
        $anket->gobi_jil = $request->gobi_jil;
        $anket->gobi_sar = $request->gobi_sar;
        $anket->nd_zawsardsan_jil = $request->nd_zawsardsan_jil;
        $anket->nd_zawsardsan_sar = $request->nd_zawsardsan_sar;
        $anket->dssurname = $request->dssurname;
        $anket->dsuls = $request->dsuls;
        $anket->dshot = $request->dshot;

        $anket->dsbegindate = $request->dsbegindate;
        $anket->dsenddate = $request->dsenddate;
        $anket->dsmergejil = $request->dsmergejil;
        $anket->dsgolch = $request->dsgolch;
        $anket->dszereg = $request->dszereg;
        $anket->ksurname = $request->ksurname;
        $anket->kuls = $request->kuls;
        $anket->khot = $request->khot;
        $anket->kbegindate = $request->kbegindate;
        $anket->kenddate = $request->kenddate;

        $anket->kmergejil = $request->kmergejil;
        $anket->kgolch = $request->kgolch;
        $anket->kzereg = $request->kzereg;
        $anket->id1surname = $request->id1surname;
        $anket->id1uls = $request->id1uls;
        $anket->id1hot = $request->id1hot;
        $anket->id1begindate = $request->id1begindate;
        $anket->id1enddate = $request->id1enddate;
        $anket->id1mergejil = $request->id1mergejil;
        $anket->id1golch = $request->id1golch;

        $anket->id1zereg = $request->id1zereg;
        $anket->id2surname = $request->id2surname;
        $anket->id2uls = $request->id2uls;
        $anket->id2hot = $request->id2hot;
        $anket->id2begindate = $request->id2begindate;
        $anket->id2enddate = $request->id2enddate;
        $anket->id2mergejil = $request->id2mergejil;
        $anket->id2golch = $request->id2golch;
        $anket->id2zereg = $request->id2zereg;
        $anket->save();


        $anket2 = new Anket2;
        $anket2->anket_id = $anket->id;
        $anket2->busadsurname = $request->busadsurname;
        $anket2->busaduls = $request->busaduls;
        $anket2->busadhot = $request->busadhot;
        $anket2->busadbegindate = $request->busadbegindate;
        $anket2->busadenddate = $request->busadenddate;
        $anket2->busadmergejil = $request->busadmergejil;
        $anket2->busadgolch = $request->busadgolch;
        $anket2->busadzereg = $request->busadzereg;
        $anket2->diplom = $request->diplom;
        $anket2->diplomzurag = $request->select_file1;
        $anket2->surgaltner1 = $request->surgaltner1;

        $anket2->surgaltuls1 = $request->surgaltuls1;
        $anket2->surgaltbegindate1 = $request->surgaltbegindate1;
        $anket2->surgaltenddate1 = $request->surgaltenddate1;
        $anket2->surgaltchiglel1 = $request->surgaltchiglel1;
        $anket2->surgaltunemleh1 = $request->surgaltunemleh1;
        $anket2->surgaltner2 = $request->surgaltner2;
        $anket2->surgaltuls2 = $request->surgaltuls2;
        $anket2->surgaltbegindate2 = $request->surgaltbegindate2;
        $anket2->surgaltenddate2 = $request->surgaltenddate2;

        $anket2->surgaltchiglel2 = $request->surgaltchiglel2;
        $anket2->surgaltunemleh2 = $request->surgaltunemleh2;
        $anket2->surgaltner3 = $request->surgaltner3;
        $anket2->surgaltuls3 = $request->surgaltuls3;
        $anket2->surgaltbegindate3 = $request->surgaltbegindate3;
        $anket2->surgaltenddate3 = $request->surgaltenddate3;
        $anket2->surgaltchiglel3 = $request->surgaltchiglel3;
        $anket2->surgaltunemleh3 = $request->surgaltunemleh3;
        $anket2->famhamaatan = $request->famhamaatan;

        $anket2->famovog = $request->famovog;
        $anket2->famner = $request->famner;
        $anket2->fambirthdate = $request->fambirthdate;
        $anket2->fambirthplace = $request->fambirthplace;
        $anket2->famwork = $request->famwork;
        $anket2->famjob = $request->famjob;
        $anket2->famphone = $request->famphone;
        $anket2->famhamaatan2 = $request->famhamaatan2;
        $anket2->famhamaatan3 = $request->famhamaatan3;

        $anket2->famhamaatan4 = $request->famhamaatan4;
        $anket2->famhamaatan5 = $request->famhamaatan5;
        $anket2->famhamaatan6 = $request->famhamaatan6;
        $anket2->famovog2 = $request->famovog2;
        $anket2->famovog3 = $request->famovog3;
        $anket2->famovog4 = $request->famovog4;
        $anket2->famovog5 = $request->famovog5;
        $anket2->famovog6 = $request->famovog6;
        $anket2->famner2 = $request->famner2;
        $anket2->famner3 = $request->famner3;
        $anket2->famner4 = $request->famner4;

        $anket2->famner5 = $request->famner5;
        $anket2->famner6 = $request->famner6;
        $anket2->fambirthdate2 = $request->fambirthdate2;
        $anket2->fambirthdate3 = $request->fambirthdate3;
        $anket2->fambirthdate4 = $request->fambirthdate4;
        $anket2->fambirthdate5 = $request->fambirthdate5;
        $anket2->fambirthdate6 = $request->fambirthdate6;
        $anket2->fambirthplace2 = $request->fambirthplace2;
        $anket2->fambirthplace3 = $request->fambirthplace3;
        $anket2->fambirthplace4 = $request->fambirthplace4;
        $anket2->fambirthplace5 = $request->fambirthplace5;
        $anket2->fambirthplace6 = $request->fambirthplace6;

        $anket2->famwork2 = $request->famwork2;
        $anket2->famwork3 = $request->famwork3;
        $anket2->famwork4 = $request->famwork4;
        $anket2->famwork5 = $request->famwork5;
        $anket2->famwork6 = $request->famwork6;
        $anket2->famjob2 = $request->famjob2;
        $anket2->famjob3 = $request->famjob3;
        $anket2->famjob4 = $request->famjob4;
        $anket2->famjob5 = $request->famjob5;
        $anket2->famjob6 = $request->famjob6;
        $anket2->famphone2 = $request->famphone2;

        $anket2->famphone3 = $request->famphone3;
        $anket2->famphone4 = $request->famphone4;
        $anket2->famphone5 = $request->famphone5;
        $anket2->famphone6 = $request->famphone6;
        $anket2->save();

        $anket3 = new Anket3;
        $anket3->anket_id = $anket->id;
        $anket3->famholhamaatan = $request->famholhamaatan;
        $anket3->famholhamaatan2 = $request->famholhamaatan2;
        $anket3->famholhamaatan3 = $request->famholhamaatan3;
        $anket3->famholhamaatan4 = $request->famholhamaatan4;
        $anket3->famholovog = $request->famholovog;
        $anket3->famholovog2 = $request->famholovog2;
        $anket3->famholovog3 = $request->famholovog3;
        $anket3->famholovog4 = $request->famholovog4;
        $anket3->famholner = $request->famholner;
        $anket3->famholner2 = $request->famholner2;

        $anket3->famholner3 = $request->famholner3;
        $anket3->famholner4 = $request->famholner4;
        $anket3->famholbirthdate = $request->famholbirthdate;
        $anket3->famholbirthdate2 = $request->famholbirthdate2;
        $anket3->famholbirthdate3 = $request->famholbirthdate3;
        $anket3->famholbirthdate4 = $request->famholbirthdate4;
        $anket3->famholbirthplace = $request->famholbirthplace;
        $anket3->famholbirthplace2 = $request->famholbirthplace2;
        $anket3->famholbirthplace3 = $request->famholbirthplace3;
        $anket3->famholbirthplace4 = $request->famholbirthplace4;

        $anket3->famholwork = $request->famholwork;
        $anket3->famholwork2 = $request->famholwork2;
        $anket3->famholwork3 = $request->famholwork3;
        $anket3->famholwork4 = $request->famholwork4;
        $anket3->famholjob = $request->famholjob;
        $anket3->famholjob2 = $request->famholjob2;
        $anket3->famholjob3 = $request->famholjob3;
        $anket3->famholjob4 = $request->famholjob4;
        $anket3->famholphone = $request->famholphone;
        $anket3->famholphone2 = $request->famholphone2;

        $anket3->famholphone3 = $request->famholphone3;
        $anket3->famholphone4 = $request->famholphone4;
        $anket3->osnuhtsul = $request->osnuhtsul;
        $anket3->mineornot = $request->mineornot;
        $anket3->shagnasan_gazar = $request->shagnasan_gazar;
        $anket3->shagnal_ner = $request->shagnal_ner;
        $anket3->shagnal_date = $request->shagnal_date;
        $anket3->shagnal_haana = $request->shagnal_haana;
        $anket3->shagnal_undeslel = $request->shagnal_undeslel;
        $anket3->shagnasan_gazar2 = $request->shagnasan_gazar2;

        $anket3->shagnasan_gazar3 = $request->shagnasan_gazar3;
        $anket3->shagnasan_gazar4 = $request->shagnasan_gazar4;
        $anket3->shagnasan_gazar5 = $request->shagnasan_gazar5;
        $anket3->shagnal_ner2 = $request->shagnal_ner2;
        $anket3->shagnal_ner3 = $request->shagnal_ner3;
        $anket3->shagnal_ner4 = $request->shagnal_ner4;
        $anket3->shagnal_ner5 = $request->shagnal_ner5;
        $anket3->shagnal_date2 = $request->shagnal_date2;
        $anket3->shagnal_date3 = $request->shagnal_date3;
        $anket3->shagnal_date4 = $request->shagnal_date4;

        $anket3->shagnal_date5 = $request->shagnal_date5;
        $anket3->shagnal_haana2 = $request->shagnal_haana2;
        $anket3->shagnal_haana3 = $request->shagnal_haana3;
        $anket3->shagnal_haana4 = $request->shagnal_haana4;
        $anket3->shagnal_haana5 = $request->shagnal_haana5;
        $anket3->shagnal_undeslel2 = $request->shagnal_undeslel2;
        $anket3->shagnal_undeslel3 = $request->shagnal_undeslel3;
        $anket3->shagnal_undeslel4 = $request->shagnal_undeslel4;
        $anket3->shagnal_undeslel5 = $request->shagnal_undeslel5;
        $anket3->burtgeltei_hayag = $request->burtgeltei_hayag;
        $anket3->odoo_bgaa_hayag = $request->odoo_bgaa_hayag;
        $anket3->save();

        $anket4 = new Anket4;
        $anket4->anket_id = $anket->id;
        $anket4->eng_speak_undrstnd = $request->eng_speak_undrstnd;
        $anket4->rus_speak_undrstnd = $request->rus_speak_undrstnd;
        $anket4->jap_speak_undrstnd = $request->jap_speak_undrstnd;
        $anket4->chi_speak_undrstnd = $request->chi_speak_undrstnd;
        $anket4->flang_speak_undrstnd = $request->flang_speak_undrstnd;
        $anket4->eng_speak = $request->eng_speak;
        $anket4->rus_speak = $request->rus_speak;
        $anket4->jap_speak = $request->jap_speak;
        $anket4->chi_speak = $request->chi_speak;
        $anket4->flang_speak = $request->flang_speak;

        $anket4->eng_read = $request->eng_read;
        $anket4->rus_read = $request->rus_read;
        $anket4->jap_read = $request->jap_read;
        $anket4->chi_read = $request->chi_read;
        $anket4->flang_read = $request->flang_read;
        $anket4->eng_write = $request->eng_write;
        $anket4->rus_write = $request->rus_write;
        $anket4->jap_write = $request->jap_write;
        $anket4->chi_write = $request->chi_write;
        $anket4->flang_write = $request->flang_write;

        $anket4->Word = $request->Word;
        $anket4->Excel = $request->Excel;
        $anket4->Access = $request->Access;
        $anket4->Project = $request->Project;
        $anket4->pp = $request->pp;
        $anket4->Publisher = $request->Publisher;
        $anket4->dmax = $request->dmax;
        $anket4->ps = $request->ps;
        $anket4->InDesign = $request->InDesign;
        $anket4->Corel = $request->Corel;

        $anket4->ERP = $request->ERP;
        $anket4->Visio = $request->Visio;
        $anket4->Daimond = $request->Daimond;
        $anket4->inputprogram1 = $request->inputprogram1;
        $anket4->inputprogram2 = $request->inputprogram2;
        $anket4->inputprogram3 = $request->inputprogram3;
        $anket4->inputprogram1name = $request->inputprogram1name;
        $anket4->inputprogram2name = $request->inputprogram2name;
        $anket4->inputprogram3name = $request->inputprogram3name;
        $anket4->toeflname = $request->toeflname;

        $anket4->toefldate = $request->toefldate;
        $anket4->ieltsname = $request->ieltsname;
        $anket4->ieltsdate = $request->ieltsdate;
        $anket4->hskname = $request->hskname;
        $anket4->hskdate = $request->hskdate;
        $anket4->othername = $request->othername;
        $anket4->otherdate = $request->otherdate;
        $anket4->turul1 = $request->turul1;
        $anket4->turul2 = $request->turul2;
        $anket4->turul3 = $request->turul3;

        $anket4->hicheelesenjil1 = $request->hicheelesenjil1;
        $anket4->hicheelesenjil2 = $request->hicheelesenjil2;
        $anket4->hicheelesenjil3 = $request->hicheelesenjil3;
        $anket4->zereg1 = $request->zereg1;
        $anket4->zereg2 = $request->zereg2;
        $anket4->zereg3 = $request->zereg3;
        $anket4->baiguulga1 = $request->baiguulga1;
        $anket4->baiguulga2 = $request->baiguulga2;
        $anket4->baiguulga3 = $request->baiguulga3;
        $anket4->save();

        $anket5 = new Anket5;
        $anket5->anket_id = $anket->id;
        $anket5->ner1_6_5 = $request->ner1_6_5;
        $anket5->baiguulga1_6_5 = $request->baiguulga1_6_5;
        $anket5->atushaal1_6_5 = $request->atushaal1_6_5;
        $anket5->phone1_6_5 = $request->phone1_6_5;
        $anket5->hedenjil1_6_5 = $request->hedenjil1_6_5;
        $anket5->ner2_6_5 = $request->ner2_6_5;
        $anket5->baiguulga2_6_5 = $request->baiguulga2_6_5;
        $anket5->atushaal2_6_5 = $request->atushaal2_6_5;
        $anket5->phone2_6_5 = $request->phone2_6_5;
        $anket5->hedenjil2_6_5 = $request->hedenjil2_6_5;

        $anket5->ner3_6_5 = $request->ner3_6_5;
        $anket5->baiguulga3_6_5 = $request->baiguulga3_6_5;
        $anket5->atushaal3_6_5 = $request->atushaal3_6_5;
        $anket5->phone3_6_5 = $request->phone3_6_5;
        $anket5->hedenjil3_6_5 = $request->hedenjil3_6_5;
        $anket5->dawuutal = $request->dawuutal;
        $anket5->sultal = $request->sultal;
        $anket5->hhhugjil = $request->hhhugjil;
        $anket5->gerbultuluwlult = $request->gerbultuluwlult;
        $anket5->albantushaal = $request->albantushaal;
        $anket5->ehsurwalj = $request->ehsurwalj;
        $anket5->gobidajildag = $request->gobidajildag;
        $anket->herudah = $request->herudah;
        $anket5->uuriinhuutalaar = $request->uuriinhuutalaar;
        $anket5->save();
        // var_dump($request->all());
        return redirect()->action('HomeController@index')->with('alert', 'Таны анкет амжилттай илгээгдлээ.')->with('path', $new_name);
    }
    public function view_anket()
    {
        $anket1 = Anket::all();
        $anket2 = Anket2::all();
        $anket3 = Anket3::all();
        $anket4 = Anket4::all();
        $anket5 = Anket5::all();

        return Voyager::view('voyager::anket_admin',['anket1'=>$anket1, 'anket2' => $anket2, 'anket3' => $anket3, 'anket4' => $anket4, 'anket5' => $anket5]);
    }
    public function viewanket(Request $request, $id)
    {
        $ankets = Anket::leftJoin('anket2', 'anket2.anket_id', '=', 'anket.id')
                        ->leftJoin('anket3', 'anket3.anket_id', '=', 'anket.id')
                        ->leftJoin('anket4', 'anket4.anket_id', '=', 'anket.id')
                        ->leftJoin('anket5', 'anket5.anket_id', '=', 'anket.id')
                        ->where('anket2.anket_id', '=', $id)
                        ->where('anket3.anket_id', '=', $id)
                        ->where('anket4.anket_id', '=', $id)
                        ->where('anket5.anket_id', '=', $id)
                        ->first();
        // return $ankets;

        return view('contents.other.viewanket',['ankets'=>$ankets]);
    }
}
