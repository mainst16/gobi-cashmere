<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    protected $table = 'deps';
    protected $primaryKey = 'dep_id';

    public function jobss(){
    	return $this->hasMany('App\Jobs', 'dep_id', 'dep_id');
    }
}
