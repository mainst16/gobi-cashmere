<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Products extends Model
{
    protected $table = 'products';
    protected $primaryKey = 'product_id';

    public function types(){
    	return $this->belongsTo('App\Product_types', 'pt_id', 'pt_id');
    }
}
