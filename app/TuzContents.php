<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TuzContents extends Model
{
    protected $table = 'tuz_contents';
    protected $primaryKey = 'tc_id';
}
