<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class TuzMenu extends Model
{
    protected $table = 'tuz_menu';
    protected $primaryKey = 'tm_id';
}
