<li class="topnav-shipping">
	<div class="shipping-dropdown">
		
			<a href="/global/international-shipping.html"><div class="flag-icon us"></div></a>
		
	</div>
</li>

<li class="myaccount-item">
	<a class="header-myaccount" href="https://www.toryburch.com/account" title="My Account">My Account</a>
	<div class="header-myaccount-dialog">
		<div class="topnav-underline"></div>	
		<div class="myaccount-container">
			<div class="header-myaccount-links">	
				<a class="header-signin" href="https://www.toryburch.com/account" title="Sign In">
					Sign In
				</a>
				<a class="header-createanaccount" href="https://www.toryburch.com/on/demandware.store/Sites-ToryBurch_US-Site/default/Account-Register" title="Create An Account">
					Create An Account
				</a>				
				<a class="header-checkorder" href="https://www.toryburch.com/account" title="Check Order Status">
					Check Order Status
				</a>
			</div>
			<div class="header-myaccount-image">
			</div>
		</div>
	</div>
</li>	

<li id="wishlist-nav" class="wishlist-nav wishlist-qty-zero">
	<a href="https://www.toryburch.com/wishlist" title="View Wish List">
		0
	</a>
</li>
			
<li id="minicart" class="minicart">
	<div class="minicarttotal">
		<a href="https://www.toryburch.com/on/demandware.store/Sites-ToryBurch_US-Site/default/Cart-Show?siteId=ToryBurch_US" title="View Bag" class="linkminicart zero">
			<span class="cartlabel">Shopping bag</span><span class="emptycart">(Empty)</span><span class="minicartqty minicartqtyzero">0</span>
		</a>
	
	</div><!-- END:minicarttotal -->
	<div class="minicartcontent minicartempty" style="display: none;">
		
		<div class="topnav-underline"></div>
		<!-- <div class="clear"></div> -->
		<div class="checkoutminicart">
			<div class="minicart-product-container">
				<div class="minicart-inner">
					
				</div>
			</div>
			<!--  <div id="scroll-to-bottom"><div></div></div> -->
		</div><!-- checkoutminicart -->
		<div class="minicart-bottom-container">
			<div class="minicarttotals">
				<div class="clear"></div>
			</div>
			<div class="clear"></div>
			<div class="link2cart">
				<a href="https://www.toryburch.com/on/demandware.store/Sites-ToryBurch_US-Site/default/Cart-Show?siteId=ToryBurch_US" title="Checkout">Checkout</a>
				<!-- <a class="continueShop">Continue Shopping</a> -->
			</div><!-- END:link2cart -->

			
		</div>
	</div><!-- END:minicartcontent-->

	<script id="basketjson">
		var basket = 
	null
	;
	</script>

	<script id="minilineitemTemplate" type="text/x-jsrender">
		<div class="summaryproduct">

		</div>
	</script>
	<script>
	function setMiniBagHeight(){
		windowMiniHeight = $(window).height() - 20;
		headerMiniCartHeight = $('.topheadernav-right').outerHeight();
		miniCheckoutHeight = $('.minicartBanner').outerHeight();
		miniCheckoutHeaderHeight = $('.minicart-header').outerHeight();
		miniCheckoutTotalHeight = $('.minicart-bottom-container').outerHeight();
		miniCartContentHeight = windowMiniHeight - headerMiniCartHeight;
	    if (navigator.userAgent.match(/(iPod|iPhone|iPad)/)) {
	 		miniCartHeightInner =   miniCartContentHeight - miniCheckoutTotalHeight - miniCheckoutHeaderHeight - 150;
	    }else{
	    	miniCartHeightInner =   miniCartContentHeight - miniCheckoutTotalHeight - miniCheckoutHeaderHeight - miniCheckoutHeight
	    }
	    $('.minicartcontent').css('max-height', miniCartContentHeight);
	    $('.minicart-product-container').css('max-height', miniCartHeightInner);
	};
	$(window).on("load",function(){
		setMiniBagHeight();
	});
	function watchArrows(){
		  var cartHeight = $(".minicart-inner").height() - $(".minicart-product-container").height();
		  if (cartHeight == $(".minicart-product-container").scrollTop()){
		      $("#scroll-to-bottom").addClass('notactive');
		  }else{
		      $("#scroll-to-bottom").removeClass('notactive');
		  }
		  if ($(".minicart-product-container").scrollTop() == 0){
		  	$("#scroll-to-bottom").removeClass('notactive');
		      $("#scroll-to-top").addClass('notactive');
		  }else{
		      $("#scroll-to-top").removeClass('notactive');
		  }
	};
	$(document).ready(function(){
		$('.minicart-product-container').bind('mousewheel DOMMouseScroll', function (e) {
		    var e0 = e.originalEvent,
		    delta = e0.wheelDelta ? e0.wheelDelta : -e0.detail;
		    this.scrollTop += ( delta < 0 ? 1 : -1 ) * 30;
		    e.preventDefault();
		});
		$("#scroll-to-bottom").unbind("mouseover").mouseover(function() {
		    $(".minicart-product-container").stop().animate({scrollTop: $(".minicart-inner").height() - $(".minicart-product-container").height()}, 2000);
		}).mouseout(function() {
		    $(".minicart-product-container").stop();
		    watchArrows();
		});
		$("#scroll-to-top").unbind("mouseover").mouseover(function() {
		    $(".minicart-product-container").stop().animate({scrollTop: 0}, 2000);
		}).mouseout(function() {
		    $(".minicart-product-container").stop();
		    watchArrows();
		});
		watchArrows();
	});
	</script>
</li><!-- END:minicart -->