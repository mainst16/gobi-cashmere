<script>
app.URLs.surveyPopupUrl= "/on/demandware.store/Sites-ToryBurch_US-Site/default/Page-Include?cid=survey-popup";
app.URLs.searchPopupUrl= "/on/demandware.store/Sites-ToryBurch_US-Site/default/Page-Include?cid=search-modal-popup";
</script>


     

    



    
    <script type="text/javascript" src="{{ asset('js/jquery-ui-1.11.2.custom.min.js')}}"></script>
    
    <script type="text/javascript" src="{{ asset('js/jquery.ui.touch-punch.js')}}"></script>
    <script type="text/javascript" src="{{ asset('js/jquery.selectBoxIt.min.js')}}" ></script>
    <script type="text/javascript" src="{{ asset('js/inview.js')}}"></script>
    <script type="text/javascript" src="{{ asset('js/jquery.plugins.footer.js')}}"></script>
    <!--[if IE 8]><script type="text/javascript" src="https://static.toryburch.com/static/on/demandware.static/Sites-ToryBurch_US-Site/-/default/v1521502672670/lib/videojs/videojs-ie8.min.js" ></script><![endif]-->





<script type="text/javascript" src="{{ asset('js/lodash.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/namespace.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/utility.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/selectWidget.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/widgetsInit.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/attrchange.js')}}"></script>
<script type="text/javascript" src="{{ asset('js/requests.js')}}"></script>






<script type="text/javascript">
    var gtmDataLayer;

    function subscribe(errorField, triggerSubmit, sourceCodeMarker,emailField,privacyField,countryField,callback,footerJson,custom) {

        var errorMessageField = jQuery("#signup_message");

        if (typeof errorField != 'undefined') {
            if (typeof errorField == 'string') {
                errorMessageField = jQuery(errorField);
            } else {
                errorMessageField = errorField;
            }
        }

        errorMessageField.html("");
        var submitTrigger = (triggerSubmit != undefined && triggerSubmit == true)?true:false;

        var $emailInput = jQuery("#emailsubscribe");
        if (typeof emailField != 'undefined') {
            if (typeof emailField == 'string') {
                $emailInput = jQuery(emailField);
            }
        }
        var $form = jQuery($emailInput.closest('form'));
        var btnSelector ='#' + $form.attr('id') + ' .ladda-btn';

        if (app.resources["SITE_ID"] == 'ToryBurch_US') {
            var btnBGColor = '#0c2340';
            var btnColor = '#FFF';
        } else {
            var btnBGColor = '#FFF';
            var btnColor = '#AAA';
        }

        var email = $emailInput.val();
        if (jQuery(btnSelector).length) {app.util.loaderLaddaAnimation( btnSelector, '#000000', '#FFF');}

        var countrySelect = jQuery("#footer-country");
        var country = countrySelect.val();
        if (typeof countryField != 'undefined') {
            if (typeof countryField == 'string') {
                country = jQuery(countryField).val();
            }
        }


        if (privacyField != "skip") {
            var legal = jQuery("input[name=legal]").prop('checked');
            if (typeof privacyField != 'undefined') {
                if (typeof privacyField == 'string') {
                    legal = jQuery(privacyField).prop('checked');
                }
            }
        } else {
            var legal = true;
        }

        var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
        if(submitTrigger){
            jQuery(window).trigger('subscribeSubmit');
        }
        if (reg.test(email) == false || email == null) {
            // app.log.write('nobueno');
            app.log.write(errorMessageField);
            //errorMessageField.html("Please enter a valid email address.");
            $emailInput.addClass("error");
            if (jQuery(btnSelector).length) {
                app.util.stopLaddaLoaderAnimation( btnSelector, btnBGColor, btnColor);
            };

            return false;
        }
        if(legal == false || legal == 'false'){
            errorMessageField.html("Please check Privacy Policy to continue").show();
            if (jQuery(btnSelector).length) {  app.util.stopLaddaLoaderAnimation( btnSelector, btnBGColor, btnColor); };
            return false;
        }
        if(custom != undefined){
            if (custom == 'fitbit') {
                var referralCodeMarker = 'TRB_US_US_EN_' + sourceCodeMarker;
            }
        }
        var temp;
        temp = app.util.appendParamToURL(app.URLs.footerSubscribe, "email", email);
        temp = app.util.appendParamToURL(temp, "country", country);

        if(sourceCodeMarker != undefined){
            temp += '&sourcecodemarker='+sourceCodeMarker
            if(referralCodeMarker != undefined){
                temp += '&referralcodemarker='+referralCodeMarker
            }
        }
        if (footerJson != undefined){
            temp += '&isfooterjson='+footerJson
        }
        //console.log(sourceCodeMarker);
        //console.log(temp);
        jQuery.ajax({
            type: "POST",
            url: temp,
            dataType: 'html',
            success: function(data) {

                if (app.util.isJsonString(data)){
                    var data = JSON.parse(data);
                }
                if (gtmDataLayer) {
                    gtmDataLayer.push({
                        "event": "subscribe"
                    });
                }
                if(custom != undefined){
                    if (custom == 'fitbit') {
                        $('.fitbit-container .emailcapture').hide();
                        $('.fitbit-container .on-success').show();
                    }
                }
                else {
                    if(data.footerSubscribe == true) {

                        if (data.siteID == "ToryBurch_US") {
                            var subscribeContent = $(".footer--subscribe-content");
                        } else {
                            var subscribeContent = $(".footer-top-section-wrapper");
                        }

                        if (data.unsubscribe == "Unsubscribed") {
                            subscribeContent.empty();
                            var html = "";
                            if (data.siteID == "ToryBurch_US") {
                                html += '<div class="subscribe-header confirmation">';
                                html += "<div class='header header--s'>Welcome Back - We're glad to have you as a subscriber again.</div>";
                                html += '<div class="body-copy body-copy--s">Please note: the 10% offer applies to first-time subscribers only.</div>';
                            } else {
                                html += '<div class="receivedwelcome-error">';
                                html += "<p class='receivedwelcome-header'>Welcome Back - We're glad to have you as a subscriber again.</p>";
                                if (data.siteID == "ToryBurch_JP") {
                                    html += '<p class="receivedwelcome-copy">Please note: the 10% offer applies to first-time subscribers only.</p>';
                                }
                            }

                            html += '</div>';
                            subscribeContent.html(html);

                        } else if(data.onList == true) {
                            $emailInput.val("");
                            $emailInput.attr("placeholder", "This email is already on our list");
                            $emailInput.addClass("error");
                        } else {
                            subscribeContent.empty();
                            var subscriberEmail = data.email;
                            var html = "";
                            if (data.siteID == "ToryBurch_US") {
                                html += '<div class="subscribe-header confirmation">';
                                html += "<div class='header header--s'>Confirmed &#8212; Thank You</div>";
                                html += "<p class='body-copy body-copy--s'>You've been added to our email list as</p>";
                                html += "<p class='body-copy body-copy--s body-copy--em'><span>" + subscriberEmail +  ".</span></p>";
                                html += "<p class='body-copy body-copy--s'>Check your inbox shortly for your exclusive offer.</p>";
                            } else {
                                html += '<div class="receivedwelcome-error">';
                                html += "<p class='receivedwelcome-header'>Confirmed &#8212; Thank You</p>";
                                if (data.siteID == "ToryBurch_JP") {
                                    html += "<p class='body-copy body-copy--s'>You've been added to our email list as <br>" + subscriberEmail +  ".<br>Check your inbox shortly for your exclusive offer.</p>";
                                } else {
                                    html += "<p class='body-copy body-copy--s'>You've been added to our email list as <br> " + subscriberEmail + "</p>";
                                }
                            }
                            html += '</div>';
                            subscribeContent.html(html);
                        }
                    } else {
                        errorMessageField.html("Thank You!");
                        $emailInput.val("");
                    }

                    jQuery("#footer-country").prop('selectedIndex',0);
                    if (jQuery(btnSelector).length) {  app.util.stopLaddaLoaderAnimation( btnSelector, btnBGColor, btnColor); };

                    if (typeof callback != 'undefined') {
                        //console.log('callback_start');
                        if (typeof callback == 'function') {
                            callback();
                        } else if (typeof callback == 'string') {
                            //console.log('callback_fire', callback);
                            window[callback]();
                        }
                    }
                }
            },
            failure: function(data){
                errorMessageField.html(data);
                $emailInput.val("");
                jQuery("#footer-country").prop('selectedIndex',0);
            }
        });

        return false;
    }

    function subscribe_UK(sourceCodeMarker) {
        jQuery("#signup_uk_message").html('&nbsp;');
        var email = jQuery("#emailsubscribe_uk").val();
        var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;

        if (reg.test(email) == false || email == null) {
            jQuery("#signup_uk_message").html("Please enter a valid email address.");
            return false;
        }
        var legalOk = false;
        jQuery("input[name=legal]").each(function(){
            if(jQuery(this).prop('checked')){
                legalOk = true;
            }
        });

        if(!legalOk){
            jQuery("#signup_uk_message").html("Please check Privacy Policy to continue");
            return false;
        }
        //global.validemailoptin
        //legal
        else {
            var temp = app.util.appendParamToURL(app.URLs.footerSubscribe, "email", email);
            if(sourceCodeMarker != undefined)
            {
                temp = app.util.appendParamToURL(temp, "sourcecodemarker", sourceCodeMarker);
            }
            jQuery.ajax({
                type: "POST",
                url: temp,
                dataType: 'html',
                success: function(data) {

                    if (gtmDataLayer) {
                        gtmDataLayer.push({
                            "event": "subscribe"
                        });
                    }
                    jQuery("#emailsubscribe_uk").val("&nbsp;");
                    jQuery("#signup_uk_message").html("Thank You!");
                    setTimeout("jQuery('.ui-dialog-content').dialog('close')",800);

                },
                failure: function(data){
                    jQuery("#signup_uk_message").html(data);
                    jQuery("#emailsubscribe_uk").val("&nbsp;");
                }
            });
        }

        return false;
    }
    function getEmail(emailAddress, errorField, triggerSubmit, sourceCodeMarker, listId)
    {
        var errorMessageField = jQuery("."+errorField);
        var email = emailAddress;
        var submitTrigger = (triggerSubmit != undefined && triggerSubmit == true)?true:false;
        var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;

        if(submitTrigger)
        {
            jQuery(window).trigger('subscribeSubmit');
        }

        if (reg.test(email) == false || email == null)
        {
            errorMessageField.html("Please enter a valid email address.");
            return false;
        }
        else
        {
            var temp = app.util.appendParamToURL(app.URLs.footerSubscribe, "email", email);
            if(sourceCodeMarker != undefined)
            {
                temp = app.util.appendParamToURL(temp, "sourcecodemarker", sourceCodeMarker);
            }
            if(listId != undefined)
            {
                temp = app.util.appendParamToURL(temp, "qsnewsletterlist", listId);
            }
            jQuery.ajax(
                {
                    type: "POST",
                    url: temp,
                    dataType: 'html',
                    success: function(data) {
                        if (gtmDataLayer)
                        {
                            gtmDataLayer.push({"event": "subscribe"});
                        }

                        errorMessageField.html("Thank You!");
                        setTimeout("jQuery('.ui-dialog-content').dialog('close')",800);
                    },
                    failure: function(data){
                        errorMessageField.html(data);
                    }});
        }
        return false;
    }


    function subscribe_sticky(errorField, triggerSubmit, sourceCodeMarker,emailField,privacyField,countryField,callback,footerJson) {
        var errorMessageField = jQuery(".footer-top-section.stickytest #signup_message");
        app.util.loaderLaddaAnimation( '.footer-top-section.stickytest .ladda-btn', '#000000', '#FFF');
        if (typeof errorField != 'undefined') {
            if (typeof errorField == 'string')
                errorMessageField = jQuery(errorField);
            else
                errorMessageField = errorField;
        }

        errorMessageField.html("");
        var submitTrigger = (triggerSubmit != undefined && triggerSubmit == true)?true:false;

        var emailInput = jQuery("#emailsubscribe-sticky");
        if (typeof emailField != 'undefined') {
            if (typeof emailField == 'string')
                emailInput = jQuery(emailField);
        }
        var email = emailInput.val();

        var countrySelect = jQuery("#footer-country");
        if (typeof countryField != 'undefined') {
            if (typeof countryField == 'string')
                country = jQuery(countryField).val();
        }
        var country = countrySelect.val();

        var legal = jQuery("input[name=legal-sticky]").prop('checked');
        if (typeof privacyField != 'undefined') {
            if (typeof privacyField == 'string')
                legal = jQuery(privacyField).prop('checked');
        }

        var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
        if(submitTrigger){
            jQuery(window).trigger('subscribeSubmit');
        }
        if (reg.test(email) == false || email == null) {
            app.log.write('nobueno');
            app.log.write(errorMessageField);
            //errorMessageField.html("Please enter a valid email address.");
            jQuery(".footer-top-section.stickytest .footernlemail_sticky").addClass("error");
            app.util.stopLaddaLoaderAnimation( '.footer-top-section.stickytest .ladda-btn', '#ffffff', '#AAAAAA');
            return false;
        }
        if(legal == false || legal == 'false'){
            errorMessageField.html("Please check Privacy Policy to continue");
            app.util.stopLaddaLoaderAnimation( '.footer-top-section.stickytest .ladda-btn', '#ffffff', '#AAAAAA');
            return false;
        }
        var temp;
        if(sourceCodeMarker != undefined){
            temp = app.util.appendParamToURL(app.URLs.footerSubscribe, "email", email);
            temp += '&sourcecodemarker='+sourceCodeMarker
        }
        if (footerJson != undefined){
            temp = app.util.appendParamToURL(app.URLs.footerSubscribe, "email", email);
            temp = app.util.appendParamToURL(temp, "country", country);
            temp += '&isfooterjson='+footerJson
        } else {
            temp = app.util.appendParamToURL(app.URLs.footerSubscribe, "email", email);
            temp = app.util.appendParamToURL(temp, "country", country);
        }

        temp += '&sourcecodemarker='+sourceCodeMarker;

        //console.log(sourceCodeMarker);
        jQuery.ajax({
            type: "POST",
            url: temp,
            dataType: 'html',
            success: function(data) {

                if (app.util.isJsonString(data)){
                    var data = JSON.parse(data);
                }
                if (gtmDataLayer) {
                    gtmDataLayer.push({
                        "event": "subscribe"
                    });
                }
                if(data.footerSubscribe == true) {
                    if (data.unsubscribe == "Unsubscribed") {
                        jQuery(".footer-top-section-wrapper-sticky").empty();
                        var html = "";
                        html += '<div class="receivedwelcome-error">';
                        html += "<p class='receivedwelcome-header'>Welcome Back - We're glad to have you as a subscriber again.</p>";
                        if (data.siteID == "ToryBurch_US") {
                            html += '<p class="receivedwelcome-copy">Please note: the 10% offer applies to first-time subscribers only.</p>';
                        }
                        html += '</div>';
                        jQuery(".footer-top-section-wrapper-sticky").html(html);

                    } else if(data.onList == true) {
                        jQuery(" .footernlemail_sticky").val("");
                        jQuery(" .footernlemail_sticky").attr("placeholder", "This email is already on our list");
                        jQuery(" .footernlemail_sticky").addClass("error");
                    } else {
                        jQuery(".footer-top-section-wrapper-sticky").empty();
                        var subscriberEmail = data.email;
                        var html = "";
                        html += '<div class="receivedwelcome-error">';
                        html += "<p class='receivedwelcome-header'>Confirmed &#8212; Thank You</p>";
                        if (data.siteID == "ToryBurch_US") {
                            html += "<p class='receivedwelcome-copy'>You've been added to our email list as " + subscriberEmail +  ".<br>Check your inbox shortly for your exclusive offer.</p>";
                        } else if (data.siteID == "ToryBurch_CN") {
                            html += "<p class='receivedwelcome-copy'>You've been added to our email list as " + subscriberEmail +  ".<br>Check your inbox shortly for your exclusive offer.</p>";
                        } else {
                            html += "<p class='receivedwelcome-copy'>You've been added to our email list as " + subscriberEmail;
                        }
                        html += '</div>';
                        jQuery(".footer-top-section-wrapper-sticky").html(html);
                    }
                } else {
                    errorMessageField.html("Thank You!");
                    emailInput.val("");
                }
                jQuery("#footer-country-sticky").prop('selectedIndex',0);
                app.util.stopLaddaLoaderAnimation( '.footer-top-section.stickytest .ladda-btn', '#ffffff', '#AAAAAA');

                if (typeof callback != 'undefined') {
                    //console.log('callback_start');
                    if (typeof callback == 'function') {
                        callback();
                    } else if (typeof callback == 'string') {
                        //console.log('callback_fire', callback);
                        window[callback]();
                    }
                }
            },
            failure: function(data){
                errorMessageField.html(data);
                emailInput.val("");
                jQuery("#footer-country-sticky").prop('selectedIndex',0);
            }
        });
        return false;
    }

    jQuery(document).ready(function(){
        if(app.resources["SITE_ID"] =="ToryBurch_US") {
            var select = new NewDesign.Widgets.Select(jQuery('#select-footer-country'), 'select-footer-country');
            var emailInput = $("#footer .email-input");

            var attachEvents = function () {
                if ($(window).width() > 770) {
                    $('#footer-country').on('change', function () {
                        if (emailInput.val() !== "") {
                            $('.footer-privacy-policy').show().addClass('animated faster fadeInUp');
                        }
                    });
                    emailInput.keyup(function () {
                        var email = emailInput.val();
                        var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
                        if (reg.test(email) == true && $('#footer-country').val !== "default") {
                            $('.footer-privacy-policy').show().addClass('animated faster fadeInUp');
                        }
                    });
                } else {
                    $('select#footer-country').on('focus', function(){
                        $('#select-footer-country').find('.select__value').css('border-color', '#191919');
                    });
                    $('select#footer-country').on('blur', function(){
                        $('#select-footer-country').find('.select__value').css('border-color', '#d3d4d5');
                    });
                }
            };

            attachEvents();
            window.addEventListener("resize", attachEvents);

            $('.js-footer-show-social').on('click', function(){
                $('.js-footer-show-social').closest('.footer--links-column').toggleClass('open');
                if ( $('.js-footer-show-social').closest('.footer--links-column').hasClass('open') ) {
                    $('.js-footer-social-list').show()//.addClass('animated faster fadeInUp');
                    $('html, body').animate({scrollTop:$(document).height()});
                    //$('.js-footer-social-list').show().addClass('animated faster fadeInUp');
                } else {
                    $('.js-footer-social-list').removeClass('fadeInUp');
                    $('.js-footer-social-list').addClass('fadeOutDown').show();
                    setTimeout(function(){
                        $('.js-footer-social-list').hide().removeClass('animated faster fadeOutDown');
                    }, 300);
                }
            });

            emailInput.focus(function(event) {
                emailInput.removeClass("error");
                emailInput.attr("placeholder","");
                $(".footer .email-label").show().addClass('animated faster fadeInUp');
                if ($(window).width() < 770) {
                    emailInput.css('border-color', '#191919');
                }
            });
            emailInput.blur(function(event) {
                emailInput.attr("placeholder","Your Email");
                $(".footer .email-label").hide().removeClass('animated faster fadeInUp');
                if ($(window).width() < 770) {
                    emailInput.css('border-color', '#d3d4d5');
                }
            });

            //init the live chat link in the footer
            app.liveChat.init({"pageName":null,"details":null,"sendUrl":true,"defaultLabel":"","sourceFooter":true});

        } else {
            var appleDevice = app.util.isAppleDevice();
            var googleDevice = app.util.isAndroidDevice();
            if (appleDevice == true){
                jQuery(".appleli").css("float","none");
                jQuery(".footer-app-icons").css("width","20px");
                jQuery(".appleli a").css("margin", "20px 0");
                jQuery(".footer-app-icons .googleli").hide();
                jQuery(".footer-app-icons .windowsli").hide();
            } else if (googleDevice == true) {
                jQuery(".googleli").css("float","none");
                jQuery(".googleli a").css("margin", "20px 0");
                jQuery(".footer-app-icons .appleli").hide();
                jQuery(".footer-app-icons .windowsli").hide();
            }
            var emailDropDown = jQuery(".desktop .footernlemail, .mobile .footernlemail");
            emailDropDown.focus(function(event) {
                jQuery("#footer .footernlemail").removeClass("error");
                jQuery("#footer .select-style").css('display', 'block');
                jQuery("#footer .footer-privacy-policy").css('display', 'block');

                jQuery("#footer .footernlemail").attr("placeholder","");
            });

            jQuery(".desktop .footernlemail, .mobile .footernlemail").blur(function(event) {
                jQuery("#footer .footernlemail").attr("placeholder","Email Address");
            });
        }

        if (app && app.customer && app.customer.geolocation && app.customer.geolocation.countryCode) {
            $("#footer-country").val(app.customer.geolocation.countryCode.toUpperCase()).trigger('change');
        }


        var emailDropDown_sticky = jQuery(".desktop .footernlemail_sticky, .mobile .footernlemail");
        emailDropDown_sticky.focus(function(event) {
            jQuery(" .footernlemail_sticky").removeClass("error");
            jQuery(" .select-style-sticky").css('display', 'block');
            jQuery(" .footer-privacy-policy-sticky").css('display', 'block');

            jQuery(" .footernlemail_sticky").attr("placeholder","");
            jQuery("#footer-country-sticky").prop('selectedIndex',1);

        });

        jQuery(".desktop .footernlemail_sticky, .mobile .footernlemail").blur(function(event) {

            jQuery(" .footernlemail_sticky").attr("placeholder","Email Address");

        });

        jQuery(".sticky_closebutton").click(function() {
            jQuery(".footer-top-section.stickytest").remove();
        });
        //
        //
        // jQuery(".newsletter_signup #dwfrm_emailsignup_email").focus(function(event) {
        //  jQuery(".newsletter_signup #dwfrm_emailsignup_email").attr("placeholder","");
        // });
        //
        // jQuery(".newsletter_signup #dwfrm_emailsignup_email").blur(function(event) {
        //  jQuery(".newsletter_signup #dwfrm_emailsignup_email").attr("placeholder","Email Address");
        //  jQuery(".newsletter_signup #dwfrm_emailsignup_email").removeClass("errorclient");
        // });

        if (jQuery("#product-mobile-video").length) {
            videojs("product-mobile-video").ready(function(){
                this.on("ended", function() {
                    this.load();
                });
            });
        }

    });






</script>









<script type="text/javascript" src="https://ssl.google-analytics.com/ga.js"></script>
<!--<script type="text/javascript">
try {
var pageTracker = _gat._getTracker('UA-446313-1');
pageTracker._trackPageview();
} catch(err) {}</script>-->



<!-- Start: pagecomponents/omniture_asset_include -->

    <!-- Omniture not enabled for site -->


<!-- End: pagecomponents/omniture_asset_include -->






    


    
    <script type="application/json" class="sfc-config sfc-temp-element">
        {"cfg":{"enabled":true,"businessUnitMID":"10914034","jsTrackingLibUrl":"//10914034.collect.igodigital.com/collect.js"},"data":{"trackingFunctions":["trackPageView"],"userInfo":null}}
    </script>
    <script type="application/json" class="sfc-user sfc-temp-element">
        

{"email":""}

    </script>
    
    <script type="text/javascript" class="sfc-temp-element">
        // only include Salesforce library if it doesn't already exist
        var el = document.getElementById('sfc-library');
        if (el === null) {
            el = document.createElement('script');
            el.id = 'sfc-library';
            el.type = 'text/javascript';
            el.async = true;
            el.onload = function() {
                // load config and tracking data, call tracking function(s)
                app.metrics.sfc.track();
            };
            el.src = '//10914034.collect.igodigital.com/collect.js';
            document.getElementsByTagName('head')[0].appendChild(el);
        }
        else {
            // reload config and tracking data, call tracking function(s)
            app.metrics.sfc.track();
        }
    </script>






<!-- Start: components/adobetagmanager/adobetagmanagerbody -->
    
    
        <!-- No ADTM ID for object -->
     
<!-- End: components/adobetagmanager/adobetagmanagerbody -->






    
    <script type="text/javascript" src="//static.atgsvcs.com/js/atgsvcs.js"></script>
    <script type='text/javascript'>
        ATGSvcs.setEEID("200106306549");
        if (document.location.protocol == "https:")
            ATGSvcs.setCfg("-eeFlag", "s");
        (function() { var l = app.URLs.customerCareHost,d=document,ss='script',s=d.getElementsByTagName(ss)[0]; function r(u) { var rn=d.createElement(ss); rn.type='text/javascript'; rn.defer=rn.async=!0; rn.src = "//" + l + u; s.parentNode.insertBefore(rn,s); } r('/rnt/rnw/javascript/vs/1/vsapi.js'); r('/vs/1/vsopts.js'); })();

        var rnWait = setInterval( function () {
            if ("undefined" !== typeof RightNow && "undefined" !== typeof RightNow.Client && "undefined" !== typeof RightNow.Client.Event) {
                clearInterval(rnWait);
                RightNow.Client.Event.evt_widgetLoaded.subscribe(app.liveChat.widgetLoaded);
            }
        },100);

        setTimeout(function () {
            clearInterval(rnWait);
        },20000);



    </script>
    <div id="chatLinkContainer"
        
            class="chat--new-design"
        
    >
        <div id="chatLink">
            <div class="chatIcon"></div>
            
                <span class="chatDescription body-copy body-copy--m">Need help?</span>
            
            <div id="chatLinkInfo"></div>
        </div>
    </div>




    



<script src="{{ asset('js/imail_conv_landing.js')}}" type="text/javascript"></script>













    
        
            
                
                <script>
                    var optimizely_url = app.URLs.optimizelySetSession;
                    jQuery.ajax({
                       type: "POST",
                       url: optimizely_url,
                       dataType: "json",
                       success: function(data){},
                       failure: function(data) {},
                       error: function(XMLHttpRequest, textStatus, errorThrown) {}
                    });
                </script>
            
        
    



 <!-- if not on PROD -->




<script>
window.IparcelVars = {};
window.IparcelVars.countriesList='{"countries":["AR","AU","CA","CN","CO","CR","EC","SV","HK","IN","IL","JM","MT","NZ","NO","PR","CH","UY"]}';
window.IparcelVars.quoteUrl ="https://www.toryburch.com/on/demandware.store/Sites-ToryBurch_US-Site/default/COShipping-UpdateShippingMethodList";
window.IparcelVars.selectShippingMethodUrl= "https://www.toryburch.com/on/demandware.store/Sites-ToryBurch_US-Site/default/COShipping-SelectShippingMethod";
</script>


<script src="{{ asset('js/iparcel.js') }}"></script>



<!-- END: footer -->

    </div><!-- END: container -->

    <script type="text/javascript" src="{{ asset('js/slick.js') }}"></script>

<script type="{{ asset('js/dwanalytics-16.9.js')}}" async="async" onload="trackPage()"></script>
<!-- Demandware Active Data (body_end-active_data.js) -->
<script src="{{ asset('js/dwac-16.9.js')}}" type="text/javascript" async="async"></script><!-- CQuotient Activity Tracking (body_end-cquotient.js) -->
<script src="{{ asset('js/gretel.min.js')}}" type="text/javascript" async="async"></script>
<!-- Demandware Apple Pay -->
<script type="text/javascript">//<!--
/* <![CDATA[ */
if (!window.dw) dw = {};
dw.applepay = {"action":{"cancel":"https:\/\/www.toryburch.com\/on\/demandware.store\/Sites-ToryBurch_US-Site\/default\/__SYSTEM__ApplePay-Cancel","getRequest":"https:\/\/www.toryburch.com\/on\/demandware.store\/Sites-ToryBurch_US-Site\/default\/__SYSTEM__ApplePay-GetRequest","onshippingcontactselected":"https:\/\/www.toryburch.com\/on\/demandware.store\/Sites-ToryBurch_US-Site\/default\/__SYSTEM__ApplePay-ShippingContactSelected","onpaymentmethodselected":"https:\/\/www.toryburch.com\/on\/demandware.store\/Sites-ToryBurch_US-Site\/default\/__SYSTEM__ApplePay-PaymentMethodSelected","onvalidatemerchant":"https:\/\/www.toryburch.com\/on\/demandware.store\/Sites-ToryBurch_US-Site\/default\/__SYSTEM__ApplePay-ValidateMerchant","onpaymentauthorized":"https:\/\/www.toryburch.com\/on\/demandware.store\/Sites-ToryBurch_US-Site\/default\/__SYSTEM__ApplePay-PaymentAuthorized","prepareBasket":"https:\/\/www.toryburch.com\/on\/demandware.store\/Sites-ToryBurch_US-Site\/default\/__SYSTEM__ApplePay-PrepareBasket","onshippingmethodselected":"https:\/\/www.toryburch.com\/on\/demandware.store\/Sites-ToryBurch_US-Site\/default\/__SYSTEM__ApplePay-ShippingMethodSelected"},"inject":{"directives":[{"css":"dw-apple-pay-button","query":"ISAPPLEPAY","action":"replace","copy":true}]}};
/* ]]> */
// -->
</script>
<script type="text/javascript" src="https://static.toryburch.com/static/on/demandware.static/Sites-ToryBurch_US-Site/-/default/v1521502672670/internal/jscript/applepay.js" async="true"></script>