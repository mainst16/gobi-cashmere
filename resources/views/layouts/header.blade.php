<div id="header" role="banner">
	
	<header>
	<meta name="viewport" content="width=device-width, initial-scale=1">
		{{--<div class="headerbanner">
			<div class="htmlslotcontainer mobile-not-available">
				
				<style type="text/css">
					.headerbanner {background: #002244;}
					.promo-message, .promo-message a {color: #FFF;}
					.message {display:none;}
					@media screen and (max-width:767px) {
					.headerbanner .promo-message .message .arrowright.mac:after {top:2px;}
					}
				</style>
				<div class="promo-message">
					<div class="message">
						Үзэсгэлэн худалдаа  <a class="contentOpenModal" href="{{ url('atrum') }}">үзэх  </a><span class="arrowright"></span>
					</div>
				</div>
				<!-- if the message is only one line use below (for mobile) -->
				<script>
					$(document).ready(function(){
					$('.headerbanner').addClass('singlemsg');

					});
				</script>
				<div style="font-family: Verdana, Geneva, Arial, Helvetica, sans-serif; font-size: 8px; color: Black;">
					
				</div>
			</div>
		</div><!-- END: headerbanner -->--}}
		<div class="topheadernav-wrapper">
			<div class="sticky-wrapper">
				<div class="topheadernav alternative">
					<div id="sidebarbtn" title="Menu">
						<div class="inner-sidebar">
							<span></span>
		  					<span></span>
		  					<span></span>
		  				</div>
		  			</div>
					<ul class="topheadernav-left">
						<li class="yama">
							<a href="{{ url('yama')}}"><img src="{{ asset('imgs/icons/no.png')}}"></a>
						</li>
						<li class="organic">
							<div class="tbf"><a href="{{url('organic')}}"><img src="{{ asset('imgs/icons/no.png')}}"></a></div>
						</li>
						<li class="kids">
							<div class="logo-blog"><a href="{{url('kids')}}"><img src="{{ asset('imgs/icons/no.png')}}"></a></div>
						</li>
					</ul>
					<ul class="topheadernav-middle">
						<li class="main-logo">
							<a href="{{ url('/')}}">
							</a>
						</li>
					</ul>
					<ul class="topheadernav-right">
						<li class="mobile-logo">
							<a href="{{ url('/')}}"><img src="{{ asset('imgs/icons/gobi.png')}}"></a>
						</li>
						<li class="search">
							<div id="searchbtn"></div>
							
							<!-- <script type="text/javascript">
							var searchSuggestURL = "/on/demandware.store/Sites-ToryBurch_US-Site/default/Search-GetSuggestions";

							jQuery(document).ready(function() {
								
							});
							</script> -->

							<div class="sitesearch mainsearch">
								<span class="arrowtip"></span>
								<span class="close"></span>
								
								
								@include('search.searchForm')
							</div>
							<!-- END: sitesearch -->
						</li>

						<!-- <li class="topnav-shipping">
							<div class="shipping-dropdown">
								
									<a href="/global/international-shipping.html"><div class="flag-icon us"></div></a>
								
							</div>
						</li> -->

						<li class="myaccount-item">					 
			
							<a class="header-myaccount" href="#/account">My Account</a>
							<div class="header-myaccount-dialog">
								<div class="topnav-underline"></div>	
								<div class="myaccount-container">
									<div class="header-myaccount-links">
									@if (Auth::guest())
			                            <a class="header-signin" href="{{ url('account')}}">
											Нэвтрэх
										</a>
										<a class="header-createanaccount" href="{{ url('account/register')}}">
											Бүртгүүлэх
										</a>
			                        @else
			                                <a class="header-signin" href="#">
			                                    Сайн уу? {{ Auth::user()->name }} <span class="caret"></span>
			                                </a>

			                                <form action="{{ url('logout') }}" method="POST">
					                            {{ csrf_field() }}
					                            <button type="submit" class="button button--small">
					                                Гарах
					                            </button>
					                        </form>
			                        @endif	
									</div>
									<div class="header-myaccount-image">				
									</div>
								</div>
							</div>
						</li>
						

						<li>
							<div class="dropdown">
							  <span><img src="{{asset('imgs/icons/online.png')}}" style="width: 23px"></span>
							  <div class="dropdown-content">
							    <a href="https://gobicashmere.com" target="_blank">
											Online shop
										</a>
							  </div>
							</div>
						</li>
					</ul>
				</div>
				<div class="navigation-wrapper">
					<div id="navigation" class="categorymenu">
						<ul class="menu-category">
							<li class="clothing categoryli">				
						
								<a href="{{ url('brand/'.$urlbrand.'/2')}}">
					  		  		Эмэгтэй
								</a>    
						        <ul data-columnlimit="null" style="background-color: #fff">        
						        
						            <li class="top-nav-group" data-navgroup="100">                
						                    
						                <div class="css-arrow left"></div>
						                <span class="mobile-flyout-menu maincats-span group-hdr mobile-subhead">Категори</span>                
						                <span class="desktop-flyout-menu group-hdr Категори ">Категори</span>
						        
							        	<ul>
							            	@foreach($fmale as $woman)
							                <li class="top-nav-group clothing-newarrivals" data-navgroup="100">
							                    <a title="{{ $woman->name }}" href="{{ url('products/3/'.$woman->gender_id.'/'.$woman->pt_id.'') }}">
							                    {{ $woman->name }}
							                    </a>
							                </li>
							                @endforeach
							            </ul>
						        
						            </li>
						            {{--<li class="top-nav-group" data-navgroup="400">
						            						            						                
    					                    <div class="divider"></div>				                
    					                    <span class="desktop-flyout-menu group-hdr Shops ">Онцлох</span>				        
    						        		<ul>
    		            
    							               <!--  <li class="top-nav-group clothing-torys-favorites" data-navgroup="400">
    							                    <a title="Tory's Favorites" href="#/clothing/torys-favorites/">
    							                    Tory's Favorites
    							                    </a>
    							                </li> -->
    							                
    							            
    							                <li class="top-nav-group clothing-must-haves" data-navgroup="400">
    							                    <a title="The Lookbook: Must-Haves" href="#/clothing-must-haves/">
    							                    The Lookbook: Must-Haves
    							                    </a>
    							                </li>
    							                
    							            
    							                <!-- <li class="top-nav-group clothing-tory-story" data-navgroup="400">
    							                    <a title="#ToryStories" href="#/clothing-tory-story/">
    							                    #ToryStories
    							                    </a>
    							                </li>
    							                
    							            
    							                <li class="top-nav-group clothing-whites" data-navgroup="400">
    							                    <a title="Spring Whites" href="#/clothing/whites/">
    							                    Spring Whites
    							                    </a>
    							                </li>
    							                
    							            
    							                <li class="top-nav-group clothing-parties-events" data-navgroup="400">
    							                    <a title="Parties &amp; Events" href="#/clothing/parties-events/">
    							                    Parties &amp; Events
    							                    </a>
    							                </li>
    							                
    							            
    							                <li class="top-nav-group clothing-workweek" data-navgroup="400">
    							                    <a title="Workweek Chic" href="#/clothing/workweek-chic/">
    							                    Workweek Chic
    							                    </a>
    							                </li>
    							                
    							            
    							                <li class="top-nav-group clothing-foundation" data-navgroup="400">
    							                    <a title="Tory Burch Foundation" href="#/clothing/tory-burch-foundation/">
    							                    Tory Burch Foundation
    							                    </a>
    							                </li> -->
    							            </ul>
    							        </li>--}}
							        <li class="categorymenu-slot">
							        	<div class="htmlslotcontainer mobile-available">
											<div class="slot-image">
												<p>
													<a data-content-position= "Clothing_MegaMenu_slot1_img" data-content-id="NewClothes" data-content-name="NewClothes_img" href="#"><img class="dd-lazy" src="{{ asset('imgs/homepage/menu/gobi3.jpg')}}" /></a></p>

												<p>
													<!-- <a data-content-position= "Clothing_MegaMenu_slot1_txt" data-content-id="NewClothes" data-content-name="NewClothes_txt" href="#/clothing/new-arrivals/" title="Shop Tory Burch New Clothing">New Arrivals</a> --></p>
											</div>

											<div class="slot-image last">
												<p>
													<a data-content-position= "Clothing_MegaMenu_slot2_img" data-content-id="Lookbook" data-content-name="Lookbook_img" href="#/clothing-must-haves/" ><img class="dd-lazy" alt="Shop the Resort Lookbook" border="0" height="231" src="{{ asset('imgs/homepage/menu/gobi4.jpg')}}" width="257" /></a></p>

												<p>
													<!-- <a data-content-position= "Clothing_MegaMenu_slot2_txt" data-content-id="Lookbook" data-content-name="Lookbook_txt" href="#/clothing-must-haves/" title="Shop Tory Burch Resort Lookbook">The Lookbook: Must-Haves</a> --></p>
											</div>
											<div style="font-family: Verdana, Geneva, Arial, Helvetica, sans-serif; font-size: 8px; color: Black;"></div>
										</div>	
										<div class="mobilehtmlslotcontainer">
											<div></div>
										</div>
									</li>
								</ul><!-- END: UL -->
							</li>
							<li class="new categoryli">
								<a href="{{ url('brand/'.$urlbrand.'/1')}}">
									Эрэгтэй
								</a>
								<ul data-columnlimit="9.0" style="background-color: #fff">
									<li class="top-nav-group" data-navgroup="100">
										<div class="css-arrow left"></div>
										<span class="mobile-flyout-menu maincats-span group-hdr mobile-subhead">Категори</span>
										<span class="desktop-flyout-menu group-hdr Категори ">Категори</span>
										<ul>
		            						@foreach($male as $man)
							                <li class="top-nav-group new-clothing" data-navgroup="100">
							                    <a title="{{ $man->name }}" href="{{ url('products/3/'.$man->gender_id.'/'.$man->pt_id.'') }}">
							                    {{ $man->name }}
							                    </a>
							                </li>
							                @endforeach
							            </ul>
							        </li>
							        <!-- <li class="top-nav-group" data-navgroup="400">
							        	<div class="divider"></div>
							        	<span class="desktop-flyout-menu group-hdr Shops ">Онцлох</span>        
								        <ul>            
								            <li class="top-nav-group new-torys-favorites" data-navgroup="400">
								                <a title="Tory's Favorites" href="#/new/torys-favorites/">
								                Tory's Favorites
								                </a>
								            </li>
								            
								        
								            <li class="top-nav-group new-must-haves" data-navgroup="400">
								                <a title="The Lookbook: Must-Haves" href="#/new-must-haves/">
								                The Lookbook: Must-Haves
								                </a>
								            </li>
								            
								        
								            <li class="top-nav-group new-essentials" data-navgroup="400">
								                <a title="The Essentials" href="#/essentials/">
								                The Essentials
								                </a>
								            </li>            
								        </ul>
								    </li> -->
								    <li class="top-nav-group" data-navgroup="120">
								    	<div class="divider"></div>
								    	<span class="desktop-flyout-menu group-hdr By Occasion ">Онцлох</span>        
							        	<ul>            
							                <li class="top-nav-group new-parties-events" data-navgroup="120">
							                    <a title="Parties &amp; Events" href="#/new/parties-events/">
							                    The Lookbook: Must-Haves
							                    </a>
							                </li>
							                
							            
							                <!-- <li class="top-nav-group new-workweek-chic" data-navgroup="120">
							                    <a title="Workweek Chic" href="#/new/workweek-chic/">
							                    Workweek Chic
							                    </a>
							                </li>
							                
							            
							                <li class="top-nav-group new-beach-getaway" data-navgroup="120">
							                    <a title="Beach Getaway" href="#/new/beach-getaway/">
							                    Beach Getaway
							                    </a>
							                </li> -->
							            </ul>				        
						            </li>
						            <li class="categorymenu-slot">
						            	<div class="htmlslotcontainer mobile-not-available">
						            		<style type="text/css">
											</style>
											<div class="slot-image">
												<p>
													<a data-content-position= "New_MegaMenu_slot1_img" data-content-id="NewClothing" data-content-name="NewClothing_img" href="#/clothing/new-arrivals/"><img class="dd-lazy" alt="Shop the New Arrivals" border="0" height="231" src="{{ asset('imgs/homepage/menu/gobi1.jpg')}}" width="257" /></a>
												</p>

												<p>
													<!-- <a data-content-position= "New_MegaMenu_slot1_txt" data-content-id="NewClothing" data-content-name="NewClothing_txt" href="#/clothing/new-arrivals/" title="Shop Tory Burch New Arrivals">New Clothing</a> -->
												</p>
											</div>

											<div class="slot-image last">
												<p>
													<a data-content-position= "New_MegaMenu_slot2_img" data-content-id="NewHandbags" data-content-name="NewHandbags_img" href="#/handbags-newarrivals/"><img class="dd-lazy" alt="Shop New Handbags" border="0" height="231" src="{{ asset('imgs/homepage/menu/gobi2.jpg')}}" width="257" /></a>
												</p>

												<p>
													<!-- <a data-content-position= "New_MegaMenu_slot2_txt" data-content-id="NewHandbags" data-content-name="NewHandbags_txt" href="#/handbags-newarrivals/" title="Shop Tory Burch New Handbags">New Handbags</a> -->
												</p>
											</div>
											<div style="font-family: Verdana, Geneva, Arial, Helvetica, sans-serif; font-size: 8px; color: Black;"></div>
										</div>
									</li>
								</ul><!-- END: UL -->
							</li>
							<li class="swim categoryli">				
								<a href="{{ url('brand/'.$urlbrand.'/3')}}">
					  		  		гэр ахуй
								</a>
		    
						        <ul data-columnlimit="null" style="background-color: #fff">        
						        
						            <li class="top-nav-group" data-navgroup="100">
						                
						                    
						                <div class="css-arrow left"></div>
						                <span class="mobile-flyout-menu maincats-span group-hdr mobile-subhead">Категори</span>
						                <span class="desktop-flyout-menu group-hdr Категори ">Категори</span>        
							        	<ul>
							        		@foreach($home as $ger)            
							                <li class="top-nav-group swim-newarrivals" data-navgroup="100">
							                    <a title="{{ $ger->name }}" href="{{ url('products/3/'.$ger->gender_id.'/'.$ger->pt_id.'') }}">
							                    {{ $ger->name }}
							                    </a>
							                </li>
							                @endforeach          
							        	</ul>
						        
						            </li>
						            <!-- <li class="top-nav-group" data-navgroup="400">                
						                <div class="divider"></div>                
						                <span class="desktop-flyout-menu group-hdr Shops ">Shops</span>        
							        	<ul>	            
							                <li class="top-nav-group swim-beach-getaway" data-navgroup="400">
							                    <a title="Beach Getaway" href="#/swim/beach-getaway/">
							                    Beach Getaway
							                    </a>
							                </li>
							                
							            
							                <li class="top-nav-group swim-little-black-swim-shop" data-navgroup="400">
							                    <a title="The Little Black Swim Shop" href="#/swim/the-little-black-swim-shop/">
							                    The Little Black Swim Shop
							                    </a>
							                </li>
							            </ul>        
						            </li> -->
						            <li class="categorymenu-slot">
						            	<div class="htmlslotcontainer mobile-available">

											<div class="slot-image">
											    <a data-content-position="Shoes_MegaMenu_slot1_img" data-content-id="NewSwim" data-content-name="NewSwim_img" href="#/swim/new-arrivals/" title="Shop New Swimwear">
											    	<img alt="Shop New Swimwear" class="shoes-dd-img dd-lazy" border="0" src="{{ asset('imgs/homepage/menu/home.jpg') }}" width="231" height="257"/>
											    </a>
											    <!-- <a data-content-position="Shoes_MegaMenu_slot1_txt" data-content-id="NewSwim" data-content-name=NewSwim_txt" href="#/swim/new-arrivals/" title="Shop New Swimwear">New Arrivals</a> -->
											</div>

											<div class="slot-image last">
											    <a data-content-position="Shoes_MegaMenu_slot1_img" data-content-id="NewSwim" data-content-name="NewSwim_img" href="#/swim/new-arrivals/" title="Shop New Swimwear">
											    	<img alt="Shop New Swimwear" class="shoes-dd-img dd-lazy" border="0" src="{{ asset('imgs/homepage/menu/home1.jpg') }}" width="231" height="257"/>
											    </a>
											    <!-- <a data-content-position="Shoes_MegaMenu_slot1_txt" data-content-id="NewSwim" data-content-name=NewSwim_txt" href="#/swim/new-arrivals/" title="Shop New Swimwear">New Arrivals</a> -->
											</div>
									
											<div style="font-family: Verdana, Geneva, Arial, Helvetica, sans-serif; font-size: 8px; color: Black;">
												
											</div>
										</div>	
										<div class="mobilehtmlslotcontainer">
											<div></div>
										</div>
									</li>
								</ul><!-- END: UL -->
							</li>
							<li class="shoes categoryli">
						
								<a href="{{ url('brand/'.$urlbrand.'/4')}}">
					  		  		аксессуар
								</a>    
						        <ul data-columnlimit="null" style="background-color: #fff">				        
						            <li class="top-nav-group" data-navgroup="100">				                    
				                        <div class="css-arrow left"></div>
				                        <span class="mobile-flyout-menu maincats-span group-hdr mobile-subhead">Категори</span>
				                        <span class="desktop-flyout-menu group-hdr Категори ">Категори</span>				        
							        	<ul>
							            	@foreach($acc as $ac)
							                <li class="top-nav-group shoes-newarrivals" data-navgroup="100">
							                    <a title="{{ $ac->name }}" href="{{ url('products/3/'.$ac->gender_id.'/'.$ac->pt_id.'') }}">
							                    {{ $ac->name }}
							                    </a>
							                </li>
							                @endforeach
							            </ul>
							        </li>
							        <!-- <li class="top-nav-group" data-navgroup="400">				                
					                    <div class="divider"></div>			                
					                    <span class="desktop-flyout-menu group-hdr Shops ">Shops</span>				        
							        	<ul>				            
							                <li class="top-nav-group shoes-neutrals" data-navgroup="400">
							                    <a title="The Neutrals" href="#/shoes/the-neutrals/">
							                    The Neutrals
							                    </a>
							                </li>
							                
							            
							                <li class="top-nav-group shoes-the-embellishment" data-navgroup="400">
							                    <a title="The Trend: Embellishment" href="#/shops-shoes-embellishment/">
							                    The Trend: Embellishment
							                    </a>
							                </li>
							            </ul>				        
						            </li> -->
						            <li class="categorymenu-slot">
						            	<div class="htmlslotcontainer mobile-available">
											<div class="slot-image">
												<p>
													<a data-content-position= "Clothing_MegaMenu_slot1_img" data-content-id="NewClothes" data-content-name="NewClothes_img" href="#/clothing/new-arrivals/"><img class="dd-lazy" alt="Shop New Clothing" border="0" height="231" src="{{ asset('imgs/homepage/menu/acc.jpg')}}" width="257" /></a></p>

												<p>
													<!-- <a data-content-position= "Clothing_MegaMenu_slot1_txt" data-content-id="NewClothes" data-content-name="NewClothes_txt" href="#/clothing/new-arrivals/" title="Shop Tory Burch New Clothing">New Arrivals</a> --></p>
											</div>

											<div class="slot-image last">
												<p>
													<a data-content-position= "Clothing_MegaMenu_slot2_img" data-content-id="Lookbook" data-content-name="Lookbook_img" href="#/clothing-must-haves/" ><img class="dd-lazy" alt="Shop the Resort Lookbook" border="0" height="231" src="{{ asset('imgs/homepage/menu/acc2.jpg')}}" width="257" /></a></p>

												<p>
													<!-- <a data-content-position= "Clothing_MegaMenu_slot2_txt" data-content-id="Lookbook" data-content-name="Lookbook_txt" href="#/clothing-must-haves/" title="Shop Tory Burch Resort Lookbook">The Lookbook: Must-Haves</a> --></p>
											</div>
											<div style="font-family: Verdana, Geneva, Arial, Helvetica, sans-serif; font-size: 8px; color: Black;"></div>
										</div>						
										<div class="mobilehtmlslotcontainer">
											<div></div>
										</div>
									</li>
								</ul><!-- END: UL -->
							</li>
							<li class="handbags categoryli">				
								<a href="{{ url('collections/3')}}">
					  		  		коллекци
								</a>    
						        <ul data-columnlimit="null" style="background-color: #fff">        
						            <li class="top-nav-group" data-navgroup="100">                    
						                <div class="css-arrow left"></div>
						                <span class="mobile-flyout-menu maincats-span group-hdr mobile-subhead">Категори</span>                
						                <span class="desktop-flyout-menu group-hdr Категори ">Категори</span>        
							        	<ul>
							            
							                <li class="top-nav-group handbags-newarrivals" data-navgroup="100">
							                    <a title="Намар / Зун 2018" href="{{ url('collection/3/1')}}">
							                    Хавар / Зун 2018
							                    </a>
							                </li>
							                
							            
							                <li class="top-nav-group handbags-view-all" data-navgroup="100">
							                    <a title="Хавар /Өвөл 17-18" href="{{ url('collection/3/2')}}">
							                    Намар /Өвөл 17-18
							                    </a>
							                </li>
							            </ul>
							        </li>
							        <!-- <li class="top-nav-group" data-navgroup="400">                
						                <div class="divider"></div>
						                <span class="desktop-flyout-menu group-hdr Shops ">Shops</span>
						                <ul>            
								            <li class="top-nav-group handbags-wallets-handbags" data-navgroup="400">
								                <a title="Matching Wallets &amp; Handbags" href="#/handbags-matching-wallets-handbags/">
								                Matching Wallets &amp; Handbags
								                </a>
								            </li>
								            
								        
								            <li class="top-nav-group handbags-neutrals" data-navgroup="400">
								                <a title="The Neutrals" href="#/handbags/the-neutrals/">
								                The Neutrals
								                </a>
								            </li>
								        </ul>
								    </li> -->
								    <!-- <li class="top-nav-group" data-navgroup="200">                
						                <div class="divider"></div>                
						                <span class="desktop-flyout-menu group-hdr Collections ">Collections</span>        
							        	<ul>
							            
							                <li class="top-nav-group handbags-half-moon" data-navgroup="200">
							                    <a title="Half-Moon" href="#/handbags/half-moon/">
							                    Half-Moon
							                    </a>
							                </li>
							                
							            
							                <li class="top-nav-group handbags-miller" data-navgroup="200">
							                    <a title="Miller" href="#/handbags/miller/">
							                    Miller
							                    </a>
							                </li>
							                
							            
							                <li class="top-nav-group handbags-kira" data-navgroup="200">
							                    <a title="Kira" href="#/handbags/kira/">
							                    Kira
							                    </a>
							                </li>
							                
							                    
							                
							            
							                <li class="top-nav-group handbags-fleming" data-navgroup="200">
							                    <a title="Fleming" href="#/handbags/fleming/">
							                    Fleming
							                    </a>
							                </li>
							                
							            
							                <li class="top-nav-group handbags-mcgraw" data-navgroup="200">
							                    <a title="McGraw" href="#/handbags/mcgraw/">
							                    McGraw
							                    </a>
							                </li>
							                
							            
							                <li class="top-nav-group handbags-the-robinson-collection" data-navgroup="200">
							                    <a title="Robinson" href="#/handbags/robinson/">
							                    Robinson
							                    </a>
							                </li>
							                
							            
							                <li class="top-nav-group handbags-ella" data-navgroup="200">
							                    <a title="Ella" href="#/handbags/ella/">
							                    Ella
							                    </a>
							                </li>
							            </ul>
							        </li> -->
							        <li class="categorymenu-slot">
							        	<div class="htmlslotcontainer mobile-available">
							        		<div class="slot-image">
												<p>
													<a data-content-position= "Handbags_MegaMenu_slot1_img" data-content-id="NewHandbags" data-content-name="NewHandbags_img"  href="{{ url('collections/3')}}"><img class="dd-lazy" border="0" src="{{ asset('imgs/homepage/menu/coll2.jpg') }}"/></a>
												</p>
												<p>
													<!-- <a data-content-position= "Handbags_MegaMenu_slot1_txt" data-content-id="NewHandbags" data-content-name="NewHandbags_txt" href="#/handbags-newarrivals/" title="Shop Tory Burch New Handbags"">New Arrivals</a> -->
												</p>
											</div>
											<div class="slot-image last">
												<p>
													<a data-content-position= "Handbags_MegaMenu_slot1_img" data-content-id="NewHandbags" data-content-name="NewHandbags_img"  href="{{ url('collections/3')}}"><img class="dd-lazy" border="0" src="{{ asset('imgs/homepage/menu/coll1.jpg') }}"/></a>
												</p>
												<p>
													<!-- <a data-content-position= "Handbags_MegaMenu_slot1_txt" data-content-id="NewHandbags" data-content-name="NewHandbags_txt" href="#/handbags-newarrivals/" title="Shop Tory Burch New Handbags"">New Arrivals</a> -->
												</p>
											</div>
											<div style="font-family: Verdana, Geneva, Arial, Helvetica, sans-serif; font-size: 8px; color: Black;"></div>
										</div>	
										<div class="mobilehtmlslotcontainer">
											<div></div>
										</div>
									</li>
								</ul><!-- END: UL -->
							</li>
						    <li class="sale categoryli">				
								<a href="{{url('sale')}}">
					  		  		Хямдрал
								</a>    
						        <!-- <ul data-columnlimit="null" style="background-color: #fff">        
						            <li class="top-nav-group" data-navgroup="100">                    
						                <div class="css-arrow left"></div>
						                <span class="mobile-flyout-menu maincats-span group-hdr mobile-subhead">Sale</span>                
						                <span class="desktop-flyout-menu group-hdr Категори ">Категори</span>        
							        	<ul>            
							                <li class="top-nav-group sale-view-all" data-navgroup="100">
							                    <a title="View All" href="#/sale/view-all/">
							                    View All
							                    </a>
							                </li>                
							            
							                <li class="top-nav-group sale-handbags" data-navgroup="100">
							                    <a title="Handbags" href="#/sale-handbags/">
							                    Handbags
							                    </a>
							                </li>                
							            
							                <li class="top-nav-group sale-shoes-sale" data-navgroup="100">
							                    <a title="Shoes" href="#/sale-shoes/">
							                    Shoes
							                    </a>
							                </li>                
							            
							                <li class="top-nav-group sale-clothing-sale" data-navgroup="100">
							                    <a title="Clothing" href="#/sale-clothing/">
							                    Clothing
							                    </a>
							                </li>                
							            
							                <li class="top-nav-group sale-accessories" data-navgroup="100">
							                    <a title="Accessories" href="#/sale-accessories/">
							                    Accessories
							                    </a>
							                </li>
							            </ul>        
						            </li>
						            <li class="categorymenu-slot">
						            	<div class="htmlslotcontainer mobile-available">
						            		<div class="slot-image">
											    <p class="sale-dd">
											        <a href="#/sale/view-all/" title="Shop Tory Burch Sale Styles"><img class="dd-lazy" alt="Shop Tory Burch Sale Styles" border="0" src="https://static.toryburch.com/static/on/demandware.static/-/Sites/default/dw2daeaa8b/slots/space.gif" data-original="https://static.toryburch.com/static/on/demandware.static/-/Sites/default/dw7735099e/slots/dropdowns/holiday-2017/TBUS_HOL17_12.19_Sale_Evergreen_Dropdown-2.jpg" width="414" height="186" /></a>
											    </p>
											</div>
											<style>
												.categorymenu ul.mega-menu .sale .sub .categorymenu-slot .slot-image { width: 100%; }


												.categorymenu .mega-menu .sale .sub .row { margin-left: 0; min-width: 20%; }
												.categorymenu .mega-menu .sale .sub .categorymenu-slot { float: left; max-width: none; }
												@media screen and (max-width: 1024px) {
												   .categorymenu .mega-menu .sale .sub .categorymenu-slot { max-width: 75%; }
												}
											</style>
										</div>	
										<div class="mobilehtmlslotcontainer">
											<div></div>
										</div>	
						        	</li>    
						        </ul> --><!-- END: UL -->
								
							
							<li class="sale categoryli">				
								<a href="{{url('sale')}}">
					  		  		Gobi Now
								</a>  
								</li>
							<!-- dwMarker="content" dwContentID="bcBlEiaag3PBEaaacRGJRviffV" -->
							<style type="text/css">
								@media all and (-ms-high-contrast: none), (-ms-high-contrast: active) {
								/* IE10+ CSS styles go here */
									.gift-guide > a {
										background-color: none !important;
										color: #0C21B6 !important;
									}
								}

								@-moz-document url-prefix() {
									.gift-guide > a {
										background-color: none !important;
										color: #0C21B6 !important;
									}
								}
								/* desktop/tablet */
								@media screen and ( min-width:768px){
								.mobile-sidebar{display: none !important;}
								}
								@media screen and ( max-width:1024px) and (orientation: portrait ){
								.categorymenu ul.menu-category .categoryli.blog {width: 70px;}
								.categorymenu .menu-background {height: 490px;}

								}
								/* mobile */
								@media screen and ( max-width:767px){
								.not-on-mobile-sb {display:none !important;}
								li.gift-guide.dc-mega-li > span {background: none !important;color:#0C21B6!important;}
								}
							</style>
							
	
							<li class="blog not-on-mobile-hp categoryli">
							    <a class="level-1" href="{{url('gift')}}"></a>
							</li>
		 					<li class="divider not-on-mobile-hp mobile-sidebar"></li>
							<li class="tbf not-on-mobile-hp mobile-sidebar">
							    <a class="level-1" href="{{url('yama')}}">Yama by Gobi</a>
							</li>
							<li class="tbf not-on-mobile-hp mobile-sidebar">
							    <a class="level-1" href="{{url('organic')}}">Gobi Organic</a>
							</li>
							<li class="tbf not-on-mobile-hp mobile-sidebar">
							    <a class="level-1" href="{{url('kids')}}">Gobi Kids</a>
							</li>
							<li class="divider mobile-sidebar not-on-mobile-hp"></li>
							<li class="my-account mobile-sidebar not-on-mobile-hp lower-content">
								<a class="level-1" href="#/account" title="My Account" omniture-track="My Account">
								    <div class="mobilenav-icon myaccount-icon"></div>
								    My Account
								</a>
							</li>
							<li class="find-store mobile-sidebar not-on-mobile-hp lower-content">
								<a class="level-1" href="#/stores" title="Find a store" omniture-track="Find A Store">
								<span class="mobilenav-icon findstore-icon"></span><span>Салбар дэргүүр</span>
								</a>
							</li>
							<li class="mobile-sidebar not-on-mobile-hp lower-content">
								<a class="level-1" href="https://customercare.toryburch.com/app/answers/detail/a_id/41" title="Gift Services" omniture-track="Gift Services">
								Урамшуулал
								</a>
							</li>
							<li class="gift-cards mobile-sidebar not-on-mobile-hp lower-content">
							  <a class="level-1" href="#/giftcards" title="Gift Cards" omniture-track="Gift Cards">
							    Бэлгийн карт
							  </a>
							</li>

							<li class="customer-care mobile-sidebar not-on-mobile-hp lower-content">
							  <a class="level-1" href="#" title="Customer Care" omniture-track="Customer Care">
							    Арчилгаа
							  </a>
							</li>
							<script>
								$(document).ready(function(){
								if (app.util.isAppleDevice) {
								$('.download-app-cta a').attr('href','https://itunes.com/apps/torydaily');
								} else if (app.util.isAndroidDevice) {
								$('.download-app-cta a').attr('href','https://play.google.com/store/apps/details?id=com.toryburch.torydaily');
								}
								});
							</script>
						</ul><!-- END: UL -->
						<div class="menu-background"></div>
					</div><!-- END: navigation categorymenu -->	
					<!--  minibar enabled -->
					<script type="text/javascript">
						
							jQuery(document).ready(function(){
								// hide-top-nav class is used in checkout
								if (!jQuery('body').hasClass('hide-top-nav')){
									app.minibar.init();
								}
							});
						
					</script>
				</div>
				<div id="global-sidebar-wrapper">
				</div>
			</div>
		</div>
		<div class="clear"></div>
	</header>    
</div><!-- END: header -->