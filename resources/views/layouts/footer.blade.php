<footer>
            
            <div class="contentasset mobile-not-available"><!-- dwMarker="content" dwContentID="bcJ0aiaag388QaaacQNNUvRbQH" -->
                <div class="footer--subscribe-content clearfix newsletter_control">
    <div class="subscribe-header">
        <div class="header header--s" style="font-weight: 300 !important">Та цахим хаягаа бүртгүүлнэ үү</div>
        <!-- <div class="body-copy body-copy--s">Receive 10% off your first purchase</div>            -->
    </div>
        
    <div class="subscribe-form-container">
        <form id="footer-form-subscribe" class="subscribe-form" action="" method="post" name="input" onsubmit="return subscribe('#privacy-error',undefined,undefined,undefined,undefined,undefined,undefined,true);">
            <div class="form-section">
                    <div class="email-label-container"> <div class="email-label body-copy body-copy--s">Your Email</div></div>
                <input class="email-input" id="emailsubscribe" name="email" placeholder="Email" size="25" type="text" />
            </div>
            <div class="form-section">
            </div>
                       <div class="form-section last">
                            <button class="button button--large button--primary ladda-btn" data-style="expand-right" data-spinner-size="20" type="submit" value="Submit">Бүртгэх</button>
                <div class="footer-privacy-policy">
                     <div class="custom-checkbox">
                     <input id="custom-checkbox--native" name="legal" type="checkbox" value="yes" />
                                     <label for="custom-checkbox--native"></label>
                    </div>
                <div class="privacy-copy">I accept the terms of the <a class="link link--underline" href="#" target="_blank"><span>Privacy Policy</span></a>.</div>
                    <div class="errormessage" id="privacy-error">&nbsp;</div>
                </div>
                      </div>
        </form>
    </div>
    
</div>
 
<div class="footer--links">
    <div class="footer--links-column">
        <ul class="">
            <li class="column-header">Тусламж</li>
            <li><a href="#">Арчиллагаа</a></li>
            <li><a href="#">Хими цэвэрлэгээ</a></li>
            <li id="footerChatLinkContainer" style="display:none;">
                <div id="footerChatLink">
                    Live Chat
                    <div id="footerChatLinkInfo"></div>
                </div>
            </li>
            <li><a href="#"></a></li>
            <li><a href="#"></a></li>
        </ul>
    </div>
    <div class="footer--links-column">
        <ul>
            <li class="column-header">Хувьцаа</li> 
            <li><a href="#">Хувьцаа эзэмшигчид</a></li>
            <li><a href="#">Компаний засаглал</a></li>
            <li><a href="#">Гүйцэтгэх удирдлага</a></li>
            <li><a href="#">Компаний хувьцаа</a></li>
        </ul>
        <!-- <ul class="v-offset-top-l">
                        <li class="column-header">Download Our App</li> 
            <li class="app-icon-li">
                <a class="app-icon apple" href="https://itunes.com/apps/torydaily" target="_blank" title="Apple Store" ></a>
            </li>
            <li class="app-icon-li">
                   <a class="app-icon android" href="https://play.google.com/store/apps/details?id=com.toryburch.torydaily" target="_blank" title="Android Shop"></a>
            </li>
        </ul> -->
    </div>
    <div class="footer--links-column">
        <ul>
            <li class="column-header">Бидний тухай</li>    
            <li><a href="{{url('about')}}">Бидний тухай</a></li>
            <li><a href="#">Үйлчилгээ</a></li>
            <li><a href="#">Ажлийн байр</a></li>
            <li><a href="#">Холбоо барих</a></li>
        </ul>
    </div>
    <div class="footer--links-column">
        <ul>
            <li class="column-header">Follow Us</li>
            <li>
                <a href="https://www.facebook.com/GobiCashmere/" target="blank" title="Follow Us on Facebook">Facebook</a>
            </li>
            <li>
                <a href="https://instagram.com/gobi_cashmere" target="_blank" title="Follow Us on Instagram">Instagram</a>
            </li>
            <li>
                <a href="https://twitter.com/GobiCashmere" target="_blank" title="Follow Us on Twitter">Twitter</a>
            </li>
            <li>
                <a href="https://www.youtube.com/channel/UCzUZQnPw4XdIw3_IHq6xywA" target="_blank" title="Follow Us on YouTube">YouTube</a>
            </li>
        </ul>
    </div>
</div>
<div class="footer--mobile-links">
    <div class="footer--links-column">
        <div class="column-header js-footer-show-social">Биднийг дага</div>               
        <ul class="js-footer-social-list">
            <li>
                <a href="https://www.facebook.com/GobiCashmere/" target="blank" title="Follow Us on Facebook">Facebook</a>
            </li>
            <li>
                <a href="https://instagram.com/gobi_cashmere" target="_blank" title="Follow Us on Instagram">Instagram</a>
            </li>
            <li>
                <a href="https://twitter.com/GobiCashmere" target="_blank" title="Follow Us on Twitter">Twitter</a>
            </li>
            <li>
                <a href="https://www.youtube.com/channel/UCzUZQnPw4XdIw3_IHq6xywA" target="_blank" title="Follow Us on YouTube">YouTube</a>
            </li>
        </ul>
    </div>
</div>
<div class="clear"></div>
<div class="footer--bottom">
    <div class="footer--bottom-left">
        <div>&copy; 1981-2018 Gobi Cashmere &nbsp;</div>
    </div>
   <!--  <div class="footer--bottom-right">
        <div><a href="#">Privacy &amp; Usage</a></div>
        <div class="middle"><a href="#" title="California Transparency Act">Corporate Governance</a></div>
        <div><a href="#">Site Map</a></div>
    </div> -->
</div>
            </div><!-- End contentasset mobile-not-available -->

</footer>