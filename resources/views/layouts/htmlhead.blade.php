<head>
<title>GOBI MONGOLIAN CASHMERE</title>

<link rel="shortcut icon" href="{{ asset('imgs/favicon.png')}}"/>
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- Global site tag (gtag.js) - Google Analytics --> <script async src="https://www.googletagmanager.com/gtag/js?id=UA-90532872-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-90532872-1');
</script>

<style>
.dropdown {
    position: relative;
    display: inline-block;
}

.dropdown-content {
    display: none;
    position: absolute;
    background-color: #FFF;
    border: 1px solid #DEDEDE;
    min-width: 105px;
    padding: 12px 16px;
    z-index: 1;
    margin-top: -7px;
    margin-left: -45px;
}

.dropdown:hover .dropdown-content {
    display: block;
}
</style>



<link href="{{ asset('css/jquery.ui.all.css')}}" type="text/css" rel="stylesheet" />

<link href="{{ asset('css/SweetSansPro.css')}}" type="text/css" rel="stylesheet" />
<link href="{{ asset('css/footericonfont.css')}}" type="text/css" rel="stylesheet" />
    


<link href="{{ asset('css/default.css')}}" type="text/css" rel="stylesheet" />
<link href="{{ asset('css/index.css')}}" type="text/css" rel="stylesheet" />
<link href="{{ asset('css/common.css')}}" type="text/css" rel="stylesheet" />
<link rel="stylesheet" href="{{ asset('css/animate.min.css')}}">

<!-- Adobe Garamond -->
<!-- <script src="{{ asset('js/van4bag.js')}}"></script> -->
<script>try{Typekit.load({ async: true });}catch(e){}</script>






    <!-- Additional Site CSS -->
<style type="text/css">

/*body {background-image: none; background-color: #FFF; }

body.homepage {background-image: none; background-color: #FFF; } */

body.homepage .home-page-hide{display:none;}
body.blog .home-page-hide{display:none;}

body.blog { background-image: none; background-color: #FFF; }
body.blog .postComments { display: none; }

body.privatesale {background-image: none; }

#srd_pd {padding: 10px 0px 5px !important;}

.beautylandingouter .headerbanner .htmlslotcontainer {background: #fff;}

.blog .headerbanner .htmlslotcontainer, .blog .headerbanner .mobilehtmlslotcontainer {background: none;}

.categorymenu ul.menu-category.mega-menu > li .blog-sub .row-wrapper {}

.categorymenu .menu-background {height:450px;}

@media only screen and (min-device-width : 768px) and (max-device-width : 1024px) {
.blog .headerbanner .promo-message {display:none; }
.categorymenu ul.mega-menu li .blog-sub .categorymenu-slot {}
.categorymenu ul.menu-category { }
}

@media only screen and (min-device-width : 768px) and (max-device-width : 1024px) and (orientation : Landscape) {
.categorymenu ul.mega-menu .swim .sub .categorymenu-slot {  }
.watches .image .productimage a img {  }
}

@media only screen and (min-device-width : 768px) and (max-device-width : 1024px) and (orientation : Portrait) {
#navigation.categorymenu {min-width: 100%;}
.mega-menu li.accessories .sub .row {}
}


@media screen and (max-width:767px){
.product.bannertile.twocolumn-banner-parent .bannertileitem { width: 324px!important; }
.menu-category .sale > span {}
}
@media screen and (min-width:768px){
.pt_cart .cart .item-actions-cell .stockstate { padding: 0;}
}
.producttile .product-banner-bcrf-pink{color:#c67f83;}

.text-menu .sale .row-container {  }
.text-menu .sale.open { }

.menu-category .clothing-sale a, .menu-category .shoes-sale a, .menu-category .handbags-sale a, .menu-category .accessories-sale a {}

/* Mega menu styles */
@media all and (-ms-high-contrast: none), (-ms-high-contrast: active) {
    /* IE10+ CSS styles go here */
}


@media screen and (-webkit-min-device-pixel-ratio: 0) {
}

@media only screen and (min-device-width : 768px) and (max-device-width : 1024px) and (orientation : Landscape) {
}

@media only screen and (min-device-width : 768px) and (max-device-width : 1024px) and (orientation : Portrait) {
    .categorymenu ul.menu-category>li>a {  }
        #navigationsport ul.menu-category > li.categorymenudivider { width: 22.5px !important; }
}

/* iPad Pro portrait */
@media only screen and (min-device-width : 1024px) and (max-device-width : 1024px) and (min-device-height : 1366px) and (max-device-height : 1366px) and (min-width: 1024px) and (max-width: 1024px) {
    #navigationsport ul.menu-category > li.categorymenudivider { width: 69px !important; }
}
/* iPad Pro landscape */
@media only screen and (min-device-width : 1024px) and (max-device-width : 1024px) and (min-device-height : 1366px) and (max-device-height : 1366px) and (min-width: 1366px) and (max-width: 1366px) {
    #navigationsport ul.menu-category > li.categorymenudivider { width: 30px !important; }
    div#navigationsport.categorymenu { width: 1000px !important; }
}
/* Microsoft Edge */
@supports (-ms-ime-align:auto) {
   #navigationsport ul.menu-category > li.categorymenudivider { width: 30px !important; }
}
.mega-menu li .sub .row-wrapper {width:100%;}
@-moz-document url-prefix() { 
  .categorymenu .menu-background { height: 470px; }
}

@media only screen and (min-width : 816px) {
#navigation {min-width:816px;}
}

#home-navigation {display:none;}
</style>

<meta name="robots" content="index, follow" />



<!--     <meta property="og:email" content="inquiries@toryburch.com" />
    <meta property="og:phone_number" content="866-480-8679" />
    <meta http-equiv="content-language" content="en-EN" />
    <meta name="application-name" content="Tory Burch" />
    <meta name="baidu-site-verification" content="XJbUNpXopF" />
    <meta name="apple-itunes-app" content="app-id=531525679" />
    <meta name="msApplication-ID" content="67f403dc-72e0-4053-9af7-c34ce7b54918" /> -->


    <script type="text/javascript" src="{{ asset('js/jquery-1.11.1.min.js')}}" ></script>
    <!-- <script type=text/javascript>
       var hostProtocol = (("#:" == document.location.protocol) ? "#" : "http");
       document.write('<scr'+'ipt src="', hostProtocol+
       '://connect.facebook.net/en_US/all.js','" type="text/JavaScript"><\/scr'+'ipt>');
    </script> -->



<link href="{{ asset('css/videojs-min-css-5.0.css')}}" type="text/css" rel="stylesheet" />

<script>var app={};</script>
    
        
            <script src="{{ asset('js/1371544684.js')}}"></script>
        
    
    <script type="text/javascript" src="{{ asset('js/app.js')}}"></script>
    <script type="text/javascript" src="{{ asset('js/app.sharing.js')}}"></script>
    <script type="text/javascript" src="{{ asset('js/app.wishlist.js')}}"></script>
    
    <script type="text/javascript" src="{{ asset('js/share-manager.js')}}"></script>


<link type="text/css" rel="stylesheet" href="{{ asset('css/grid.css')}}" />


<script id="app_customer">
// global customer variable, to be used for live chat and other customer interaction
app.customer = {"authenticated":false,"registered":false,"customerNo":null,"email":null,"firstName":null,"lastName":null,"hashedEmail":null,"hashedCno":null,"geolocation":{"latitude":47.9167,"longitude":106.9167,"city":"Ulan Bator","regionCode":"1","regionName":"Ulaanbaatar Hot","countryCode":"MN","countryName":"Mongolia","postalCode":"XX"},"customerGroups":["Everyone","Unregistered","isNotMobilecustomer"],"wishlist":{"ID":null,"count":0,"items":[]}};
</script> 



<script type="text/javascript">
    var protocol = document.location.protocol;
    
    app.resources["MISSINGVAL"]         = "Please Enter {0}";
    app.resources["SERVER_ERROR"]       = "Server connection failed!";
    app.resources["MISSING_LIB"]        = "jQuery is undefined.";
    app.resources["BAD_RESPONSE"]       = "Bad response, Parser error";
    app.resources["INVALID_PHONE"]      = "Please specify a valid phone number.";
    app.resources["SITE_ID"]            = "ToryBurch_US";
    app.resources["CHECKOUT_SITE_ID"]   = null || "ToryBurch_US";
    app.resources["OMNITURE_ENABLED"]   = false;
    app.resources["BacktoTop"]          = "Back to Top";
    app.resources["ADDEDTO"]            = "Added to";
    app.resources["ADDTO"]              = "Add to";
    app.resources["SIGNOUT"]            = "Sign Out";
    app.resources["MYPROFILE"]          = "My Profile";
    app.resources["MYWISHLIST"]         = "Wish List";
    app.resources["MYORDERS"]           = "Order History";
    app.resources["ClearRefinement"]    = "Clear";
    app.resources["PurchaseLimit"]      = 5 || '5';
    app.resources["MONTH_NAMES"]        = [ "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" ];
    app.resources["SEARCH"]             = "Search";
    app.resources["RECENTSEARCH"]       = "Recent Searches";
    app.resources["RECENTSEARCHCLEAR"]  = "Clear all";

    app.resources["DISTANCE_UNIT"] = "mi";
    app.resources["STORE_HOURS"] = "Store Hours";

    app.resources["SCENE7_TOOLTIPS"] = {
        "en": {
            "ZoomInButton.TOOLTIP": "Zoom In",
            "ZoomOutButton.TOOLTIP": "Zoom Out",
            "ZoomResetButton.TOOLTIP": "Reset Zoom",
            "FullScreenButton.TOOLTIP_UNSELECTED": "Full Screen",
            "FullScreenButton.TOOLTIP_SELECTED": "Minimize",
            "ScrollLeftButton.TOOLTIP": "product.scrollleft",
            "ScrollRightButton.TOOLTIP": "product.scrollright",
            "ScrollUpButton.TOOLTIP": "Scroll Up",
            "ScrollDownButton.TOOLTIP": "Scroll Down",
            "CloseButton.TOOLTIP": "Close",
            "ZoomIcon.TOUCHMESSAGE": "Double \\A Tap \\A to Zoom"
        },
        defaultLocale: "en"
    };

    
    app.wishlist.guestEnabled = true || false;
    app.wishlist.gridEnabled = true || false;
    app.wishlist.addingMasterAllowed = true || false;
    app.wishlist.savedMsgPrimary = "Saved!";
    app.wishlist.savedMsgSecondary = "This item has been saved to your <a href=\"localhost/wishlist\" class=\"link--underline\"><span>Wish List</span></a>.";
</script>

<!-- Start: components/omniture/headervars -->


<script type="text/javascript" language="javascript">
    // Category Hierarchy
    app.currentPage.categoryHierarchy = [];
    // Breadcrumbs
    app.currentPage.breadcrumbs = [];   
</script>


<script type="text/javascript">
// Content Page Hierarchy
app.currentPage.contentHierarchy = [];
</script>
<!-- End: components/omniture/headervars -->


    <script type="text/javascript" src="{{ asset('js/metrics.js')}}"></script>
    <script type="text/javascript" src="{{ asset('js/product.js')}}"></script>
    <script type="text/javascript" src="{{ asset('js/searchsuggest.js')}}"></script>



<script>
    var pageContext = {"name":"Home:Home","type":"Home Page"};
    if (app)
        if (app.metrics)
            app.metrics.setContext(pageContext);
</script>


    
        <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0" />
    
    
    <link rel="stylesheet" href="{{ asset('css/responsive.css')}}" />
    <script src="{{ asset('js/responsive.js')}}" type="text/javascript"></script>

    <script type="text/javascript">
    
        app.device = "desktop";
        app.bandwidth = "";
        //alert('device='+app.device+'\r\nsetDeviceViewport=true\r\nloadResponsive=true');
    
        var hideUrlBar = function() {
            if (window.pageYOffset <= 0)
                window.scrollTo(0,1);
        };
        
        if (app.device == 'mobile') {
            window.addEventListener('load',function(){
                window.setTimeout(hideUrlBar, 0); 
                window.setTimeout(hideUrlBar, 200);
            }, false);          
        }

        jQuery(document).ready(function() {
            jQuery('html').addClass('device-' + app.device);
            if (screen.width < 768) {
                jQuery('#footer .mobile-site-link').show();
            } else {
                jQuery('#footer .full-site-link').hide();
            }
        });
        
    </script>


<script type="text/javascript">//<!--
/* <![CDATA[ (head-active_data.js) */
var dw = (window.dw || {});
dw.ac = {
    _analytics: null,
    _events: [],
    _category: "",
    _capture: function(configs) {
        if (Object.prototype.toString.call(configs) === "[object Array]") {
            configs.forEach(captureObject);
            return;
        }
        dw.ac._events.push(configs);
    },
    capture: function() { dw.ac._capture(arguments); },
    EV_PRD_SEARCHHIT: "searchhit",
    EV_PRD_DETAIL: "detail",
    EV_PRD_RECOMMENDATION: "recommendation",
    EV_PRD_SETPRODUCT: "setproduct",
    applyContext: function(context) {
        if (typeof context === "object" && context.hasOwnProperty("category")) {
            dw.ac._category = context.category;
        }
    },
    setDWAnalytics: function(analytics) {
        dw.ac._analytics = analytics;
    }
};
/* ]]> */
// -->
</script><script type="text/javascript">//<!--
/* <![CDATA[ (head-cquotient.js) */
var CQuotient = window.CQuotient = {};
CQuotient.clientId = 'Gobi';
CQuotient.activities = [];
CQuotient.cqcid='';
CQuotient.cquid='';
CQuotient.initFromCookies = function () {
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
      var c = ca[i];
      while (c.charAt(0)==' ') c = c.substring(1,c.length);
      if (c.indexOf('cqcid=') == 0) {
          CQuotient.cqcid=c.substring('cqcid='.length,c.length);
      } else if (c.indexOf('cquid=') == 0) {
          CQuotient.cquid=c.substring('cquid='.length,c.length);
          break;
      }
    }
}
CQuotient.getCQCookieId = function () {
    if(window.CQuotient.cqcid == '')
        window.CQuotient.initFromCookies();
    return window.CQuotient.cqcid;
};
CQuotient.getCQUserId = function () {
    if(window.CQuotient.cquid == '')
        window.CQuotient.initFromCookies();
    return window.CQuotient.cquid;
};
/* ]]> */
// -->
</script>
  @yield('styles')
</head>