<div id="header" role="banner">
	<header>
		<div class="headerbanner singlemsg">		
			<div class="htmlslotcontainer mobile-not-available">

				<style type="text/css">
					.headerbanner {background: #002244;}
					.promo-message, .promo-message a {color: #FFF;}
					.message {display:none;}
					@media screen and (max-width:767px) {
					.headerbanner .promo-message .message .arrowright.mac:after {top:2px;}
					}
				</style>

				<div class="promo-message">
					<div class="message">
						Үзэсгэнлэн  Худалдаа  <a class="contentOpenModal" href="#">үзэх  </a><span class="arrowright"></span>
					</div>
				</div>
	<!-- if the message is only one line use below (for mobile) -->
				<script>
				$(document).ready(function(){
				$('.headerbanner').addClass('singlemsg');

				});
				</script>	
				
				<div style="font-family: Verdana, Geneva, Arial, Helvetica, sans-serif; font-size: 8px; color: Black;">
				</div>
			</div>
		</div><!-- END: headerbanner -->
		 
		<div class="topheadernav-wrapper">
			
			<div class="sticky-wrapper">
				<div class="topheadernav alternative">
					<div id="sidebarbtn" title="Menu">
						<div class="inner-sidebar">
							<span></span>
		  					<span></span>
		  					<span></span>
		  				</div>
			  		</div>
					<ul class="topheadernav-left">
							<li class="yama mobile-hidden">
								<a href="#"></a>
							</li>
							<li class="organic">
								<div class="tbf"><a href="#" target="_blank"></a></div>
							</li>
							<li class="kids">
								<div class="logo-blog"><a href="#"></a></div>
							</li>
					</ul>
					<ul class="topheadernav-middle">
						<li class="main-logo">
							<a href="{{ url('/')}}">
							</a>
						</li>
					</ul>
					<ul class="topheadernav-right">
						<li class="mobile-logo">
							<a href="{{ url('/')}}"></a>
						</li>
						<li class="search">
							<div id="searchbtn" title="Search"></div>
							
							<!-- <script type="text/javascript">
							var searchSuggestURL = "/on/demandware.store/Sites-ToryBurch_US-Site/default/Search-GetSuggestions";

							jQuery(document).ready(function() {
								
							});
							</script> -->



							<div class="sitesearch mainsearch">
								<span class="arrowtip"></span>
								<span class="close"></span>
								<form action="#" method="get" id="SimpleSearchForm" class="simplesearch" novalidate="novalidate">
									<fieldset>
										<label>Хайлт</label>
										<input type="search" name="q" value="" class="simplesearchinput" id="searchinput" title="Хайлт" placeholder="Хайлт">
										<button type="submit" value="Хайлт" name="simplesearch"><span>Хайлт</span></button>
									</fieldset>
								</form>
							</div>
							<!-- END: sitesearch -->
						</li>
						<!-- <li class="topnav-shipping">
							<div class="shipping-dropdown">
								
									<a href="/global/international-shipping.html"><div class="flag-icon us"></div></a>
								
							</div>
						</li> -->

						<li class="myaccount-item">
							<a class="header-myaccount" href="#/account" title="My Account">My Account</a>
							<div class="header-myaccount-dialog">
								<div class="topnav-underline"></div>	
								<div class="myaccount-container">
									<div class="header-myaccount-links">	
										<a class="header-signin" href="#/account" title="Sign In">
											Нэвтрэх
										</a>
										<a class="header-createanaccount" href="#/register" title="Create An Account">
											Бүртгүүлэх
										</a>
									</div>
									<div class="header-myaccount-image">
									</div>
								</div>
							</div>
						</li>	

						<li id="wishlist-nav" class="wishlist-nav wishlist-qty-zero">
							<a href="#/wishlist" title="View Wish List">
								0
							</a>
						</li>
									
						{{--<li id="minicart" class="minicart">
							<div class="minicarttotal">
								<a href="#/on/demandware.store/Sites-ToryBurch_US-Site/default/Cart-Show?siteId=ToryBurch_US" title="View Bag" class="linkminicart zero">
									<span class="cartlabel">Shopping bag</span><span class="emptycart">(Empty)</span><span class="minicartqty minicartqtyzero">0</span>
								</a>
							
							</div><!-- END:minicarttotal -->
							<div class="minicartcontent minicartempty" style="display: none;">
								
								<div class="topnav-underline"></div>
								<!-- <div class="clear"></div> -->
								<div class="checkoutminicart">
									<div class="minicart-product-container">
										<div class="minicart-inner">
											
										</div>
									</div>
									<!--  <div id="scroll-to-bottom"><div></div></div> -->
								</div><!-- checkoutminicart -->
								<div class="minicart-bottom-container">
									<div class="minicarttotals">
										<div class="clear"></div>
									</div>
									<div class="clear"></div>
									<div class="link2cart">
										<a href="#/on/demandware.store/Sites-ToryBurch_US-Site/default/Cart-Show?siteId=ToryBurch_US" title="Checkout">Checkout</a>
										<!-- <a class="continueShop">Continue Shopping</a> -->
									</div><!-- END:link2cart -->

									
								</div>
							</div><!-- END:minicartcontent-->

							<script id="basketjson">
								var basket = 
							null
							;
							</script>

							<script id="minilineitemTemplate" type="text/x-jsrender">
								<div class="summaryproduct">

								</div>
							</script>
							<script>
							function setMiniBagHeight(){
								windowMiniHeight = $(window).height() - 20;
								headerMiniCartHeight = $('.topheadernav-right').outerHeight();
								miniCheckoutHeight = $('.minicartBanner').outerHeight();
								miniCheckoutHeaderHeight = $('.minicart-header').outerHeight();
								miniCheckoutTotalHeight = $('.minicart-bottom-container').outerHeight();
								miniCartContentHeight = windowMiniHeight - headerMiniCartHeight;
							    if (navigator.userAgent.match(/(iPod|iPhone|iPad)/)) {
							 		miniCartHeightInner =   miniCartContentHeight - miniCheckoutTotalHeight - miniCheckoutHeaderHeight - 150;
							    }else{
							    	miniCartHeightInner =   miniCartContentHeight - miniCheckoutTotalHeight - miniCheckoutHeaderHeight - miniCheckoutHeight
							    }
							    $('.minicartcontent').css('max-height', miniCartContentHeight);
							    $('.minicart-product-container').css('max-height', miniCartHeightInner);
							};
							$(window).on("load",function(){
								setMiniBagHeight();
							});
							function watchArrows(){
								  var cartHeight = $(".minicart-inner").height() - $(".minicart-product-container").height();
								  if (cartHeight == $(".minicart-product-container").scrollTop()){
								      $("#scroll-to-bottom").addClass('notactive');
								  }else{
								      $("#scroll-to-bottom").removeClass('notactive');
								  }
								  if ($(".minicart-product-container").scrollTop() == 0){
								  	$("#scroll-to-bottom").removeClass('notactive');
								      $("#scroll-to-top").addClass('notactive');
								  }else{
								      $("#scroll-to-top").removeClass('notactive');
								  }
							};
							$(document).ready(function(){
								$('.minicart-product-container').bind('mousewheel DOMMouseScroll', function (e) {
								    var e0 = e.originalEvent,
								    delta = e0.wheelDelta ? e0.wheelDelta : -e0.detail;
								    this.scrollTop += ( delta < 0 ? 1 : -1 ) * 30;
								    e.preventDefault();
								});
								$("#scroll-to-bottom").unbind("mouseover").mouseover(function() {
								    $(".minicart-product-container").stop().animate({scrollTop: $(".minicart-inner").height() - $(".minicart-product-container").height()}, 2000);
								}).mouseout(function() {
								    $(".minicart-product-container").stop();
								    watchArrows();
								});
								$("#scroll-to-top").unbind("mouseover").mouseover(function() {
								    $(".minicart-product-container").stop().animate({scrollTop: 0}, 2000);
								}).mouseout(function() {
								    $(".minicart-product-container").stop();
								    watchArrows();
								});
								watchArrows();
							});
							</script>
						</li><!-- END:minicart -->--}}
					</ul>
				</div>
				<div class="navigation-wrapper" style="max-height: 50px; overflow: visible; top: 90px;">
		    	
					<div id="navigation" class="categorymenu mega-nav" style="width: 813px; opacity: 1;">
			
						<div class="minibar-logo"><a href="{{ url('/')}}" title="Tory Burch">Tory Burch</a></div>
						<ul class="menu-category mega-menu full-width" style="top: 0px; opacity: 1;">				
							<li class="homepage first">
								<a href="http://www.toryburch.com/">Home</a>
							</li>
							<div class="dds-container">
								<li class="new categoryli dc-mega-li">
									<a href="#/new/">
						  		  		эрэгтэй
									</a>
									<div class="sub-container mega" style="width: 1903px; left: -543.5px; top: 20px; z-index: 1000;">
										<ul data-columnlimit="9.0" class="sub" style="display: none;">
											<div class="row-container">
												<div class="row-wrapper">
													<div class="row 100">
														<li class="top-nav-group mega-unit mega-hdr" data-navgroup="100">
															<div class="css-arrow left"></div>
															<span class="mobile-flyout-menu maincats-span group-hdr mobile-subhead">New</span>
															<span class="desktop-flyout-menu group-hdr Categories ">Categories</span>
															<ul>
												                <li class="top-nav-group new-clothing" data-navgroup="100">
												                    <a title="Clothing" href="#/new/clothing/">
												                    Clothing
												                    </a>
												                </li>
												                <li class="top-nav-group new-swim" data-navgroup="100">
												                    <a title="Swim" href="#/new/swim/">
												                    Swim
												                    </a>
												                </li>
												                <li class="top-nav-group new-shoes" data-navgroup="100">
												                    <a title="Shoes" href="#/new/shoes/">
												                    Shoes
												                    </a>
												                </li>
												                <li class="top-nav-group new-handbags" data-navgroup="100">
												                    <a title="Handbags" href="#/new/handbags/">
												                    Handbags
												                    </a>
												                </li>
												                <li class="top-nav-group new-accessories" data-navgroup="100">
												                    <a title="Accessories" href="#/new/accessories/">
												                    Accessories
												                    </a>
												                </li>
												            </ul>
												        </li>
												        <li class="top-nav-group mega-unit mega-hdr" data-navgroup="400">
												        	<div class="divider"></div>		                
										                    <span class="desktop-flyout-menu group-hdr Shops ">Shops</span>										        
													        <ul>
												                <li class="top-nav-group new-torys-favorites" data-navgroup="400">
												                    <a title="Tory&#39;s Favorites" href="#/new/torys-favorites/">
												                    Tory's Favorites
												                    </a>
												                </li>
												                <li class="top-nav-group new-must-haves" data-navgroup="400">
												                    <a title="The Lookbook: Must-Haves" href="#/new-must-haves/">
												                    The Lookbook: Must-Haves
												                    </a>
												                </li>
												                <li class="top-nav-group new-essentials" data-navgroup="400">
												                    <a title="The Essentials" href="#/essentials/">
												                    The Essentials
												                    </a>
												                </li>					            
					        								</ul>
		        
		            									</li>
		            								</div>
		            								<div class="row 120">
		            									<li class="top-nav-group mega-unit mega-hdr" data-navgroup="120">
		            										<div class="divider"></div>
		            										<span class="desktop-flyout-menu group-hdr By Occasion ">By Occasion</span>
		            										<ul>
												                <li class="top-nav-group new-parties-events" data-navgroup="120">
												                    <a title="Parties &amp; Events" href="#/new/parties-events/">
												                    Parties &amp; Events
												                    </a>
												                </li>
												                <li class="top-nav-group new-workweek-chic" data-navgroup="120">
												                    <a title="Workweek Chic" href="#/new/workweek-chic/">
												                    Workweek Chic
												                    </a>
												                </li>
												                <li class="top-nav-group new-beach-getaway" data-navgroup="120">
												                    <a title="Beach Getaway" href="#/new/beach-getaway/">
												                    Beach Getaway
												                    </a>
												                </li>													            
													        </ul>
													    </li>
													    <li class="top-nav-group mega-unit mega-hdr" data-navgroup="1200">
										                    <div class="divider"></div>
										                    <span class="desktop-flyout-menu group-hdr Runway ">Runway</span>
										                    <ul>
												                <li class="top-nav-group new-runway-fall-2018" data-navgroup="1200">
												                    <a title="Fall/Winter 2018" href="#/runway-fall-2018/">
												                    Fall/Winter 2018
												                    </a>
												                </li>
												                <li class="top-nav-group new-runway-spring-2018" data-navgroup="1200">
												                    <a title="Spring/Summer 2018" href="#/runway-spring-2018/">
												                    Spring/Summer 2018
												                    </a>
												                </li>
												                <li class="top-nav-group new-runway-fall-2017" data-navgroup="1200">
												                    <a title="Fall/Winter 2017" href="#/runway-fall-2017/">
												                    Fall/Winter 2017
												                    </a>
												                </li>  
													        </ul>
													        
													    </li>
													</div>
													<li class="categorymenu-slot">
														<div class="htmlslotcontainer mobile-not-available">
															<div class="slot-image">
																<p>
																	<a data-content-position="New_MegaMenu_slot1_img" data-content-id="NewClothing" data-content-name="NewClothing_img" href="#/clothing/new-arrivals/" title="Shop Tory Burch New Arrivals">
																		<img class="dd-lazy" alt="Shop the New Arrivals" border="0" height="231" src="./Women&#39;s Clothing, Designer Shoes &amp; Flats, Handbags &amp; Accessories _ Tory Burch_files/space.gif" data-original="https://static.toryburch.com/static/on/demandware.static/-/Sites/default/dw8843d2b4/slots/dropdowns/spring-2018/TBUS_Spring18_3.8_Dropdown_New_01_3x.jpg" title="Shop Tory Burch New Arrivals" width="257">
																	</a>
																</p>

																<p><a data-content-position="New_MegaMenu_slot1_txt" data-content-id="NewClothing" data-content-name="NewClothing_txt" href="#/clothing/new-arrivals/" title="Shop Tory Burch New Arrivals">New Clothing</a>
																</p>
															</div>
															<div class="slot-image last">
																<p>
																	<a data-content-position="New_MegaMenu_slot2_img" data-content-id="NewHandbags" data-content-name="NewHandbags_img" href="#/handbags-newarrivals/" title="Shop Tory Burch New Handbags"><img class="dd-lazy" alt="Shop New Handbags" border="0" height="231" src="./Women&#39;s Clothing, Designer Shoes &amp; Flats, Handbags &amp; Accessories _ Tory Burch_files/space.gif" data-original="https://static.toryburch.com/static/on/demandware.static/-/Sites/default/dwc3f86e5c/slots/dropdowns/spring-2018/TBUS_Spring18_3.8_Dropdown_New_02_3x.jpg" title="Shop Tory Burch New Handbags" width="257"></a>
																</p>
																<p>
																	<a data-content-position="New_MegaMenu_slot2_txt" data-content-id="NewHandbags" data-content-name="NewHandbags_txt" href="#/handbags-newarrivals/" title="Shop Tory Burch New Handbags">New Handbags</a>
																</p>
															</div>
															<div style="font-family: Verdana, Geneva, Arial, Helvetica, sans-serif; font-size: 8px; color: Black;">
																			
															</div>
														</div>
		        									</li>
		        								</div>
		        							</div>
		        						</ul>
		        					</div><!-- END: UL -->
		        				</li>
		        				<li class="clothing categoryli dc-mega-li">
		        					<a href="#/clothing/">
						  		  		эмэгтэй
									</a>
									<div class="sub-container mega" style="width: 1903px; left: -543.5px; top: 20px; z-index: 1000;">
										<ul data-columnlimit="null" class="sub" style="display: none;">
											<div class="row-container">
												<div class="row-wrapper">
													<div class="row 100">
														<li class="top-nav-group mega-unit mega-hdr" data-navgroup="100">
															<div class="css-arrow left"></div>
															<span class="mobile-flyout-menu maincats-span group-hdr mobile-subhead">Clothing</span>
															<span class="desktop-flyout-menu group-hdr Categories ">Categories</span>
															<ul>
					            
												                <li class="top-nav-group clothing-newarrivals" data-navgroup="100">
												                    <a title="New Arrivals" href="#/clothing/new-arrivals/">
												                    New Arrivals
												                    </a>
												                </li>
												                
												            
												                <li class="top-nav-group clothing-view-all" data-navgroup="100">
												                    <a title="View All" href="#/clothing/view-all/">
												                    View All
												                    </a>
												                </li>
												                
												            
												                <li class="top-nav-group clothing-dresses" data-navgroup="100">
												                    <a title="Dresses" href="#/clothing/dresses/">
												                    Dresses
												                    </a>
												                </li>
												                
												            
												                <li class="top-nav-group clothing-tops" data-navgroup="100">
												                    <a title="Tops &amp; Shirts" href="#/clothing/tops-shirts/">
												                    Tops &amp; Shirts
												                    </a>
												                </li>
												                
												            
												                <li class="top-nav-group clothing-tunics" data-navgroup="100">
												                    <a title="Tunics &amp; Caftans" href="#/clothing/tunics-caftans/">
												                    Tunics &amp; Caftans
												                    </a>
												                </li>
												                
												            
												                <li class="top-nav-group clothing-sweaters" data-navgroup="100">
												                    <a title="Sweaters" href="#/clothing/sweaters/">
												                    Sweaters
												                    </a>
												                </li>
												                
												            
												                <li class="top-nav-group clothing-jackets-outerwear" data-navgroup="100">
												                    <a title="Jackets" href="#/clothing/jackets/">
												                    Jackets
												                    </a>
												                </li>
												                
												            
												                <li class="top-nav-group clothing-coats-outerwear" data-navgroup="100">
												                    <a title="Coats &amp; Outerwear" href="#/clothing/coats-outerwear/">
												                    Coats &amp; Outerwear
												                    </a>
												                </li>
												                
												            
												                <li class="top-nav-group clothing-skirts" data-navgroup="100">
												                    <a title="Skirts" href="#/clothing/skirts/">
												                    Skirts
												                    </a>
												                </li>
												                
												            
												                <li class="top-nav-group clothing-pants" data-navgroup="100">
												                    <a title="Pants" href="#/clothing/pants/">
												                    Pants
												                    </a>
												                </li>
												                
												            
												                <li class="top-nav-group clothing-denim" data-navgroup="100">
												                    <a title="Denim" href="#/clothing-denim/">
												                    Denim
												                    </a>
												                </li>
												                
												            
												                <li class="top-nav-group clothing-sale" data-navgroup="100">
												                    <a title="Sale" href="#/clothing/sale/">
												                    Sale
												                    </a>
												                </li>
												            </ul>
												        </li>
												    </div>
												    <div class="row 400">
												    	<li class="top-nav-group mega-unit mega-hdr" data-navgroup="400">
												    		<div class="divider"></div>
												    		<span class="desktop-flyout-menu group-hdr Shops ">Shops</span>
													        <ul>
													            
												                <li class="top-nav-group clothing-torys-favorites" data-navgroup="400">
												                    <a title="Tory&#39;s Favorites" href="#/clothing/torys-favorites/">
												                    Tory's Favorites
												                    </a>
												                </li>
												                
												            
												                <li class="top-nav-group clothing-must-haves" data-navgroup="400">
												                    <a title="The Lookbook: Must-Haves" href="#/clothing-must-haves/">
												                    The Lookbook: Must-Haves
												                    </a>
												                </li>
												                
												            
												                <li class="top-nav-group clothing-tory-story" data-navgroup="400">
												                    <a title="#ToryStories" href="#/clothing-tory-story/">
												                    #ToryStories
												                    </a>
												                </li>
												                
												            
												                <li class="top-nav-group clothing-whites" data-navgroup="400">
												                    <a title="Spring Whites" href="#/clothing/whites/">
												                    Spring Whites
												                    </a>
												                </li>
												                
												            
												                <li class="top-nav-group clothing-parties-events" data-navgroup="400">
												                    <a title="Parties &amp; Events" href="#/clothing/parties-events/">
												                    Parties &amp; Events
												                    </a>
												                </li>
												                
												            
												                <li class="top-nav-group clothing-workweek" data-navgroup="400">
												                    <a title="Workweek Chic" href="#/clothing/workweek-chic/">
												                    Workweek Chic
												                    </a>
												                </li>
												                
												            
												                <li class="top-nav-group clothing-foundation" data-navgroup="400">
												                    <a title="Tory Burch Foundation" href="#/clothing/tory-burch-foundation/">
												                    Tory Burch Foundation
												                    </a>
												                </li>
												            </ul>
												        </li>
												    </div>
												    <li class="categorymenu-slot">
												    	<div class="htmlslotcontainer mobile-available">
												    		<div class="slot-image">
																<p><a data-content-position="Clothing_MegaMenu_slot1_img" data-content-id="NewClothes" data-content-name="NewClothes_img" href="#/clothing/new-arrivals/" title="Shop Tory Burch New Clothing"><img class="dd-lazy" alt="Shop New Clothing" border="0" height="231" src="./Women&#39;s Clothing, Designer Shoes &amp; Flats, Handbags &amp; Accessories _ Tory Burch_files/space.gif" data-original="https://static.toryburch.com/static/on/demandware.static/-/Sites/default/dwa3fcab59/slots/dropdowns/spring-2018/TBUS_Spring18_3.8_Dropdown_Clothing_01_3x.jpg" title="Shop Tory Burch New Clothing" width="257"></a></p>

																<p><a data-content-position="Clothing_MegaMenu_slot1_txt" data-content-id="NewClothes" data-content-name="NewClothes_txt" href="#/clothing/new-arrivals/" title="Shop Tory Burch New Clothing">New Arrivals</a></p>
															</div>
															<div class="slot-image last">
																<p><a data-content-position="Clothing_MegaMenu_slot2_img" data-content-id="Lookbook" data-content-name="Lookbook_img" href="#/clothing-must-haves/" title="Shop Tory Burch Resort Lookbook"><img class="dd-lazy" alt="Shop the Resort Lookbook" border="0" height="231" src="./Women&#39;s Clothing, Designer Shoes &amp; Flats, Handbags &amp; Accessories _ Tory Burch_files/space.gif" data-original="https://static.toryburch.com/static/on/demandware.static/-/Sites/default/dw7ac84956/slots/dropdowns/spring-2018/TBUS_Spring18_3.8_Dropdown_Clothing_02_3x.jpg" title="Shop Tory Burch Resort Lookbook" width="257"></a></p>

																<p><a data-content-position="Clothing_MegaMenu_slot2_txt" data-content-id="Lookbook" data-content-name="Lookbook_txt" href="#/clothing-must-haves/" title="Shop Tory Burch Resort Lookbook">The Lookbook: Must-Haves</a></p>
															</div>
															<div style="font-family: Verdana, Geneva, Arial, Helvetica, sans-serif; font-size: 8px; color: Black;"></div>
														</div>
														<div class="mobilehtmlslotcontainer">
															<div></div>
														</div>
													</li>
												</div>
											</div>
										</ul>
									</div><!-- END: UL -->
								</li>
								<li class="swim categoryli dc-mega-li">
									<a href="#/swim/">
						  		  		гэр ахуй
									</a>
									<div class="sub-container mega" style="width: 1903px; left: -543.5px; top: 20px; z-index: 1000;">
										<ul data-columnlimit="null" class="sub" style="display: none;">
											<div class="row-container">
												<div class="row-wrapper">
													<div class="row 100">
														<li class="top-nav-group mega-unit mega-hdr" data-navgroup="100">
															<div class="css-arrow left"></div>
															<span class="mobile-flyout-menu maincats-span group-hdr mobile-subhead">Swim</span>
															<span class="desktop-flyout-menu group-hdr Categories ">Categories</span>
															<ul>
												                <li class="top-nav-group swim-newarrivals" data-navgroup="100">
												                    <a title="New Arrivals" href="#/swim/new-arrivals/">
												                    New Arrivals
												                    </a>
												                </li>
												                
												            
												                <li class="top-nav-group swim-swimwear" data-navgroup="100">
												                    <a title="Swimwear" href="#/swim/swimwear/">
												                    Swimwear
												                    </a>
												                </li>
												                
												            
												                <li class="top-nav-group swim-swimwear-one-pieces" data-navgroup="100">
												                    <a title="One Pieces" href="#/swim/one-pieces/">
												                    One Pieces
												                    </a>
												                </li>
												                
												            
												                <li class="top-nav-group swim-swimwear-two-pieces" data-navgroup="100">
												                    <a title="Two Pieces" href="#/swim/two-pieces/">
												                    Two Pieces
												                    </a>
												                </li>
												                
												            
												                <li class="top-nav-group swim-tunics-coverups" data-navgroup="100">
												                    <a title="Cover-Ups &amp; Tunics" href="#/swim/cover-ups-tunics/">
												                    Cover-Ups &amp; Tunics
												                    </a>
												                </li>
												                
												            
												                <li class="top-nav-group swim-accessories" data-navgroup="100">
												                    <a title="Beach Accessories" href="#/swim/beach-accessories/">
												                    Beach Accessories
												                    </a>
												                </li>
												        	</ul>
												        </li>
												        <li class="top-nav-group mega-unit mega-hdr" data-navgroup="400">
		                
										                    <div class="divider"></div>										                
										                    <span class="desktop-flyout-menu group-hdr Shops ">Shops</span>													        
													        <ul>													            
												                <li class="top-nav-group swim-beach-getaway" data-navgroup="400">
												                    <a title="Beach Getaway" href="#/swim/beach-getaway/">
												                    Beach Getaway
												                    </a>
												                </li>
												                
												            
												                <li class="top-nav-group swim-little-black-swim-shop" data-navgroup="400">
												                    <a title="The Little Black Swim Shop" href="#/swim/the-little-black-swim-shop/">
												                    The Little Black Swim Shop
												                    </a>
												                </li>
												            </ul>
												        </li>
												    </div>
												    <li class="categorymenu-slot">
												    	<div class="htmlslotcontainer mobile-available">
												    		<style type="text/css">
															    .categorymenu ul.mega-menu .sub .categorymenu-slot .slot-image.full-width {
															        width: 100%;
															    }

															    video {
															        max-width: 100%;
															    }
															</style>

															<div class="slot-image full-width">
															    <a data-content-position="Shoes_MegaMenu_slot1_img" data-content-id="NewSwim" data-content-name="NewSwim_img" href="#/swim/new-arrivals/" title="Shop New Swimwear">
															    <img alt="Shop New Swimwear" class="shoes-dd-img dd-lazy" border="0" src="./Women&#39;s Clothing, Designer Shoes &amp; Flats, Handbags &amp; Accessories _ Tory Burch_files/space.gif" data-original="https://static.toryburch.com/static/on/demandware.static/-/Sites/default/dwda1923ad/slots/dropdowns/spring-2018/TBUS_Spring18_3.8_Dropdown_Swim_3x.jpg" title="Shop New Swimwear" width="231" height="257">
															    </a>
															    <a data-content-position="Shoes_MegaMenu_slot1_txt" data-content-id="NewSwim" data-content-name="NewSwim_txt&quot;" href="#/swim/new-arrivals/" title="Shop New Swimwear">New Arrivals</a>
															</div>					
															<div style="font-family: Verdana, Geneva, Arial, Helvetica, sans-serif; font-size: 8px; color: Black;"></div>
														</div>			
														<div class="mobilehtmlslotcontainer">
															<div></div>
														</div>
													</li>
												</div>
											</div>
										</ul>
									</div><!-- END: UL -->
								</li>
								<li class="shoes categoryli dc-mega-li">						
									<a href="#/shoes/">
						  		  		аксесуар
									</a>
									<div class="sub-container mega" style="width: 1903px; left: -543.5px; top: 20px; z-index: 1000;">
										<ul data-columnlimit="null" class="sub" style="display: none;">
											<div class="row-container">
												<div class="row-wrapper">
													<div class="row 100">
														<li class="top-nav-group mega-unit mega-hdr" data-navgroup="100">
															<div class="css-arrow left"></div>
		                        							<span class="mobile-flyout-menu maincats-span group-hdr mobile-subhead">Shoes</span>
		                        							<span class="desktop-flyout-menu group-hdr Categories ">Categories</span>		        
													        <ul>													            
													                <li class="top-nav-group shoes-newarrivals" data-navgroup="100">
													                    <a title="New Arrivals" href="#/shoes-newarrivals/">
													                    New Arrivals
													                    </a>
													                </li>
													                
													            
													                <li class="top-nav-group shoes-view-all" data-navgroup="100">
													                    <a title="View All" href="#/shoes/view-all/">
													                    View All
													                    </a>
													                </li>
													                
													            
													                <li class="top-nav-group shoes-sandals" data-navgroup="100">
													                    <a title="Sandals" href="#/shoes/sandals/">
													                    Sandals
													                    </a>
													                </li>
													                
													            
													                <li class="top-nav-group shoes-heels" data-navgroup="100">
													                    <a title="Heels &amp; Pumps" href="#/shoes/heels-pumps/">
													                    Heels &amp; Pumps
													                    </a>
													                </li>
													                
													            
													                <li class="top-nav-group shoes-espadrilles" data-navgroup="100">
													                    <a title="Espadrilles" href="#/shoes/espadrilles/">
													                    Espadrilles
													                    </a>
													                </li>
													                
													            
													                <li class="top-nav-group shoes-wedges" data-navgroup="100">
													                    <a title="Wedges" href="#/shoes/wedges/">
													                    Wedges
													                    </a>
													                </li>
													                
													            
													                <li class="top-nav-group shoes-flip-flops" data-navgroup="100">
													                    <a title="Flip-Flops" href="#/shoes/flip-flops/">
													                    Flip-Flops
													                    </a>
													                </li>
													                
													            
													                <li class="top-nav-group shoes-flats" data-navgroup="100">
													                    <a title="Flats" href="#/shoes/flats/">
													                    Flats
													                    </a>
													                </li>
													                
													            
													                <li class="top-nav-group shoes-ballet" data-navgroup="100">
													                    <a title="Ballet Flats" href="#/shoes/ballet-flats/">
													                    Ballet Flats
													                    </a>
													                </li>
													                
													            
													                <li class="top-nav-group shoes-slides" data-navgroup="100">
													                    <a title="Mules &amp; Slides" href="#/shoes/mules-slides/">
													                    Mules &amp; Slides
													                    </a>
													                </li>
													                
													            
													                <li class="top-nav-group shoes-sneakers" data-navgroup="100">
													                    <a title="Sneakers" href="#/shoes/sneakers/">
													                    Sneakers
													                    </a>
													                </li>
													                
													            
													                <li class="top-nav-group shoes-slippers-loafers" data-navgroup="100">
													                    <a title="Slippers &amp; Loafers" href="#/shoes/slippers-loafers/">
													                    Slippers &amp; Loafers
													                    </a>
													                </li>
													                
													            
													                <li class="top-nav-group shoes-boots" data-navgroup="100">
													                    <a title="Boots" href="#/shoes/boots/">
													                    Boots
													                    </a>
													                </li>
													                
													            
													                <li class="top-nav-group shoes-booties" data-navgroup="100">
													                    <a title="Booties" href="#/shoes/booties/">
													                    Booties
													                    </a>
													                </li>
													                
													            
													                <li class="top-nav-group shoes-sale" data-navgroup="100">
													                    <a title="Sale" href="#/shoes/sale/">
													                    Sale
													                    </a>
													                </li>
													        </ul>
													    </li>
													</div>
													<div class="row 400">
														<li class="top-nav-group mega-unit mega-hdr" data-navgroup="400">
															<div class="divider"></div>
															<span class="desktop-flyout-menu group-hdr Shops ">Shops</span>		        
													        <ul>													            
												                <li class="top-nav-group shoes-neutrals" data-navgroup="400">
												                    <a title="The Neutrals" href="#/shoes/the-neutrals/">
												                    The Neutrals
												                    </a>
												                </li>
												                
												            
												                <li class="top-nav-group shoes-the-embellishment" data-navgroup="400">
												                    <a title="The Trend: Embellishment" href="#/shops-shoes-embellishment/">
												                    The Trend: Embellishment
												                    </a>
												                </li>													            
													        </ul>
													    </li>
													    <li class="top-nav-group mega-unit mega-hdr" data-navgroup="200">
													    	<div class="divider"></div>
													    	<span class="desktop-flyout-menu group-hdr Collections ">Collections</span>		        
													        <ul>													            
													                <li class="top-nav-group shoes-chelsea" data-navgroup="200">
													                    <a title="Chelsea" href="#/shoes/chelsea/">
													                    Chelsea
													                    </a>
													                </li>
													                
													            
													                <li class="top-nav-group shoes-miller" data-navgroup="200">
													                    <a title="Miller" href="#/shoes/miller/">
													                    Miller
													                    </a>
													                </li>
													                
													            
													                <li class="top-nav-group shoes-minnie" data-navgroup="200">
													                    <a title="Minnie" href="#/shoes/minnie/">
													                    Minnie
													                    </a>
													                </li>													            
													        </ul>
													    </li>
													</div>
													<li class="categorymenu-slot">
														<div class="htmlslotcontainer mobile-available">
															<style type="text/css">
																.categorymenu ul.mega-menu .sub .categorymenu-slot .slot-image.full-width {width: 100%;}
																video {max-width: 100%;}
															</style>
															<div class="slot-image full-width">
																<a data-content-position="Shoes_MegaMenu_slot1_img" data-content-id="Miller" data-content-name="Miller_img" href="#/shoes/miller/" title="Shop Tory Burch Miller Collection">
																<img alt="Shop Miller Shoes" class="shoes-dd-img dd-lazy" border="0" src="./Women&#39;s Clothing, Designer Shoes &amp; Flats, Handbags &amp; Accessories _ Tory Burch_files/space.gif" data-original="https://static.toryburch.com/static/on/demandware.static/-/Sites/default/dw4239e58f/slots/dropdowns/spring-2018/TBUS_Spring18_2.6_Dropdown_Shoes_01_3x.jpg" title="Shop Tory Burch Miller Collection" width="231" height="257">
																</a>
																<a data-content-position="Shoes_MegaMenu_slot1_txt" data-content-id="Miller" data-content-name="Miller_txt&quot;" href="#/shoes/miller/" title="Shop Tory Burch Miller Collection">Miller</a>
															</div>
														</div>
														<div class="mobilehtmlslotcontainer">
															<div></div>
														</div>
													</li>
												</div>
											</div>
										</ul>
									</div><!-- END: UL -->
								</li>
								<li class="handbags categoryli dc-mega-li">						
									<a href="#/handbags/">
						  		  		коллекц
									</a>
									<div class="sub-container mega" style="width: 1903px; left: -543.5px; top: 20px; z-index: 1000;">
										<ul data-columnlimit="null" class="sub" style="display: none;">
											<div class="row-container">
												<div class="row-wrapper">
													<div class="row 100">
														<li class="top-nav-group mega-unit mega-hdr" data-navgroup="100">
															<div class="css-arrow left"></div>
															<span class="mobile-flyout-menu maincats-span group-hdr mobile-subhead">Handbags</span>
															<span class="desktop-flyout-menu group-hdr Categories ">Categories</span>		        
													        <ul>													            
												                <li class="top-nav-group handbags-newarrivals" data-navgroup="100">
												                    <a title="New Arrivals" href="#/handbags-newarrivals/">
												                    New Arrivals
												                    </a>
												                </li>
												                
												            
												                <li class="top-nav-group handbags-view-all" data-navgroup="100">
												                    <a title="View All" href="#/handbags/view-all/">
												                    View All
												                    </a>
												                </li>
												                
												            
												                <li class="top-nav-group handbags-crossbody-bags" data-navgroup="100">
												                    <a title="Cross-Body Bags" href="#/handbags/cross-body-bags/">
												                    Cross-Body Bags
												                    </a>
												                </li>
												                
												            
												                <li class="top-nav-group handbags-totes" data-navgroup="100">
												                    <a title="Totes" href="#/handbags/totes/">
												                    Totes
												                    </a>
												                </li>
												                
												            
												                <li class="top-nav-group handbags-satchels" data-navgroup="100">
												                    <a title="Satchels" href="#/handbags/satchels/">
												                    Satchels
												                    </a>
												                </li>
												                
												            
												                <li class="top-nav-group handbags-shoulder-bags" data-navgroup="100">
												                    <a title="Shoulder Bags" href="#/handbags/shoulder-bags/">
												                    Shoulder Bags
												                    </a>
												                </li>
												                
												            
												                <li class="top-nav-group handbags-clutches" data-navgroup="100">
												                    <a title="Clutches &amp; Evening Bags" href="#/handbags/clutches-evening-bags/">
												                    Clutches &amp; Evening Bags
												                    </a>
												                </li>
												                
												            
												                <li class="top-nav-group handbags-mini-bags" data-navgroup="100">
												                    <a title="Mini Bags" href="#/handbags/mini-bags/">
												                    Mini Bags
												                    </a>
												                </li>
												                
												            
												                <li class="top-nav-group handbags-babybags" data-navgroup="100">
												                    <a title="Baby Bags" href="#/handbags/baby-bags/">
												                    Baby Bags
												                    </a>
												                </li>
												                
												            
												                <li class="top-nav-group handbags-backpacks" data-navgroup="100">
												                    <a title="Backpacks" href="#/handbags/backpacks/">
												                    Backpacks
												                    </a>
												                </li>
												                
												            
												                <li class="top-nav-group handbags-sale" data-navgroup="100">
												                    <a title="Sale" href="#/handbags/sale/">
												                    Sale
												                    </a>
												                </li>
												            </ul>
												        </li>
												    </div>
												    <div class="row 400">
												    	<li class="top-nav-group mega-unit mega-hdr" data-navgroup="400">
												    		<div class="divider"></div>
												    		<span class="desktop-flyout-menu group-hdr Shops ">Shops</span>		        
													        <ul>													            
												                <li class="top-nav-group handbags-wallets-handbags" data-navgroup="400">
												                    <a title="Matching Wallets &amp; Handbags" href="#/handbags-matching-wallets-handbags/">
												                    Matching Wallets &amp; Handbags
												                    </a>
												                </li>
												                
												            
												                <li class="top-nav-group handbags-neutrals" data-navgroup="400">
												                    <a title="The Neutrals" href="#/handbags/the-neutrals/">
												                    The Neutrals
												                    </a>
												                </li>													            
													        </ul>
													    </li>
													    <li class="top-nav-group mega-unit mega-hdr" data-navgroup="200">		                
										                    <div class="divider"></div>										                
										                    <span class="desktop-flyout-menu group-hdr Collections ">Collections</span>													        
													        <ul>													            
												                <li class="top-nav-group handbags-half-moon" data-navgroup="200">
												                    <a title="Half-Moon" href="#/handbags/half-moon/">
												                    Half-Moon
												                    </a>
												                </li>
												                
												            
												                <li class="top-nav-group handbags-miller" data-navgroup="200">
												                    <a title="Miller" href="#/handbags/miller/">
												                    Miller
												                    </a>
												                </li>
												                
												            
												                <li class="top-nav-group handbags-kira" data-navgroup="200">
												                    <a title="Kira" href="#/handbags/kira/">
												                    Kira
												                    </a>
												                </li>
												                
												                    
												                
												            
												                <li class="top-nav-group handbags-fleming" data-navgroup="200">
												                    <a title="Fleming" href="#/handbags/fleming/">
												                    Fleming
												                    </a>
												                </li>
												                
												            
												                <li class="top-nav-group handbags-mcgraw" data-navgroup="200">
												                    <a title="McGraw" href="#/handbags/mcgraw/">
												                    McGraw
												                    </a>
												                </li>
												                
												            
												                <li class="top-nav-group handbags-the-robinson-collection" data-navgroup="200">
												                    <a title="Robinson" href="#/handbags/robinson/">
												                    Robinson
												                    </a>
												                </li>
												                
												            
												                <li class="top-nav-group handbags-ella" data-navgroup="200">
												                    <a title="Ella" href="#/handbags/ella/">
												                    Ella
												                    </a>
												                </li>													            
													        </ul>
													    </li>
													</div>
													<li class="categorymenu-slot">
														<div class="htmlslotcontainer mobile-available">
															<style type="text/css">
																
															</style>
															<div class="slot-image">
																<p>
																<a data-content-position="Handbags_MegaMenu_slot1_img" data-content-id="NewHandbags" data-content-name="NewHandbags_img" href="#/handbags-newarrivals/" title="Shop Tory Burch New Handbags"><img class="dd-lazy" alt="Shop Handbags" border="0" height="231" src="./Women&#39;s Clothing, Designer Shoes &amp; Flats, Handbags &amp; Accessories _ Tory Burch_files/space.gif" data-original="https://static.toryburch.com/static/on/demandware.static/-/Sites/default/dwc3f86e5c/slots/dropdowns/spring-2018/TBUS_Spring18_3.8_Dropdown_New_02_3x.jpg" title="Shop Tory Burch New Handbags" width="257"></a></p>

																<p>
																<a data-content-position="Handbags_MegaMenu_slot1_txt" data-content-id="NewHandbags" data-content-name="NewHandbags_txt" href="#/handbags-newarrivals/" title="Shop Tory Burch New Handbags" "="">New Arrivals</a></p>
															</div>


															<div class="slot-image last">
																<p>
																<a data-content-position="Handbags_MegaMenu_slot2_img" data-content-id="McGraw" data-content-name="McGraw_img" href="#/handbags/mcgraw/" title="Shop the McGraw Collection"><img class="dd-lazy" alt="Shop the McGraw Collection" border="0" height="231" src="./Women&#39;s Clothing, Designer Shoes &amp; Flats, Handbags &amp; Accessories _ Tory Burch_files/space.gif" data-original="https://static.toryburch.com/static/on/demandware.static/-/Sites/default/dwaa080093/slots/dropdowns/spring-2018/TBUS_Spring18_3.8_Dropdown_Handbags_02_3x.jpg" title="Shop the Tory Burch McGraw Collection" width="257"></a></p>

																<p>
																<a data-content-position="Handbags_MegaMenu_slot2_txt" data-content-id="McGraw" data-content-name="McGraw_txt" href="#/handbags/mcgraw/" title="Shop the Tory Burch McGraw Collection">McGraw</a></p>
															</div>
															<div style="font-family: Verdana, Geneva, Arial, Helvetica, sans-serif; font-size: 8px; color: Black;"></div>
														</div>
														<div class="mobilehtmlslotcontainer">
															<div></div>
														</div>
													</li>
												</div>
											</div>
										</ul>
									</div><!-- END: UL -->
								</li>
								<li class="handbags categoryli dc-mega-li">						
									<a href="#/handbags/">
						  		  		бидний туай
									</a>
									<div class="sub-container mega" style="width: 1903px; left: -543.5px; top: 20px; z-index: 1000;">
										<ul data-columnlimit="null" class="sub" style="display: none;">
											<div class="row-container">
												<div class="row-wrapper">
													<div class="row 100">
														<li class="top-nav-group mega-unit mega-hdr" data-navgroup="100">
															<div class="css-arrow left"></div>
															<span class="mobile-flyout-menu maincats-span group-hdr mobile-subhead">Handbags</span>
															<span class="desktop-flyout-menu group-hdr Categories ">Categories</span>		        
													        <ul>													            
												                <li class="top-nav-group handbags-newarrivals" data-navgroup="100">
												                    <a title="New Arrivals" href="#/handbags-newarrivals/">
												                    New Arrivals
												                    </a>
												                </li>
												                
												            
												                <li class="top-nav-group handbags-view-all" data-navgroup="100">
												                    <a title="View All" href="#/handbags/view-all/">
												                    View All
												                    </a>
												                </li>
												                
												            
												                <li class="top-nav-group handbags-crossbody-bags" data-navgroup="100">
												                    <a title="Cross-Body Bags" href="#/handbags/cross-body-bags/">
												                    Cross-Body Bags
												                    </a>
												                </li>
												                
												            
												                <li class="top-nav-group handbags-totes" data-navgroup="100">
												                    <a title="Totes" href="#/handbags/totes/">
												                    Totes
												                    </a>
												                </li>
												                
												            
												                <li class="top-nav-group handbags-satchels" data-navgroup="100">
												                    <a title="Satchels" href="#/handbags/satchels/">
												                    Satchels
												                    </a>
												                </li>
												                
												            
												                <li class="top-nav-group handbags-shoulder-bags" data-navgroup="100">
												                    <a title="Shoulder Bags" href="#/handbags/shoulder-bags/">
												                    Shoulder Bags
												                    </a>
												                </li>
												                
												            
												                <li class="top-nav-group handbags-clutches" data-navgroup="100">
												                    <a title="Clutches &amp; Evening Bags" href="#/handbags/clutches-evening-bags/">
												                    Clutches &amp; Evening Bags
												                    </a>
												                </li>
												                
												            
												                <li class="top-nav-group handbags-mini-bags" data-navgroup="100">
												                    <a title="Mini Bags" href="#/handbags/mini-bags/">
												                    Mini Bags
												                    </a>
												                </li>
												                
												            
												                <li class="top-nav-group handbags-babybags" data-navgroup="100">
												                    <a title="Baby Bags" href="#/handbags/baby-bags/">
												                    Baby Bags
												                    </a>
												                </li>
												                
												            
												                <li class="top-nav-group handbags-backpacks" data-navgroup="100">
												                    <a title="Backpacks" href="#/handbags/backpacks/">
												                    Backpacks
												                    </a>
												                </li>
												                
												            
												                <li class="top-nav-group handbags-sale" data-navgroup="100">
												                    <a title="Sale" href="#/handbags/sale/">
												                    Sale
												                    </a>
												                </li>
												            </ul>
												        </li>
												    </div>
												    <div class="row 400">
												    	<li class="top-nav-group mega-unit mega-hdr" data-navgroup="400">
												    		<div class="divider"></div>
												    		<span class="desktop-flyout-menu group-hdr Shops ">Shops</span>		        
													        <ul>													            
												                <li class="top-nav-group handbags-wallets-handbags" data-navgroup="400">
												                    <a title="Matching Wallets &amp; Handbags" href="#/handbags-matching-wallets-handbags/">
												                    Matching Wallets &amp; Handbags
												                    </a>
												                </li>
												                
												            
												                <li class="top-nav-group handbags-neutrals" data-navgroup="400">
												                    <a title="The Neutrals" href="#/handbags/the-neutrals/">
												                    The Neutrals
												                    </a>
												                </li>													            
													        </ul>
													    </li>
													    <li class="top-nav-group mega-unit mega-hdr" data-navgroup="200">		                
										                    <div class="divider"></div>										                
										                    <span class="desktop-flyout-menu group-hdr Collections ">Collections</span>													        
													        <ul>													            
												                <li class="top-nav-group handbags-half-moon" data-navgroup="200">
												                    <a title="Half-Moon" href="#/handbags/half-moon/">
												                    Half-Moon
												                    </a>
												                </li>
												                
												            
												                <li class="top-nav-group handbags-miller" data-navgroup="200">
												                    <a title="Miller" href="#/handbags/miller/">
												                    Miller
												                    </a>
												                </li>
												                
												            
												                <li class="top-nav-group handbags-kira" data-navgroup="200">
												                    <a title="Kira" href="#/handbags/kira/">
												                    Kira
												                    </a>
												                </li>
												                
												                    
												                
												            
												                <li class="top-nav-group handbags-fleming" data-navgroup="200">
												                    <a title="Fleming" href="#/handbags/fleming/">
												                    Fleming
												                    </a>
												                </li>
												                
												            
												                <li class="top-nav-group handbags-mcgraw" data-navgroup="200">
												                    <a title="McGraw" href="#/handbags/mcgraw/">
												                    McGraw
												                    </a>
												                </li>
												                
												            
												                <li class="top-nav-group handbags-the-robinson-collection" data-navgroup="200">
												                    <a title="Robinson" href="#/handbags/robinson/">
												                    Robinson
												                    </a>
												                </li>
												                
												            
												                <li class="top-nav-group handbags-ella" data-navgroup="200">
												                    <a title="Ella" href="#/handbags/ella/">
												                    Ella
												                    </a>
												                </li>													            
													        </ul>
													    </li>
													</div>
													<li class="categorymenu-slot">
														<div class="htmlslotcontainer mobile-available">
															<style type="text/css">
																
															</style>
															<div class="slot-image">
																<p>
																<a data-content-position="Handbags_MegaMenu_slot1_img" data-content-id="NewHandbags" data-content-name="NewHandbags_img" href="#/handbags-newarrivals/" title="Shop Tory Burch New Handbags"><img class="dd-lazy" alt="Shop Handbags" border="0" height="231" src="./Women&#39;s Clothing, Designer Shoes &amp; Flats, Handbags &amp; Accessories _ Tory Burch_files/space.gif" data-original="https://static.toryburch.com/static/on/demandware.static/-/Sites/default/dwc3f86e5c/slots/dropdowns/spring-2018/TBUS_Spring18_3.8_Dropdown_New_02_3x.jpg" title="Shop Tory Burch New Handbags" width="257"></a></p>

																<p>
																<a data-content-position="Handbags_MegaMenu_slot1_txt" data-content-id="NewHandbags" data-content-name="NewHandbags_txt" href="#/handbags-newarrivals/" title="Shop Tory Burch New Handbags" "="">New Arrivals</a></p>
															</div>


															<div class="slot-image last">
																<p>
																<a data-content-position="Handbags_MegaMenu_slot2_img" data-content-id="McGraw" data-content-name="McGraw_img" href="#/handbags/mcgraw/" title="Shop the McGraw Collection"><img class="dd-lazy" alt="Shop the McGraw Collection" border="0" height="231" src="./Women&#39;s Clothing, Designer Shoes &amp; Flats, Handbags &amp; Accessories _ Tory Burch_files/space.gif" data-original="https://static.toryburch.com/static/on/demandware.static/-/Sites/default/dwaa080093/slots/dropdowns/spring-2018/TBUS_Spring18_3.8_Dropdown_Handbags_02_3x.jpg" title="Shop the Tory Burch McGraw Collection" width="257"></a></p>

																<p>
																<a data-content-position="Handbags_MegaMenu_slot2_txt" data-content-id="McGraw" data-content-name="McGraw_txt" href="#/handbags/mcgraw/" title="Shop the Tory Burch McGraw Collection">McGraw</a></p>
															</div>
															<div style="font-family: Verdana, Geneva, Arial, Helvetica, sans-serif; font-size: 8px; color: Black;"></div>
														</div>
														<div class="mobilehtmlslotcontainer">
															<div></div>
														</div>
													</li>
												</div>
											</div>
										</ul>
									</div><!-- END: UL -->
								</li>
								<li class="accessories categoryli dc-mega-li">						
									<a href="#/accessories/">
						  		  		холбоо барих
									</a>
									<div class="sub-container mega" style="width: 1903px; left: -543.5px; top: 20px; z-index: 1000;">
										<ul data-columnlimit="null" class="sub" style="display: none;">
											<div class="row-container">
												<div class="row-wrapper">
													<div class="row 100">
														<li class="top-nav-group mega-unit mega-hdr" data-navgroup="100">
															<div class="css-arrow left"></div>
															<span class="mobile-flyout-menu maincats-span group-hdr mobile-subhead">Accessories</span>
															<span class="desktop-flyout-menu group-hdr Categories ">Categories</span>		        
													        <ul>													            
												                <li class="top-nav-group accessories-newarrivals" data-navgroup="100">
												                    <a title="New Arrivals" href="#/accessories-newarrivals/">
												                    New Arrivals
												                    </a>
												                </li>
												                
												            
												                <li class="top-nav-group accessories-eyewear" data-navgroup="100">
												                    <a title="Sunglasses &amp; Eyewear" href="#/accessories/sunglasses-eyewear/">
												                    Sunglasses &amp; Eyewear
												                    </a>
												                </li>
												                
												            
												                <li class="top-nav-group accessories-mini-bags" data-navgroup="100">
												                    <a title="Mini Bags" href="#/accessories/mini-bags/">
												                    Mini Bags
												                    </a>
												                </li>
												                
												            
												                <li class="top-nav-group accessories-wallets" data-navgroup="100">
												                    <a title="Wallets" href="#/accessories/wallets/">
												                    Wallets
												                    </a>
												                </li>
												                
												            
												                <li class="top-nav-group accessories-wristlets" data-navgroup="100">
												                    <a title="Wristlets" href="#/accessories/wristlets/">
												                    Wristlets
												                    </a>
												                </li>
												                
												            
												                <li class="top-nav-group accessories-lanyards" data-navgroup="100">
												                    <a title="Lanyards" href="#/accessories/lanyards/">
												                    Lanyards
												                    </a>
												                </li>
												                
												            
												                <li class="top-nav-group accessories-small-accessories" data-navgroup="100">
												                    <a title="Cosmetic Cases" href="#/accessories/cosmetic-cases/">
												                    Cosmetic Cases
												                    </a>
												                </li>
												                
												            
												                <li class="top-nav-group accessories-keyfobs" data-navgroup="100">
												                    <a title="Bag Charms &amp; Key Rings" href="#/accessories/bag-charms-key-rings/">
												                    Bag Charms &amp; Key Rings
												                    </a>
												                </li>
												                
												            
												                <li class="top-nav-group accessories-scarves" data-navgroup="100">
												                    <a title="Scarves" href="#/accessories/scarves/">
												                    Scarves
												                    </a>
												                </li>
												                
												            
												                <li class="top-nav-group accessories-hats-gloves" data-navgroup="100">
												                    <a title="Hats &amp; Gloves" href="#/accessories/hats-gloves/">
												                    Hats &amp; Gloves
												                    </a>
												                </li>
												                
												            
												                <li class="top-nav-group accessories-belts" data-navgroup="100">
												                    <a title="Belts" href="#/accessories/belts/">
												                    Belts
												                    </a>
												                </li>
												                
												            
												                <li class="top-nav-group accessories-sale" data-navgroup="100">
												                    <a title="Sale" href="#/accessories/sale/">
												                    Sale
												                    </a>
												                </li>													            
													        </ul>
													    </li>
													</div>
													<div class="row 2000">
														<li class="top-nav-group mega-unit mega-hdr" data-navgroup="2000">
															<div class="divider"></div>
															<span class="desktop-flyout-menu group-hdr Watches ">Watches</span>
															<ul>		            
												                <li class="top-nav-group accessories-watches" data-navgroup="2000">
												                    <a title="View All Watches" href="#/accessories/view-all-watches/">
												                    View All Watches
												                    </a>
												                </li>
												                
												            
												                <li class="top-nav-group accessories-smartwatch" data-navgroup="2000">
												                    <a title="ToryTrack Hybrid Smartwatch" href="#/accessories/torytrack-hybrid-smartwatch/">
												                    ToryTrack Hybrid Smartwatch
												                    </a>
												                </li>
													        </ul>
													    </li>
													    <li class="top-nav-group mega-unit mega-hdr" data-navgroup="1600">
													    	<div class="divider"></div>
													    	<span class="desktop-flyout-menu group-hdr Jewelry ">Jewelry</span>
													    	<ul>													            
												                <li class="top-nav-group accessories-jewelry" data-navgroup="1600">
												                    <a title="View All Jewelry" href="#/accessories/jewelry/">
												                    View All Jewelry
												                    </a>
												                </li>
												                <li class="top-nav-group accessories-jewelry-necklaces">
									                                <a title="Necklaces" href="#/jewelry-necklaces/">
									                                Necklaces
									                                </a>
									                            </li>
									                            <li class="top-nav-group accessories-jewelry-bracelets">
									                                <a title="Bracelets" href="#/jewelry-bracelets/">
									                                Bracelets
									                                </a>
									                            </li>
									                            <li class="top-nav-group accessories-jewelry-earrings">
									                                <a title="Earrings" href="#/accessories/view-all-jewelry/earrings/">
									                                Earrings
									                                </a>
									                            </li>
									                            <li class="top-nav-group accessories-jewelry-rings">
									                                <a title="Rings" href="#/accessories/view-all-jewelry/rings/">
									                                Rings
									                                </a>
									                            </li>
									                        </ul>
									                    </li>
									                    <li class="top-nav-group mega-unit mega-hdr" data-navgroup="1700">		                
										                    <div class="divider"></div>		                
										                    <span class="desktop-flyout-menu group-hdr Tech ">Tech</span>		        
												        	<ul>		            
												                <li class="top-nav-group accessories-fitbit" data-navgroup="1700">
												                    <a title="Tory Burch for Fitbit" href="#/tory-burch-fitbit/">
												                    Tory Burch for Fitbit
												                    </a>
												                </li>
												                
												            
												                <li class="top-nav-group accessories-tech-accessories" data-navgroup="1700">
												                    <a title="Tech Accessories" href="#/accessories/tech-accessories/">
												                    Tech Accessories
												                    </a>
												                </li>
												            </ul>
												        </li>
												    </div>
												    <div class="row 400">
												    	<li class="top-nav-group mega-unit mega-hdr" data-navgroup="400">
												    		<div class="divider"></div>
												    		<span class="desktop-flyout-menu group-hdr Shops ">Shops</span>
												    		<ul>
		            
												                <li class="top-nav-group accessories-wallets-handbags" data-navgroup="400">
												                    <a title="Matching Wallets &amp; Handbags" href="#/accessories-matching-w-h/">
												                    Matching Wallets &amp; Handbags
												                    </a>
												                </li>
												                
												            
												                <li class="top-nav-group accessories-neutrals" data-navgroup="400">
												                    <a title="The Neutrals" href="#/accessories/the-neutrals/">
												                    The Neutrals
												                    </a>
												                </li>
												                
												            
												                <li class="top-nav-group accessories-seedbox-foundation" data-navgroup="400">
												                    <a title="Tory Burch Foundation" href="#/accessories-tbfoundation/">
												                    Tory Burch Foundation
												                    </a>
												                </li>
												            </ul>
												        </li>
												    </div>
												    <li class="categorymenu-slot">
												    	<div class="htmlslotcontainer mobile-available">
												    		<style type="text/css">
																.categorymenu ul.mega-menu .accessories .sub .categorymenu-slot {max-width: 25%;}
																.categorymenu ul.mega-menu .sub .categorymenu-slot img {max-width: 100%;}
															</style> 

															<p><a data-content-position="Accessories_MegaMenu_slot1_img" data-content-id="Foundation" data-content-name="Foundation_img" href="#/accessories-tbfoundation/" title="Shop New Tory Burch Foundation"><img alt="Shop The Tory Burch Foundation" border="0" src="./Women&#39;s Clothing, Designer Shoes &amp; Flats, Handbags &amp; Accessories _ Tory Burch_files/TBUS_Spring18_3.8_Dropdown_Accessories_3x.jpg" title="Shop Tory Burch Foundation"></a></p>

															<p><a data-content-position="Accessories_MegaMenu_slot1_txt" data-content-id="Foundation" data-content-name="Foundation_txt" href="#/accessories-tbfoundation/" title="Shop New Tory Burch Foundation">Tory Burch Foundation</a></p>
														</div>			
														<div class="mobilehtmlslotcontainer">
															<div></div>
														</div>
													</li>
												</div>
											</div>
										</ul>
									</div><!-- END: UL -->
								</li>
								
							</div>
							<!-- dwMarker="content" dwContentID="bcBlEiaag3PBEaaacRGJRviffV" -->
							<style type="text/css">
								@media all and (-ms-high-contrast: none), (-ms-high-contrast: active) {
								/* IE10+ CSS styles go here */
									.gift-guide > a {
										background-color: none !important;
										color: #0C21B6 !important;
									}
								}

								@-moz-document url-prefix() {
									.gift-guide > a {
										background-color: none !important;
										color: #0C21B6 !important;
									}
								}
								/* desktop/tablet */
								@media screen and ( min-width:768px){
								.mobile-sidebar{display: none !important;}
								}
								@media screen and ( max-width:1024px) and (orientation: portrait ){
								.categorymenu ul.menu-category .categoryli.blog {width: 70px;}
								.categorymenu .menu-background {height: 490px;}

								}
								/* mobile */
								@media screen and ( max-width:767px){
								.not-on-mobile-sb {display:none !important;}
								li.gift-guide.dc-mega-li > span {background: none !important;color:#0C21B6!important;}
								}
							</style>
							<li class="blog not-on-mobile-hp categoryli">
							    <a class="level-1" href="#"></a>
							</li>

							<li class="divider not-on-mobile-hp mobile-sidebar">
							 	
							</li>
							<li class="sport not-on-mobile-hp mobile-sidebar">
							    <a class="level-1" href="http://www.torysport.com/" title="Tory Sport" omniture-track="Tory Sport">Tory Sport</a>
							</li>
							<li class="tbf not-on-mobile-hp mobile-sidebar">
							    <a class="level-1" href="http://www.toryburchfoundation.org/" target="_blank" title="Tory Burch Foundation">Tory Burch Foundation</a>
							</li>
		 					<li class="divider mobile-sidebar not-on-mobile-hp"></li>
						  	<li class="my-account mobile-sidebar not-on-mobile-hp lower-content" style="display: list-item;">
							    <a class="level-1" href="#/account" title="My Account" omniture-track="My Account">
							        <div class="mobilenav-icon myaccount-icon"></div>
							        My Account
							    </a>
						  	</li>
						  	<li class="find-store mobile-sidebar not-on-mobile-hp lower-content" style="display: list-item;">
							    <a class="level-1" href="http://www.toryburch.com/stores" title="Find a store" omniture-track="Find A Store">
								<span class="mobilenav-icon findstore-icon"></span><span>Find a Store</span>
							    </a>
						  	</li>
							<li class="mobile-sidebar not-on-mobile-hp lower-content" style="display: list-item;">
						      <a class="level-1" href="https://customercare.toryburch.com/app/answers/detail/a_id/41" title="Gift Services" omniture-track="Gift Services">
						        Gift Services
						      </a>
						  	</li>
					  		<li class="gift-cards mobile-sidebar not-on-mobile-hp lower-content" style="display: list-item;">
						      <a class="level-1" href="#/giftcards" title="Gift Cards" omniture-track="Gift Cards">
						        Gift Cards
						      </a>
						  	</li>
						  	<li class="customer-care mobile-sidebar not-on-mobile-hp lower-content" style="display: list-item;">
						      	<a class="level-1" href="http://customercare.toryburch.com/app" title="Customer Care" omniture-track="Customer Care">
						        	Customer Care
						      	</a>
						  	</li>
						  	<li class="mobile-sidebar not-on-mobile-hp lower-content" style="display: list-item;">
						     	<a class="level-1" href="#/about-us/about-us-main.html" title="About Tory Burch">About Us</a>
							</li>
							<li class="download-app-cta mobile-sidebar not-on-mobile-hp lower-content" style="display: list-item;">
							    <a class="level-1" href="https://itunes.com/apps/torydaily" title="Download our app">Download our app</a>
							</li>
						  	<li class="shipping-to mobile-sidebar not-on-mobile-hp lower-content" style="display: list-item;">
						      	<a class="us" href="#/s/ToryBurch_US/global/international-shipping.html">Ship to</a>
						  	</li>
		  					<li class="spacer mobile-sidebar not-on-mobile-hp lower-content" style="display: list-item;"> </li>

							<script>
								$(document).ready(function(){
								if (app.util.isAppleDevice) {
								$('.download-app-cta a').attr('href','https://itunes.com/apps/torydaily');
								} else if (app.util.isAndroidDevice) {
								$('.download-app-cta a').attr('href','https://play.google.com/store/apps/details?id=com.toryburch.torydaily');
								}
								});
							</script>
						</ul><!-- END: UL -->
						<div class="menu-background"></div>
					</div><!-- END: navigation categorymenu -->

			
					<!--  minibar enabled -->
					<script type="text/javascript">
						
						jQuery(document).ready(function(){
							// hide-top-nav class is used in checkout
							if (!jQuery('body').hasClass('hide-top-nav')){
								app.minibar.init();
							}
						});
						
					</script>
				</div>
				<div id="global-sidebar-wrapper"><div id="global-sidebar" style="display: none;"><div id="global-sidebar-content" class="overthrow text-menu" style="left: -185%;"></div></div>
				</div>
			</div>
		</div>
		<div class="clear"></div>
	</header>
</div><!-- END: header -->