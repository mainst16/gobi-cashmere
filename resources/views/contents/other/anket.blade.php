<!DOCTYPE html>
<html>
<style>
body{
	margin-top: 10px;
	margin-bottom: 10px;
	margin-right: 200px;
	margin-left: 200px;
	font-family:sans-serif;
	background-color: gray;
}
@media only screen and (max-width: 600px) {
    body {
    	margin-top: 0px;
	margin-bottom: 0px;
	margin-right: 0px;
	margin-left: 0px;
	font-family:sans-serif;
	background-color: #fff;
    }
}
table#sanamj td, th
{	
    border:1px solid #fff !important;
    border-collapse:collapse;

}
table, th, td {
	border: 1px solid black;
    border-collapse: collapse;
    text-align: left;
    padding: 1px;
}



div {
	background-color:#fff;
	padding: 25px;
}


	input[type=text],[type=number],[type=email],[type=date] {
    width: 100%;
    padding: 12px 20px;
    margin: 8px 0;
    box-sizing: border-box;
}
#ulaan{
	border: 1px solid red;
}

</style>
<head>
	<title>Анкет</title>
</head>
<body>
	<div class=container>
	
		<img src="{{asset('imgs/logos/gobi.png')}}" style="width: 500px; margin: -100px">
		<h3 style="text-align: center;">ГОВЬ ХК-ИЙН АЖИЛ ГОРИЛОГЧИЙН АНКЕТ</h3>
		<form action="{{url('/anket_send')}}" method="post" enctype="multipart/form-data">
			{!! csrf_field() !!}
			@if (count($errors) > 0)
			<div class="alert alert-danger">
			 Upload Validation Error<br><br>
			 <ul>
			  @foreach ($errors->all() as $error)
			   <li>{{ $error }}</li>
			  @endforeach
			 </ul>
			</div>
			@endif
			<table id="sanamj">
				<tr>
					<td> <strong>Санамж:</strong></td>
					<td>
						<i>1. Анкетыг бөглөхдөө гаргацтай үнэн зөв дэлгэрэнгүй, үг товчлохгүй бичнэ үү.</i>
					</td>
				</tr>
				<tr>
					<td></td>
					<td>
						<i>2. Цээж зураг, их дээд сургуулийн дипломын хуулбарыг заавал хавсаргана уу.</i>
					</td>
				</tr>
				<tr>
					<td></td>
					<td>
						<i>3. Зөвхөн анкетаар шалгарсан тохиолдолд бид таныг ярилцлагад урина.</i>
					</td>
				</tr>
				<tr>
					<td></td>
					<td>
						<i>4. Дээрх хуулбар болон энэхүү анкетыг буцаан олгохгүй бөгөөд анкетыг хүлээж авснаар ажил горилогчийн өмнө манай компани хариуцлага хүлээхгүй болно.</i>
					</td>
				</tr>
			</table>
			<br>
			<table id="sanamj">
				<tr>
					<td>Ажиллахыг хүсч буй албан тушаал</td>
				</tr>
				<tr>
					<td><input type="text" name="wish_albantushaal"></td>
				</tr>
				<tr>
					<td>Та манай байгууллагад үндсэн мэргэжлээсээ гадна ямар ажлын байр дээр ажиллах сонирхолтой байна вэ?</td>
				</tr>
				<tr>
					<td><input type="text" name="wish_sonirhol1"></td>
				</tr>
				<tr>
					<td><input type="text" name="wish_sonirhol2"></td>
				</tr>
				<tr>
					<td>Таны хүсч буй цалингийн доод хэмжээ /төгрөгөөр/:</td>
				</tr>
				<tr>
					<td><input type="number" name="tsalin"></td>
				</tr>
			</table>
			<p>Цээж зураг оруулна:</p>
			<input type="file" name="select_file" />
			<h3>I. ХУВИЙН МЭДЭЭЛЭЛ</h3>
			<table>
				<tr>
					<td><p>Ургийн овог:</p></td>
					<td><input type="text" name="uovog" placeholder="Ургийн овог"></td>
				</tr>
				<tr>
					<td><p>Эцэг/эх-ийн нэр:</p></td>
					<td><input type="text" name="ovog" placeholder="Эцэг/эх-ийн нэр"></td>
				</tr>
				<tr>
					<td><p>Нэр:</p></td>
					<td><input type="text" name="ner" placeholder="Нэр"></td>
				</tr>
				<tr>
					<td><p>Гэрийн утас:</p></td>
					<td><input type="number" name="homephone" placeholder="Гэрийн утас"></td>
				</tr>
				<tr>
					<td><p>Үндэс угсаа:</p></td>
					<td><input type="text" name="undes" placeholder="Үндэс угсаа"></td>
				</tr>
				<tr>
					<td><p>Цусны бүлэг:</p></td>
					<td>
						<input type="radio" name="blood" value="1">I бүлгийн цус (0)<br>
						<input type="radio" name="blood" value="2">II бүлгийн цус (A)<br>
						<input type="radio" name="blood" value="3">III бүлгийн цус (B)<br>
						<input type="radio" name="blood" value="4">IV бүлгийн цус (AB)
					</td>
				</tr>
				<tr>
					<td><p>Биеийн жин:</p></td>
					<td><input type="number" name="kg"></td>
				</tr>
				<tr>
					<td><p>Биеийн өндөр:</p></td>
					<td><input type="number" name="cm"></td>
				</tr>
				<tr>
					<td>РД:</td>
					<td><input type="text" name="regno"></td>
				</tr>
				<tr>
					<td>ЭМД №:</td>
					<td><input type="text" name="emdno"></td>
				</tr>
				<tr>
					<td>НДД №:</td>
					<td><input type="text" name="nddno"></td>
				</tr>
				<tr>
					<td>Гадаад паспорт:</td>
					<td><input type="text" name="fpno"></td>
				</tr>
				<tr>
					<td>Жолооны үнэмлэх №:</td>
					<td>
						<input type="text" name="drivno"><br>
						<input type="text" name="angilal" placeholder="Ангилал"><br>
						<input type="text" name="barisanjil" placeholder="Жолоо барьсан жил">
					</td>
				</tr>
				<tr>
					<td>Гар утас:</td>
					<td><input type="number" name="mobile"></td>
				</tr>
				<tr>
					<td>Шаардлагатай тохиолдолд холбоо барих хүний утас</td>
					<td><input type="text" name="musthavephoneno"></td>
				</tr>
				<tr>
					<td>Хувийн и-мэйл хаяг:</td>
					<td><input type="email" name="email"></td>
				</tr>
				<tr>
					<td>Ажлын и-мэйл хаяг:</td>
					<td><input type="email" name="workemail"></td>
				</tr>
				<tr>
					<td><p>Боловсролын мэдээлэл:</p></td>
					<td>
						<input type="radio" value="бага" name="bolowsrol"> бага<br>
						<input type="radio" value="бүрэн дунд" name="bolowsrol"> бүрэн дунд<br>
						<input type="radio" value="тусгай дунд" name="bolowsrol"> тусгай дунд<br>
						<input type="radio" value="дээд" name="bolowsrol"> дээд
					</td>
				</tr>
				<tr>
					<td>Мэргэжил:</td>
					<td><input type="text" name="job"></td>
				</tr>
				<tr>
					<td>Мэргэжлийн зэрэг:</td>
					<td><input type="text" name="jobrank"></td>
				</tr>
				<tr>
					<td><p>Гэр бүлийн байдал: </p></td>
					<td>
						<input type="radio" value="гэрлэсэн" name="marredornot"> гэрлэсэн<br>
						<input type="radio" value="ганц бие" name="marredornot"> ганц бие<br>
						<input type="radio" value="өрх толгойлсон" name="marredornot"> өрх толгойлсон /эцэг, эх/
					</td>
				</tr>
				<tr>
					<td>Төрсөн улс:</td>
					<td><input type="text" name="borncnty"></td>
				</tr>
				<tr>
					<td>Төрсөн хот:</td>
					<td><input type="text" name="borncity"></td>
				</tr>
				<tr>
					<td>Төрсөн аймаг:</td>
					<td><input type="text" name="bornaimag"></td>
				</tr>
				<tr>
					<td>Төрсөн сум/дүүрэг:</td>
					<td><input type="text" name="bornduureg"></td>
				</tr>
				<tr>
					<td>Төрсөн он сар өдөр</td>
					<td><input type="date" name="birth"></td>
				</tr>
				<tr>
					<td>Хүйс: </td>
					<td>
						<input type="radio" name="sex" value="эмэгтэй">эмэгтэй<br>
						<input type="radio" name="sex" value="эрэгтэй">эрэгтэй
					</td>
				</tr>
				<tr>
					<td>Цэргийн алба хаасан эсэх:</td>
					<td>
						<input type="radio" name="tsereg" value="тийм">тийм<br>
						<input type="radio" name="tsereg" value="үгүй">үгүй
					</td>
				</tr>
				<tr>
					<td>Хэрэв тийм бол цэргийн батлах №:</td>
					<td><input type="text" name="alba_batlah_no"></td>
				</tr>
				<tr>
					<td>Нийгмийн гарал:</td>
					<td>
						<input type="radio" name="niigmiingaral" value="Малчин">Малчин<br>
						<input type="radio" name="niigmiingaral" value="Ажилчин">Ажилчин<br>
						<input type="radio" name="niigmiingaral" value="Сэхээтэн">Сэхээтэн
					</td>
				</tr>
				<tr>
					<td>Жил:</td>
					<td><input type="text" name="jil"></td>
				</tr>
				<tr>
					<td>Орд:</td>
					<td><input type="text" name="ord"></td>
				</tr>
				<tr>
					<td>Facebook хаяг:</td>
					<td><input type="text" name="fbaddress"></td>
				</tr>
				<tr>
					<td>Twitter хаяг:</td>
					<td><input type="text" name="twitaddress"></td>
				</tr>
			</table>
			<br>
			<h3>II. АЖЛЫН ТУРШЛАГА</h3>
			<p><i>Та албан тушаал, ажлын байрны нэрийг бүтнээр бичнэ үү</i></p>
			<div style="overflow-x:auto;">
			<table>
				<tr>
					<td>№</td>
					<td>Одоогийн болон урьд ажиллаж байсан байгууллага, компанийн нэр</td>
					<td>Албан тушаал, ажлын байрны нэр</td>
					<td>Ажилд орсон он, сар, өдөр</td>
					<td>Ажлаас гарсан он, сар, өдөр</td>
					<td>Ажлаас гарсаншалтгаан</td>
				</tr>
				<tr>
					<td rowspan="2">1</td>
					<td><input type="text" name="old_work1"></td>
					<td><input type="text" name="old_albantushaal1"></td>
					<td><input type="date" name="old_work_enter1"></td>
					<td><input type="date" name="old_work_out1"></td>
					<td><textarea rows="4" cols="40" name="old_work_out_shaltgaan1"></textarea></td>
				</tr>
				<tr>
					<td>Хийж гүйцэтгэсэн ажил үүргүүд /товч бичих/:</td>
					<td colspan="4"><textarea rows="3" cols="50" name="old_done_work1"></textarea></td>
				</tr>
				<tr>
					<td rowspan="2">2</td>
					<td><input type="text" name="old_work2"></td>
					<td><input type="text" name="old_albantushaal2"></td>
					<td><input type="date" name="old_work_enter2"></td>
					<td><input type="date" name="old_work_out2"></td>
					<td><textarea rows="3" cols="40" name="old_work_out_shaltgaan2"></textarea></td>
				</tr>
				<tr>
					<td>Хийж гүйцэтгэсэн ажил үүргүүд /товч бичих/:</td>
					<td colspan="4"><textarea rows="3" cols="50" name="old_done_work2"></textarea></td>
				</tr>
				<tr>
					<td rowspan="2">3</td>
					<td><input type="text" name="old_work3"></td>
					<td><input type="text" name="old_albantushaal3"></td>
					<td><input type="date" name="old_work_enter3"></td>
					<td><input type="date" name="old_work_out3"></td>
					<td><textarea rows="3" cols="40" name="old_work_out_shaltgaan3"></textarea></td>
				</tr>
				<tr>
					<td>Хийж гүйцэтгэсэн ажил үүргүүд /товч бичих/:</td>
					<td colspan="4"><textarea rows="3" cols="50" name="old_done_work3"></textarea></td>
				</tr>
				<tr>
					<td rowspan="2">4</td>
					<td><input type="text" name="old_work4"></td>
					<td><input type="text" name="old_albantushaal4"></td>
					<td><input type="date" name="old_work_enter4"></td>
					<td><input type="date" name="old_work_out4"></td>
					<td><textarea rows="3" cols="40" name="old_work_out_shaltgaan4"></textarea></td>
				</tr>
				<tr>
					<td>Хийж гүйцэтгэсэн ажил үүргүүд /товч бичих/:</td>
					<td colspan="4"><textarea rows="3" cols="50" name="old_done_work4"></textarea></td>
				</tr>
				<tr>
					<td rowspan="2">5</td>
					<td><input type="text" name="old_work5"></td>
					<td><input type="text" name="old_albantushaal5"></td>
					<td><input type="date" name="old_work_enter5"></td>
					<td><input type="date" name="old_work_out5"></td>
					<td><textarea rows="3" cols="40" name="old_work_out_shaltgaan5"></textarea></td>
				</tr>
				<tr>
					<td>Хийж гүйцэтгэсэн ажил үүргүүд /товч бичих/:</td>
					<td colspan="4"><textarea rows="3" cols="50" name="old_done_work5"></textarea></td>
				</tr>
			</table>
			</div>
			<p>Үүнээс: <input type="date" name=""> өдрийн байдлаар:</p>
			<table>
				<tr>
					<td>Улсад ажилласан нийт:</td>
					<td>Говь ХК-д ажилласан нийт:</td>
					<td>Нийгмийн даатгал завсардсан:</td>
				</tr>
				<tr>
					<td>
						<input type="number" name="uls_jil" placeholder="жил"><input type="number" name="uls_sar" placeholder="сар">
					</td>
					<td><input type="number" name="gobi_jil" placeholder="жил"><input type="number" name="gobi_sar" placeholder="сар"></td>
					<td><input type="number" name="nd_zawsardsan_jil" placeholder="жил"><input type="number" name="nd_zawsardsan_sar" placeholder="сар"></td>
				</tr>
			</table>
			<p>Хэрэв та удаан хугацаагаар ажил хийгээгүй бол шалтгаанаа бичнэ үү.</p>
			<textarea rows="5" cols="100"></textarea>

			<br>
			<h3>III. БОЛОВСРОЛ</h3>
			<p>3.1. Дунд, тусгай дунд, дээд боловсролын мэдээлэл</p>
			<div style="overflow-x:auto;">
			<table>
				<tr>
					<td>№ </td>
					<td></td>
					<td>Сургуулийн нэр</td>
					<td>Улс</td>
					<td>Хот</td>
					<td>Элссэн он, сар, өдөр</td>
					<td>Төгссөн он, сар, өдөр</td>
					<td>Эзэмшсэн мэргэжил</td>
					<td>Голч дүн</td>
					<td>Эрдмийн зэрэг</td>
				</tr>
				<tr>
					<td>1</td>
					<td>Дунд сургууль</td>
					<td><input type="text" name="dssurname"></td>
					<td><input type="text" name="dsuls"></td>
					<td><input type="text" name="dshot"></td>
					<td><input type="date" name="dsbegindate"></td>
					<td><input type="date" name="dsenddate"></td>
					<td><input type="text" name="dsmergejil"></td>
					<td><input type="text" name="dsgolch"></td>
					<td><input type="text" name="dszereg"></td>
				</tr>
				<tr>
					<td>2</td>
					<td>Тусгай дунд, коллеж</td>
					<td><input type="text" name="ksurname"></td>
					<td><input type="text" name="kuls"></td>
					<td><input type="text" name="khot"></td>
					<td><input type="date" name="kbegindate"></td>
					<td><input type="date" name="kenddate"></td>
					<td><input type="text" name="kmergejil"></td>
					<td><input type="text" name="kgolch"></td>
					<td><input type="text" name="kzereg"></td>
				</tr>
				<tr>
					<td>3</td>
					<td>Их, дээд</td>
					<td><input type="text" name="id1surname"></td>
					<td><input type="text" name="id1uls"></td>
					<td><input type="text" name="id1hot"></td>
					<td><input type="date" name="id1begindate"></td>
					<td><input type="date" name="id1enddate"></td>
					<td><input type="text" name="id1mergejil"></td>
					<td><input type="text" name="id1golch"></td>
					<td><input type="text" name="id1zereg"></td>
				</tr>
				<tr>
					<td>4</td>
					<td>Их, дээд</td>
					<td><input type="text" name="id2surname"></td>
					<td><input type="text" name="id2uls"></td>
					<td><input type="text" name="id2hot"></td>
					<td><input type="date" name="id2begindate"></td>
					<td><input type="date" name="id2enddate"></td>
					<td><input type="text" name="id2mergejil"></td>
					<td><input type="text" name="id2golch"></td>
					<td><input type="text" name="id2zereg"></td>
				</tr>
				<tr>
					<td>5</td>
					<td>Бусад</td>
					<td><input type="text" name="busadsurname"></td>
					<td><input type="text" name="busaduls"></td>
					<td><input type="text" name="busadhot"></td>
					<td><input type="date" name="busadbegindate"></td>
					<td><input type="date" name="busadenddate"></td>
					<td><input type="text" name="busadmergejil"></td>
					<td><input type="text" name="busadgolch"></td>
					<td><input type="text" name="busadzereg"></td>
				</tr>
			</table>
			</div>
			
			
			<p>Дипломын ажлын сэдэв :</p>
			<textarea rows="5" cols="100" name="diplom"></textarea>
			<p>Дипломын хавсралтыг оруулна уу:</p>
			<input type="file" name="select_file1" /><br><br>
			<p>3.2. Мэргэжлээрээ болон бусад чиглэлээр хамрагдаж байсан сургалтууд</p>
			<div style="overflow-x:auto;">
			<table>
				<tr>
					<td>№</td>
					<td>Сургалтын байгууллагын нэр</td>
					<td>Улс, хот</td>
					<td>Элссэн он, сар, өдөр</td>
					<td>Төгссөн он, сар, өдөр</td>
					<td>Ямар чиглэлээр</td>
					<td>Үнэмлэх, сертификатын дугаар</td>
				</tr>
				<tr>
					<td>1</td>
					<td><input type="text" name="surgaltner1"></td>
					<td><input type="text" name="surgaltuls1"></td>
					<td><input type="date" name="surgaltbegindate1"></td>
					<td><input type="date" name="surgaltenddate1"></td>
					<td><input type="text" name="surgaltchiglel1"></td>
					<td><input type="text" name="surgaltunemleh1"></td>
				</tr>
				<tr>
					<td>2</td>
					<td><input type="text" name="surgaltner2"></td>
					<td><input type="text" name="surgaltuls2"></td>
					<td><input type="date" name="surgaltbegindate2"></td>
					<td><input type="date" name="surgaltenddate2"></td>
					<td><input type="text" name="surgaltchiglel2"></td>
					<td><input type="text" name="surgaltunemleh2"></td>
				</tr>
				<tr>
					<td>3</td>
					<td><input type="text" name="surgaltner3"></td>
					<td><input type="text" name="surgaltuls3"></td>
					<td><input type="date" name="surgaltbegindate3"></td>
					<td><input type="date" name="surgaltenddate3"></td>
					<td><input type="text" name="surgaltchiglel3"></td>
					<td><input type="text" name="surgaltunemleh3"></td>
				</tr>
			</table>
			</div>
			<br>
			<h3>IV. ГЭР БҮЛИЙН МЭДЭЭЛЭЛ</h3>
			<p>4.1. Ам бүлийн тоо:<input type="number" name="family"></p>
			<p>4.2. Ам бүлийн гишүүдийн мэдээлэл <i>/Зөвхөн хамт амьдардаг хүмүүсийг бичнэ үү /</i></p>
			<div style="overflow-x:auto;">
			<table>
				<tr>
					<td>№</td>
					<td>Таны юу болох</td>
					<td>Овог</td>
					<td>Нэр</td>
					<td>Төрсөн он, сар, өдөр</td>
					<td>Төрсөн газар</td>
					<td>Хаана ямар байгууллагад ажилладаг вэ?</td>
					<td>Албан тушаал </td>
					<td>Холбоо барих утасны дугаар</td>
				</tr>
				<tr>
					<td>1</td>
					<td><input type="text" name="famhamaatan"></td>
					<td><input type="text" name="famovog"></td>
					<td><input type="text" name="famner"></td>
					<td><input type="date" name="fambirthdate"></td>
					<td><input type="text" name="fambirthplace"></td>
					<td><input type="text" name="famwork"></td>
					<td><input type="text" name="famjob"></td>
					<td><input type="number" name="famphone"></td>
				</tr>
				<tr>
					<td>2</td>
					<td><input type="text" name="2famhamaatan"></td>
					<td><input type="text" name="2famovog"></td>
					<td><input type="text" name="2famner"></td>
					<td><input type="date" name="2fambirthdate"></td>
					<td><input type="text" name="2fambirthplace"></td>
					<td><input type="text" name="2famwork"></td>
					<td><input type="text" name="2famjob"></td>
					<td><input type="number" name="2famphone"></td>
				</tr>
				<tr>
					<td>3</td>
					<td><input type="text" name="3famhamaatan"></td>
					<td><input type="text" name="3famovog"></td>
					<td><input type="text" name="3famner"></td>
					<td><input type="date" name="3fambirthdate"></td>
					<td><input type="text" name="3fambirthplace"></td>
					<td><input type="text" name="3famwork"></td>
					<td><input type="text" name="3famjob"></td>
					<td><input type="number" name="3famphone"></td>
				</tr>
				<tr>
					<td>4</td>
					<td><input type="text" name="4famhamaatan"></td>
					<td><input type="text" name="4famovog"></td>
					<td><input type="text" name="4famner"></td>
					<td><input type="date" name="4fambirthdate"></td>
					<td><input type="text" name="4fambirthplace"></td>
					<td><input type="text" name="4famwork"></td>
					<td><input type="text" name="4famjob"></td>
					<td><input type="number" name="4famphone"></td>
				</tr>
				<tr>
					<td>5</td>
					<td><input type="text" name="5famhamaatan"></td>
					<td><input type="text" name="5famovog"></td>
					<td><input type="text" name="5famner"></td>
					<td><input type="date" name="5fambirthdate"></td>
					<td><input type="text" name="5fambirthplace"></td>
					<td><input type="text" name="5famwork"></td>
					<td><input type="text" name="5famjob"></td>
					<td><input type="number" name="5famphone"></td>
				</tr>
				<tr>
					<td>6</td>
					<td><input type="text" name="6famhamaatan"></td>
					<td><input type="text" name="6famovog"></td>
					<td><input type="text" name="6famner"></td>
					<td><input type="date" name="6fambirthdate"></td>
					<td><input type="text" name="6fambirthplace"></td>
					<td><input type="text" name="6famwork"></td>
					<td><input type="text" name="6famjob"></td>
					<td><input type="number" name="6famphone"></td>
				</tr>
			</table>
			</div>
			<p>4.3. Ураг төрлийн мэдээлэл <i>/Тусдаа амьдардаг төрсөн эцэг, эх, ах дүүсийг бичнэ үү /</i></p>
			<div style="overflow-x:auto;">
				<table>
					<tr>
						<td>№</td>
						<td>Таны юу болох</td>
						<td>Овог</td>
						<td>Нэр</td>
						<td>Төрсөн он, сар, өдөр</td>
						<td>Төрсөн газар</td>
						<td>Хаана ямар байгууллагад ажилладаг вэ?</td>
						<td>Албан тушаал </td>
						<td>Холбоо барих утасны дугаар</td>
					</tr>
					<tr>
						<td>1</td>
						<td><input type="text" name="famholhamaatan"></td>
						<td><input type="text" name="famholovog"></td>
						<td><input type="text" name="famholner"></td>
						<td><input type="date" name="famholbirthdate"></td>
						<td><input type="text" name="famholbirthplace"></td>
						<td><input type="text" name="famholwork"></td>
						<td><input type="text" name="famholjob"></td>
						<td><input type="number" name="famholphone"></td>
					</tr>
					<tr>
						<td>2</td>
						<td><input type="text" name="famholhamaatan2"></td>
						<td><input type="text" name="famholovog2"></td>
						<td><input type="text" name="famholner2"></td>
						<td><input type="date" name="famholbirthdate2"></td>
						<td><input type="text" name="famholbirthplace2"></td>
						<td><input type="text" name="famholwork2"></td>
						<td><input type="text" name="famholjob2"></td>
						<td><input type="number" name="famholphone2"></td>
					</tr>
					<tr>
						<td>3</td>
						<td><input type="text" name="famholhamaatan3"></td>
						<td><input type="text" name="famholovog3"></td>
						<td><input type="text" name="famholner3"></td>
						<td><input type="date" name="famholbirthdate3"></td>
						<td><input type="text" name="famholbirthplace3"></td>
						<td><input type="text" name="famholwork3"></td>
						<td><input type="text" name="famholjob3"></td>
						<td><input type="number" name="famholphone3"></td>
					</tr>
					<tr>
						<td>4</td>
						<td><input type="text" name="famholhamaatan4"></td>
						<td><input type="text" name="famholovog4"></td>
						<td><input type="text" name="famholner4"></td>
						<td><input type="date" name="famholbirthdate4"></td>
						<td><input type="text" name="famholbirthplace4"></td>
						<td><input type="text" name="famholwork4"></td>
						<td><input type="text" name="famholjob4"></td>
						<td><input type="number" name="famholphone4"></td>
					</tr>
				</table>
			</div>
			<p>4.4. Оршин суугаа хаяг <i>/Тодорхой бичнэ үү/</i></p>
			<table>
				<tr>
					<td>4.3.1. Иргэний үнэмлэх дээр бүртгэлтэй хаяг:</td>
					<td>4.3.2. Одоо оршин суугаа хаяг:</td>
				</tr>
				<tr>
					<td><textarea name="burtgeltei_hayag"></textarea></td>
					<td><textarea name="odoo_bgaa_hayag"></textarea></td>
				</tr>
			</table>
			<p>4.5. Орон сууцны нөхцөл <i>/Сонгоно уу/</i></p>
			<input type="radio" name="osnuhtsul" value="Орон сууц">Орон сууц
			<input type="radio" name="osnuhtsul" value="Хашаа байшин">Хашаа байшин
			<input type="radio" name="osnuhtsul" value="Гэр">Гэр
			<p> 4.6. Өөрийн байр эсэх<i>/Сонгоно уу/</i></p>
			<input type="radio" name="mineornot" value="Хувийн">Хувийн
			<input type="radio" name="mineornot" value="Түрээсийн">Түрээсийн
			<input type="radio" name="mineornot" value="Эцэг, эх, ах, эгчийн байр">Эцэг, эх, ах, эгчийн байр
			<p>5.1. Та байгууллага, яам, салбар, нийслэлийн болон төрийн шагнал авсан бол бичнэ үү.</p>
			<div style="overflow-x:auto;">
				<table>
					<tr>
						<td>№</td>
						<td>Шагнал олгосон газар</td>
						<td>Шагналын нэр</td>
						<td>Шагнагдсан он, сар, өдөр</td>
						<td>Хаана ажиллаж байх хугацаанд шагнагдсан бэ?</td>
						<td>Шагнагдсан үндэслэл</td>
					</tr>
					<tr>
						<td>1</td>
						<td><input type="text" name="shagnasan_gazar"></td>
						<td><input type="text" name="shagnal_ner"></td>
						<td><input type="date" name="shagnal_date"></td>
						<td><input type="text" name="shagnal_haana"></td>
						<td><input type="text" name="shagnal_undeslel"></td>
					</tr>
					<tr>
						<td>2</td>
						<td><input type="text" name="shagnasan_gazar2"></td>
						<td><input type="text" name="shagnal_ner2"></td>
						<td><input type="date" name="shagnal_date2"></td>
						<td><input type="text" name="shagnal_haana2"></td>
						<td><input type="text" name="shagnal_undeslel2"></td>
					</tr>
					<tr>
						<td>3</td>
						<td><input type="text" name="shagnasan_gazar3"></td>
						<td><input type="text" name="shagnal_ner3"></td>
						<td><input type="date" name="shagnal_date3"></td>
						<td><input type="text" name="shagnal_haana3"></td>
						<td><input type="text" name="shagnal_undeslel3"></td>
					</tr>
					<tr>
						<td>4</td>
						<td><input type="text" name="shagnasan_gazar4"></td>
						<td><input type="text" name="shagnal_ner4"></td>
						<td><input type="date" name="shagnal_date4"></td>
						<td><input type="text" name="shagnal_haana4"></td>
						<td><input type="text" name="shagnal_undeslel4"></td>
					</tr>
					<tr>
						<td>5</td>
						<td><input type="text" name="shagnasan_gazar5"></td>
						<td><input type="text" name="shagnal_ner5"></td>
						<td><input type="date" name="shagnal_date5"></td>
						<td><input type="text" name="shagnal_haana5"></td>
						<td><input type="text" name="shagnal_undeslel5"></td>
					</tr>
				</table>
			</div>
			<h3>VI. УР ЧАДВАРЫН МЭДЭЭЛЭЛ</h3>
			<p>6.1. Гадаад хэлний мэдлэг</p>
			<table>
				<tr>
					<td rowspan="2"></td>
					<td rowspan="2">Гадаад хэлний нэр</td>
					<td colspan="4" style="text-align: center;">Түвшин</td>
				</tr>
				<tr>
					<td>Маш сайн</td>
					<td>Cайн</td>
					<td>Дунд</td>
					<td>Муу</td>
				</tr>
				<tr>
					<td rowspan="5">Ярьсныг ойлгох</td>
					<td>Англи</td>
					<td><input type="radio" value="Маш сайн" name="eng_speak_undrstnd"></td>
					<td><input type="radio" value="Сайн" name="eng_speak_undrstnd"></td>
					<td><input type="radio" value="Дунд" name="eng_speak_undrstnd"></td>
					<td><input type="radio" value="Муу" name="eng_speak_undrstnd"></td>
				</tr>
				<tr>
					<td>Орос</td>
					<td><input type="radio" value="Маш сайн" name="rus_speak_undrstnd"></td>
					<td><input type="radio" value="Сайн" name="rus_speak_undrstnd"></td>
					<td><input type="radio" value="Дунд" name="rus_speak_undrstnd"></td>
					<td><input type="radio" value="Муу" name="rus_speak_undrstnd"></td>
				</tr>
				<tr>
					<td>Япон</td>
					<td><input type="radio" value="Маш сайн" name="jap_speak_undrstnd"></td>
					<td><input type="radio" value="Сайн" name="jap_speak_undrstnd"></td>
					<td><input type="radio" value="Дунд" name="jap_speak_undrstnd"></td>
					<td><input type="radio" value="Муу" name="jap_speak_undrstnd"></td>
				</tr>
				<tr>
					<td>Хятад</td>
					<td><input type="radio" value="Маш сайн" name="chi_speak_undrstnd"></td>
					<td><input type="radio" value="Сайн" name="chi_speak_undrstnd"></td>
					<td><input type="radio" value="Дунд" name="chi_speak_undrstnd"></td>
					<td><input type="radio" value="Муу" name="chi_speak_undrstnd"></td>
				</tr>
				<tr>
					<td><input type="text" name="flang"></td>
					<td><input type="radio" value="Маш сайн" name="flang_speak_undrstnd"></td>
					<td><input type="radio" value="Сайн" name="flang_speak_undrstnd"></td>
					<td><input type="radio" value="Дунд" name="flang_speak_undrstnd"></td>
					<td><input type="radio" value="Муу" name="flang_speak_undrstnd"></td>
				</tr>
				<tr>
					<td rowspan="5">Ярих</td>
					<td>Англи</td>
					<td><input type="radio" value="Маш сайн" name="eng_speak"></td>
					<td><input type="radio" value="Сайн" name="eng_speak"></td>
					<td><input type="radio" value="Дунд" name="eng_speak"></td>
					<td><input type="radio" value="Муу" name="eng_speak"></td>
				</tr>
				<tr>
					<td>Орос</td>
					<td><input type="radio" value="Маш сайн" name="rus_speak"></td>
					<td><input type="radio" value="Сайн" name="rus_speak"></td>
					<td><input type="radio" value="Дунд" name="rus_speak"></td>
					<td><input type="radio" value="Муу" name="rus_speak"></td>
				</tr>
				<tr>
					<td>Япон</td>
					<td><input type="radio" value="Маш сайн" name="jap_speak"></td>
					<td><input type="radio" value="Сайн" name="jap_speak"></td>
					<td><input type="radio" value="Дунд" name="jap_speak"></td>
					<td><input type="radio" value="Муу" name="jap_speak"></td>
				</tr>
				<tr>
					<td>Хятад</td>
					<td><input type="radio" value="Маш сайн" name="chi_speak"></td>
					<td><input type="radio" value="Сайн" name="chi_speak"></td>
					<td><input type="radio" value="Дунд" name="chi_speak"></td>
					<td><input type="radio" value="Муу" name="chi_speak"></td>
				</tr>
				<tr>
					<td><input type="text" name="flang"></td>
					<td><input type="radio" value="Маш сайн" name="flang_speak"></td>
					<td><input type="radio" value="Сайн" name="flang_speak"></td>
					<td><input type="radio" value="Дунд" name="flang_speak"></td>
					<td><input type="radio" value="Муу" name="flang_speak"></td>
				</tr>
				<tr>
					<td rowspan="5">Уншиж ойлгох</td>
					<td>Англи</td>
					<td><input type="radio" value="Маш сайн" name="eng_read"></td>
					<td><input type="radio" value="Сайн" name="eng_read"></td>
					<td><input type="radio" value="Дунд" name="eng_read"></td>
					<td><input type="radio" value="Муу" name="eng_read"></td>
				</tr>
				<tr>
					<td>Орос</td>
					<td><input type="radio" value="Маш сайн" name="rus_read"></td>
					<td><input type="radio" value="Сайн" name="rus_read"></td>
					<td><input type="radio" value="Дунд" name="rus_read"></td>
					<td><input type="radio" value="Муу" name="rus_read"></td>
				</tr>
				<tr>
					<td>Япон</td>
					<td><input type="radio" value="Маш сайн" name="jap_read"></td>
					<td><input type="radio" value="Сайн" name="jap_read"></td>
					<td><input type="radio" value="Дунд" name="jap_read"></td>
					<td><input type="radio" value="Муу" name="jap_read"></td>
				</tr>
				<tr>
					<td>Хятад</td>
					<td><input type="radio" value="Маш сайн" name="chi_read"></td>
					<td><input type="radio" value="Сайн" name="chi_read"></td>
					<td><input type="radio" value="Дунд" name="chi_read"></td>
					<td><input type="radio" value="Муу" name="chi_read"></td>
				</tr>
				<tr>
					<td><input type="text" name="flang"></td>
					<td><input type="radio" value="Маш сайн" name="flang_read"></td>
					<td><input type="radio" value="Сайн" name="flang_read"></td>
					<td><input type="radio" value="Дунд" name="flang_read"></td>
					<td><input type="radio" value="Муу" name="flang_read"></td>
				</tr>
				<tr>
					<td rowspan="5">Бичиж орчуулах</td>
					<td>Англи</td>
					<td><input type="radio" value="Маш сайн" name="eng_write"></td>
					<td><input type="radio" value="Сайн" name="eng_write"></td>
					<td><input type="radio" value="Дунд" name="eng_write"></td>
					<td><input type="radio" value="Муу" name="eng_write"></td>
				</tr>
				<tr>
					<td>Орос</td>
					<td><input type="radio" value="Маш сайн" name="rus_write"></td>
					<td><input type="radio" value="Сайн" name="rus_write"></td>
					<td><input type="radio" value="Дунд" name="rus_write"></td>
					<td><input type="radio" value="Муу" name="rus_write"></td>
				</tr>
				<tr>
					<td>Япон</td>
					<td><input type="radio" value="Маш сайн" name="jap_write"></td>
					<td><input type="radio" value="Сайн" name="jap_write"></td>
					<td><input type="radio" value="Дунд" name="jap_write"></td>
					<td><input type="radio" value="Муу" name="jap_write"></td>
				</tr>
				<tr>
					<td>Хятад</td>
					<td><input type="radio" value="Маш сайн" name="chi_write"></td>
					<td><input type="radio" value="Сайн" name="chi_write"></td>
					<td><input type="radio" value="Дунд" name="chi_write"></td>
					<td><input type="radio" value="Муу" name="chi_write"></td>
				</tr>
				<tr>
					<td><input type="text" name="flang"></td>
					<td><input type="radio" value="Маш сайн" name="flang_write"></td>
					<td><input type="radio" value="Сайн" name="flang_write"></td>
					<td><input type="radio" value="Дунд" name="flang_write"></td>
					<td><input type="radio" value="Муу" name="flang_write"></td>
				</tr>
			</table>

			<p>6.2. Компьютерийн мэдлэг</p>
			<table>
				<tr>
					<td rowspan="2">Програмын нэр</td>
					<td colspan="3">Програм эзэмшсэн байдал</td>
				</tr>
				<tr>
					<td>Анхан шатны </td>
					<td>Хэрэглээний</td>
					<td>Бүрэн</td>
				</tr>
				<tr>
					<td>MS Word</td>
					<td><input type="radio" name="Word" value="Анхан шатны"></td>
					<td><input type="radio" name="Word" value="Хэрэглээний"></td>
					<td><input type="radio" name="Word" value="Бүрэн"></td>
				</tr>
				<tr>
					<td>MS Excel</td>
					<td><input type="radio" name="Excel" value="Анхан шатны"></td>
					<td><input type="radio" name="Excel" value="Хэрэглээний"></td>
					<td><input type="radio" name="Excel" value="Бүрэн"></td>
				</tr>
				<tr>
					<td>MS Access</td>
					<td><input type="radio" name="Access" value="Анхан шатны"></td>
					<td><input type="radio" name="Access" value="Хэрэглээний"></td>
					<td><input type="radio" name="Access" value="Бүрэн"></td>
				</tr>
				<tr>
					<td>MS Project</td>
					<td><input type="radio" name="Project" value="Анхан шатны"></td>
					<td><input type="radio" name="Project" value="Хэрэглээний"></td>
					<td><input type="radio" name="Project" value="Бүрэн"></td>
				</tr>
				<tr>
					<td>MS Power Point</td>
					<td><input type="radio" name="pp" value="Анхан шатны"></td>
					<td><input type="radio" name="pp" value="Хэрэглээний"></td>
					<td><input type="radio" name="pp" value="Бүрэн"></td>
				</tr>
				<tr>
					<td>MS Publisher</td>
					<td><input type="radio" name="Publisher" value="Анхан шатны"></td>
					<td><input type="radio" name="Publisher" value="Хэрэглээний"></td>
					<td><input type="radio" name="Publisher" value="Бүрэн"></td>
				</tr>
				<tr>
					<td>3D Max</td>
					<td><input type="radio" name="dmax" value="Анхан шатны"></td>
					<td><input type="radio" name="dmax" value="Хэрэглээний"></td>
					<td><input type="radio" name="dmax" value="Бүрэн"></td>
				</tr>
				<tr>
					<td>Photoshop</td>
					<td><input type="radio" name="ps" value="Анхан шатны"></td>
					<td><input type="radio" name="ps" value="Хэрэглээний"></td>
					<td><input type="radio" name="ps" value="Бүрэн"></td>
				</tr>
				<tr>
					<td>InDesign</td>
					<td><input type="radio" name="InDesign" value="Анхан шатны"></td>
					<td><input type="radio" name="InDesign" value="Хэрэглээний"></td>
					<td><input type="radio" name="InDesign" value="Бүрэн"></td>
				</tr>
				<tr>
					<td>Corel Draw</td>
					<td><input type="radio" name="Corel" value="Анхан шатны"></td>
					<td><input type="radio" name="Corel" value="Хэрэглээний"></td>
					<td><input type="radio" name="Corel" value="Бүрэн"></td>
				</tr>
				<tr>
					<td>Green ERP</td>
					<td><input type="radio" name="ERP" value="Анхан шатны"></td>
					<td><input type="radio" name="ERP" value="Хэрэглээний"></td>
					<td><input type="radio" name="ERP" value="Бүрэн"></td>
				</tr>
				<tr>
					<td>MS Visio</td>
					<td><input type="radio" name="Visio" value="Анхан шатны"></td>
					<td><input type="radio" name="Visio" value="Хэрэглээний"></td>
					<td><input type="radio" name="Visio" value="Бүрэн"></td>
				</tr>
				<tr>
					<td>Daimond</td>
					<td><input type="radio" name="Daimond" value="Анхан шатны"></td>
					<td><input type="radio" name="Daimond" value="Хэрэглээний"></td>
					<td><input type="radio" name="Daimond" value="Бүрэн"></td>
				</tr>
				<tr>
					<td><input type="text" name="inputprogram3name"></td>
					<td><input type="radio" name="inputprogram3" value="Анхан шатны"></td>
					<td><input type="radio" name="inputprogram3" value="Хэрэглээний"></td>
					<td><input type="radio" name="inputprogram3" value="Бүрэн"></td>
				</tr>
				<tr>
					<td><input type="text" name="inputprogram1name"></td>
					<td><input type="radio" name="inputprogram1" value="Анхан шатны"></td>
					<td><input type="radio" name="inputprogram1" value="Хэрэглээний"></td>
					<td><input type="radio" name="inputprogram1" value="Бүрэн"></td>
				</tr>
				<tr>
					<td><input type="text" name="inputprogram2name"></td>
					<td><input type="radio" name="inputprogram2" value="Анхан шатны"></td>
					<td><input type="radio" name="inputprogram2" value="Хэрэглээний"></td>
					<td><input type="radio" name="inputprogram2" value="Бүрэн"></td>
				</tr>
			</table>
			<br>
			<table>
				<tr>
					<td></td>
					<td>Оноо</td>
					<td>Огноо</td>
				</tr>
				<tr>
					<td>TOEFL өгсөн эсэх</td>
					<td><input type="number" name="toeflname"></td>
					<td><input type="date" name="toefldate"></td>
				</tr>
				<tr>
					<td>IELTS өгсөн эсэх</td>
					<td><input type="number" name="ieltsname"></td>
					<td><input type="date" name="ieltsdate"></td>
				</tr>
				<tr>
					<td>HSK өгсөн эсэх</td>
					<td><input type="number" name="hskname"></td>
					<td><input type="date" name="hskdate"></td>
				</tr>
				<tr>
					<td>Бусад</td>
					<td><input type="number" name="othername"></td>
					<td><input type="date" name="otherdate"></td>
				</tr>
			</table>
			<p>6.3. Та урлаг, спортын төрлөөр хичээллэдэг үү?</p>
			<div style="overflow-x:auto;">
				<table>
					<tr>
						<td>№</td>
						<td>Урлаг, спортын төрөл</td>
						<td>Хэдэн жил хичээллэсэн</td>
						<td>Зэрэг, цол, шагналтай эсэх</td>
						<td>Олгосон байгууллага</td>
					</tr>
					<tr>
						<td>1</td>
						<td><input type="text" name="turul1"></td>
						<td><input type="text" name="hicheelesenjil1"></td>
						<td><input type="text" name="zereg1"></td>
						<td><input type="text" name="baiguulga1"></td>
					</tr>
					<tr>
						<td>2</td>
						<td><input type="text" name="turul2"></td>
						<td><input type="text" name="hicheelesenjil2"></td>
						<td><input type="text" name="zereg2"></td>
						<td><input type="text" name="baiguulga2"></td>
					</tr><tr>
						<td>3</td>
						<td><input type="text" name="turul3"></td>
						<td><input type="text" name="hicheelesenjil3"></td>
						<td><input type="text" name="zereg3"></td>
						<td><input type="text" name="baiguulga3"></td>
					</tr>
				</table>
			</div>
			<p>6.4. Таны сонирхол, хобби:</p>
			<textarea></textarea>
			<p>6.5. Мэдлэг, ур чадварын тодорхойлолт <i>/Таны ажил мэргэжил, туршлага, ур чадвар, хувь хүний талаар дэлгэрэнгүй мэдээлэл өгч чадах 3 хүнийг бичнэ үү/</i></p>
			<div style="overflow-x:auto;">
				<table>
					<tr>
						<td>№</td>
						<td>Нэр</td>
						<td>Ажиллаж буй байгууллагын нэр</td>
						<td>Албан тушаал</td>
						<td>Утасны дугаар</td>
						<td>Таныг хэдэн жилийн<br>өмнөөс мэдэх</td>
					</tr>
					<tr>
						<td>1</td>
						<td><input type="text" name="ner1_6_5"></td>
						<td><input type="text" name="baiguulga1_6_5"></td>
						<td><input type="text" name="atushaal1_6_5"></td>
						<td><input type="number" name="phone1_6_5"></td>
						<td><input type="number" name="hedenjil1_6_5"></td>
					</tr>
					<tr>
						<td>2</td>
						<td><input type="text" name="ner2_6_5"></td>
						<td><input type="text" name="baiguulga2_6_5"></td>
						<td><input type="text" name="atushaal2_6_5"></td>
						<td><input type="number" name="phone2_6_5"></td>
						<td><input type="number" name="hedenjil2_6_5"></td>
					</tr>
					<tr>
						<td>3</td>
						<td><input type="text" name="ner3_6_5"></td>
						<td><input type="text" name="baiguulga3_6_5"></td>
						<td><input type="text" name="atushaal3_6_5"></td>
						<td><input type="number" name="phone3_6_5"></td>
						<td><input type="number" name="hedenjil3_6_5"></td>
					</tr>
				</table>
			</div>
			<h3>VII. БУСАД МЭДЭЭЛЭЛ</h3>
			<p>7.1. Хувь хүний онцлогууд</p>
			<table>
				<tr>
					<td>Таны давуу тал</td>
					<td>Таны сайжруулах шаардлагатай тал</td>
				</tr>
				<tr>
					<td><textarea name="dawuutal"></textarea></td>
					<td><textarea name="sultal"></textarea></td>
				</tr>
			</table>
			<p>7.2. Ойрын 1-3 жилийн зорилго</p>
			<table>
				<tr>
					<td>Хувь хүний хөгжил <i>/өөрийгөө хөгжүүлэх/</i></td>
					<td>Гэр бүлийн талаарх төлөвлөлт <i>/гэр бүлтэй болох, хүүхэд<br>төрүүлэх, орон сууц авах гэх мэт/</i></td>
					<td>Албан тушаалын төлөвлөлт</td>
				</tr>
				<tr>
					<td><textarea name="hhhugjil"></textarea></td>
					<td><textarea name="gerbultuluwlult"></textarea></td>
					<td><textarea name="albantushaal"></textarea></td>
				</tr>
			</table>
			<p>7.3. Танд өвддөг хууч өвчин, эрүүл мэндийн хувьд анхаарах ямар нэгэн зовиур байдаг уу? </p>
			<textarea name="uwchin"></textarea>
			<p>7.4. Та ажлын байрны талаарх мэдээллийг ямар эх сурвалжаас авсан бэ?</p>
			<input type="radio" name="ehsurwalj" value="Сонин">Сонин<br>
			<input type="radio" name="ehsurwalj" value="Зарын сайт">Зарын сайт<br>
			<input type="radio" name="ehsurwalj" value="Бизнетворк">Бизнетворк<br>
			<input type="radio" name="ehsurwalj" value="Танилын шугамаар">Танилын шугамаар<br>
			<input type="radio" name="ehsurwalj" value="Хөдөлмөрийн бирж">Хөдөлмөрийн бирж
			<p>7.5. Таны хамаатан садан, найз нөхөд Говь ХК-д ажилладаг уу? </p>
			<input type="radio" name="gobidajildag" value="Тийм">Тийм<br>
			<input type="radio" name="gobidajildag" value="Үгүй">Үгүй<br>
			Хэрэв тийм бол: Таны хэн болох<input type="text" name="henboloh"> Албаны тушаал<input type="text" name="7_5albantushaal">Нэр:<input type="text" name="7_5ner">
			<p>7.6. Та манай компанид хэдэн жил ажиллахаар төлөвлөж байна вэ?</p>
			<input type="radio" name="herudah" value="1 хүртэл">1 хүртэл<br>
			<input type="radio" name="herudah" value="2-3 жил">2-3 жил<br>
			<input type="radio" name="herudah" value="4-5 жил">4-5 жил<br>
			<input type="radio" name="herudah" value="5-аас дээш жил">5-аас дээш жил<br>
			<p>7.7. Өөрийнхөө талаар нэмж танилцуулах боломж</p>
			<textarea name="uuriinhuutalaar"></textarea>
			<p>7.8. Ажилд орох боломжтой хугацаа: <input type="date" name="ajildoroh"></p>
			<p>Энэхүү дэлгэрэнгүй бүртгэл нь байгууллагын өмч бөгөөд архивт 50 жил хадгалагдана. Монгол Улсын "Байгууллагын нууцын тухай", "Хувь
			хүний нууцын тухай" хуулийн дагуу дээрх мэдээлэлтэй зөвхөн холбогдох эрх бүхий албан тушаалтан танилцах эрхтэй.</p>
			<button type="submit" style="margin-left: 50%">Илгээх</button>

		</div>

	</form>
</body>
</html>