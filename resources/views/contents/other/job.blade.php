@extends('contents.other.about1')
@section('style')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">

<style>
.accordion {
    background-color: #eee;
    color: #444;
    cursor: pointer;
    padding: 18px;
    width: 100%;
    border: none;
    text-align: left;
    outline: none;
    font-size: 15px;
    transition: 0.4s;
}

.active, .accordion:hover {
    background-color: #ccc;
}

.accordion:after {
    content: '\002B';
    color: #777;
    font-weight: bold;
    float: right;
    margin-left: 5px;
}

.active:after {
    content: "\2212";
}

.panel {
    padding: 0 18px;
    background-color: white;
    max-height: 0;
    overflow: hidden;
    transition: max-height 0.2s ease-out;
    font-size: 13px;
}
</style>
@endsection
@section('content')
<div id="rn_PageTitle" class="rn_AnswerDetail">
  <!-- <div id="rn_SearchControls" class="tb_Mobile_Show">
      <h1 class="tb_Mobile_Title">Frequently Asked Questions</h1>
      <h1 class="rn_ScreenReaderOnly">Search</h1>
      <form onsubmit="return false;">
          <div class="rn_SearchInput">
              <rn:widget path="search/AdvancedSearchDialog"/>
              <rn:widget path="search/KeywordText" label_text="" placeholder_text="Search a frequently asked question" label_text="Find the answer to your question" initial_focus="false"/>
          </div>
          <div class="">
              <rn:widget path="search/SearchButton" icon_path="images/tb_search_icon.png" report_page_url="/app/answers/list"/>
          </div>
      </form>
      <rn:widget path="search/DisplaySearchFilters"/>
  </div> -->
  
</div>
<div class="aboutSubContent" style="font-size: 14px">
  <img src="{{asset('imgs/abouts/job.jpg')}}" style="width: 800px">
    <h4 style="text-transform: uppercase;">Нээлттэй ажлын байр</h4>
    <p><strong></strong> <strong></strong> Та нээлттэй ажлын байранд шаардлага хангаж байна гэж үзвэл ажилд орох хүсэлт буюу анкет бөглөн hr@gobi.mn рүү илгээнэ үү. <a href="{{ url('../../ckfinder/userfiles/files/gobi-anket.docx') }}"><strong>Анкет татах</strong></a></p>
    @foreach($deps as $dep)
    
    <button class="accordion">{{ $dep->name }} ( {{$dep->jobss->count()}} )</button>
    <div class="panel">
      <table id="example" class="display" style="width:100%">
              <tr>
                <th>Ажлын байр</th>
                <th>Нээгдсэн огноо</th>
                <th>Хаагдах огноо</th>
              </tr>
              @foreach( $dep->jobss as $job)
              <tr>
                <td><a href="{{ url('about/view-jobs/'.$job->job_id.'/ажлын-байр')}}">{{ $job->name }}</a></td>
                <td>{{ $job->begin_date }}</td>
                <td>{{ $job->end_date }}</td>
              </tr>
              @endforeach
            </table>
    </div>
    @endforeach
</div>
@endsection
@section('scripts')
<script type="text/javascript" src="{{ asset('js/jquery.dataTables.min.js')}}"></script>
<script>
var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var panel = this.nextElementSibling;
    if (panel.style.maxHeight){
      panel.style.maxHeight = null;
    } else {
      panel.style.maxHeight = panel.scrollHeight + "px";
    } 
  });
}
</script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#example').DataTable({
      "searching": false,
      "lengthChange" : false,
      "language": {
            "info": " Нийт _TOTAL_ үр дүн. Энэ хуудсанд _START_  - _END_",
            "paginate": {
              "previous": "Өмнөх",
              "next": "Дараах"
            }
        }
    });
} );
</script>
@endsection