@extends('welcome')
@section('styles')
<link href="{{ asset('css/aboutus.css')}}" type="text/css" rel="stylesheet" />
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    @yield('style')
@endsection
@section('pro')
<div id="main">
    <div id="mainCtnt">
        <div id="leftcolumn" class="nobackground">

            <ul class="mainCtntMenu">
                <li><a class="headerlink" href="{{url('about')}}">Бидний тухай</a></li>
                <li><a class="navlink" href="{{url('about/brands')}}">Брэндүүд</a></li>
                <li><a class="navlink" href="{{url('about/branches')}}">Салбар дэлгүүр</a></li>
                <li><a class="navlink" href="{{ url('about/jobs/ажлын-байр') }}">Ажлын байр</a></li>
                <li><a class="navlink" href="{{ url('about/нийгмийн/хариуцлага')}}">Нийгмийн хариуцлага</a></li>
                <li><a href="{{url('about/тендер')}}">Тендерийн материал</a></li>
                <li class="lastlink" style="border-bottom:none;"><a class="navlink" href="{{ url('хувьцаа-эзэмшигчдэд/7')}}">Төлөөлөн удирдах зөвлөл</a></li>

            </ul>
            <script type="text/javascript" language="javascript">
                jQuery(document).ready( function() {
                            
                    var addr = $(location).attr('href');
                    
                    var names = addr.split("=");
                    names = names[names.length-1];
                    
                    jQuery('.navgroup a[href$='+names[0]+']').css('color', '#D8D8D8');
                    
                    jQuery('.navgroup h3:last').addClass('borderLastNav');
                });
            </script>
            <div class="clear"></div>
        </div>
    <div id="content">
        <div class="breadcrumb" itemprop="breadcrumb">
            <a href="{{url('/')}}" title="Home">Нүүр</a>
            <span class="divider">/</span><a href="{{url('about')}}">Бидний тухай</a>
        </div>
        <script language="javascript" type="text/javascript">
            app.currentPage.breadcrumbs = [];
            app.currentPage.breadcrumbs.push("About Us"); 
              
              
        </script>
        <div class="contentasset mobile-available">
            <style type="text/css">.aboutContentWrapper p {font-family:SamsungSans; color:#606061; font-size:12px !important}
            .aboutContentWrapper .thirdColumns a {font-size: 12px;}
            </style>
            <div class="aboutContentWrapper nonBordered">
                @if(Request::is('about'))
                <a href="#/about-tory/about-tory.html">
                    <img height="408" src="{{asset('imgs/GobiaboutIMG.jpg')}}" width="717" /> 
                </a>
                <div class="pageHeader">&nbsp;
                    <h2>Компанийн танилцуулга<br></h2>
                    <p style="text-align: justify;">
                        Говь ХК нь Монголчуудын бахархал болсон ноос ноолуураар чанар, дэгжин байдлыг илэрхийлэхүйц хувцсыг урласаар 37 жилийг ардаа үлдээжээ.<br>
                        Засгийн газрын эзэмшилд 30 жил байсан Говь компани нь 2007 оны 7 сард хувьцаат компанийн замналаар явж, хувцас загварын түүхэнд цоо шинэ өнгө төрхийг нэмсэн. Говь брэндийн чанар, сэтгэл татам чанарыг хэрэглэгчдэд таниулах зорилгоор ажиллаж, үйлдвэрлэл, дизайны өвөрмөц шийдлээрээ дэлхийн топ 5 ноолууран бүтээгдэхүүн үйлдвэрлэгчийн тоонд зүй ёсоор багтжээ.<br>
                        Бид 1600 гаруй чадварлаг боловсон хүчнийг эгнээндээ нэгтгэж, ноолуурын түүхий эд бэлтгэхээс эхлээд бэлэн бүтээгдэхүүн үйлдвэрлэх хүртэлх бүхий л шатны дамжлагыг 100% Монголдоо боловсруулдаг цорын ганц үйлдвэр юм.
                        </p>
                </div>
                <!-- <div class="aboutSubContent">
                    <ul class="thirdColumns">
                        <li class="alignleft">
                            <a alt="Stay Connected on Social Media" href="#/social-media/about-us-social-media-main.html">
                                <img alt="" height="173" src="https://static.toryburch.com/static/on/demandware.static/-/Sites-ToryBurch_US-Library/default/dw0ca082ae/images/aboutus/01-ABOUTus-LANDING_SUB_1.jpg" title="Stay Connected on Social Media" width="226" /> </a>
                            <p><a href="#/social-media/about-us-social-media-main.html">Говь ХК</a></p>
                            <a href="#/social-media/about-us-social-media-main.html"> </a>
                        </li>
                        <li class="aligncenter">
                            <a href="#">
                                <img alt="The Tory Burch Foundation" height="173" src="https://static.toryburch.com/static/on/demandware.static/-/Sites-ToryBurch_US-Library/default/dw0ca082ae/images/aboutus/01-ABOUTus-LANDING_SUB_1.jpg" width="226" /> 
                            </a>
                            <p>
                                <a href="#/about-tory-burch-foundation/about-us-foundation-main.html">Брэндүүд</a>
                            </p>
                            <a href="#/about-tory-burch-foundation/about-us-foundation-main.html"> </a>
                        </li>
                        <li class="alignright">
                            <a alt="Tory Burch Careers" href="#">
                                <img alt="" height="173" src="https://static.toryburch.com/static/on/demandware.static/-/Sites-ToryBurch_US-Library/default/dwd3a19e32/01-ABOUTus-LANDING_01.jpg" title="Tory Burch Careers" width="226" /> 
                            </a>
                            <p><a href="#">Салбар дэлгүүр</a>
                            </p>
                            <a href="#"> </a>
                        </li>
                    </ul>
                    <ul class="thirdColumns">
                        <li class="alignleft">
                            <a alt="Stay Connected on Social Media" href="#/social-media/about-us-social-media-main.html">
                                <img alt="" height="173" src="https://static.toryburch.com/static/on/demandware.static/-/Sites-ToryBurch_US-Library/default/dw0ca082ae/images/aboutus/01-ABOUTus-LANDING_SUB_1.jpg" title="Stay Connected on Social Media" width="226" /> </a>
                            <p><a href="#/social-media/about-us-social-media-main.html">Ажлын байр</a></p>
                            <a href="#/social-media/about-us-social-media-main.html"></a>
                        </li>
                        <li class="aligncenter">
                            <a href="#">
                                <img alt="The Tory Burch Foundation" height="173" src="https://static.toryburch.com/static/on/demandware.static/-/Sites-ToryBurch_US-Library/default/dw0ca082ae/images/aboutus/01-ABOUTus-LANDING_SUB_1.jpg" width="226" /> 
                            </a>
                            <p>
                                <a href="#/about-tory-burch-foundation/about-us-foundation-main.html">Нийгмийн хариуцлага</a>
                            </p>
                            <a href="#/about-tory-burch-foundation/about-us-foundation-main.html"> </a>
                        </li>
                        <li class="alignright">
                            <a href="#">
                                <img alt="" height="173" src="https://static.toryburch.com/static/on/demandware.static/-/Sites-ToryBurch_US-Library/default/dwd3a19e32/01-ABOUTus-LANDING_01.jpg" title="Tory Burch Careers" width="226" /> 
                            </a>
                            <p><a href="#">Төлөөлөн удирдах зөвлөл</a>
                            </p>
                            <a href="#"> </a>
                        </li>
                    </ul>
                </div> -->
                @else
                @yield('content')
                @endif
            </div>
        </div>
        <div class="mobilecontentasset">
            <style type="text/css">.aboutContentWrapper p {font-family:SamsungSans; text-transform: uppercase;
            </style>
            <div class="aboutContentWrapper nonBordered">
                <a href="#/about-tory/about-tory.html">
                    <img alt="Explore Tory's Story" height="408" src="{{asset('imgs/GobiaboutIMG.jpg')}}" width="717" /> 
                </a>
                <div class="pageHeader">&nbsp;
                    <p style="text-align: justify;">Монголын ноос, ноолуурын салбарын анхдагч “Говь” үйлдвэр нь 1981 онд Япон улсын хөрөнгө оруулалттайгаар байгуулагдаж эдүгээ 30 гаруй жил тасралтгүй үйл ажиллагаагаа амжилттай явуулж байгаа үндэсний томоохон үйлдвэрлэгч юм. “Говь” ХК-ийн төрийн мэдлийн хувьцааг 2007 оны 7 сард “ЭФ СИ АЙ” ХХК Олон улсын нээлттэй дуудлага худалдаагаар худалдан авснаар “Говь” ХК өнөөг хүртэл хувийн хэвшлийн бүтэц зохион байгуулалт, менежменттэйгээр үйл ажиллагаагаа явуулж байна.</p>
                    <p style="text-align: justify;">Монгол ямааны 100% ноолуур болон тэмээний ноосыг ашиглан дэлхийн зэрэглэлийн хамгийн сүүлийн үеийн техник, технологиор үйлдвэрлэл явуулдаг “Говь” ХК нь үйлдвэрлэл, хүчин чадал, чанар, үйлчилгээ зэрэг бүх талын үзүүлэлтээрээ дэлхийн топ 5 ноолууран бүтээгдэхүүн үйлдвэрлэгчийн тоонд зүй ёсоор тооцогддог билээ. 1600 гаруй чадварлаг боловсон хүчнийг эгнээндээ нэгтгэсэн “Говь” ХК жилдээ 1100 тн түүхий ноос, ноолуур 30 тн бөмбөгөн ноос, ноолуур 210 тн самнасан ноолуур боловсруулах хүчин чадалтай ба түүхий эд бэлтгэхээс эхлээд бэлэн бүтээгдэхүүн үйлдвэрлэх хүртэлх бүхий л шатны үйлдвэрлэл, дамжлагыг 100% Монголдоо боловсруулдаг цорын ганц үйлдвэр юм.</p>
                </div>
            </div>
        </div>
        <div class="clear"></div>
    </div>
    <div class="clear"></div>
    </div>
</div>

@endsection
@section('customscripts')
    @yield('scripts')
@endsection