@extends('contents.other.about1')
@section('style')

@endsection
@section('content')
<div id="rn_PageTitle" class="rn_AnswerDetail">
  <!-- <div id="rn_SearchControls" class="tb_Mobile_Show">
      <h1 class="tb_Mobile_Title">Frequently Asked Questions</h1>
      <h1 class="rn_ScreenReaderOnly">Search</h1>
      <form onsubmit="return false;">
          <div class="rn_SearchInput">
              <rn:widget path="search/AdvancedSearchDialog"/>
              <rn:widget path="search/KeywordText" label_text="" placeholder_text="Search a frequently asked question" label_text="Find the answer to your question" initial_focus="false"/>
          </div>
          <div class="">
              <rn:widget path="search/SearchButton" icon_path="images/tb_search_icon.png" report_page_url="/app/answers/list"/>
          </div>
      </form>
      <rn:widget path="search/DisplaySearchFilters"/>
  </div> -->
  
</div>
<div class="aboutSubContent" style="font-size: 14px">
  <div class="main-content career">
    <h2 style="text-transform: uppercase;">Нээлттэй ажлын байр</h2> 
    <hr> 
              
    <div class="desc-wrap">         
      <div class="left ">
        <div class="pic ">                   
        </div>  
        <div class="body" style="color: #2b2b2b;">
          <div style="margin-bottom: 10px;"><span style="text-transform: uppercase;">Алба/Нэгж:</span> &nbsp; {{$view->depss->name}}</div>
          <div style="margin-bottom: 10px;"><span style="text-transform: uppercase;">Ажлын байр:</span> &nbsp; {{$view->name}}</div>                                               
          <div style="margin-bottom: 10px;"><span style="text-transform: uppercase;">Нээлтийн огноо:</span> &nbsp; {{$view->begin_date}}</div>
          <div style="margin-bottom: 10px;"><span style="text-transform: uppercase;">Хаалтын огноо:</span> &nbsp; {{$view->end_date}}</div>
          <div style="margin-bottom: 10px;">
            <span style="text-transform: uppercase;">Ажлын байрны зорилго/ үүрэг:</span> &nbsp; 
            {!! $view->zorilgo !!}
          </div>
          <div style="margin-bottom: 10px;">
            <span style="text-transform: uppercase;">Тавигдах шаардлага:</span> &nbsp; 
            {!!$view->shaardlaga!!}
            
          </div>
          <a href="#" style="color: #0B3F79; font-weight: bold; text-transform: uppercase; text-decoration: underline;" >Анкет бөглөх</a>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('scripts')

@endsection