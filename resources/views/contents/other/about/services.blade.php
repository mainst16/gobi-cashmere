@extends('welcome')
@section('styles')
<link href="{{ asset('css/aboutus.css')}}" type="text/css" rel="stylesheet" />

    @yield('style')
@endsection
@section('pro')
<div id="main">
    <div id="mainCtnt">
        <div id="leftcolumn" class="nobackground">

            <ul class="mainCtntMenu">
                <li><a class="headerlink" href="{{url('services')}}">Үйлчилгээ</a></li>
                <li><a class="navlink" href="{{url('services')}}">Манай байршил</a></li>
                <li class="lastlink" style="border-bottom:none;"><a class="navlink" href="{{url('services/care')}}">Хими цэвэрлэгээ</a></li>

            </ul>
            <script type="text/javascript" language="javascript">
                jQuery(document).ready( function() {
                            
                    var addr = $(location).attr('href');
                    
                    var names = addr.split("=");
                    names = names[names.length-1];
                    
                    jQuery('.navgroup a[href$='+names[0]+']').css('color', '#D8D8D8');
                    
                    jQuery('.navgroup h3:last').addClass('borderLastNav');
                });
            </script>
            <div class="clear"></div>
        </div>
    <div id="content">
        <div class="breadcrumb" itemprop="breadcrumb">
            <a href="{{url('/')}}" title="Home">Нүүр</a>
            <span class="divider">/</span><a href="{{url('about')}}">Бидний тухай</a>
        </div>
        <script language="javascript" type="text/javascript">
            app.currentPage.breadcrumbs = [];
            app.currentPage.breadcrumbs.push("About Us"); 
              
              
        </script>
        <div class="contentasset mobile-available">
            <style type="text/css">.aboutContentWrapper p {font-family:SamsungSans; color:#606061; font-size:12px !important}
            .aboutContentWrapper .thirdColumns a {font-size: 12px;}
            </style>
            <div class="aboutContentWrapper nonBordered">
                @if(Request::is('services'))
                <div class="pageHeader">&nbsp;
                    <iframe src="https://www.google.com/maps/d/embed?mid=1AXFDWEBepdstYK5pH4jXBCywqc_N5xCV" width="100%" height="700px" style="margin-top: -63px"></iframe>
                </div>
                @else
                @yield('content')
                @endif
            </div>
        </div>
        <div class="mobilecontentasset">
            <style type="text/css">.aboutContentWrapper p {font-family:SamsungSans; text-transform: uppercase;}
            </style>
            <div class="aboutContentWrapper nonBordered">
                <a href="#/about-tory/about-tory.html">
                    <img alt="Explore Tory's Story" height="408" src="{{asset('imgs/GobiaboutIMG.jpg')}}" width="717" /> 
                </a>
                <div class="pageHeader">&nbsp;
                    <iframe src="https://www.google.com/maps/d/embed?mid=1LLdaTCHj2mH8OJTejevn4k2eO3WzTp19" width="100%"  height="480"></iframe>
                </div>
            </div>
        </div>
        <div class="clear"></div>
    </div>
    <div class="clear"></div>
    </div>
</div>

@endsection
@section('customscripts')
    @yield('scripts')
@endsection