@extends('contents.other.about1')
@section('style')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">

<style>
.accordion {
    background-color: #eee;
    color: #444;
    cursor: pointer;
    padding: 18px;
    width: 100%;
    border: none;
    text-align: left;
    outline: none;
    font-size: 15px;
    transition: 0.4s;
}

.active, .accordion:hover {
    background-color: #ccc;
}

.accordion:after {
    content: '\002B';
    color: #777;
    font-weight: bold;
    float: right;
    margin-left: 5px;
}

.active:after {
    content: "\2212";
}

.panel {
    padding: 0 18px;
    background-color: white;
    max-height: 0;
    overflow: hidden;
    transition: max-height 0.2s ease-out;
    font-size: 13px;
}
</style>
@endsection
@section('content')
<div class="aboutSubContent" style="font-size: 14px">
    @foreach($tenders as $tender)
    
      <button class="accordion">{{$tender->title}}</button>
      <div class="panel">
        {{$tender->text}}
      </div>
    @endforeach
</div>
@endsection
@section('scripts')
<script type="text/javascript" src="{{ asset('js/jquery.dataTables.min.js')}}"></script>
<script>
var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var panel = this.nextElementSibling;
    if (panel.style.maxHeight){
      panel.style.maxHeight = null;
    } else {
      panel.style.maxHeight = panel.scrollHeight + "px";
    } 
  });
}
</script>
<script type="text/javascript">
  $(document).ready(function() {
    $('#example').DataTable({
      "searching": false,
      "lengthChange" : false,
      "language": {
            "info": " Нийт _TOTAL_ үр дүн. Энэ хуудсанд _START_  - _END_",
            "paginate": {
              "previous": "Өмнөх",
              "next": "Дараах"
            }
        }
    });
} );
</script>
@endsection