@extends('contents.other.about1')
<link href="https://cdn.rawgit.com/sachinchoolur/lightgallery.js/master/dist/css/lightgallery.css" rel="stylesheet">
@section('style')
<style>

.demo-gallery > ul {
                margin-bottom: 0;
                margin-left:3px;
            }
            .demo-gallery > ul > li {
                float: left;
                margin-bottom: 19px;
                margin-right: 51px;
                margin-left: 15px;
                width: 300px;
            }
            #t {
    font-style: italic;
}
</style>
@endsection
@section('content')

<div id="Library"></div>
<div id="rn_PageTitle" class="rn_AnswerDetail">
            
<h2 style="text-transform: uppercase; text-align:center">Их хотын зүрхэн хэсэгт сүндэрлэх Галлериа Улаанбаатар төв албан ёсоор нээгдлээ</h2> 
    <hr> 
        <p>Зөөлөн салхи өдрөөс өдөрт холдсон Намрын энэ өдрүүдэд шинэхэн коллекциор өнгө нэмсэн архитектурын өвөрмөц шийдэл бүхий 2 давхар Говийн дэлгүүр таныг огтхон ч уйдаахгүй.
          Монгол хүний ур ухаанаар Дэлхийн жишигт нийцсэн худалдаа үйлчилгээний төв танд гайхалтай мэдрэмжийг төрүүлэх болно.</p>
          <br>  
  
</div>
<div class="aboutSubContent" style="font-size: 14px">
  <div class="main-content career">
    
          <div class="main-hero asset-template asset-template--content-breaker-1" >
          <div style="float: left; width: 100%; margin: 10px 0 80px;" class="asset-template__wrapper">     
              <div class="asset-template__image-wrapper">
                  <video style="width: 660px" height="400" autoplay loop muted>
                        <source src="{{ asset('videos/galleria.mp4')}}"/>
                        Your browser does not support the video tag.
                  </video>
              </div>

          </div>
      </div>
          <hr>

                <p id="t">Зураг дээр дарж ТОМООР үзэх боломжтой. </p>
              <div class="demo-gallery">
                <ul id="lightgallery" class="list-unstyled row">
                
                <li class="col-sm-6 col-sm-6" data-responsive="" data-src="{{asset('imgs/abouts/gallieria05.jpg')}}" alt="Thumb-0">
                    <a href="">
                        <img class="img-responsive" src="{{asset('imgs/abouts/gallieria05.jpg')}}" alt="Thumb-1" width="270" height="179">
                    </a>
                </li>
                <li class="col-xs-6 col-sm-4 col-md-3" data-responsive="img/2-375.jpg 375, img/2-480.jpg 480, img/2.jpg 800" data-src="{{asset('imgs/abouts/gallieria02.jpg')}}">
                    <a href="">
                        <img class="img-responsive" src="{{asset('imgs/abouts/gallieria02.jpg')}}" alt="Thumb-2">
                    </a>
                </li>
                <li class="col-xs-6 col-sm-4 col-md-3" data-responsive="img/13-375.jpg 375, img/13-480.jpg 480, img/13.jpg 800" data-src="{{asset('imgs/abouts/gallieria03.jpg')}}" data-sub-html="">
                    <a href="">
                        <img class="img-responsive" src="{{asset('imgs/abouts/gallieria03.jpg')}}" alt="Thumb-3">
                    </a>
                </li>
                <li class="col-xs-6 col-sm-4 col-md-3" data-responsive="img/13-375.jpg 375, img/13-480.jpg 480, img/13.jpg 800" data-src="{{asset('imgs/abouts/gallieria04.jpg')}}" data-sub-html="">
                    <a href="">
                        <img class="img-responsive" src="{{asset('imgs/abouts/gallieria04.jpg')}}" alt="Thumb-4">
                    </a>
                </li>
                <li class="col-xs-6 col-sm-4 col-md-3" data-responsive="img/13-375.jpg 375, img/13-480.jpg 480, img/13.jpg 800" data-src="{{asset('imgs/abouts/gallieria06.jpg')}}" data-sub-html="">
                    <a href="">
                        <img class="img-responsive" src="{{asset('imgs/abouts/gallieria06.jpg')}}" alt="Thumb-5">
                    </a>
                </li>
                <li class="col-xs-6 col-sm-4 col-md-3" data-responsive="img/13-375.jpg 375, img/13-480.jpg 480, img/13.jpg 800" data-src="{{asset('imgs/abouts/gallieria07.jpg')}}" data-sub-html="">
                    <a href="">
                        <img class="img-responsive" src="{{asset('imgs/abouts/gallieria07.jpg')}}" alt="Thumb-6">
                    </a>
                </li>
                <li class="col-xs-6 col-sm-4 col-md-3" data-responsive="img/13-375.jpg 375, img/13-480.jpg 480, img/13.jpg 800" data-src="{{asset('imgs/abouts/gallieria08.jpg')}}" data-sub-html="">
                    <a href="">
                        <img class="img-responsive" src="{{asset('imgs/abouts/gallieria08.jpg')}}" alt="Thumb-7">
                    </a>
                </li>
                <li class="col-xs-6 col-sm-4 col-md-3" data-responsive="img/13-375.jpg 375, img/13-480.jpg 480, img/13.jpg 800" data-src="{{asset('imgs/abouts/gallieria09.jpg')}}" data-sub-html="">
                    <a href="">
                        <img class="img-responsive" src="{{asset('imgs/abouts/gallieria09.jpg')}}" alt="Thumb-8">
                    </a>
                </li>
                <li class="col-xs-6 col-sm-4 col-md-3" data-responsive="img/13-375.jpg 375, img/13-480.jpg 480, img/13.jpg 800" data-src="{{asset('imgs/abouts/gallieria10.jpg')}}" data-sub-html="">
                    <a href="">
                        <img class="img-responsive" src="{{asset('imgs/abouts/gallieria10.jpg')}}" alt="Thumb-9">
                    </a>
                </li>
                <li class="col-xs-6 col-sm-4 col-md-3" data-responsive="img/13-375.jpg 375, img/13-480.jpg 480, img/13.jpg 800" data-src="{{asset('imgs/abouts/gallieria11.jpg')}}" data-sub-html="">
                    <a href="">
                        <img class="img-responsive" src="{{asset('imgs/abouts/gallieria11.jpg')}}" alt="Thumb-10">
                    </a>
                </li>
                <li class="col-xs-6 col-sm-4 col-md-3" data-responsive="img/13-375.jpg 375, img/13-480.jpg 480, img/13.jpg 800" data-src="{{asset('imgs/abouts/gallieria12.jpg')}}" data-sub-html="">
                    <a href="">
                        <img class="img-responsive" src="{{asset('imgs/abouts/gallieria12.jpg')}}" alt="Thumb-11">
                    </a>
                </li>
                <li class="col-xs-6 col-sm-4 col-md-3" data-responsive="img/13-375.jpg 375, img/13-480.jpg 480, img/13.jpg 800" data-src="{{asset('imgs/abouts/gallieria14.jpg')}}" data-sub-html="">
                    <a href="">
                        <img class="img-responsive" src="{{asset('imgs/abouts/gallieria14.jpg')}}" alt="Thumb-12">
                    </a>
                </li>
                
                
            </ul>
        </div>


                <hr>



        

<!-- <div class="product-tile__qs-heart like-btn wishlist-heart-btn qs-like-button"></div>

<div class="product-tile__arrow product-tile__arrow--left js-product-tile-arrow" data-direction="left"></div>
<div class="product-tile__arrow product-tile__arrow--right js-product-tile-arrow" data-direction="right"></div> -->


    
  <!-- isproductimage -->	
  <div class="aboutSubContent" style="font-size: 14px">
            <img src="{{asset('imgs/abouts/galleria.jpg')}}" style="width: 717px">
          </div>

     



</div>
         
<script src="https://cdn.jsdelivr.net/picturefill/2.3.1/picturefill.min.js"></script>
        <script src="https://cdn.rawgit.com/sachinchoolur/lightgallery.js/master/dist/js/lightgallery.js"></script>
        <script src="https://cdn.rawgit.com/sachinchoolur/lg-pager.js/master/dist/lg-pager.js"></script>
        <script src="https://cdn.rawgit.com/sachinchoolur/lg-autoplay.js/master/dist/lg-autoplay.js"></script>
        <script src="https://cdn.rawgit.com/sachinchoolur/lg-fullscreen.js/master/dist/lg-fullscreen.js"></script>
        <script src="https://cdn.rawgit.com/sachinchoolur/lg-zoom.js/master/dist/lg-zoom.js"></script>
        <script src="https://cdn.rawgit.com/sachinchoolur/lg-hash.js/master/dist/lg-hash.js"></script>
        <script src="https://cdn.rawgit.com/sachinchoolur/lg-share.js/master/dist/lg-share.js"></script>
        <script>
            lightGallery(document.getElementById('lightgallery'));
        </script>
  </div>  
  </div>
@endsection
@section('scripts')

@endsection