@extends('welcome')
@section('styles')
<link href="{{ asset('css/aboutus.css')}}" type="text/css" rel="stylesheet" />
    @yield('style')
@endsection
@section('pro')
<div id="main">
    <div id="mainCtnt">
        <div id="leftcolumn" class="nobackground">

            <ul class="mainCtntMenu">
                <li><a class="headerlink" href="{{url('хувьцаа-эзэмшигчдэд/7')}}">ХУВЬЦАА ЭЗЭМШИГЧДЭД</a></li>
                <li><a class="navlink" href="{{ url('хувьцаа-эзэмшигчдэд/компаний-засаглал/1')}}">Компанийн засаглал</a></li>
                <li><a class="navlink" href="{{ url('хувьцаа-эзэмшигчдэд/хувьцаа-эзэмшигч/2')}}">Хувьцаа эзэмшигчид</a></li>
                <li><a class="navlink" href="{{ url('хувьцаа-эзэмшигчдэд/төлөөлөн-удирдах-зөвлөл/3') }}">Төлөөлөн удирдах зөвлөл</a></li>
                <li><a class="navlink" href="{{ url('хувьцаа-эзэмшигчдэд/гүйцэтгэх-удирдлага/4') }}">Гүйцэтгэх удирдлага болон эрх бүхий албан тушаалтан</a></li>
                <li><a class="navlink" href="{{ url('хувьцаа-эзэмшигчдэд/хөрөнгийн-зах-зээл/5')}}">Хөрөнгийн зах зээл ба компанийн хувьцаа</a></li>
                <li><a class="navlink" href="{{ url('хувьцаа-эзэмшигчдэд/хөрөнгийн-зах-зээл/8')}}">Ногдол ашиг</a></li>
                <li class="lastlink" style="border-bottom:none;"><a class="navlink" href="{{ url('хувьцаа-эзэмшигчдэд/мэдээ/6') }}">Мэдээ мэдээлэл</a></li>

            </ul>
            <script type="text/javascript" language="javascript">
                jQuery(document).ready( function() {
                            
                    var addr = $(location).attr('href');
                    
                    var names = addr.split("=");
                    names = names[names.length-1];
                    
                    jQuery('.navgroup a[href$='+names[0]+']').css('color', '#D8D8D8');
                    
                    jQuery('.navgroup h3:last').addClass('borderLastNav');
                });
            </script>
            <div class="clear"></div>
        </div>
    <div id="content">
        <div class="breadcrumb" itemprop="breadcrumb">
            <a href="{{url('/')}}" title="Home">Нүүр</a>
            <span class="divider">/</span><a href="{{url('about')}}">Бидний тухай</a>
        </div>
        <script language="javascript" type="text/javascript">
            app.currentPage.breadcrumbs = [];
            app.currentPage.breadcrumbs.push("About Us"); 
              
              
        </script>
        <div class="contentasset mobile-available">
            <style type="text/css">.aboutContentWrapper p {font-family:SamsungSans; color:#8c8a8a; font-size:12px !important}
            .aboutContentWrapper .thirdColumns a {font-size: 12px;}
            </style>
            <div class="aboutContentWrapper nonBordered">
                @if(Request::is('хувьцаа-эзэмшигчдэд/7'))
                @foreach($tuzs as $tuz)
                <div class="pageHeader">&nbsp;
                    <h2>{{ $tuz->title}}</h2>
                       {!! $tuz->desc !!}
                </div>
                @endforeach
                @else
                @yield('content')
                @endif
            </div>
        </div>
        <div class="mobilecontentasset">
            <style type="text/css">.aboutContentWrapper p {font-family:SamsungSans; text-transform: uppercase;
            </style>
            <div class="aboutContentWrapper nonBordered">
                @if(Request::is('хувьцаа-эзэмшигчдэд/7'))
                @foreach($tuzs as $tuz)
                <div class="pageHeader">&nbsp;
                    <h2>{{ $tuz->title}}</h2>
                       {!! $tuz->desc !!}
                </div>
                @endforeach
                @else
                @yield('content')
                @endif
            </div>
        </div>
        <div class="clear"></div>
    </div>
    <div class="clear"></div>
    </div>
</div>

@endsection
@section('customscripts')
    @yield('scripts')
@endsection