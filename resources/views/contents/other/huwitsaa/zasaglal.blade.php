@extends('contents.other.huwitsaa.huwicaa')
@section('style')
<style type="text/css">
  img {
    -webkit-filter: grayscale(100%); /* Safari 6.0 - 9.0 */
    filter: grayscale(100%);
</style>
@endsection
@section('content')
<div id="rn_PageTitle" class="rn_AnswerDetail">
  <!-- <div id="rn_SearchControls" class="tb_Mobile_Show">
      <h1 class="tb_Mobile_Title">Frequently Asked Questions</h1>
      <h1 class="rn_ScreenReaderOnly">Search</h1>
      <form onsubmit="return false;">
          <div class="rn_SearchInput">
              <rn:widget path="search/AdvancedSearchDialog"/>
              <rn:widget path="search/KeywordText" label_text="" placeholder_text="Search a frequently asked question" label_text="Find the answer to your question" initial_focus="false"/>
          </div>
          <div class="">
              <rn:widget path="search/SearchButton" icon_path="images/tb_search_icon.png" report_page_url="/app/answers/list"/>
          </div>
      </form>
      <rn:widget path="search/DisplaySearchFilters"/>
  </div> -->
  
</div>
<div class="aboutSubContent" style="font-size: 14px">
  @foreach($tuzs as $tuz)
  <h2>{{$tuz->title}}</h2>
    @if(empty($tuz->pics))
    
    @else
    <img src="{{ asset('storage/'.$tuz->pics.'') }}" class="img-thumbnail" style="width: 717px">
    @endif
    {!! $tuz->desc !!}
    <hr> 
  @endforeach
</div>
@endsection
@section('scripts')

@endsection