<!DOCTYPE html>
<html>
<head>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<title>Анкет</title>



	
</head>

<style>
body{
	margin-top: 10px !important;
	margin-bottom: 10px !important;
	margin-right: 200px !important;
	margin-left: 200px !important;
	font-family:sans-serif !important;
	background-color: gray !important;
}
@media only screen and (max-width: 600px) {
    body {
    margin-top: 0px;
	margin-bottom: 0px;
	margin-right: 0px;
	margin-left: 0px;
	font-family:sans-serif;
	background-color: #fff;
    }
}
table#sanamj td, th
{
    border:1px solid #fff !important;
    border-collapse:collapse;
}
table, th, td {
	border: 1px solid black;
    border-collapse: collapse;
	text-align: left;
	
}
.table td, .table th {

    border-top: 1px solid #000000;
}

div {
	background-color:#fff;
	padding: 25px;
}
td, th {
    padding: 6px;
}
#image{ 
	padding-left: 50px;
}

</style>

<body>
	<div class="container">
		
			<img src="{{asset('imgs/logos/gobi.png')}}" style="width: 500px; margin: -100px">
			<h3 style="text-align: center;">ГОВЬ ХК-ИЙН АЖИЛ ГОРИЛОГЧИЙН АНКЕТ</h3>
			{!! csrf_field() !!}
			@if (count($errors) > 0)
				<div class="alert alert-danger">
				 Upload Validation Error<br><br>
				 <ul>
				  @foreach ($errors->all() as $error)
				   <li>{{ $error }}</li>
				  @endforeach
				 </ul>
				</div>
			@endif
			<div class="row">
				<div class="col-md-9">
					<table id="sanamj" class="table-bordered">
						<tr>
							<td> <strong>Санамж:</strong></td>
							<td>
								<i>1. Анкетыг бөглөхдөө гаргацтай үнэн зөв дэлгэрэнгүй, үг товчлохгүй бичнэ үү.</i>
							</td>
						</tr>
						<tr>
							<td></td>
							<td>
								<i>2. Цээж зураг, их дээд сургуулийн дипломын хуулбарыг заавал хавсаргана уу.</i>
							</td>
						</tr>
						<tr>
							<td></td>
							<td>
								<i>3. Зөвхөн анкетаар шалгарсан тохиолдолд бид таныг ярилцлагад урина.</i>
							</td>
						</tr>
						<tr>
							<td></td>
							<td>
								<i>4. Дээрх хуулбар болон энэхүү анкетыг буцаан олгохгүй бөгөөд анкетыг хүлээж авснаар ажил горилогчийн өмнө манай компани хариуцлага хүлээхгүй болно.</i>
							</td>
						</tr>
					</table>
				</div>
				<div class="col-md-3">
					<p id="image"><img src="{{ asset('imgs/anketpics/'.$ankets->zurag.'')}}" width="170" height="200" /></p>
					<p><i style="font-size:10px">Цээж зураг</i></p>
				</div>
			</div>
		<div class="row">	
				<div class="col-md-12">
			<table class="table">
				<tr>
					<td>Ажиллахыг хүсч буй албан тушаал</td>
				</tr>
				<tr>
					<td><strong>{{ $ankets->wish_albantushaal }}</strong></td>
				</tr>
				<tr>
					<td>Та манай байгууллагад үндсэн мэргэжлээсээ гадна ямар ажлын байр дээр ажиллах сонирхолтой байна вэ?</td>
				</tr>
				<tr>
					<td><strong>{{ $ankets->wish_sonirhol1 }}</strong></td>
				</tr>
				<tr>
					<td><strong>{{ $ankets->wish_sonirhol2 }}</strong></td>
				</tr>
				<tr>
					<td>Таны хүсч буй цалингийн доод хэмжээ /төгрөгөөр/:</td>
				</tr>
				<tr>
					<td><strong>{{ $ankets->wish_tsalin }}</strong></td>
				</tr>
			</table>
			</div>

			<div class="col-md-12">
			<h3>I. ХУВИЙН МЭДЭЭЛЭЛ</h3>
			<table>
				<tr>
					<td><p>Ургийн овог:</p></td>
					<td><strong>{{ $ankets->uovog }}</strong></td>
				</tr>
				<tr>
					<td><p>Эцэг/эх-ийн нэр:</p></td>
					<td><strong>{{ $ankets->ovog }}</strong></td>
				</tr>
				<tr>
					<td><p>Нэр:</p></td>
					<td><strong>{{ $ankets->ner }}</strong></td>
				</tr>
				<tr>
					<td><p>Гэрийн утас:</p></td>
					<td><strong>{{ $ankets->homephone }}</strong></td>
				</tr>
				<tr>
					<td><p>Үндэс угсаа:</p></td>
					<td><strong>{{ $ankets->undes }}</strong></td>
				</tr>
				<tr>
					<td><p>Цусны бүлэг:</p></td>
					<td>
						<strong>{{ $ankets->blood }}</strong>
					</td>
				</tr>
				<tr>
					<td><p>Биеийн жин:</p></td>
					<td><strong>{{ $ankets->kg }}</strong></td>
				</tr>
				<tr>
					<td><p>Биеийн өндөр:</p></td>
					<td><strong>{{ $ankets->cm }}</strong></td>
				</tr>
				<tr>
					<td>РД:</td>
					<td><strong>{{ $ankets->regno }}</strong></td>
				</tr>
				<tr>
					<td>ЭМД №:</td>
					<td><strong>{{ $ankets->emdno }}</strong></td>
				</tr>
				<tr>
					<td>НДД №:</td>
					<td><strong>{{ $ankets->nddno }}</strong></td>
				</tr>
				<tr>
					<td>Гадаад паспорт:</td>
					<td><strong>{{ $ankets->fpno }}</strong></td>
				</tr>
				<tr>
					<td>Жолооны үнэмлэх №:</td>
					<td>
						үнэмлэх № <strong>{{ $ankets->driveno }}</strong><br>
						Ангилал <strong>{{ $ankets->angilal }}</strong><br>
						Барьсан жил <strong>{{ $ankets->barisanjil }}</strong>
					</td>
				</tr>
				<tr>
					<td>Гар утас:</td>
					<td> <strong>{{ $ankets->mobile }} </strong></td>
				</tr>
				<tr>
					<td>Шаардлагатай тохиолдолд холбоо барих хүний утас</td>
					<td> <strong>{{ $ankets->musthavephoneno }} </strong></td>
				</tr>
				<tr>
					<td>Хувийн и-мэйл хаяг:</td>
					<td><strong>{{ $ankets->email }}</strong></td>
				</tr>
				<tr>
					<td>Ажлын и-мэйл хаяг:</td>
					<td><strong>{{ $ankets->workemail }}</strong></td>
				</tr>
				<tr>
					<td><p>Боловсролын мэдээлэл:</p></td>
					<td>
						<strong>{{ $ankets->bolowsrol }}</strong>
					</td>
				</tr>
				<tr>
					<td>Мэргэжил:</td>
					<td><strong>{{ $ankets->job }}</strong></td>
				</tr>
				<tr>
					<td>Мэргэжлийн зэрэг:</td>
					<td><strong>{{ $ankets->jobrank }}</strong></td>
				</tr>
				<tr>
					<td><p>Гэр бүлийн байдал: </p></td>
					<td>
						<strong>{{ $ankets->marredornot }}</strong>
					</td>
				</tr>
				<tr>
					<td>Төрсөн улс:</td>
					<td><strong>{{ $ankets->borncnty }}</strong></td>
				</tr>
				<tr>
					<td>Төрсөн хот:</td>
					<td><strong>{{ $ankets->borncity }}</strong></td>
				</tr>
				<tr>
					<td>Төрсөн аймаг:</td>
					<td><strong>{{ $ankets->bornaimag }}</strong></td>
				</tr>
				<tr>
					<td>Төрсөн сум/дүүрэг:</td>
					<td><strong>{{ $ankets->bornduureg }}</strong></td>
				</tr>
				<tr>
					<td>Төрсөн он сар өдөр</td>
					<td><strong>{{ $ankets->birth }}</strong></td>
				</tr>
				<tr>
					<td>Хүйс: </td>
					<td>
						<strong>{{ $ankets->sex }}</strong>
					</td>
				</tr>
				<tr>
					<td>Цэргийн алба хаасан эсэх:</td>
					<td>
						<strong>{{ $ankets->tsereg }}</strong>
					</td>
				</tr>
				<tr>
					<td>Хэрэв тийм бол цэргийн батлах №:</td>
					<td><strong>{{ $ankets->alba_batlah_no }}</strong></td>
				</tr>
				<tr>
					<td>Нийгмийн гарал:</td>
					<td>
						<strong>{{ $ankets->niigmiingaral }}</strong>
					</td>
				</tr>
				<tr>
					<td>Жил:</td>
					<td><strong>{{ $ankets->jil }}</strong></td>
				</tr>
				<tr>
					<td>Орд:</td>
					<td><strong>{{ $ankets->ord }}</strong></td>
				</tr>
				<tr>
					<td>Facebook хаяг:</td>
					<td><strong>{{ $ankets->fbaddress }}</strong></td>
				</tr>
				<tr>
					<td>Twitter хаяг:</td>
					<td><strong>{{ $ankets->twitaddress }}</strong></td>
				</tr>
			</table>
			<br>
			<h3>II. АЖЛЫН ТУРШЛАГА</h3>
			<p><i>Та албан тушаал, ажлын байрны нэрийг бүтнээр бичнэ үү</i></p>
			<div style="overflow-x:auto;">
			<table>
				<tr>
					<td>№</td>
					<td>Одоогийн болон урьд ажиллаж байсан байгууллага, компанийн нэр</td>
					<td>Албан тушаал, ажлын байрны нэр</td>
					<td>Ажилд орсон он, сар, өдөр</td>
					<td>Ажлаас гарсан он, сар, өдөр</td>
					<td>Ажлаас гарсаншалтгаан</td>
				</tr>
				<tr>
					<td rowspan="2">1</td>
					<td><strong>{{ $ankets->old_work1 }}</strong></td>
					<td><strong>{{ $ankets->old_albantushaal1 }}</strong></td>
					<td><strong>{{ $ankets->old_work_enter1 }}</strong></td>
					<td><strong>{{ $ankets->old_work_out1 }}</strong></td>
					<td><strong>{{ $ankets->old_work_out_shaltgaan1 }}</strong></td>
				</tr>
				<tr>
					<td>Хийж гүйцэтгэсэн ажил үүргүүд /товч бичих/:</td>
					<td colspan="4"><strong>{{ $ankets->old_done_work1 }}</strong></td>
				</tr>
				<tr>
					<td rowspan="2">2</td>
					<td><strong>{{ $ankets->old_work2 }}</strong></td>
					<td><strong>{{ $ankets->old_albantushaal2 }}</strong></td>
					<td><strong>{{ $ankets->old_work_enter2 }}</strong></td>
					<td><strong>{{ $ankets->old_work_out2 }}</strong></td>
					<td><textarea rows="3" cols="40" name="old_work_out_shaltgaan2"></textarea></td>
				</tr>
				<tr>
					<td>Хийж гүйцэтгэсэн ажил үүргүүд /товч бичих/:</td>
					<td colspan="4"><textarea rows="3" cols="50" name="old_done_work2"></textarea></td>
				</tr>
				<tr>
					<td rowspan="2">3</td>
					<td><strong>{{ $ankets->old_work3 }}</strong></td>
					<td><strong>{{ $ankets->old_albantushaal3 }}</strong></td>
					<td><strong>{{ $ankets->old_work_enter3 }}</strong></td>
					<td><strong>{{ $ankets->old_work_out3 }}</strong></td>
					<td><strong>{{ $ankets->old_work_out_shaltgaan3 }}</strong></td>
				</tr>
				<tr>
					<td>Хийж гүйцэтгэсэн ажил үүргүүд /товч бичих/:</td>
					<td colspan="4"><strong>{{ $ankets->old_done_work3}}</strong></textarea></td>
				</tr>
				<tr>
					<td rowspan="2">4</td>
					<td><strong>{{ $ankets->old_work4 }}</strong></td>
					<td><strong>{{ $ankets->old_albantushaal4 }}</strong></td>
					<td><strong>{{ $ankets->old_work_enter4 }}</strong></td>
					<td><strong>{{ $ankets->old_work_out4 }}</strong></td>
					<td><strong>{{ $ankets->old_work_out_shaltgaan4 }}</strong></td>
				</tr>
				<tr>
					<td>Хийж гүйцэтгэсэн ажил үүргүүд /товч бичих/:</td>
					<td colspan="4"><strong>{{ $ankets->old_done_work4}}</strong></textarea></td>
				</tr>
				<tr>
					<td rowspan="2">5</td>
					<td><strong>{{ $ankets->old_work5 }}</strong></td>
					<td><strong>{{ $ankets->old_albantushaal5 }}</strong></td>
					<td><strong>{{ $ankets->old_work_enter5 }}</strong></td>
					<td><strong>{{ $ankets->old_work_out5 }}</strong></td>
					<td><strong>{{ $ankets->old_work_shaltgaan5 }}</strong></td>
				</tr>
				<tr>
					<td>Хийж гүйцэтгэсэн ажил үүргүүд /товч бичих/:</td>
					<td colspan="4"><strong>{{ $ankets->old_done_work5}}</strong></textarea></td>
				</tr>
			</table>
			</div>
			<p>Үүнээс: <strong>{{ $ankets->uunes}}</strong> өдрийн байдлаар:</p>
			<table>
				<tr>
					<td>Улсад ажилласан нийт:</td>
					<td>Говь ХК-д ажилласан нийт:</td>
					<td>Нийгмийн даатгал завсардсан:</td>
				</tr>
				<tr>
					<td>
						<strong>{{ $ankets->uls_jil }} </strong>
						<strong>{{ $ankets->uls_sar }} </strong>
					</td>
					<td><strong>{{ $ankets->gobi_jil}}</strong><strong>{{ $ankets->gobi_sar}}</strong></td>
					<td><strong>{{ $ankets->nd_zawsardsan_jil}}</strong><strong>{{ $ankets->nd_zawsardsan_sar}}</strong></td>
				</tr>
			</table>
			<p>Хэрэв та удаан хугацаагаар ажил хийгээгүй бол шалтгаанаа бичнэ үү.</p>
			<strong>{{ $ankets->udaan_workout}}</strong>

			<br>
			<h3>III. БОЛОВСРОЛ</h3>
			<p>3.1. Дунд, тусгай дунд, дээд боловсролын мэдээлэл</p>
			<div style="overflow-x:auto;">
			<table>
				<tr>
					<td>№ </td>
					<td></td>
					<td>Сургуулийн нэр</td>
					<td>Улс</td>
					<td>Хот</td>
					<td>Элссэн он, сар, өдөр</td>
					<td>Төгссөн он, сар, өдөр</td>
					<td>Эзэмшсэн мэргэжил</td>
					<td>Голч дүн</td>
					<td>Эрдмийн зэрэг</td>
				</tr>
				<tr>
					<td>1</td>
					<td>Дунд сургууль</td>
					<td><strong>{{ $ankets->dssurname }}</strong></td>
					<td><strong>{{ $ankets->dsuls }}</strong></td>
					<td><strong>{{ $ankets->dshot }}</strong></td>
					<td><strong>{{ $ankets->desbegindate }}</strong></td>
					<td<strong>{{ $ankets->dsenddate }}</strong></td>
					<td><strong>{{ $ankets->dsmergejil }}</strong></td>
					<td><strong>{{ $ankets->dsgolch }}</strong></td>
					<td><strong>{{ $ankets->dszereg }}</strong></td>
				</tr>
				<tr>
					<td>2</td>
					<td>Тусгай дунд, коллеж</td>
					<td><strong>{{ $ankets->ksurname }}</strong></td>
					<td><strong>{{ $ankets->kuls }}</strong></td>
					<td><strong>{{ $ankets->khot }}</strong></td>
					<td><strong>{{ $ankets->kbegindate }}</strong></td>
					<td><strong>{{ $ankets->kenddate }}</strong></td>
					<td><strong>{{ $ankets->kmergejil }}</strong></td>
					<td><strong>{{ $ankets->kgolch }}</strong></td>
					<td><strong>{{ $ankets->kzereg }}</strong></td>
				</tr>
				<tr>
					<td>3</td>
					<td>Их, дээд</td>
					<td><strong>{{ $ankets->id1surname }}</strong></td>
					<td><strong>{{ $ankets->id1uls }}</strong></td>
					<td><strong>{{ $ankets->id1hot }}</strong></td>
					<td><strong>{{ $ankets->id1begindate }}</strong></td>
					<td><strong>{{ $ankets->id1enddate }}</strong></td>
					<td><strong>{{ $ankets->id1mergejil }}</strong></td>
					<td><strong>{{ $ankets->id1golch }}</strong></td>
					<td><strong>{{ $ankets->id1zereg }}</strong></td>
				</tr>
				<tr>
					<td>4</td>
					<td>Их, дээд</td>
					<td><strong>{{ $ankets->id2surname }}</strong></td>
					<td><strong>{{ $ankets->id2uls }}</strong></td>
					<td><strong>{{ $ankets->id2hot }}</strong></td>
					<td><strong>{{ $ankets->id2begindate }}</strong></td>
					<td><strong>{{ $ankets->id2enddate }}</strong></td>
					<td><strong>{{ $ankets->id2mergejil }}</strong></td>
					<td><strong>{{ $ankets->id2golch }}</strong></td>
					<td><strong>{{ $ankets->id2zereg }}</strong></td>
				</tr>
				<tr>
					<td>5</td>
					<td>Бусад</td>
					<td><strong>{{ $ankets->busadsurname }}</strong></td>
					<td><strong>{{ $ankets->busaduls }}</strong></td>
					<td><strong>{{ $ankets->busadhot }}</strong></td>
					<td><strong>{{ $ankets->busadbegindate }}</strong></td>
					<td><strong>{{ $ankets->busadenddate }}</strong></td>
					<td><strong>{{ $ankets->busadmergejil }}</strong></td>
					<td><strong>{{ $ankets->busadgolch }}</strong></td>
					<td><strong>{{ $ankets->busadzereg }}</strong></td>
				</tr>
			</table>
			</div>
			<p>Дипломын ажлын сэдэв :</p>
			<strong>{{ $ankets->diplom}}</strong>
			<p>3.2. Мэргэжлээрээ болон бусад чиглэлээр хамрагдаж байсан сургалтууд</p>
			<div style="overflow-x:auto;">
			<table>
				<tr>
					<td>№</td>
					<td>Сургалтын байгууллагын нэр</td>
					<td>Улс, хот</td>
					<td>Элссэн он, сар, өдөр</td>
					<td>Төгссөн он, сар, өдөр</td>
					<td>Ямар чиглэлээр</td>
					<td>Үнэмлэх, сертификатын дугаар</td>
				</tr>
				<tr>
					<td>1</td>
					<td><strong>{{ $ankets->surgaltner1}}</strong></td>
					<td><strong>{{ $ankets->surgaltuls1}}</strong></td>
					<td><strong>{{ $ankets->surgaltbegindate1}}</strong></td>
					<td><strong>{{ $ankets->surgaltenddate1}}</strong></td>
					<td><strong>{{ $ankets->surgaltchiglel1}}</strong></td>
					<td><strong>{{ $ankets->surgaltunemleh1}}</strong></td>
				</tr>
				<tr>
					<td>2</td>
					<td><strong>{{ $ankets->surgaltner2}}</strong></td>
					<td><strong>{{ $ankets->surgaltuls2}}</strong></td>
					<td><strong>{{ $ankets->surgaltbegindate2}}</strong></td>
					<td><strong>{{ $ankets->surgaltenddate2}}</strong></td>
					<td><strong>{{ $ankets->surgaltchiglel2}}</strong></td>
					<td><strong>{{ $ankets->surgaltunemleh2}}</strong></td>
				</tr>
				<tr>
					<td>3</td>
					<td><strong>{{ $ankets->surgaltner3}}</strong></td>
					<td><strong>{{ $ankets->surgaltuls3}}</strong></td>
					<td><strong>{{ $ankets->surgaltbegindate3}}</strong></td>
					<td><strong>{{ $ankets->surgaltenddate3}}</strong></td>
					<td><strong>{{ $ankets->surgaltchiglel3}}</strong></td>
					<td><strong>{{ $ankets->surgaltunemleh3}}</strong></td>
				</tr>
			</table>
			</div>
			<br>
			<h3>IV. ГЭР БҮЛИЙН МЭДЭЭЛЭЛ</h3>
			<p>4.1. Ам бүлийн тоо:<strong>{{ $ankets->family}}</strong></p>
			<p>4.2. Ам бүлийн гишүүдийн мэдээлэл <i>/Зөвхөн хамт амьдардаг хүмүүсийг бичнэ үү /</i></p>
			<div style="overflow-x:auto;">
			<table>
				<tr>
					<td>№</td>
					<td>Таны юу болох</td>
					<td>Овог</td>
					<td>Нэр</td>
					<td>Төрсөн он, сар, өдөр</td>
					<td>Төрсөн газар</td>
					<td>Хаана ямар байгууллагад ажилладаг вэ?</td>
					<td>Албан тушаал </td>
					<td>Холбоо барих утасны дугаар</td>
				</tr>
				<tr>
					<td>1</td>
					<td><strong>{{ $ankets->famhamaatan}}</strong></td>
					<td><strong>{{ $ankets->famovog}}</strong></td>
					<td><strong>{{ $ankets->famner}}</strong></td>
					<td><strong>{{ $ankets->fambirthdate}}</strong></td>
					<td><strong>{{ $ankets->fambirthplace}}</strong></td>
					<td><strong>{{ $ankets->famwork}}</strong></td>
					<td><strong>{{ $ankets->famjob}}</strong></td>
					<td><strong>{{ $ankets->famphone}}</strong></td>
				</tr>
				<tr>
					<td>2</td>
					<td><strong>{{ $ankets->famhamaatan2}}</strong></td>
					<td><strong>{{ $ankets->famovog2}}</strong></td>
					<td><strong>{{ $ankets->famner2}}</strong></td>
					<td><strong>{{ $ankets->fambrithdate2}}</strong></td>
					<td><strong>{{ $ankets->fambrithplace2}}</strong></td>
					<td><strong>{{ $ankets->famwork2}}</strong></td>
					<td><strong>{{ $ankets->famjob2}}</strong></td>
					<td><strong>{{ $ankets->famphone2}}</strong></td>
				</tr>
				<tr>
					<td>3</td>
					<td><strong>{{ $ankets->famhamaatan3}}</strong></td>
					<td><strong>{{ $ankets->famovog3}}</strong></td>
					<td><strong>{{ $ankets->famner3}}</strong></td>
					<td><strong>{{ $ankets->fambirthdate3}}</strong></td>
					<td><strong>{{ $ankets->fambrithplace3}}</strong></td>
					<td><strong>{{ $ankets->famwork3}}</strong></td>
					<td><strong>{{ $ankets->famjob3}}</strong></td>
					<td><strong>{{ $ankets->famphone3}}</strong></td>
				</tr>
				<tr>
					<td>4</td>
					<td><strong>{{ $ankets->famhamaatan4}}</strong></td>
					<td><strong>{{ $ankets->famovog4}}</strong></td>
					<td><strong>{{ $ankets->famner4}}</strong></td>
					<td><strong>{{ $ankets->fambirthdate4}}</strong></td>
					<td><strong>{{ $ankets->fambirthplace4}}</strong></td>
					<td><strong>{{ $ankets->famwork4}}</strong></td>
					<td><strong>{{ $ankets->famjob4}}</strong></td>
					<td><strong>{{ $ankets->famphone4}}</strong></td>
				</tr>
				<tr>
					<td>5</td>
					<td><strong>{{ $ankets->famhamaatan5}}</strong></td>
					<td><strong>{{ $ankets->famovog5}}</strong></td>
					<td><strong>{{ $ankets->famner5}}</strong></td>
					<td><strong>{{ $ankets->fambirthdate5}}</strong></td>
					<td><strong>{{ $ankets->fambirthplace5}}</strong></td>
					<td><strong>{{ $ankets->famwork5}}</strong></td>
					<td><strong>{{ $ankets->famjob5}}</strong></td>
					<td><strong>{{ $ankets->famphone5}}</strong></td>
				</tr>
				<tr>
					<td>6</td>
					<td><strong>{{ $ankets->famhamaatan6}}</strong></td>
					<td><strong>{{ $ankets->famovog6}}</strong></td>
					<td><strong>{{ $ankets->famner6}}</strong></td>
					<td><strong>{{ $ankets->fambirthdate6}}</strong></td>
					<td><strong>{{ $ankets->fambirthplace6}}</strong></td>
					<td><strong>{{ $ankets->famwork6}}</strong></td>
					<td><strong>{{ $ankets->famjob6}}</strong></td>
					<td><strong>{{ $ankets->famphone6}}</strong></td>
				</tr>
			</table>
			</div>
			<p>4.3. Ураг төрлийн мэдээлэл <i>/Тусдаа амьдардаг төрсөн эцэг, эх, ах дүүсийг бичнэ үү /</i></p>
			<div style="overflow-x:auto;">
				<table>
					<tr>
						<td>№</td>
						<td>Таны юу болох</td>
						<td>Овог</td>
						<td>Нэр</td>
						<td>Төрсөн он, сар, өдөр</td>
						<td>Төрсөн газар</td>
						<td>Хаана ямар байгууллагад ажилладаг вэ?</td>
						<td>Албан тушаал </td>
						<td>Холбоо барих утасны дугаар</td>
					</tr>
					<tr>
						<td>1</td>
						<td><strong>{{ $ankets->famholhamaatan }}</strong></td>
						<td><strong>{{ $ankets->famholovog }}</strong></td>
						<td><strong>{{ $ankets->famholner }}</strong></td>
						<td><strong>{{ $ankets->famholbirthdate }}</strong></td>
						<td><strong>{{ $ankets->famholbirthplace }}</strong></td>
						<td><strong>{{ $ankets->famholwork }}</strong></td>
						<td><strong>{{ $ankets->famholjob }}</strong></td>
						<td><strong>{{ $ankets->famholphone }}</strong></td>
					</tr>
					<tr>
						<td>2</td>
						<td><strong>{{ $ankets->famholhamaatan2 }} </strong></td>
						<td><strong>{{ $ankets->famholovog2 }} </strong></td>
						<td><strong>{{ $ankets->famholner2 }} </strong></td>
						<td><strong>{{ $ankets->famholbirthdate2 }} </strong></td>
						<td><strong>{{ $ankets->famholbirthplace2 }} </strong></td>
						<td><strong>{{ $ankets->famholwork2 }} </strong></td>
						<td><strong>{{ $ankets->famholjob2 }} </strong></td>
						<td><strong>{{ $ankets->famholphone2 }} </strong></td>
					</tr>
					<tr>
						<td>3</td>
						<td><strong>{{ $ankets->famholhamaatan3 }} </strong></td>
						<td><strong>{{ $ankets->famholovog3 }} </strong></td>
						<td><strong>{{ $ankets->famholner3 }} </strong></td>
						<td><strong>{{ $ankets->famholbirthdate3 }} </strong></td>
						<td><strong>{{ $ankets->famholbirthplace3 }} </strong></td>
						<td><strong>{{ $ankets->famholwork3 }} </strong></td>
						<td><strong>{{ $ankets->famholjob3 }} </strong></td>
						<td><strong>{{ $ankets->famholphone3 }} </strong></td>
					</tr>
					<tr>
						<td>4</td>
						<td><strong>{{ $ankets->famholhamaatan4 }} </strong></td>
						<td><strong>{{ $ankets->famholovog4 }} </strong></td>
						<td><strong>{{ $ankets->famholner4 }} </strong></td>
						<td><strong>{{ $ankets->famholbirthdate4 }} </strong></td>
						<td><strong>{{ $ankets->famholbirthplace4 }} </strong></td>
						<td><strong>{{ $ankets->famholwork4 }} </strong></td>
						<td><strong>{{ $ankets->famholjob4 }} </strong></td>
						<td><strong>{{ $ankets->famholphone4 }} </strong></td>
					</tr>
				</table>
			</div>
			<p>4.4. Оршин суугаа хаяг <i>/Тодорхой бичнэ үү/</i></p>
			<table>
				<tr>
					<td>4.3.1. Иргэний үнэмлэх дээр бүртгэлтэй хаяг:</td>
					<td>4.3.2. Одоо оршин суугаа хаяг:</td>
				</tr>
				<tr>
					<td><strong>{{ $ankets->burtgeltei_hayag }}</strong></td>
					<td><strong>{{ $ankets->odoo_bgaa_hayag }}</strong></td>
				</tr>
			</table>
			<p>4.5. Орон сууцны нөхцөл <i>/Сонгоно уу/</i></p>
			<strong>{{ $ankets->osnuhtsul }}</strong>
			<p> 4.6. Өөрийн байр эсэх<i>/Сонгоно уу/</i></p>
			<strong>{{ $ankets->mineornot }}</strong>
			<p>5.1. Та байгууллага, яам, салбар, нийслэлийн болон төрийн шагнал авсан бол бичнэ үү.</p>
			<div style="overflow-x:auto;">
				<table>
					<tr>
						<td>№</td>
						<td>Шагнал олгосон газар</td>
						<td>Шагналын нэр</td>
						<td>Шагнагдсан он, сар, өдөр</td>
						<td>Хаана ажиллаж байх хугацаанд шагнагдсан бэ?</td>
						<td>Шагнагдсан үндэслэл</td>
					</tr>
					<tr>
						<td>1</td>
						<td><strong>{{ $ankets->shagnasan_gazar }}</strong></td>
						<td><strong>{{ $ankets->shagnal_ner }}</strong></td>
						<td><strong>{{ $ankets->shagnal_date }}</strong></td>
						<td><strong>{{ $ankets->shagnal_haana }}</strong></td>
						<td><strong>{{ $ankets->shagnal_undeslel }}</strong></td>
					</tr>
					<tr>
						<td>2</td>
						<td><strong>{{ $ankets->shagnasan_gazar2 }} </strong></td>
						<td><strong>{{ $ankets->shagnal_ner2 }} </strong></td>
						<td><strong>{{ $ankets->shagnal_date2 }} </strong></td>
						<td><strong>{{ $ankets->shagnal_haana2 }} </strong></td>
						<td><strong>{{ $ankets->shagnal_undeslel2 }} </strong></td>
					</tr>
					<tr>
						<td>3</td>
						<td><strong>{{ $ankets->shagnasan_gazar3 }}</strong></td>
						<td><strong>{{ $ankets->shagnal_ner3 }}</strong></td>
						<td><strong>{{ $ankets->shagnal_date3 }}</strong></td>
						<td><strong>{{ $ankets->shagnal_haana3 }}</strong></td>
						<td><strong>{{ $ankets->shagnal_undeslel3 }}</strong></td>
					</tr>
					<tr>
						<td>4</td>
						<td><strong>{{ $ankets->shagnasan_gazar4 }} </strong></td>
						<td><strong>{{ $ankets->shagnal_ner4 }} </strong></td>
						<td><strong>{{ $ankets->shagnal_date4 }} </strong></td>
						<td><strong>{{ $ankets->shagnal_haana4 }} </strong></td>
						<td><strong>{{ $ankets->shagnal_undeslel4 }} </strong></td>
					</tr>
					<tr>
						<td>5</td>
						<td><strong>{{ $ankets->shagnasan_gazar5 }}</strong></td>
						<td><strong>{{ $ankets->shagnal_ner5 }}</strong></td>
						<td><strong>{{ $ankets->shagnal_date5 }}</strong></td>
						<td><strong>{{ $ankets->shagnal_haana5 }}</strong></td>
						<td><strong>{{ $ankets->shagnal_undeslel5 }}</strong></td>
					</tr>
				</table>
			</div>
			<h3>VI. УР ЧАДВАРЫН МЭДЭЭЛЭЛ</h3>
			<p>6.1. Гадаад хэлний мэдлэг</p>
			<table>
				<tr>
					<td rowspan="2"></td>
					<td rowspan="2">Гадаад хэлний нэр</td>
					<td colspan="4" style="text-align: center;">Түвшин</td>
				</tr>
				<tr>
				</tr>
				<tr>
					<td rowspan="5">Ярьсныг ойлгох</td>
					<td>Англи</td>
					<td><strong>{{ $ankets->eng_speak_undrstnd }}</strong></td>
				</tr>
				<tr>
					<td>Орос</td>
					<td><strong>{{ $ankets->rus_speak_undrstnd }}</strong></td>
				</tr>
				<tr>
					<td>Япон</td>
					<td><strong>{{ $ankets->jap_speak_undrstnd }}</strong></td>
				</tr>
				<tr>
					<td>Хятад</td>
					<td><strong>{{ $ankets->chi_speak_undrstnd }}</strong></td>
				</tr>
				<tr>
					<td><strong>{{ $ankets->flang1 }}</strong></td>
					<td><strong>{{ $ankets->flang_speak_undrstnd }}</strong></td>
				</tr>
				<tr>
					<td rowspan="5">Ярих</td>
					<td>Англи</td>
					<td><strong>{{ $ankets->eng_speak }}</strong></td>
				</tr>
				<tr>
					<td>Орос</td>
					<td><strong>{{ $ankets->rus_speak }}</strong></td>
				</tr>
				<tr>
					<td>Япон</td>
					<td><strong>{{ $ankets->jap_speak }}</strong></td>
				</tr>
				<tr>
					<td>Хятад</td>
					<td><strong>{{ $ankets->chi_speak }}</strong></td>
				</tr>
				<tr>
					<td><strong>{{ $ankets->flang2 }}</strong></td>
					<td><strong>{{ $ankets->flang_speak }}</strong></td>
				</tr>
				<tr>
					<td rowspan="5">Уншиж ойлгох</td>
					<td>Англи</td>
					<td><strong>{{ $ankets->eng_read }}</strong></td>
				</tr>
				<tr>
					<td>Орос</td>
					<td><strong>{{ $ankets->rus_read }}</strong></td>
				</tr>
				<tr>
					<td>Япон</td>
					<td><strong>{{ $ankets->jap_read }}</strong></td>
				</tr>
				<tr>
					<td>Хятад</td>
					<td><strong>{{ $ankets->chi_read }}</strong></td>
				</tr>
				<tr>
					<td><strong>{{ $ankets->flang4 }}</strong></td>
					<td><strong>{{ $ankets->flang_read }}</strong></td>
				</tr>
				<tr>
					<td rowspan="5">Бичиж орчуулах</td>
					<td>Англи</td>
					<td><strong>{{ $ankets->eng_write }}</strong></td>
				</tr>
				<tr>
					<td>Орос</td>
					<td><strong>{{ $ankets->rus_write }}</strong></td>
				</tr>
				<tr>
					<td>Япон</td>
					<td><strong>{{ $ankets->jap_write }}</strong></td>
				</tr>
				<tr>
					<td>Хятад</td>
					<td><strong>{{ $ankets->chi_write }}</strong></td>
				</tr>
				<tr>
					<td><strong>{{ $ankets->flang4 }}</strong></td>
					<td><strong>{{ $ankets->flang_write }}</strong></td>
				</tr>
			</table>

			<p>6.2. Компьютерийн мэдлэг</p>
			<table>
				<tr>
					<td rowspan="2">Програмын нэр</td>
					<td colspan="3">Програм эзэмшсэн байдал</td>
				</tr>
				<tr>
				</tr>
				<tr>
					<td>MS Word</td>
					<td><strong>{{ $ankets->Word }}</strong></td>
				</tr>
				<tr>
					<td>MS Excel</td>
					<td><strong>{{ $ankets->Excel }}</strong></td>
				</tr>
				<tr>
					<td>MS Access</td>
					<td><strong>{{ $ankets->Access }}</strong></td>
				</tr>
				<tr>
					<td>MS Project</td>
					<td><strong>{{ $ankets->Project }}</strong></td>
				</tr>
				<tr>
					<td>MS Power Point</td>
					<td><strong>{{ $ankets->pp }}</strong></td>
				</tr>
				<tr>
					<td>MS Publisher</td>
					<td><strong>{{ $ankets->Publisher }}</strong></td>
				</tr>
				<tr>
					<td>3D Max</td>
					<td><strong>{{ $ankets->dmax }}</strong></td>
				</tr>
				<tr>
					<td>Photoshop</td>
					<td><strong>{{ $ankets->ps }}</strong></td>
				</tr>
				<tr>
					<td>InDesign</td>
					<td><strong>{{ $ankets->InDesign }}</strong></td>
				</tr>
				<tr>
					<td>Corel Draw</td>
					<td><strong>{{ $ankets->Corel }}</strong></td>
				</tr>
				<tr>
					<td>Green ERP</td>
					<td><strong>{{ $ankets->ERP }}</strong></td>
				</tr>
				<tr>
					<td>MS Visio</td>
					<td><strong>{{ $ankets->Visio }}</strong></td>
				</tr>
				<tr>
					<td>Daimond</td>
					<td><strong>{{ $ankets->Daimond }}</strong></td>
				</tr>
				<tr>
					<td><strong>{{ $ankets->inputprogram3name }}</strong></td>
					<td><strong>{{ $ankets->inputprogram3 }}</strong></td>
				</tr>
				<tr>
					<td><strong>{{ $ankets->inputprogram1name }}</strong></td>
					<td><strong>{{ $ankets->inputprogram1 }}</strong></td>
				</tr>
				<tr>
					<td><strong>{{ $ankets->inputprogram2name }}</strong></td>
					<td><strong>{{ $ankets->inputprogram2 }}</strong></td>
				</tr>
			</table>
			<br>
			<table>
				<tr>
					<td></td>
					<td>Оноо</td>
					<td>Огноо</td>
				</tr>
				<tr>
					<td>TOEFL өгсөн эсэх</td>
					<td><strong>{{ $ankets->toeflname }}</strong></td>
					<td><strong>{{ $ankets->toefldate }}</strong></td>
				</tr>
				<tr>
					<td>IELTS өгсөн эсэх</td>
					<td><strong>{{ $ankets->ieltsname }}</strong></td>
					<td><strong>{{ $ankets->ieltsdate }}</strong></td>
				</tr>
				<tr>
					<td>HSK өгсөн эсэх</td>
					<td><strong>{{ $ankets->hskname }}</strong></td>
					<td><strong>{{ $ankets->hskdate }}</strong></td>
				</tr>
				<tr>
					<td>Бусад</td>
					<td><strong>{{ $ankets->othername }}</strong></td>
					<td><strong>{{ $ankets->otherdate }}</strong></td>
				</tr>
			</table>
			<p>6.3. Та урлаг, спортын төрлөөр хичээллэдэг үү?</p>
			<div style="overflow-x:auto;">
				<table>
					<tr>
						<td>№</td>
						<td>Урлаг, спортын төрөл</td>
						<td>Хэдэн жил хичээллэсэн</td>
						<td>Зэрэг, цол, шагналтай эсэх</td>
						<td>Олгосон байгууллага</td>
					</tr>
					<tr>
						<td>1</td>
						<td><strong>{{ $ankets->turul1 }}</strong></td>
						<td><strong>{{ $ankets->hicheelesenjil1 }}</strong></td>
						<td><strong>{{ $ankets->zereg1 }}</strong></td>
						<td><strong>{{ $ankets->baiguulga1 }}</strong></td>
					</tr>
					<tr>
						<td>2</td>
						<td><strong>{{ $ankets->turul2 }}</strong></td>
						<td><strong>{{ $ankets->hicheelesenjil2 }}</strong></td>
						<td><strong>{{ $ankets->zereg2 }}</strong></td>
						<td><strong>{{ $ankets->baiguulga2 }}</strong></td>
					</tr><tr>
						<td>3</td>
						<td><strong>{{ $ankets->turul3 }}</strong></td>
						<td><strong>{{ $ankets->hicheelesenjil3 }}</strong></td>
						<td><strong>{{ $ankets->zereg3 }}</strong></td>
						<td><strong>{{ $ankets->baiguulga3 }}</strong></td>
					</tr>
				</table>
			</div>
			<p>6.4. Таны сонирхол, хобби:</p>
			<strong>{{ $ankets->hobbi }}</strong>
			<p>6.5. Мэдлэг, ур чадварын тодорхойлолт <i>/Таны ажил мэргэжил, туршлага, ур чадвар, хувь хүний талаар дэлгэрэнгүй мэдээлэл өгч чадах 3 хүнийг бичнэ үү/</i></p>
			<div style="overflow-x:auto;">
				<table>
					<tr>
						<td>№</td>
						<td>Нэр</td>
						<td>Ажиллаж буй байгууллагын нэр</td>
						<td>Албан тушаал</td>
						<td>Утасны дугаар</td>
						<td>Таныг хэдэн жилийн<br>өмнөөс мэдэх</td>
					</tr>
					<tr>
						<td>1</td>
						<td><strong>{{ $ankets->ner1_6_5 }}</strong></td>
						<td><strong>{{ $ankets->baiguulga1_6_5 }}</strong></td>
						<td><strong>{{ $ankets->atushaal1_6_5 }}</strong></td>
						<td><strong>{{ $ankets->phone1_6_5 }}</strong></td>
						<td><strong>{{ $ankets->hedenjil1_6_5 }}</strong></td>
					</tr>
					<tr>
						<td>2</td>
						<td><strong>{{ $ankets->ner2_6_5 }}</strong></td>
						<td><strong>{{ $ankets->baiguulga2_6_5 }}</strong></td>
						<td><strong>{{ $ankets->atushaal2_6_5 }}</strong></td>
						<td><strong>{{ $ankets->phone2_6_5 }}</strong></td>
						<td><strong>{{ $ankets->hedenjil2_6_5 }}</strong></td>
					</tr>
					<tr>
						<td>3</td>
						<td><strong>{{ $ankets->ner3_6_5 }}</strong></td>
						<td><strong>{{ $ankets->baiguulga3_6_5 }}</strong></td>
						<td><strong>{{ $ankets->atushaal3_6_5 }}</strong></td>
						<td><strong>{{ $ankets->phone3_6_5 }}</strong></td>
						<td><strong>{{ $ankets->hedenjil3_6_5 }}</strong></td>
					</tr>
				</table>
			</div>
			<h3>VII. БУСАД МЭДЭЭЛЭЛ</h3>
			<p>7.1. Хувь хүний онцлогууд</p>
			<table>
				<tr>
					<td>Таны давуу тал</td>
					<td>Таны сайжруулах шаардлагатай тал</td>
				</tr>
				<tr>
					<td><strong>{{ $ankets->dawuutal }}</strong></td>
					<td><strong>{{ $ankets->sultal }}</strong></td>
				</tr>
			</table>
			<p>7.2. Ойрын 1-3 жилийн зорилго</p>
			<table>
				<tr>
					<td>Хувь хүний хөгжил <i>/өөрийгөө хөгжүүлэх/</i></td>
					<td>Гэр бүлийн талаарх төлөвлөлт <i>/гэр бүлтэй болох, хүүхэд<br>төрүүлэх, орон сууц авах гэх мэт/</i></td>
					<td>Албан тушаалын төлөвлөлт</td>
				</tr>
				<tr>
					<td><strong>{{ $ankets->hhhugjil }}</strong></td>
					<td><strong>{{ $ankets->gerbultuluwlult }}</strong></td>
					<td><strong>{{ $ankets->albantushaal }}</strong></td>
				</tr>
			</table>
			<p>7.3. Танд өвддөг хууч өвчин, эрүүл мэндийн хувьд анхаарах ямар нэгэн зовиур байдаг уу? </p>
			<strong>{{ $ankets->uwchin }}</strong>
			<p>7.4. Та ажлын байрны талаарх мэдээллийг ямар эх сурвалжаас авсан бэ?</p>
			<td><strong>{{ $ankets->ehsurwalj }}</strong></td>
			<p>7.5. Таны хамаатан садан, найз нөхөд Говь ХК-д ажилладаг уу? </p>
			<strong>{{ $ankets->gobidajildag }}</strong>
			Хэрэв тийм бол: Таны хэн болох <strong>{{ $ankets->henboloh }}</strong> Албаны тушаал <strong>{{ $ankets->albantushaal7_5 }}</strong> Нэр: <strong>{{ $ankets->ner7_5 }}</strong>
			<p>7.6. Та манай компанид хэдэн жил ажиллахаар төлөвлөж байна вэ?</p>
			<td><strong>{{ $ankets->herudah }}</strong></td>
			<p>7.7. Өөрийнхөө талаар нэмж танилцуулах боломж</p>
			<td><strong>{{ $ankets->uuriinhuutalaar }}</strong></td>
			<p>7.8. Ажилд орох боломжтой хугацаа: <strong>{{ $ankets->ajildoroh }}</strong></p>
			<p>Энэхүү дэлгэрэнгүй бүртгэл нь байгууллагын өмч бөгөөд архивт 50 жил хадгалагдана. Монгол Улсын "Байгууллагын нууцын тухай", "Хувь
			хүний нууцын тухай" хуулийн дагуу дээрх мэдээлэлтэй зөвхөн холбогдох эрх бүхий албан тушаалтан танилцах эрхтэй.</p>
			</div>
		</div>
	</div>
 <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

</body>
</html>