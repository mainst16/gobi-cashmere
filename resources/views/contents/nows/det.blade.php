@extends('contents.now')
@section('customstyles')
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>

{
  box-sizing: border-box;
}

.row > .column {
  padding: 8px 8px;
}

.row:after {
  content: "";
  display: table;
  clear: both;
}

.column {
  float: left;
  width: 19%;
}

/* The Modal (background) */
.modal {
  display: none;
  position: fixed;
  z-index: 102;
  padding-top: 100px;
  /*margin-top: 156px;*/
  left: 0;
  top: 0;
  width: 100%;
  height: 100%;
  overflow: auto;
  background-color: #fff;
}

/* Modal Content */
.modal-content {
  position: relative;
  /*background-color: #fefefe;*/
  margin: auto;
  padding: 0;
  width: 100%;
  max-width: 1200px;
}

/* The Close Button */
.close {
  color: #999;
  position: absolute;
  top: 10px;
  right: 25px;
  font-size: 35px;
  font-weight: bold;
}

.close:hover,
.close:focus {
  color: #999;
  text-decoration: none;
  cursor: pointer;
}

.mySlides {
  display: none;
}

.cursor {
  cursor: pointer
}

/* Next & previous buttons */
.prev,
.next {
  cursor: pointer;
  position: absolute;
  top: 50%;
  width: auto;
  padding: 16px;
  margin-top: -50px;
  color: #999;
  font-weight: bold;
  font-size: 20px;
  transition: 0.6s ease;
  border-radius: 0 3px 3px 0;
  user-select: none;
  -webkit-user-select: none;
}

/* Position the "next button" to the right */
.next {
  right: 0;
  border-radius: 3px 0 0 3px;
}

/* On hover, add a black background color with a little bit see-through */
.prev:hover,
.next:hover {
  background-color: rgba(0, 0, 0, 0.8);
}

/* Number text (1/3 etc) */
.numbertext {
  color: #f2f2f2;
  font-size: 12px;
  padding: 8px 12px;
  position: absolute;
  top: 0;
}

img {
  margin-bottom: -4px;
}

.caption-container {
  text-align: center;
  background-color: white;
  padding: 2px 16px;
  color: white;
}

.demo {
  opacity: 0.6;
}

.active,
.demo:hover {
  opacity: 1;
}

img.hover-shadow {
  transition: 0.3s;
}

.hover-shadow:hover {
  box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19)
}
</style>
@endsection
@section('content')
<div class="blogWrapper">
	<div class="blogPostDetail blogWideContentWrapper">
		<div id="post-main-408564" class="blogPostOuter">
			<div class="postTemplate7">
				<div class="postMeta">
					<h2 style="font-family: SamsungSans_th">{{ $readmore->title}}</h2>
				</div>
				<div class="postImage" align="center">
					<img style="width: 950px" src="{{ asset('storage/'.$readmore->pics.'')}}"/></p>
					<!-- <div class="wp-caption-text" align="center">Ээж охин</div> -->
				</div>
				<div class="postBody" style="padding: 25px  25% 0 25%;">
					<h3 style="text-align: center;">{{ $readmore->longtitle}}</h3>
					{!! $readmore->desc !!}
				</div>
				<div>
					@if( !empty ($pic) )
					<div class="row" style="    padding: 0px 20% 0% 28%;">
						@foreach(json_decode($pic, true) as $key=>$images)
					  <div class="column">
					  	<div class="product-tile__thumb-container js-product-tile-thumb" data-default-color="001">
							<a  class="product-tile__thumb" onclick="openModal();currentSlide({{$key}}+1)" style="background-image: url({{ asset('storage/'.$images)}}); background-size: auto 541px; background-repeat: no-repeat;background-position: center;">
					    		<!-- <img src="{{ asset('storage/'.$images)}}" style="height: 100%" onclick="openModal();currentSlide(1)" class="hover-shadow cursor"> -->
					    	</a>
					    </div>
					  </div>
					  @endforeach
					</div>
					
					
					<div id="myModal" class="modal">
					  <span class="close cursor" onclick="closeModal()">&times;</span>
					  <div class="modal-content">
					  	@foreach(json_decode($pic, true) as $images)
					    <div class="mySlides">
					      <div class="numbertext"></div>
					      <img src="{{ asset('storage/'.$images)}}" style="width:auto; height: 800px;margin: auto; display: block;">
					    </div>
						@endforeach	    
					    <a class="prev" onclick="plusSlides(-1)">&#10094;</a>
					    <a class="next" onclick="plusSlides(1)">&#10095;</a>

					    <div class="caption-container">
					      <p id="caption"></p>
					    </div>
					  </div>
					</div>
					@else
					<div></div>
					@endif
				</div>
				<!-- <div class="postLabels test">
					<label>Labels:</label>
					<a href="#">ХЭЛХГЭР ПИДЖАК</a>
				</div>
				<div class="clear"></div> -->
			</div>
		</div>
	</div>
	<div class="clear"></div>
</div>
@endsection
@section('customscripts')
<script>
function openModal() {
  document.getElementById('myModal').style.display = "block";
}

function closeModal() {
  document.getElementById('myModal').style.display = "none";
}

var slideIndex = 1;
showSlides(slideIndex);

function plusSlides(n) {
  showSlides(slideIndex += n);
}

function currentSlide(n) {
  showSlides(slideIndex = n);
}

function showSlides(n) {
  var i;
  var slides = document.getElementsByClassName("mySlides");
  var dots = document.getElementsByClassName("demo");
  var captionText = document.getElementById("caption");
  if (n > slides.length) {slideIndex = 1}
  if (n < 1) {slideIndex = slides.length}
  for (i = 0; i < slides.length; i++) {
      slides[i].style.display = "none";
  }
  for (i = 0; i < dots.length; i++) {
      dots[i].className = dots[i].className.replace(" active", "");
  }
  slides[slideIndex-1].style.display = "block";
  dots[slideIndex-1].className += " active";
  captionText.innerHTML = dots[slideIndex-1].alt;
}
</script>
@endsection