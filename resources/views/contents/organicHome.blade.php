@extends('Organic')

@section('content')
<div id="content">
  <div class="homepage">
    <div class="htmlslotcontainer mobile-available">
      <style>
        .hp-main {
          text-align: center;
          color: #191919;
        }
        .hp-main .asset-template__wrapper {position: relative;}
        .hp-main .header--xxl {font-size: 40px;}
        .body-copy--grey {color: #515152;}
        .hp-block {width: 100%; max-width: 1280px; margin-left: auto; margin-right: auto;position: relative;}
        .hp-block-copy.orange {color: #f77800;}
        .width-80 {width: 80%;margin: 0 auto;}
        .hp-block img { display: block; margin: 0 auto; max-width: 100%;}
        .hp-block-overlay {position: absolute;width: 100%; top: 50%; transform: translateY(-50%);text-align: center;}
        .hp-block-overlay.offset-10 {top: 60%;}
        .hp-block-overlay .cta-button {
            background-color: #fff;
            color: #191919;
            font-family: SweetSans,sans-serif;
            font-weight: 600;
            height: 50px;
            width: 200px;
            font-size: 10px;
            line-height: 50px;
            text-align: center;
            text-transform: uppercase;
            display: inline-block;
            text-decoration: none;
        }
        .width-50 {width: 50%;}
        .hp-block-cta-link a {color: #191919;text-decoration: none;border-bottom: 1px solid; padding-bottom: 2px;}
        .hp-block-overlay .header {color: #FFF;}
        .block-column {
          float: left;
          display: inline-block;
        }
        .block-column.left {
          width: 33%;
          margin-right: 100px;
        }
        .block-column.right {
          width: calc(67% - 100px);
        }
        .hp-slider .slick-dots li button {height: 8px; width: 8px;}
        .hp-slider .slick-dots li button:before {height: 8px; width: 8px;}
      </style>
      <div class="hp-main">
        <div class="main-hero asset-template asset-template--content-breaker-1" >
            <div class="asset-template__wrapper">     
                 <div class="asset-template__image-wrapper">
                    <video autoplay loop muted>
                          <source src="{{ asset('videos/govi1.mp4')}}" type="video/mp4"/>
                          Your browser does not support the video tag.
                     </video>
                </div>
                <div class="asset-template__content">
                    <span class="header header--xxl header--desktop">FALL / WINTER 17-18</span>
                    <div>
                        <div class="v-offset-top-m">
                            <a href="{{ url('collections/2')}}" class="cta-button ">
                                <span>
                                Шинэ коллекц
                                </span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- BLOCK 1 -->
        <div class="hp-block v-offset-xxxl">
          <h2 style="font-size: 40px; font-family: SamsungSans, sans-serif">GOBI ORGANIC CASHMERE.</h2>
          <div class="hp-block-copy body-copy body-copy--l body-copy--grey v-offset-top-s">
            Ноолуурийн байглийн үндсэн 4 өнгө болох цайвар, цайвар шаргал,<br> хөх саарал, бор өнгүүдийг ашигласан будаагүй, цайруулаагүй<br> GOBI ORGANIC CASHMERE брэнд.
          </div>
        </div>

        <!-- BLOCK 2 -->
        <div class="hp-block split-layout">

          <div class="block-column left">
            
            <div class="block-image v-offset-top-xxxl">
              <img src=" {{ asset('imgs/homepage/organic2.jpg') }} " />
              <div class="hp-block-copy title title--s v-offset-top-l">
                Эрэгтэй хувцасны загварууд
              </div>
              <div class="hp-block-cta-link title title--xs v-offset-top-s">
                <a href="{{ url('brand/2/1')}}">Дэлгэрэнгүй</a>
              </div>
            </div>
            <div class="block-image v-offset-top-xxl">
              <img src=" {{ asset('imgs/homepage/organic3.jpg') }} " />
              <div class="hp-block-copy title title--s v-offset-top-l">
                Organic аксесуар
              </div>
              <div class="hp-block-cta-link title title--xs v-offset-top-s">
                <a href="{{ url('brand/2/4')}}">Дэлгэрэнгүй</a>
              </div>
            </div>
          </div>

          <div class="block-column right">
            <div class="block-image">
              <img src=" {{ asset('imgs/homepage/organic1.jpg') }} " />
              <div class="hp-block-copy title title--s v-offset-top-l">
                Эмэгтэй хувцасны загварууд
              </div>
              <div class="hp-block-cta-link title title--xs v-offset-top-s">
                <a href="{{ url('brand/2/2')}}">Дэлгэрэнгүй</a>
              </div>
            </div>
            
          </div>
          <div class="clear"></div>
        </div>

        <!-- BLOCK 3 -->
        <!-- end blocks -->
      </div>
    </div>
    <div class="mobilehtmlslotcontainer">
      <style>
        @media screen and (max-width: 770px) {
            .hp-main {
              text-align: center;
              color: #191919;
            }
            .main-hero.asset-template:not(.asset-template--mobile-header) {display: block;}
            .hp-main .body-copy--xl {font-size: 25px;}
            .hp-main .title--xl {font-size: 20px;}
            .body-copy--grey {color: #515152; font-size: 12.5px}
            .hp-block {width: 100%; max-width: 1200px; margin-left: auto; margin-right: auto;position: relative;}
            .hp-block-copy.orange.header--l {color: #f77800; font-size: 22.5px}
            .width-80 {width: calc(100% - 60px);margin-left: auto; margin-right: auto;}
            .width-sm {width: 60%; margin-left: auto; margin-right: auto;}
            .hp-block img { display: block; margin: 0 auto; max-width: 100%;}
            .hp-block-overlay {position: absolute;width: 100%; top: 50%; transform: translateY(-50%);}
             .hp-block-overlay.offset-10 {top: 60%;}
            .hp-block-overlay.negative-offset-5 {top: 45%;}
            .cta-button {
                background-color: #fff;
                color: #191919;
                font-family: SweetSans,sans-serif;
                font-weight: 600;
                height: 50px;
                width: 200px;
                font-size: 10px;
                line-height: 50px;
                text-align: center;
                text-transform: uppercase;
                display: inline-block;
                text-decoration: none;
            }
            .cta-button.outline {
                border: 1px solid;
            }
            .hp-block-cta-link a {color: #191919;text-decoration: none;border-bottom: 1px solid; padding-bottom: 2px;}
            .hp-block-overlay .header {color: #FFF;}
            .block-column {
              float: left;
              display: inline-block;
            }
            .block-column.left {
              width: calc(50% - 30px);
              margin-left: 20px;
              margin-right: 10px;
            }
            .block-column.right {
              width: calc(50% - 30px);
              margin-right: 20px;
              margin-left: 10px;
            }

        }
      </style>
      <div class="hp-main">
        <div class="main-hero asset-template asset-template--content-breaker-1" >
            <div class="asset-template__wrapper">     
                 <div class="asset-template__image-wrapper">
                    <a href="{{ url('collections/2')}}">
                          <img src="{{ asset('imgs/homepage/organic17.jpg') }}" />
                          <span class="header header--l header--mobile">Fall/ winter 17-18</span>
                    </a>
                    <div class="hp-block-overlay">
                        <span class="header header--l">Fall/ winter 17-18</span>     
                    </div>
                </div>
            </div>
        </div>
        <!-- BLOCK 1 -->
        <div class="hp-block v-offset-xl">
          <h2 style="font-size: 30px; font-family: SamsungSans, sans-serif">GOBI ORGANIC CASHMERE.</h2>
          <div class="hp-block-copy body-copy body-copy--grey v-offset-top-l">
           Ноолуурийн байглийн үндсэн 4 өнгө болох цайвар, цайвар шаргал,<br> хөх саарал, бор өнгүүдийг ашигласан будаагүй, цайруулаагүй<br> GOBI ORGANIC CASHMERE брэнд.
          </div>
        </div>

        <!-- BLOCK 2 -->
        <div class="hp-block split-layout v-offset-top-xxl">

          <div class="block-column left">
            <div class="block-image ">
              <img src="{{ asset('imgs/homepage/organic1.jpg') }}" />
              <div class="hp-block-copy title title--xs v-offset-top-m">
                Эмэгтэй хувцасны загварууд
              </div>
              <div class="hp-block-cta-link title title--xs v-offset-top-s">
                <a href="{{ url('brand/2/2')}}">Дэлгэрэнгүй</a>
              </div>
            </div>
          </div>

          <div class="block-column right">
              <div class="block-image v-offset-top-xxl">
                  <img src=" {{ asset('imgs/homepage/organic2.jpg') }} " />
                  <div class="hp-block-copy title title--xs v-offset-top-m">
                      Эрэгтэй хувцасны загварууд
                  </div>
                  <div class="hp-block-cta-link title title--xs v-offset-top-s">
                      <a href="{{ url('brand/2/1')}}">Дэлгэрэнгүй</a>
                  </div>
              </div>
          </div>

          <div class="clear"></div>
        </div>
        <!-- BLOCK 3 -->
        <div class="hp-block v-offset-xl">
            <div class="width-sm">
                 <img src=" {{ asset('imgs/homepage/organic3.jpg') }} " />
            </div>
            <div class="hp-block-copy title title--xs v-offset-top-m">
                      Organic аксесуар
                  </div>
            <div class="hp-block-cta-link title title--xs v-offset-top-s">
                <a href="{{ url('brand/2/4')}}">Дэлгэрэнгүй</a>
            </div>
        </div>

        <!-- BLOCK 4 -->
        
        <!-- end blocks -->
      </div>
    </div>
    <div class="htmlslotcontainer mobile-available">
      <style>
        .hp-tiles-container {max-width: 1194px; margin: 0 auto;}
        .hp-product-tile {
        display: inline-block;
        float:left;
        margin: 0 8px;
        width: calc(20% - 16px);
        }
        .hp-product-tile img {
        max-width:100%;
        margin: 0 auto; 
        display: block;
        }
        .hp-product-tile--copy {
           color: #191919;
        text-align: center;
        font-size:10px;
        margin-top: 16px;
        font-weight: 600;
        }
        .hp-product-tile--copy:hover {
            opacity: 0.6;
        }
        .hp-slider  {
            width: 100%; 
            max-width: 980px;
            margin-left: auto;
            margin-right: auto;
            overflow: hidden;
        }
        .hp-slider img {
            width: 100%;
            max-width: 100%;
        }
        .hp-slide {
            position: relative;
        }
        .hp-slide a {
            display: block;
        }
        .slide-overlay {
            position: absolute;
            color: #191919;
        }
        .hp-block-overlay.slide-overlay .header {
            color: #191919;
        }
        .slide-overlay.position-1 {
            left: 12%;
            width: auto;
            top: 50%;
            transform: translateY(-50%);
            text-align: left;
        }
        .slide-overlay.position-2 {
            left: 0px;
            width: 100%;
            text-align: center;
            top: 0;
            transform: none;
            margin-top: 80px;
        }
        .slide-overlay.position-3 {
            right: 0;
            width: 55%;
        }
        .slide-overlay.position-4 {width: 33%;right: 2%; text-align:center;}
        .slide-overlay.position-3 .cta-button {background-color: transparent; color: #FFF; border: 1px solid #FFF;}
        .slide-overlay.position-3 .header,  .slide-overlay.position-3 .body-copy{
            color: #FFF;
        }
        .slide-overlay.position-4 .cta-button {border: 1px solid #191919;}
        .slick-dots li button:before {height: 12px; width: 12px;}

        @media screen and (max-width: 1024px) {
            .slick-track {max-width: none;}
        }
      </style>
      <!-- BLOCK 4 -->
      <!-- <div class="hp-tiles-container">

        <div class="hp-product-tile">
            <a href="#/handbags-newarrivals/">
                 <img alt="" src="https://static.toryburch.com/static/on/demandware.static/-/Sites/default/dwd7164737/slots/homepage/3.9hp/TBUS_SS18_3.9_1_HP_Desktop_09_Tiles_HB_3x.jpg" title="" />
                 <div class="hp-product-tile--copy">
                     HANDBAGS
                 </div>
             </a>
        </div>
        <div class="hp-product-tile">
            <a href="#/shoes/sneakers/">
                 <img alt="" src="https://static.toryburch.com/static/on/demandware.static/-/Sites/default/dwa5dad4c6/slots/homepage/3.9hp/TBUS_SS18_3.9_1_HP_Desktop_10_Tiles_SNEAKS_3x.jpg" title="" />
                 <div class="hp-product-tile--copy">
                    SNEAKERS
                 </div>
             </a>
        </div>
        <div class="hp-product-tile">
            <a href="#/shoes/mules-slides/">
                 <img alt="" src="https://static.toryburch.com/static/on/demandware.static/-/Sites/default/dw18e3581f/slots/homepage/3.9hp/TBUS_SS18_3.9_1_HP_Desktop_12_Tiles_SLIDES_3x.jpg" title="" />
                 <div class="hp-product-tile--copy">
                     MULES & SLIDES
                 </div>
             </a>
        </div>
        <div class="hp-product-tile">
            <a href="#/accessories/jewelry/">
                 <img alt="" src="https://static.toryburch.com/static/on/demandware.static/-/Sites/default/dw6fb76cb1/slots/homepage/3.9hp/TBUS_SS18_3.9_1_HP_Desktop_11_Tiles_ACC_3x.jpg" title="" />
                 <div class="hp-product-tile--copy">
                     JEWELRY
                 </div>
             </a>
        </div>

        <div class="hp-product-tile">
            <a href="#/clothing/new-arrivals/">
                 <img alt="" src="https://static.toryburch.com/static/on/demandware.static/-/Sites/default/dw2832b13a/slots/homepage/3.9hp/TBUS_SS18_3.9_1_HP_Desktop_13_Tiles_CLOTH_3x.jpg" title="" />
                 <div class="hp-product-tile--copy">
                     CLOTHING
                 </div>
             </a>
        </div>
        <div class="clear"></div>
      </div> -->
      <script>
         jQuery(document).ready(function () {
             var slickOptions = {
                 "infinite": true,
                 "arrows": false,
                 "dots": true,
                 "swipeToSlide": true,
                 "speed": 250,
                 "easing": "easeOutCubic",
                 "slide": ".hp-slide",
                 "autoplay": true,
                 "autoplaySpeed": 5000
             };
             if($(window).width() > 767) {
                  $("#hp-desktop-slider").slick(slickOptions);
            } else {
                if ($("#hp-desktop-slider").hasClass('slick-initialized')) {
                    $("#hp-desktop-slider").slick("unslick");
                }
            }

            $(window).resize(function () {
                if($(window).width() > 767) {
                    if (!$("#hp-desktop-slider").hasClass('slick-initialized')) {
                           $("#hp-desktop-slider").slick(slickOptions);
                    }
                } else {
                    if ($("#hp-desktop-slider").hasClass('slick-initialized')) {
                       $("#hp-desktop-slider").slick("unslick");
                    }
                }
            });
            
         });
      </script>
    </div>
    <div class="mobilehtmlslotcontainer">
      <style>
        @media screen and (max-width: 767px) {
        .hp-product-tile {
        display: inline-block;
        float:left;
        margin: 16px 16px;
        width: calc(50% - 32px);
        }
        .hp-product-tile img {
        max-width:100%;
        margin: 0 auto; 
        display: block;
        }
        .hp-product-tile--copy {
           color: #191919;
        text-align: center;
        font-size:10px;
        margin-top: 16px;
        }

        .hp-slider  {
            width: 100%;
        }
        .hp-slider img {
            width: 100%;
            max-width: 100%;
        }
        .hp-slide {
            position: relative;
        }
        .hp-slide a {
            display: block;
        }
        .slide-overlay {
            position: absolute;
            color: #191919;
        }
        .hp-block-overlay.slide-overlay .header {
            color: #191919;
        }
        .slide-overlay.position-2 {
            left: 0px;
            width: 100%;
            text-align: center;
            top: 0;
            transform: none;
            margin-top: 60px;
        }
        .slide-overlay.position-3 {
            left: 0px;
            width: 100%;
            bottom : 200px;
            transform: none;
            margin-top: 20px;
             text-align: left;
        }
        .slide-overlay.position-3 .slide-overlay-inner {margin: 0 auto; width: 69%;}
        .slide-overlay.position-3 .cta-button {background-color: transparent; color: #FFF; border: 1px solid #FFF;}
        .slide-overlay.position-3 .header,  .slide-overlay.position-3 .body-copy{
            color: #FFF;
        }

        .slide-overlay.position-4 {
            left: 0px;
            top: unset;
            width: 100%;
            bottom : 100px;
            transform: none;
            text-align: center;
        }
        .slide-overlay.position-4 .cta-button {border: 1px solid #191919;}
        .body-copy.body-copy--xs {font-size: 14px;}
        .slick-track {max-width: none;}
        .hp-mobile-slider .slick-dots li button:before {height: 7px;width: 7px;}
        .hp-mobile-slider .slick-prev {left: 0px;}
        .hp-mobile-slider .slick-prev:before {
        background-image: none;
        opacity: 50%;
        width: 0;
        height: 0;
        border: 0 solid transparent;
        border-top-width: 10px;
        border-bottom-width: 10px;
        border-right: 9px solid rgba(0,0,0,0.3);
        }
        .hp-mobile-slider .slick-next {right: 10px;}
        .hp-mobile-slider .slick-next:before {
        background-image: none;
        opacity: 50%;
        width: 0;
        height: 0;
        border: 0 solid transparent;
        border-bottom-width: 10px;
        border-top-width: 10px;
        border-left: 9px solid rgba(0,0,0,0.3);
        }
        .hp-slider .slick-dots {margin-top: 15px;}
        .hp-slider .slick-dots li {margin-top: 0;}
        }

        @media screen and (max-width: 414px) {
            .slide-overlay.position-2 {margin-top: 40px}
            .slide-overlay.position-3 .slide-overlay-inner {width: 70%;}
            .slide-overlay.position-4 {bottom: 50px;}
        }
        @media screen and (max-width: 375px) {
            .slide-overlay.position-2 {margin-top: 40px}
            .slide-overlay.position-3 .slide-overlay-inner {width: 70%;}
            .slide-overlay.position-4 {bottom: 35px;}
        }
      </style>
      
      <!-- BLOCK 5 -->
      <!-- <div class="hp-block v-offset-top-l v-offset-bottom-s">
          <div class="parallax-content ">             
            <img src="https://static.toryburch.com/static/on/demandware.static/-/Sites/default/dw7b819c85/slots/homepage/3.9hp/mob/TBUS_SS18_3.9_1_HP_Mobile_07_ToryDaily_3x.jpg" alt="">                 
          </div>
          <div class="hp-block-overlay offset-10">
             <span class="header header--xxl header--desktop"></span>     
             <div class="v-offset-top-m">
              <a href="http://www.toryburch.com/blog/torys-blog.html" class="cta-button title title--xs">
                <span>Read more</span>
              </a>
              </div>
          </div>
      </div> -->
      <!-- <div class="hp-tiles-container">
          <div class="hp-product-tile">
              <a href="#/clothing/new-arrivals/">
                   <img alt="" src="https://static.toryburch.com/static/on/demandware.static/-/Sites/default/dw9b2c3f75/slots/homepage/3.9hp/mob/TBUS_SS18_3.9_1_HP_Mobile_09_Tile_Clothing_3x.jpg" title="" />
                   <div class="hp-product-tile--copy">
                       CLOTHING
                   </div>
               </a>
          </div>
          <div class="hp-product-tile">
              <a href="#/handbags-newarrivals/">
                   <img alt="" src="https://static.toryburch.com/static/on/demandware.static/-/Sites/default/dw3ceffe53/slots/homepage/3.9hp/mob/TBUS_SS18_3.9_1_HP_Mobile_10_Tile_Handbags_3x.jpg" title="" />
                   <div class="hp-product-tile--copy">
                       HANDBAGS
                   </div>
               </a>
          </div>
          <div class="hp-product-tile">
              <a href="#/shoes/mules-slides/">
                   <img alt="" src="https://static.toryburch.com/static/on/demandware.static/-/Sites/default/dwabb299fd/slots/homepage/3.9hp/mob/TBUS_SS18_3.9_1_HP_Mobile_11_Tile_Sandals_3x.jpg" title="" />
                   <div class="hp-product-tile--copy">
                       MULES & SLIDES
                   </div>
               </a>
          </div>
          <div class="hp-product-tile">
              <a href="#/accessories-newarrivals/">
                   <img alt="" src="https://static.toryburch.com/static/on/demandware.static/-/Sites/default/dwd746b8fe/slots/homepage/3.9hp/mob/TBUS_SS18_3.9_1_HP_Mobile_12_Tile_Accessories_3x.jpg" title="" />
                   <div class="hp-product-tile--copy">
                       ACCESSORIES
                   </div>
               </a>
          </div>
          <div class="hp-product-tile">
              <a href="#/accessories/jewelry/">
                   <img alt="" src="https://static.toryburch.com/static/on/demandware.static/-/Sites/default/dw7f07c486/slots/homepage/3.8hp/TBUS_SS18_3.8_1_HP_Mobile_10_ACCS_3x.jpg" title="" />
                   <div class="hp-product-tile--copy">
                       JEWELRY
                   </div>
               </a>
          </div>
          <div class="hp-product-tile">
               <a href="#/shoes/sneakers/">
                   <img alt="" src="https://static.toryburch.com/static/on/demandware.static/-/Sites/default/dw54fce2a2/slots/homepage/3.9hp/mob/TBUS_SS18_3.9_1_HP_Mobile_14_Tile_Sneakers_3x.jpg" title="" />
                   <div class="hp-product-tile--copy">
                       SHOES
                   </div>
               </a>
          </div>
          <div class="clear"></div>  
      </div> -->
      <script>
         jQuery(document).ready(function () {
           var slideH = $(window).width() * 1.267;
            var slickOptions = {
                "infinite": true,
                "arrows": false,
                "dots": true,
                "swipeToSlide": true,
                "speed": 250,
                "easing": "easeOutCubic",
                "slide": ".hp-slide",
                "autoplay": true,
                "autoplaySpeed": 4000,

            };
            if($(window).width() < 767) {
                $("#hp-mobile-slider").slick(slickOptions);
                $(".slick-track").css("height", slideH);
            } else {
                if ($("#hp-mobile-slider").hasClass('slick-initialized')) {
                    $("#hp-mobile-slider").slick("unslick");
                }
            }

           $(window).resize(function () {
                if($(window).width() < 767) {
                    if (!$("#hp-mobile-slider").hasClass('slick-initialized')) {
                         $("#hp-mobile-slider").slick(slickOptions);
                    }
                } else {
                    if ($("#hp-mobile-slider").hasClass('slick-initialized')) {
                        $("#hp-mobile-slider").slick("unslick");
                    }
                }
            });
         });
      </script>
    </div>
    <div class="mobile-home-links clearfix">
      
    </div>
    <div id="home-navigation" class="text-menu"></div>
  </div>
  <!-- CODE UPDATE JS  -->
  <div class="clear"></div>
</div><!-- END: content --> 
@endsection