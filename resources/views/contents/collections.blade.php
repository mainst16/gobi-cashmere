@extends((Request::is('collections/4')) ? 'Kids' : ((Request::is('collections/3')) ? 'welcome': ((Request::is('collections/1')) ? 'Yama' : 'Organic')))

@section('pro')
<div id="container" class="pt_productsearchresult" data-cgid="clothing--outerwear">

	<div id="main_new" role="main" class="main_grid">
		<div id='content' class="clearfix">
			<div class="producthits">
				<div class="category-landing-search-banner">
					<div class="htmlslotcontainer mobile-not-available">
						<div class="asset-template asset-template--header-1">
						    <div class="asset-template__wrapper">
						        <span class="header header--m">коллекци</span>
						        <br/>
						        <span class="body-copy body-copy--m" style="font-family:'SamsungSans', sans-serif;">
						        	@if($collect->isEmpty())
						 				Тун удахгүй
						 			@else
						 				
						 			@endif
						        </span>
						    </div>
						</div>
					</div>
				</div>

				<div id="search" class="search">
					<div class="productresultareaheader">
						<h1 id="results"></h1>
					</div>		

					<script type="text/javascript">
						var omnRefinementType = 'product';
						var omnRefinementName, omnRefinementValue;
					</script>
					
					<div class="productresultarea ">

						<div class="searchresults" data-gtm-search-list="searchresults">
							
							<p style="font-size: 21px; padding-left: 16px; padding-bottom: 5px; text-transform: uppercase; font-family: SamsungSans; color: #606061;">коллекци</p>
							<div class="product-grid__group ">

								@foreach($collect as $coll)
								<div class="col-xs-6 col-sm-6 col-md-4 col-lg-3-big grid-tile-container js-grid-tile-container ">
											<!-- CQuotient Activity Tracking (viewCategory-cquotient.js) -->
									<script type="text/javascript">//<!--
									/* <![CDATA[ */
									(function(){
									try {
									    if(window.CQuotient) {
										var cq_params = {};
										
										cq_params.cookieId = window.CQuotient.getCQCookieId();
										cq_params.userId = window.CQuotient.getCQUserId();
										cq_params.accumulate = true;
										cq_params.products = [{
										    id: '46054',
										    sku: ''
										}];
										cq_params.categoryId = 'clothing--outerwear';
										cq_params.refinements = '[{\"name\":\"Category\",\"value\":\"clothing--outerwear\"}]';
										cq_params.personalized = 'false';
										cq_params.sortingRule = 'merchant-picks-and-price';
										
										if(window.CQuotient.sendActivity)
										    window.CQuotient.sendActivity(CQuotient.clientId, 'viewCategory', cq_params);
										else
										    window.CQuotient.activities.push({
										    	activityType: 'viewCategory',
										    	parameters: cq_params
										    });
									  	}
										} catch(err) {}
										})();
										/* ]]> */
										// -->
										</script>
										<script type="text/javascript">//<!--
										/* <![CDATA[ (viewProduct-active_data.js) */
										dw.ac.capture({id: "46054", type: "searchhit"});
										/* ]]> */
										// -->
										</script>
												

									

									<div
										class="product-tile  product-list__item js-product-tile js-product-list-item"
										data-product-variant="190041828272"
										data-product-colors="001"
										data-product-id="46054"
										data-product-imageset="{&quot;001&quot;:{&quot;imageset&quot;:[&quot;TB_46054_001&quot;,&quot;TB_46054_001_A&quot;,&quot;TB_46054_001_B&quot;,&quot;TB_46054_001_C&quot;],&quot;video&quot;:&quot;TB_46054_001_VID-AVS&quot;,&quot;poster&quot;:&quot;TB_46054_001_E&quot;}}">

										
										<div class="product-tile__thumb-container js-product-tile-thumb" data-default-color="001">

											<!-- <div class="product-tile__qs-heart like-btn wishlist-heart-btn qs-like-button"></div>

											<div class="product-tile__arrow product-tile__arrow--left js-product-tile-arrow" data-direction="left"></div>
											<div class="product-tile__arrow product-tile__arrow--right js-product-tile-arrow" data-direction="right"></div> -->

											<a href="{{ url('collection/'.$coll->brand_id.'/'.$coll->id.'')}}">
													
												<!-- isproductimage -->	
												<!-- <img src="{{ asset('storage/'.$coll->pics.'')}}" />collections\June2018\xfOEh1PWbI84Z6553EaL.jpg -->
										        <div class="main-hero asset-template asset-template--content-breaker-1" >
										            <div style="float: left; width: 100%; margin: 10px 0 80px;" class="asset-template__wrapper">     
										                 <div class="asset-template__image-wrapper">
										                    <video style="width: 660px" height="400" autoplay loop muted>
										                          <source src="{{ asset('videos/'.$coll->pics.'')}}" type="video/mp4"/>
										                          Your browser does not support the video tag.
										                     </video>
										                </div>
										                <div style="left: 0%" class="asset-template__content">
										                    <span class="header header--xxl header--desktop">{{ $coll->name }}</span>
										                        <div class="v-offset-top-m">
										                                <span class="cta-button ">
										                                Шинэ коллекц
										                                </span>
										                        </div>
										                    </div>
										            </div>
										        </div>


											</a>
										</div>
									</div>

									<script>
									var sftileCount = 1;
									jQuery(".js-product-list-item").each(function(){
										var sfprodprice = jQuery(this).find(".js-product-price").attr("data-number-price");

										jQuery(this).attr("data-product-price",sfprodprice);
										jQuery(this).attr("data-product-position",sftileCount);
										sftileCount++;
									});
									</script>
								</div>
								@endforeach	

							<!--  end: row -->
								
							</div>
						
						</div><!-- END: productresultarea -->
					
					</div><!-- END: search -->
				<div class="category-landing-search-banner"></div>
			</div><!-- END: producthits -->
		
			
			<div class="htmlslotcontainer mobile-not-available">
					
				<script>

					jQuery(document).ready(function() {
					function getQSValue(name) {
						var i;
						var qs = window.location.search.replace("?","");;
						//console.log(qs);
						var pairs = qs.split('&');
						//console.log(pairs);
						var pairsLength = pairs.length;
						if(pairsLength){
							for(i = 0; i<pairsLength; i++){
								var currentPair = pairs[i].split("=");
								var currentPairName = currentPair[0];
								if(currentPairName == name){
									return currentPair[1];
								}
							}
						}
					}
					app.log.write(getQSValue('searchterm'));
					var qs = window.location.search.replace("?","");
					if(qs.length){
						var pairs = qs.split("&");
						if(pairs.length){
						}
					}
					if(qs.indexOf('searchmodal=yes') != -1){
						app.createDialog({
							id: 'search-modal-popup',
							options: {
								height: 404,
								width: 587,
								dialogClass: 'ui-no-theme ui-no-close ui-no-title',
							   	resizable: false,
							   	draggable: false
							   	//,position:top
							}
						});
						jQuery('#search-modal-popup').css({width:587,height:404});
						jQuery('#search-modal-popup').dialog('open');
						app.ajax.load({
							selector: '#search-modal-popup',
							url: app.URLs.searchPopupUrl,
							callback: function(responseText, textStatus){
								app.dialog.moveToMiddle('#search-modal-popup');
								jQuery('#searchRedirectString').html((getQSValue('searchterm'))?getQSValue('searchterm'):'');
								jQuery('.search-modal-close').click(function(){
									jQuery("#search-modal-popup").dialog("close");
								});
							}
						});
					}
				});
				</script>	
			</div>
		</div><!-- END: content -->
		<div class="clear"></div>
	</div><!-- END: main -->
</div>
@endsection
@section('customscripts')
    <script type="text/javascript" src="{{ asset('js/metrics_new.js')}}"></script>
    <script type="text/javascript" src="{{ asset('js/searchresults2.js')}}"></script>
    <script type="text/javascript" src="{{ asset('js/in-grid-video.js')}}"></script>
    <script type="text/javascript" src="{{ asset('js/new-in-grid-video.js')}}"></script>
    <script type="text/javascript" src="{{ asset('js/templates.js')}}"></script>
    <script type="text/javascript" src="{{ asset('js/modals.js')}}"></script>
    <script type="text/javascript" src="{{ asset('js/productTileCarousel.js')}}"></script>
    <script type="text/javascript" src="{{ asset('js/quickshopWidget.js')}}"></script>
    <script type="text/javascript" src="{{ asset('js/productsetWidget.js')}}"></script>
    <script type="text/javascript" src="{{ asset('js/CustomVideoViewer.js')}}"></script>
    <script type="text/javascript" src="{{ asset('js/shopTheLook.js')}}"></script>
    <script type="text/javascript" src="{{ asset('js/breakersParallax.js')}}"></script>
    <script type="text/javascript" src="{{ asset('js/product2.js')}}"></script>
    <script type="text/javascript" src="{{ asset('js/breakersVideo.js')}}"></script>
    <script type="text/javascript" src="{{ asset('js/dwanalytics-18.3.js')}}"></script>
    <script type="text/javascript" src="{{ asset('js/gretel.min.js')}}"></script>
@endsection
