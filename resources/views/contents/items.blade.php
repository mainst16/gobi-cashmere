@extends((Request::is('details/4/*')) ? 'Kids' : ((Request::is('details/3/*')) ? 'welcome': ((Request::is('details/1/*')) ? 'Yama' : 'Organic')))

@section('pro')


		<div id="content">
		
			<script type="text/javascript" src="{{ asset('js/product2.js')}}"></script>
			<link type="text/css" rel="stylesheet" href="{{ asset('css/pdp.css')}}" />
			<link rel="stylesheet" href="{{asset('css/animate.min.css')}}">
			

			<style>
			    .custom-cursor--zoom-in {
			        cursor: url(""), pointer;
			        cursor: -webkit-image-set(
			                url({{ asset('imgs/icons/tb-cursor-l-zoom-in.png')}}) 1x,
			                url({{ asset('imgs/icons/tb-cursor-l-zoom-in@2x.png')}}) 2x),
			                pointer;
			    }
			    .custom-cursor--zoom-out {
			        cursor: url(""), pointer;
			        cursor: -webkit-image-set(
			                url({{ asset('imgs/icons/tb-cursor-l-zoom-out.png')}}) 1x,
			                url({{ asset('imgs/icons/tb-cursor-l-zoom-out@2x.png')}}),
			                pointer;
			    }

			    .custom-cursor--zoom-out:active {
			        cursor: move;
			    }
			</style>
			

			<div class="pdp-page__product-image-background"></div>
			<div class="page-container" >

			

			{{--<div class="new-breadcrumb">
				<span itemprop="breadcrumb">
					<ol vocab="http://schema.org/" typeof="BreadcrumbList">
						<script type="text/javascript">//<!--
							/* <![CDATA[ (viewCategory-active_data.js) */
							dw.ac.applyContext({category: "clothing-jackets-outerwear"});
							/* ]]> */
							// -->
						</script>										
						<li property="itemListElement" typeof="ListItem">
							<a class="link--underline" property="item" typeof="WebPage" href="#">
								<span property="name">Brand name</span>
							</a>
							<meta property="position" content="1">
						</li>						
							<span class="divider"> / </span>						
						<li property="itemListElement" typeof="ListItem">
							<a class="link--underline last" property="item" typeof="WebPage" href="#">
								<span property="name">clothing name</span>
							</a>
							<meta property="position" content="2">
						</li>						
					</ol>
				</span>
			</div>--}}

			<div id="pdpMain_new" class="pdp-page" >
				<!-- CQuotient Activity Tracking (viewProduct-cquotient.js) -->
				
				<div class="pdp-page__product-image">

					<div class="product-image">


					    <div class="product-image__header v-offset-top-xs v-offset-bottom-xs">
					        <div class="product-image__name">
					            
								<div itemprop="name" class="product-name">
									<h1 class="title title--l">
										{{ $products->name }}
									</h1>
								</div>
					        </div>
					        <div class="product-image__price v-offset-top-xxxs">	
						
								<div id="pricing-190041832972" class="pricing" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
									<span itemprop="priceCurrency" class="posoffpage">MNT</span>									
									<div class="price pricing--l" id="price-45435">											
											<span class="price--standard notonsale" data-number-price="348.0">
												
													₮{{ $products->price }}
												
											</span>	
												
									
									</div>			
								</div>            
					        </div>
					    </div>

					    <div class="product-image__image-container product-image__thumb-container" >
					        <div class="product-image__image custom-cursor--zoom-in js-show-image-viewer js-main-image"
					             data-s7-images="[&quot;TB_45435_278_A&quot;,&quot;TB_45435_278_D&quot;]"
					             style="background-image: url({{ asset('storage/'.$pic.'')}})" data-maincategory="clothing" data-special="null">
					        </div>
					        <div class="product-image__arrow product-image__arrow-left product-tile__arrow product-tile__arrow--left js-change-main-image" data-direction="left"></div>
					        <div class="product-image__arrow product-image__arrow-right product-tile__arrow product-tile__arrow--right js-change-main-image" data-direction="right"></div>

					        <div class="product-image__video-container"></div>

					    </div>

				        <div class="product-image__image-mobile js-embedded-image-viewer js-show-image-viewer">
				        	@foreach($pics as $pic)
				            <div class="mobile-image">                
				                <img src="{{ asset('storage/'.str_replace('\\', '/', $pic->pics).'')}}"></img>                
				            </div>
				            @endforeach
				    	</div>

    					<div class="product-image__gutter"></div>
					</div>
					<div class="image-viewer-popup" role="dialog" aria-label="Displays the product gallery. Use the left and right arrow keys to switch between images. Press on the ESC key to close the image viewer." style="display:none">
    					<div class="image-viewer-popup__wrapper">
        					<div class="image-viewer-popup__close js-close-image-viewer" aria-label="Close" role="button"></div>
		        			<div class="image-viewer-popup__arrow image-viewer-popup__arrow--left js-show-prev-image" aria-label="Previous Image" role="button">
					            <div class="image-viewer-popup__tooltip js-tooltip-text title--m"></div>
					        </div>
					        <div class="image-viewer-popup__arrow image-viewer-popup__arrow--right js-show-next-image" aria-label="Next Image" role="button">
					            <div class="image-viewer-popup__tooltip js-tooltip-text title--m"></div>
					        </div>
					        <div class="image-viewer-popup__images">
					            <div class="image-viewer">

					                <!-- <div class="js-video-wrapper image-viewer__current-video js-swipe-elem" data-video-exists="true"></div> -->
					                <div class="image-viewer__current-image js-current-image custom-cursor--zoom-in" style=""></div>
					                <div class="image-viewer__current-image-wrapper js-image-toggle">
					                    <img class="image-viewer__current-image-mobile js-current-image-mobile js-swipe-elem" src="" />
					                </div>

					                <div class="image-viewer__image-list">
					                    <div class="image-list js-viewer-image-container">
					                        
					                            
					                                @foreach($pics as $pic)
					                                    <div class="0 image-list__item js-show-current-image" data-image-index="{{$pic->dd}}">
					                                        <img src="{{ asset('storage/'.$pic->pics.'')}}" data-imgrl="{{ asset('storage/'.str_replace('\\', '/', $pic->pics).'')}}"></img>
					                                    </div>
					                                @endforeach
					                                
					                            
					                                
					                                    <!-- <div id="show-video-item" data-video-id="TB_45435_278_VID-AVS"  class="2 image-list__item js-show-current-image" data-image-index="3">
					                                        <div class="video-wrapper">
					                                           <div class="play-video-btn video-s-btn"></div>
					                                           <img src="https://s7.toryburch.com/is/image/ToryBurchNA/TB_45435_278_E?$mainline_pdp_thumbnail$"></img>
					                                        </div>
					                                    </div>
 -->					                                
					                            
					                        
					                    </div>
					                </div>
					            </div>
					        </div>
					    </div>
					</div>
				</div>
				<div class="pdp-page__flexible-column v-offset-bottom-xxl">
					
					
						<!-- <div class="product-image-gallery ">
							<div class="product-image-gallery__column">
									<div class="product-image-gallery__item custom-cursor--zoom-in js-show-image-viewer js-product-gallery-image 1"
										data-gallery-index="1"
										data-name="ToryBurchNA/TB_45435_278_D"
										data-type="image"
										data-index="1">
										

										<img src="{{ asset('imgs/products/coats/_MG_4625.png')}}" />

										<div class="product-image-gallery__item-badge body-copy body-copy--s">2											
											<span class="tooltip-index-slash">/</span>
											<span class="js-tooltip-img-sum">2</span>
										</div>
									</div>

									<div class="product-image-gallery__item custom-cursor--zoom-in js-show-image-viewer js-product-gallery-image 3"
										data-gallery-index="3"
										data-name=""
										data-type=""
										data-index="">
										

										<img src="" />

										<div class="product-image-gallery__item-badge body-copy body-copy--s">

											
												4
											
											<span class="tooltip-index-slash">/</span>
											<span class="js-tooltip-img-sum">3</span>
										</div>
									</div>
								
							</div>
						</div> -->
						<div class="btn-play-video">play</div>
							
						</div>
						<div class="pdp-page__fixed-column" >
							<div class="product-info-panel__wrap js-product-info-wrapper">
								<div class="product-info-panel product-info-panel--centred">
									<div class="product-description v-offset-top-xxl">
										<div class="product-description__header">
											<h6 class="header header--xs">details</h6>
										</div>
										<div class="v-offset-top-m body-copy--s body-copy product-description__content">		
											<p>
											    {!! $products->desc !!}
											</p>		
											<div class="panelContent v-offset-top-m" id="longDescription">
												<ul>
													<li> 100% ноолуур</li>
													<!--<li> Front zipper closure</li>-->
													<!--<li> Folds up into a pocket pouch</li>-->
													<li style="text-transform:uppercase;"> Style Number: {{$products->style_no}}</li>
												</ul>				
											</div>
										</div>
									</div>
								</div>
								<!-- END: productdetailcolumn -->
							</div>
						</div >
						<div class="pdp-page__additional-descriptions">
							{{--<div class="product-list product-list--styled-with js-styled-with-root" data-product-list="styledwith">
								<div class="product-list__header header header--xs">
									Styled With
								</div>

								<div class="product-list__content js-product-list-content js-styled-with-content">
									<div class="product-tile  product-list__item js-product-tile js-product-list-item" data-product-variant="190041809264" data-product-colors="260,100,267,430" data-product-id="47126" data-product-ismaster="true" data-product-name="SIENNA FLAT SLIDE" data-product-category="shops-parties-events" data-product-availability="true" data-product-price="" data-product-position="" data-product-imageset="{&quot;260&quot;:{&quot;imageset&quot;:[&quot;TB_47126_260&quot;,&quot;TB_47126_260_C&quot;,&quot;TB_47126_260_D&quot;],&quot;video&quot;:&quot;TB_47126_260_VID-AVS&quot;,&quot;poster&quot;:&quot;TB_47126_260_E&quot;,&quot;masterid&quot;:&quot;47126&quot;},&quot;100&quot;:{&quot;imageset&quot;:[&quot;TB_47126_100&quot;,&quot;TB_47126_100_C&quot;,&quot;TB_47126_100_D&quot;],&quot;video&quot;:&quot;TB_47126_100_VID-AVS&quot;,&quot;poster&quot;:&quot;TB_47126_100_E&quot;,&quot;masterid&quot;:&quot;47126&quot;},&quot;267&quot;:{&quot;imageset&quot;:[&quot;TB_47126_267&quot;,&quot;TB_47126_267_C&quot;,&quot;TB_47126_267_D&quot;],&quot;video&quot;:&quot;TB_47126_267_VID-AVS&quot;,&quot;poster&quot;:&quot;TB_47126_267_E&quot;,&quot;masterid&quot;:&quot;47126&quot;},&quot;430&quot;:{&quot;imageset&quot;:[&quot;TB_47126_430&quot;,&quot;TB_47126_430_C&quot;,&quot;TB_47126_430_D&quot;],&quot;video&quot;:&quot;TB_47126_430_VID-AVS&quot;,&quot;poster&quot;:&quot;TB_47126_430_E&quot;,&quot;masterid&quot;:&quot;47126&quot;}}">
										<div class="product-tile__thumb-container js-product-tile-thumb" data-default-color="260">

											<div class="product-tile__qs-heart like-btn wishlist-heart-btn qs-like-button"></div>

											<div class="product-tile__arrow product-tile__arrow--left js-product-tile-arrow" data-direction="left"></div>
											<div class="product-tile__arrow product-tile__arrow--right js-product-tile-arrow" data-direction="right"></div>
											<a href="https://www.toryburch.com/sienna-flat-slide/47126.html?icampid=styled" title="SIENNA FLAT SLIDE" class="product-tile__thumb">
												<!-- isproductimage -->
												<img src="https://s7.toryburch.com/is/image/ToryBurchNA/TB_47126_260?$mainline_grid_desktop$" alt="SIENNA FLAT SLIDE" />
											</a>
											<!-- <div class="product-tile__quickshop-button js-show-quickshop">Quickshop</div> -->
										</div>
										<div class="product-tile__info-container">
											<a href="https://www.toryburch.com/sienna-flat-slide/47126.html?icampid=styled" title="SIENNA FLAT SLIDE" class="product-tile__name title title--xs">
												SIENNA FLAT SLIDE
											</a>
											<div title="SIENNA FLAT SLIDE" class="product-tile__price pricing pricing--s">
												<span class="price--standard notonsale js-product-price" data-number-price="298.0">
											
													<!-- CCD-531
														$298.00-->
														$298				
												</span>
											</div>

											<div class="product-tile__colors swatches v-offset-top-xxxs">
												<ul class="swatches__list" data-color-array="260,100,267,430">											
													<li class="4 product-tile__colors__swatch swatches__item swatches__item--sm selected js-product-tile-swatch" data-value="260" data-variant="190041809264" data-product-image="https://s7.toryburch.com/is/image/ToryBurchNA/TB_47126_260?$trb_grid_512x581$">
														<div class="swatches__disp-name">NATURAL VACHETTA / Multi</div>
														<img class="swatches__image" src="https://s7.toryburch.com/is/image/ToryBurchNA/TB_47126_260_S?$swatch$" alt="NATURAL VACHETTA / Multi"/>
													</li>
												
											
												
													
													<li class="4 product-tile__colors__swatch swatches__item swatches__item--sm  js-product-tile-swatch" data-value="100" data-variant="190041809134" data-product-image="https://s7.toryburch.com/is/image/ToryBurchNA/TB_47126_100?$trb_grid_512x581$">
														<div class="swatches__disp-name">White</div>
														<img class="swatches__image" src="https://s7.toryburch.com/is/image/ToryBurchNA/TB_47126_100_S?$swatch$" alt="White"/>
													</li>
												
											
												
													
													<li class="4 product-tile__colors__swatch swatches__item swatches__item--sm  js-product-tile-swatch" data-value="267" data-variant="190041830534" data-product-image="https://s7.toryburch.com/is/image/ToryBurchNA/TB_47126_267?$trb_grid_512x581$">
														<div class="swatches__disp-name">NATURAL VACHETTA</div>
														<img class="swatches__image" src="https://s7.toryburch.com/is/image/ToryBurchNA/TB_47126_267_S?$swatch$" alt="NATURAL VACHETTA"/>
													</li>
												
											
												
													
													<li class="4 product-tile__colors__swatch swatches__item swatches__item--sm  js-product-tile-swatch" data-value="430" data-variant="190041809394" data-product-image="https://s7.toryburch.com/is/image/ToryBurchNA/TB_47126_430?$trb_grid_512x581$">
														<div class="swatches__disp-name">PERFECT NAVY</div>
														<img class="swatches__image" src="https://s7.toryburch.com/is/image/ToryBurchNA/TB_47126_430_S?$swatch$" alt="PERFECT NAVY"/>
													</li>
												</ul>
											</div>
											
										</div>
									</div>

									<script>
									var sftileCount = 1;
									jQuery(".js-product-list-item").each(function(){
										var sfprodprice = jQuery(this).find(".js-product-price").attr("data-number-price");

										jQuery(this).attr("data-product-price",sfprodprice);
										jQuery(this).attr("data-product-position",sftileCount);
										sftileCount++;
									});
									</script>

									<div
										class="product-tile  product-list__item js-product-tile js-product-list-item"
										data-product-variant="190041841189"
										data-product-colors="952"
										data-product-id="48510"
										data-product-ismaster="true"
										data-product-name="KIT SHORT"
										data-product-category="clothing-view-all"
										data-product-availability="true"
										data-product-price=""
										data-product-position=""
										data-product-imageset="{&quot;952&quot;:{&quot;imageset&quot;:[&quot;TB_48510_952&quot;,&quot;TB_48510_952_A&quot;,&quot;TB_48510_952_E&quot;],&quot;video&quot;:&quot;null&quot;,&quot;poster&quot;:&quot;null&quot;,&quot;masterid&quot;:&quot;48510&quot;}}">

						
										<div class="product-tile__thumb-container js-product-tile-thumb" data-default-color="952">

											<div class="product-tile__qs-heart like-btn wishlist-heart-btn qs-like-button"></div>

											<div class="product-tile__arrow product-tile__arrow--left js-product-tile-arrow" data-direction="left"></div>
											<div class="product-tile__arrow product-tile__arrow--right js-product-tile-arrow" data-direction="right"></div>

											<a href="https://www.toryburch.com/kit-short/48510.html?icampid=styled" title="KIT SHORT" class="product-tile__thumb">
									
												<!-- isproductimage -->
												<img src="https://s7.toryburch.com/is/image/ToryBurchNA/TB_48510_952?$mainline_grid_desktop$" alt="KIT SHORT" />
											</a>
										</div>
										<div class="product-tile__info-container">
											<a href="https://www.toryburch.com/kit-short/48510.html?icampid=styled" title="KIT SHORT" class="product-tile__name title title--xs">
												KIT SHORT
											</a>
											<div title="KIT SHORT" class="product-tile__price pricing pricing--s">		
												<span class="price--standard notonsale js-product-price" data-number-price="198.0">
													
														<!-- CCD-531
															$198.00-->
															$198
													
												</span>
											</div>
											<div class="product-tile__colors swatches v-offset-top-xxxs">
												<ul class="swatches__list" data-color-array="952">
																
												</ul>
											</div>
											
										</div>
									</div>

									<script>
									var sftileCount = 1;
									jQuery(".js-product-list-item").each(function(){
										var sfprodprice = jQuery(this).find(".js-product-price").attr("data-number-price");

										jQuery(this).attr("data-product-price",sfprodprice);
										jQuery(this).attr("data-product-position",sftileCount);
										sftileCount++;
									});
									</script>
									<div
										class="product-tile  product-list__item js-product-tile js-product-list-item"
										data-product-variant="190041833610"
										data-product-colors="100"
										data-product-id="45599"
										data-product-ismaster="true"
										data-product-name="LANE TOP"
										data-product-category="clothing-view-all"
										data-product-availability="true"
										data-product-price=""
										data-product-position=""
										data-product-imageset="{&quot;100&quot;:{&quot;imageset&quot;:[&quot;TB_45599_100&quot;,&quot;TB_45599_100_A&quot;,&quot;TB_45599_100_B&quot;,&quot;TB_45599_100_C&quot;,&quot;TB_45599_100_H&quot;],&quot;video&quot;:&quot;TB_45599_100_VID-AVS&quot;,&quot;poster&quot;:&quot;TB_45599_100_E&quot;,&quot;masterid&quot;:&quot;45599&quot;}}">

										
										<div class="product-tile__thumb-container js-product-tile-thumb" data-default-color="100">

											<div class="product-tile__qs-heart like-btn wishlist-heart-btn qs-like-button"></div>

											<div class="product-tile__arrow product-tile__arrow--left js-product-tile-arrow" data-direction="left"></div>
											<div class="product-tile__arrow product-tile__arrow--right js-product-tile-arrow" data-direction="right"></div>

											<a href="https://www.toryburch.com/lane-top/45599.html?icampid=styled" title="LANE TOP" class="product-tile__thumb">
													
												<!-- isproductimage -->
												<img src="https://s7.toryburch.com/is/image/ToryBurchNA/TB_45599_100?$mainline_grid_desktop$" alt="LANE TOP" />
											</a>
										</div>
										<div class="product-tile__info-container">
											<a href="https://www.toryburch.com/lane-top/45599.html?icampid=styled" title="LANE TOP" class="product-tile__name title title--xs">
												LANE TOP
											</a>
											<div title="LANE TOP" class="product-tile__price pricing pricing--s">
												<span class="price--standard notonsale js-product-price" data-number-price="228.0">
											
														<!-- CCD-531
															$228.00-->
															$228
													
												</span>
											</div>
											<div class="product-tile__colors swatches v-offset-top-xxxs">
												<ul class="swatches__list" data-color-array="100">
													
												</ul>
											</div>
											
										</div>
									</div>

									<script>
									var sftileCount = 1;
									jQuery(".js-product-list-item").each(function(){
										var sfprodprice = jQuery(this).find(".js-product-price").attr("data-number-price");

										jQuery(this).attr("data-product-price",sfprodprice);
										jQuery(this).attr("data-product-position",sftileCount);
										sftileCount++;
									});
									</script>
									<div
										class="product-tile  product-list__item js-product-tile js-product-list-item"
										data-product-variant="190041817870"
										data-product-colors="022"
										data-product-id="47941"
										data-product-ismaster="true"
										data-product-name="GEO PENDANT NECKLACE"
										data-product-category="accessories-jewelry"
										data-product-availability="true"
										data-product-price=""
										data-product-position=""
										data-product-imageset="{&quot;022&quot;:{&quot;imageset&quot;:[&quot;TB_47941_022&quot;,&quot;TB_47941_022_C&quot;,&quot;TB_47941_022_E&quot;,&quot;TB_47941_022_J&quot;],&quot;video&quot;:&quot;null&quot;,&quot;poster&quot;:&quot;null&quot;,&quot;masterid&quot;:&quot;47941&quot;}}">

										
										<div class="product-tile__thumb-container js-product-tile-thumb" data-default-color="022">

											<div class="product-tile__qs-heart like-btn wishlist-heart-btn qs-like-button"></div>

											<div class="product-tile__arrow product-tile__arrow--left js-product-tile-arrow" data-direction="left"></div>
											<div class="product-tile__arrow product-tile__arrow--right js-product-tile-arrow" data-direction="right"></div>

											<a href="https://www.toryburch.com/geo-pendant-necklace/47941.html?icampid=styled" title="GEO PENDANT NECKLACE" class="product-tile__thumb">
												<!-- isproductimage -->
												<img src="https://s7.toryburch.com/is/image/ToryBurchNA/TB_47941_022?$mainline_grid_desktop$" alt="GEO PENDANT NECKLACE" />
											</a>
										</div>
										<div class="product-tile__info-container">
											<a href="https://www.toryburch.com/geo-pendant-necklace/47941.html?icampid=styled" title="GEO PENDANT NECKLACE" class="product-tile__name title title--xs">
												GEO PENDANT NECKLACE
											</a>
											<div title="GEO PENDANT NECKLACE" class="product-tile__price pricing pricing--s">
												<span class="price--standard notonsale js-product-price" data-number-price="158.0">
							
														<!-- CCD-531
															$158.00-->
															$158
													
												</span>
											</div>
											<div class="product-tile__colors swatches v-offset-top-xxxs">
												<ul class="swatches__list" data-color-array="022">
													
												</ul>
											</div>
										</div>
									</div>

									<script>
									var sftileCount = 1;
									jQuery(".js-product-list-item").each(function(){
										var sfprodprice = jQuery(this).find(".js-product-price").attr("data-number-price");

										jQuery(this).attr("data-product-price",sfprodprice);
										jQuery(this).attr("data-product-position",sftileCount);
										sftileCount++;
									});
									</script>
								</div>
							</div>
							<div style="display: none;" id="recommendations-container" class="product-list product-list--recommendations" data-product-list="recommended">

								<div class="product-list__header header header--xs">
									
										You May Also Like
									
								</div>

								
								

								<div id="recommendations" class="product-list__content js-recommended-content js-product-list-content">
									
								</div>
							</div>
							<!-- /recommendations productcarousel -->
							<div class="product-list" data-product-list="recentlyviewed"">
								<div class="product-list__header header header--xs">
									Recently Viewed
								</div>
								<ul class="product-list__content js-product-list-content js-lastvisited-content">
									<div
										class="product-tile  product-list__item js-product-tile js-product-list-item"
										data-product-variant="190041857258"
										data-product-colors="433"
										data-product-id="48585"
										data-product-ismaster="true"
										data-product-name="TAPESTRY GEO FLOUNCE TOP"
										data-product-category="swim-newarrivals"
										data-product-availability="true"
										data-product-price=""
										data-product-position=""
										data-product-imageset="{&quot;433&quot;:{&quot;imageset&quot;:[&quot;TB_48585_433&quot;,&quot;TB_48585_433_A&quot;],&quot;video&quot;:&quot;null&quot;,&quot;poster&quot;:&quot;null&quot;,&quot;masterid&quot;:&quot;48585&quot;}}">

										
										<div class="product-tile__thumb-container js-product-tile-thumb" data-default-color="433">

											<div class="product-tile__qs-heart like-btn wishlist-heart-btn qs-like-button"></div>

											<div class="product-tile__arrow product-tile__arrow--left js-product-tile-arrow" data-direction="left"></div>
											<div class="product-tile__arrow product-tile__arrow--right js-product-tile-arrow" data-direction="right"></div>

											<a href="https://www.toryburch.com/tapestry-geo-flounce-top/48585.html" title="TAPESTRY GEO FLOUNCE TOP" class="product-tile__thumb">
												<!-- isproductimage -->
												<img src="https://s7.toryburch.com/is/image/ToryBurchNA/TB_48585_433?$mainline_grid_desktop$" alt="TAPESTRY GEO FLOUNCE TOP" />
											</a>

											<!-- <div class="product-tile__quickshop-button js-show-quickshop">Quickshop</div> -->
										</div>
										<div class="product-tile__info-container">
											<a href="https://www.toryburch.com/tapestry-geo-flounce-top/48585.html" title="TAPESTRY GEO FLOUNCE TOP" class="product-tile__name title title--xs">
												TAPESTRY GEO FLOUNCE TOP
											</a>
											<div title="TAPESTRY GEO FLOUNCE TOP" class="product-tile__price pricing pricing--s">
												<span class="price--standard notonsale js-product-price" data-number-price="138.0">
											
												<!-- CCD-531
													$138.00-->
													$138
												</span>
											</div>

											<div class="product-tile__colors swatches v-offset-top-xxxs">
												<ul class="swatches__list" data-color-array="433">
																
												</ul>
											</div>
											
										</div>
									</div>

									<script>
									var sftileCount = 1;
									jQuery(".js-product-list-item").each(function(){
										var sfprodprice = jQuery(this).find(".js-product-price").attr("data-number-price");

										jQuery(this).attr("data-product-price",sfprodprice);
										jQuery(this).attr("data-product-position",sftileCount);
										sftileCount++;
									});
									</script>
									<div
										class="product-tile  product-list__item js-product-tile js-product-list-item"
										data-product-variant="190041858194"
										data-product-colors="671"
										data-product-id="48632"
										data-product-ismaster="true"
										data-product-name="HICKS GARDEN PARTY ONE-PIECE"
										data-product-category="swim-newarrivals"
										data-product-availability="true"
										data-product-price=""
										data-product-position=""
										data-product-imageset="{&quot;671&quot;:{&quot;imageset&quot;:[&quot;TB_48632_671&quot;,&quot;TB_48632_671_A&quot;,&quot;TB_48632_671_B&quot;,&quot;TB_48632_671_C&quot;,&quot;TB_48632_671_D&quot;],&quot;video&quot;:&quot;TB_48632_671_VID-AVS&quot;,&quot;poster&quot;:&quot;TB_48632_671_E&quot;,&quot;masterid&quot;:&quot;48632&quot;}}">

										
										<div class="product-tile__thumb-container js-product-tile-thumb" data-default-color="671">

											<div class="product-tile__qs-heart like-btn wishlist-heart-btn qs-like-button"></div>

											<div class="product-tile__arrow product-tile__arrow--left js-product-tile-arrow" data-direction="left"></div>
											<div class="product-tile__arrow product-tile__arrow--right js-product-tile-arrow" data-direction="right"></div>

											<a href="https://www.toryburch.com/hicks-garden-party-one-piece/48632.html" title="HICKS GARDEN PARTY ONE-PIECE" class="product-tile__thumb">
												<!-- isproductimage -->
												<img src="https://s7.toryburch.com/is/image/ToryBurchNA/TB_48632_671?$mainline_grid_desktop$" alt="HICKS GARDEN PARTY ONE-PIECE" />

											</a>

											<!-- <div class="product-tile__quickshop-button js-show-quickshop">Quickshop</div> -->
										</div>
										<div class="product-tile__info-container">
											<a href="https://www.toryburch.com/hicks-garden-party-one-piece/48632.html" title="HICKS GARDEN PARTY ONE-PIECE" class="product-tile__name title title--xs">
												HICKS GARDEN PARTY ONE-PIECE
											</a>
											<div title="HICKS GARDEN PARTY ONE-PIECE" class="product-tile__price pricing pricing--s">
												<span class="price--standard notonsale js-product-price" data-number-price="258.0">
											
												<!-- CCD-531
													$258.00-->
													$258
											
												</span>
											</div>

											<div class="product-tile__colors swatches v-offset-top-xxxs">
												<ul class="swatches__list" data-color-array="671">
													
												</ul>
											</div>
											
										</div>
									</div>

									<script>
									var sftileCount = 1;
									jQuery(".js-product-list-item").each(function(){
										var sfprodprice = jQuery(this).find(".js-product-price").attr("data-number-price");

										jQuery(this).attr("data-product-price",sfprodprice);
										jQuery(this).attr("data-product-position",sftileCount);
										sftileCount++;
									});
									</script>
									<div
										class="product-tile  product-list__item js-product-tile js-product-list-item"
										data-product-variant="190041839322"
										data-product-colors="671"
										data-product-id="47070"
										data-product-ismaster="true"
										data-product-name="SERENA DRESS"
										data-product-category="clothing-view-all"
										data-product-availability="true"
										data-product-price=""
										data-product-position=""
										data-product-imageset="{&quot;671&quot;:{&quot;imageset&quot;:[&quot;TB_47070_671&quot;,&quot;TB_47070_671_A&quot;],&quot;video&quot;:&quot;null&quot;,&quot;poster&quot;:&quot;null&quot;,&quot;masterid&quot;:&quot;47070&quot;}}">

										
										<div class="product-tile__thumb-container js-product-tile-thumb" data-default-color="671">

											<div class="product-tile__qs-heart like-btn wishlist-heart-btn qs-like-button"></div>

											<div class="product-tile__arrow product-tile__arrow--left js-product-tile-arrow" data-direction="left"></div>
											<div class="product-tile__arrow product-tile__arrow--right js-product-tile-arrow" data-direction="right"></div>

											<a href="https://www.toryburch.com/serena-dress/47070.html" title="SERENA DRESS" class="product-tile__thumb">
												<!-- isproductimage -->
												<img src="https://s7.toryburch.com/is/image/ToryBurchNA/TB_47070_671?$mainline_grid_desktop$" alt="SERENA DRESS" />

											</a>

											<!-- <div class="product-tile__quickshop-button js-show-quickshop">Quickshop</div> -->
										</div>
										<div class="product-tile__info-container">
											<a href="https://www.toryburch.com/serena-dress/47070.html" title="SERENA DRESS" class="product-tile__name title title--xs">
												SERENA DRESS
											</a>
											<div title="SERENA DRESS" class="product-tile__price pricing pricing--s">
												<span class="price--standard notonsale js-product-price" data-number-price="598.0">
			
														<!-- CCD-531
															$598.00-->
															$598
													
												</span>
											</div>
											<div class="product-tile__colors swatches v-offset-top-xxxs">
												<ul class="swatches__list" data-color-array="671">
																		
												</ul>
											</div>
										</div>
									</div>

									
								</ul><!-- /product-list__content -->
							</div><!-- /product-list -->--}}
						</div >
					</div >
				</div >


		<script type="text/javascript" src="{{ asset('js/imageViewer.js')}}"></script>
	
		</div>
@endsection
@section('customscripts')
@endsection