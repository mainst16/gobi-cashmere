@extends('welcome')
@section('styles')
@endsection
@section('pro')
<div id="main" role="main">
	<div id="accountcreate">
		<div class="account-header"><a href="{{url('account/register')}}">Бүртгүүлэх</a></div>
		<div class="account-profilewrapper registration">
			<div class="account-profile registrationform">
				<form action="{{ url('account/aregister')}}" method="post" id="RegistrationForm" autocomplete="off" class="customvalidation">
					{!! csrf_field() !!}
					<fieldset class="register">
						<div class="formfields" id="personaldata">
							<div class="formfield firstname">
								<div class="label">
									<span class="requiredindicator">*</span>
									<span class="labeltext">Овог</span>
								</div>
								<div class="value">
									<input class="textinput required" id="dwfrm_profile_customer_firstname" type="text" name="fname" value="" maxlength="50" placeholder="Овог" />
								</div>
								<div class="clear"></div>
							</div>
							<div class="formfield lastname">
								<div class="clear"></div>
								<div class="label">
									<span class="requiredindicator">*</span>
									<span class="labeltext">Нэр</span>
								</div> 
								<div class="value">
									<input class="textinput required" id="dwfrm_profile_customer_lastname" type="text" name="name" value="" maxlength="50" placeholder="Нэр" />
								</div>
								<div class="clear"></div>
							</div>
						</div>
						<div class="formfields" id="personaldata">
							<div class="formfield email">
								<div class="label">
									<span class="requiredindicator">*</span>
									<span class="labeltext">Цахим хаяг</span>
								</div>
								<div class="value">
									<input class="textinput required" id="dwfrm_profile_customer_email" type="email" name="email" value="" maxlength="50" placeholder="Цахим хаяг" />
								</div>
								<div class="clear"></div>
							</div>
							<div class="formfield  password">
								<div class="label">
									<span class="requiredindicator">*</span>
									<span class="labeltext">Нууц үг</span>
								</div>
								<div class="value">
									<input class="textinputpw required" id="dwfrm_profile_login_password" type="password" name="password" value="" maxlength="21" placeholder="Нууц үг" />
								</div>
								<div class="clear"></div>
							</div>
							<div class="formfield  passwordconfirm">
								<div class="label">
									<span class="requiredindicator">*</span>
									<span class="labeltext">Нууц үг давтах</span>
								</div>
								<div class="value">
									<input class="textinputpw required" id="dwfrm_profile_login_passwordconfirm" type="password" name="passwordconfirm" value="" maxlength="2147483647" placeholder="Нууц үг" equalTo="#dwfrm_profile_login_password" />
								</div>
								<div class="clear"></div>
							</div><!-- 
							<div class="formfield password-requirements">
								<div class="password-details-body">
									<p>Your password should be 7-21 characters of upper and lower case, with at least one number.</p>
								</div>
							</div> -->
							<div class="formfield   country">
								<div class="label">
									<span class="requiredindicator">*</span>
									<span class="labeltext">Улс</span>
								</div>
								<div class="value">
									<select class="selectbox country required" id="dwfrm_profile_customer_country" name="country">
										<option class="selectoption" label="Country" value="">Улс</option>
										<option class="selectoption" label="United States" value="US">United States</option>
										<option class="selectoption" label="Afghanistan" value="AF">Afghanistan</option>
										<option class="selectoption" label="Albania" value="AL">Albania</option>
										<option class="selectoption" label="Algeria" value="DZ">Algeria</option>
										<option class="selectoption" label="American Samoa" value="AS">American Samoa</option>
										<option class="selectoption" label="Andorra" value="AD">Andorra</option>
										<option class="selectoption" label="Angola" value="AO">Angola</option>
										<option class="selectoption" label="Anguilla" value="AI">Anguilla</option>
										<option class="selectoption" label="Antigua &amp; Barbuda" value="AG">Antigua &amp; Barbuda</option>
										<option class="selectoption" label="Argentina" value="AR">Argentina</option>
										<option class="selectoption" label="Armenia" value="AM">Armenia</option>
										<option class="selectoption" label="Aruba" value="AW">Aruba</option>
										<option class="selectoption" label="Australia" value="AU">Australia</option>
										<option class="selectoption" label="Austria" value="AT">Austria</option>
										<option class="selectoption" label="Azerbaijan" value="AZ">Azerbaijan</option>
										<option class="selectoption" label="Bahamas" value="BS">Bahamas</option>
										<option class="selectoption" label="Bahrain" value="BH">Bahrain</option>
										<option class="selectoption" label="Bangladesh" value="BD">Bangladesh</option>
										<option class="selectoption" label="Barbados" value="BB">Barbados</option>
										<option class="selectoption" label="Belarus" value="BY">Belarus</option>
										<option class="selectoption" label="Belgium" value="BE">Belgium</option>
										<option class="selectoption" label="Belize" value="BZ">Belize</option>
										<option class="selectoption" label="Benin" value="BJ">Benin</option>
										<option class="selectoption" label="Bermuda" value="BM">Bermuda</option>
										<option class="selectoption" label="Bhutan" value="BT">Bhutan</option>
										<option class="selectoption" label="Bolivia" value="BO">Bolivia</option>
										<option class="selectoption" label="Bonaire" value="BQ">Bonaire</option>
										<option class="selectoption" label="Bosnia &amp; Herzegovina" value="BA">Bosnia &amp; Herzegovina</option>
										<option class="selectoption" label="Botswana" value="BW">Botswana</option>
										<option class="selectoption" label="Brazil" value="BR">Brazil</option>
										<option class="selectoption" label="British Indian Ocean Ter" value="IO">British Indian Ocean Ter</option>
										<option class="selectoption" label="Brunei" value="IO">Brunei</option>
										<option class="selectoption" label="Bulgaria" value="BG">Bulgaria</option>
										<option class="selectoption" label="Burkina Faso" value="BF">Burkina Faso</option>
										<option class="selectoption" label="Burundi" value="BI">Burundi</option>
										<option class="selectoption" label="Cambodia" value="KH">Cambodia</option>
										<option class="selectoption" label="Cameroon" value="CM">Cameroon</option>
										<option class="selectoption" label="Canada" value="CA">Canada</option>
										<option class="selectoption" label="Canary Islands" value="CS">Canary Islands</option>
										<option class="selectoption" label="Cape Verde" value="CV">Cape Verde</option>
										<option class="selectoption" label="Cayman Islands" value="KY">Cayman Islands</option>
										<option class="selectoption" label="Central African Republic" value="CF">Central African Republic</option>
										<option class="selectoption" label="Chad" value="TD">Chad</option>
										<option class="selectoption" label="Channel Islands" value="CB">Channel Islands</option>
										<option class="selectoption" label="Chile" value="CL">Chile</option>
										<option class="selectoption" label="China" value="CN">China</option>
										<option class="selectoption" label="Christmas Island" value="CX">Christmas Island</option>
										<option class="selectoption" label="Cocos Island" value="CC">Cocos Island</option>
										<option class="selectoption" label="Colombia" value="CO">Colombia</option>
										<option class="selectoption" label="Comoros" value="KM">Comoros</option>
										<option class="selectoption" label="Congo" value="CG">Congo</option>
										<option class="selectoption" label="Cook Islands" value="CK">Cook Islands</option>
										<option class="selectoption" label="Costa Rica" value="CR">Costa Rica</option>
										<option class="selectoption" label="Cote D'Ivoire" value="CI">Cote D&#39;Ivoire</option>
										<option class="selectoption" label="Croatia" value="HR">Croatia</option>
										<option class="selectoption" label="Cuba" value="CU">Cuba</option>
										<option class="selectoption" label="Curacao" value="CW">Curacao</option>
										<option class="selectoption" label="Cyprus" value="CY">Cyprus</option>
										<option class="selectoption" label="Czech Republic" value="CZ">Czech Republic</option>
										<option class="selectoption" label="Denmark" value="DK">Denmark</option>
										<option class="selectoption" label="Djibouti" value="DJ">Djibouti</option>
										<option class="selectoption" label="Dominica" value="DM">Dominica</option>
										<option class="selectoption" label="Dominican Republic" value="DO">Dominican Republic</option>
										<option class="selectoption" label="East Timor" value="EX">East Timor</option>
										<option class="selectoption" label="Ecuador" value="EC">Ecuador</option>
										<option class="selectoption" label="Egypt" value="EG">Egypt</option>
										<option class="selectoption" label="El Salvador" value="SV">El Salvador</option>
										<option class="selectoption" label="Equatorial Guinea" value="GQ">Equatorial Guinea</option>
										<option class="selectoption" label="Eritrea" value="ER">Eritrea</option>
										<option class="selectoption" label="Estonia" value="EE">Estonia</option>
										<option class="selectoption" label="Ethiopia" value="ET">Ethiopia</option>
										<option class="selectoption" label="Falkland Islands" value="FK">Falkland Islands</option>
										<option class="selectoption" label="Faroe Islands" value="FO">Faroe Islands</option>
										<option class="selectoption" label="Fiji" value="FJ">Fiji</option>
										<option class="selectoption" label="Finland" value="FI">Finland</option>
										<option class="selectoption" label="France" value="FR">France</option>
										<option class="selectoption" label="French Guiana" value="GF">French Guiana</option>
										<option class="selectoption" label="French Polynesia" value="PF">French Polynesia</option>
										<option class="selectoption" label="French Southern Ter" value="TF">French Southern Ter</option>
										<option class="selectoption" label="Gabon" value="GA">Gabon</option>
										<option class="selectoption" label="Gambia" value="GM">Gambia</option>
										<option class="selectoption" label="Georgia" value="GE">Georgia</option>
										<option class="selectoption" label="Germany" value="DE">Germany</option>
										<option class="selectoption" label="Ghana" value="GH">Ghana</option>
										<option class="selectoption" label="Gibraltar" value="GI">Gibraltar</option>
										<option class="selectoption" label="Great Britain" value="GB">Great Britain</option>
										<option class="selectoption" label="Greece" value="GR">Greece</option>
										<option class="selectoption" label="Greenland" value="GL">Greenland</option>
										<option class="selectoption" label="Grenada" value="GD">Grenada</option>
										<option class="selectoption" label="Guadeloupe" value="GP">Guadeloupe</option>
										<option class="selectoption" label="Guam" value="GU">Guam</option>
										<option class="selectoption" label="Guatemala" value="GT">Guatemala</option>
										<option class="selectoption" label="Guinea" value="GN">Guinea</option>
										<option class="selectoption" label="Guyana" value="GY">Guyana</option>
										<option class="selectoption" label="Haiti" value="HT">Haiti</option>
										<option class="selectoption" label="Honduras" value="HN">Honduras</option>
										<option class="selectoption" label="Hong Kong" value="HK">Hong Kong</option>
										<option class="selectoption" label="Hungary" value="HU">Hungary</option>
										<option class="selectoption" label="Iceland" value="IS">Iceland</option>
										<option class="selectoption" label="India" value="IN">India</option>
										<option class="selectoption" label="Indonesia" value="ID">Indonesia</option>
										<option class="selectoption" label="Iran" value="IR">Iran</option>
										<option class="selectoption" label="Iraq" value="IQ">Iraq</option>
										<option class="selectoption" label="Ireland" value="IE">Ireland</option>
										<option class="selectoption" label="Isle of Man" value="IM">Isle of Man</option>
										<option class="selectoption" label="Israel" value="IL">Israel</option>
										<option class="selectoption" label="Italy" value="IT">Italy</option>
										<option class="selectoption" label="Jamaica" value="JM">Jamaica</option>
										<option class="selectoption" label="Japan" value="JP">Japan</option>
										<option class="selectoption" label="Jordan" value="JO">Jordan</option>
										<option class="selectoption" label="Kazakhstan" value="KZ">Kazakhstan</option>
										<option class="selectoption" label="Kenya" value="KE">Kenya</option>
										<option class="selectoption" label="Kiribati" value="KI">Kiribati</option>
										<option class="selectoption" label="Korea North" value="KP">Korea North</option>
										<option class="selectoption" label="Korea South" value="KR">Korea South</option>
										<option class="selectoption" label="Kuwait" value="KW">Kuwait</option>
										<option class="selectoption" label="Kyrgyzstan" value="KG">Kyrgyzstan</option>
										<option class="selectoption" label="Laos" value="LA">Laos</option>
										<option class="selectoption" label="Latvia" value="LV">Latvia</option>
										<option class="selectoption" label="Lebanon" value="LB">Lebanon</option>
										<option class="selectoption" label="Lesotho" value="LS">Lesotho</option>
										<option class="selectoption" label="Liberia" value="LR">Liberia</option>
										<option class="selectoption" label="Libya" value="LY">Libya</option>
										<option class="selectoption" label="Liechtenstein" value="LI">Liechtenstein</option>
										<option class="selectoption" label="Lithuania" value="LT">Lithuania</option>
										<option class="selectoption" label="Luxembourg" value="LU">Luxembourg</option>
										<option class="selectoption" label="Macau" value="MO">Macau</option>
										<option class="selectoption" label="Macedonia" value="MK">Macedonia</option>
										<option class="selectoption" label="Madagascar" value="MG">Madagascar</option>
										<option class="selectoption" label="Malaysia" value="MY">Malaysia</option>
										<option class="selectoption" label="Malawi" value="MW">Malawi</option>
										<option class="selectoption" label="Maldives" value="MV">Maldives</option>

										<option class="selectoption" label="Mali" value="ML">Mali</option>
										<option class="selectoption" label="Malta" value="MT">Malta</option>
										<option class="selectoption" label="Marshall Islands" value="MH">Marshall Islands</option>
										<option class="selectoption" label="Martinique" value="MQ">Martinique</option>
										<option class="selectoption" label="Mauritania" value="MR">Mauritania</option>
										<option class="selectoption" label="Mauritius" value="MU">Mauritius</option>
										<option class="selectoption" label="Mayotte" value="YT">Mayotte</option>
										<option class="selectoption" label="Mexico" value="MX">Mexico</option>
										<option class="selectoption" label="Midway Islands" value="MP">Midway Islands</option>
										<option class="selectoption" label="Moldova" value="MD">Moldova</option>
										<option class="selectoption" label="Monaco" value="MC">Monaco</option>
										<option class="selectoption" label="Mongolia" value="MN">Mongolia</option>
										<option class="selectoption" label="Montserrat" value="MS">Montserrat</option>
										<option class="selectoption" label="Morocco" value="MA">Morocco</option>
										<option class="selectoption" label="Mozambique" value="MZ">Mozambique</option>
										<option class="selectoption" label="Myanmar" value="MM">Myanmar</option>
										<option class="selectoption" label="Nambia" value="NA">Nambia</option>
										<option class="selectoption" label="Nauru" value="NR">Nauru</option>
										<option class="selectoption" label="Nepal" value="NP">Nepal</option>
										<option class="selectoption" label="Netherland Antilles" value="NX">Netherland Antilles</option>
										<option class="selectoption" label="Netherlands" value="NL">Netherlands</option>
										<option class="selectoption" label="Nevis" value="NV">Nevis</option>
										<option class="selectoption" label="New Caledonia" value="NC">New Caledonia</option>
										<option class="selectoption" label="New Zealand" value="NZ">New Zealand</option>
										<option class="selectoption" label="Nicaragua" value="NI">Nicaragua</option>
										<option class="selectoption" label="Niger" value="NE">Niger</option>
										<option class="selectoption" label="Nigeria" value="NG">Nigeria</option>
										<option class="selectoption" label="Niue" value="NU">Niue</option>
										<option class="selectoption" label="Norfolk Island" value="NF">Norfolk Island</option>
										<option class="selectoption" label="Norway" value="NO">Norway</option>
										<option class="selectoption" label="Oman" value="OM">Oman</option>
										<option class="selectoption" label="Pakistan" value="PK">Pakistan</option>
										<option class="selectoption" label="Palau Island" value="PW">Palau Island</option>
										<option class="selectoption" label="Palestine" value="PS">Palestine</option>
										<option class="selectoption" label="Panama" value="PA">Panama</option>
										<option class="selectoption" label="Papua New Guinea" value="PG">Papua New Guinea</option>
										<option class="selectoption" label="Paraguay" value="PY">Paraguay</option>
										<option class="selectoption" label="Peru" value="PE">Peru</option>
										<option class="selectoption" label="Philippines" value="PH">Philippines</option>
										<option class="selectoption" label="Pitcairn Island" value="PN">Pitcairn Island</option>
										<option class="selectoption" label="Poland" value="PL">Poland</option>
										<option class="selectoption" label="Portugal" value="PT">Portugal</option>
										<option class="selectoption" label="Puerto Rico" value="PR">Puerto Rico</option>
										<option class="selectoption" label="Qatar" value="QA">Qatar</option>
										<option class="selectoption" label="Republic of Montenegro" value="RM">Republic of Montenegro</option>
										<option class="selectoption" label="Republic of Serbia" value="RS">Republic of Serbia</option>
										<option class="selectoption" label="Reunion" value="RE">Reunion</option>
										<option class="selectoption" label="Romania" value="RO">Romania</option>
										<option class="selectoption" label="Russia" value="RU">Russia</option>
										<option class="selectoption" label="Rwanda" value="RW">Rwanda</option>
										<option class="selectoption" label="St Barthelemy" value="BL">St Barthelemy</option>
										<option class="selectoption" label="St Eustatius" value="SS">St Eustatius</option>
										<option class="selectoption" label="St Helena" value="SH">St Helena</option>
										<option class="selectoption" label="St Kitts-Nevis" value="KN">St Kitts-Nevis</option>
										<option class="selectoption" label="St Lucia" value="LC">St Lucia</option>
										<option class="selectoption" label="St Maarten" value="MF ">St Maarten</option>
										<option class="selectoption" label="St Pierre &amp; Miquelon" value="PM">St Pierre &amp; Miquelon</option>
										<option class="selectoption" label="St Vincent &amp; Grenadines" value="VC">St Vincent &amp; Grenadines</option>
										<option class="selectoption" label="Saipan" value="ZT">Saipan</option>
										<option class="selectoption" label="Samoa" value="WS">Samoa</option>
										<option class="selectoption" label="Samoa American" value="SZ">Samoa American</option>
										<option class="selectoption" label="San Marino" value="SM">San Marino</option>
										<option class="selectoption" label="Sao Tome &amp; Principe" value="ST ">Sao Tome &amp; Principe</option>
										<option class="selectoption" label="Saudi Arabia" value="SA">Saudi Arabia</option>
										<option class="selectoption" label="Senegal" value="SN">Senegal</option>
										<option class="selectoption" label="Seychelles" value="SC">Seychelles</option>
										<option class="selectoption" label="Sierra Leone" value="SL">Sierra Leone</option>
										<option class="selectoption" label="Singapore" value="SG">Singapore</option>
										<option class="selectoption" label="Slovakia" value="SK">Slovakia</option>
										<option class="selectoption" label="Slovenia" value="SI">Slovenia</option>
										<option class="selectoption" label="Solomon Islands" value="SB">Solomon Islands</option>
										<option class="selectoption" label="Somalia" value="SO">Somalia</option>
										<option class="selectoption" label="South Africa" value="ZA">South Africa</option>
										<option class="selectoption" label="Spain" value="ES">Spain</option>
										<option class="selectoption" label="Sri Lanka" value="LK">Sri Lanka</option>
										<option class="selectoption" label="Sudan" value="SD">Sudan</option>
										<option class="selectoption" label="Suriname" value="SR">Suriname</option>
										<option class="selectoption" label="Swaziland" value="SZ">Swaziland</option>
										<option class="selectoption" label="Sweden" value="SE">Sweden</option>
										<option class="selectoption" label="Switzerland" value="CH">Switzerland</option>
										<option class="selectoption" label="Syria" value="SY">Syria</option>
										<option class="selectoption" label="Tahiti" value="TY">Tahiti</option>
										<option class="selectoption" label="Taiwan" value="TW">Taiwan</option>
										<option class="selectoption" label="Tajikistan" value="TJ">Tajikistan</option>
										<option class="selectoption" label="Tanzania" value="TZ">Tanzania</option>
										<option class="selectoption" label="Thailand" value="TH">Thailand</option>
										<option class="selectoption" label="Togo" value="TG">Togo</option>
										<option class="selectoption" label="Tokelau" value="TK">Tokelau</option>
										<option class="selectoption" label="Tonga" value="TO">Tonga</option>
										<option class="selectoption" label="Trinidad &amp; Tobago" value="TT">Trinidad &amp; Tobago</option>
										<option class="selectoption" label="Tunisia" value="TN">Tunisia</option>
										<option class="selectoption" label="Turkey" value="TR">Turkey</option>
										<option class="selectoption" label="Turkmenistan" value="TM">Turkmenistan</option>
										<option class="selectoption" label="Turks &amp; Caicos Is" value="TC">Turks &amp; Caicos Is</option>
										<option class="selectoption" label="Tuvalu" value="TV">Tuvalu</option>
										<option class="selectoption" label="Uganda" value="UG">Uganda</option>
										<option class="selectoption" label="Ukraine" value="UA">Ukraine</option>
										<option class="selectoption" label="United Arab Emirates" value="AE">United Arab Emirates</option>
										<option class="selectoption" label="United Kingdom" value="GB">United Kingdom</option>
										<option class="selectoption" label="Uruguay" value="UY">Uruguay</option>
										<option class="selectoption" label="Uzbekistan" value="UZ">Uzbekistan</option>
										<option class="selectoption" label="Vanuatu" value="VU">Vanuatu</option>
										<option class="selectoption" label="Vatican City State" value="VA">Vatican City State</option>
										<option class="selectoption" label="Venezuela" value="VE">Venezuela</option>
										<option class="selectoption" label="Vietnam" value="VN">Vietnam</option>
										<option class="selectoption" label="Virgin Islands (Brit)" value="VG">Virgin Islands &#40;Brit&#41;</option>
										<option class="selectoption" label="Virgin Islands (USA)" value="VI">Virgin Islands &#40;USA&#41;</option>
										<option class="selectoption" label="Wake Island" value="WI">Wake Island</option>
										<option class="selectoption" label="Wallis &amp; Futana Is" value="WF">Wallis &amp; Futana Is</option>
										<option class="selectoption" label="Yemen" value="YE">Yemen</option>
										<option class="selectoption" label="Zaire" value="ZA">Zaire</option>
										<option class="selectoption" label="Zambia" value="ZM">Zambia</option>
										<option class="selectoption" label="Zimbabwe" value="ZW">Zimbabwe</option>
									</select>
								</div>
								<div class="clear"></div>
							</div>
							<div class="formfield labels-floated gender">
								<div class="label">
									<span class="labeltext">Хүйс</span>
								</div>
								<div class="value">
									<select class="selectbox" id="dwfrm_profile_customer_gender" name="gender">
										<option class="selectoption" label="Эм" value="Эм">Эм</option>
										<option class="selectoption" label="Эр" value="Эр">Эр</option>
									</select>
								</div>
								<div class="clear"></div>
							</div>
						</div>
					</fieldset>
					<div class="formactions">
						<button class="button button--large" type="submit" value="Apply" name="dwfrm_profile_confirm" style=" margin-left: 10px; width: 393px; height: 40px"><span>Бүртгүүлэх</span></button>
					</div>
				</form>
			</div>	
		</div>
	</div>
</div>
@endsection
@section('customscripts')
@endsection