<?php

use Illuminate\Database\Seeder;

class JobSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('jobs')->insert([
        	'dep_id' => 1,
        	'name'=>'Нэхмэлийн технологич инженер',
            'shaardlaga' => '<h3>Шаардлага:&nbsp;</h3>',
        	'begin_date' => '2018/03/01',
        	'end_date' => '2018/03/31'
        ]);
        DB::table('jobs')->insert([
        	'dep_id' => 1,
        	'name'=>'Худалдааны зөвлөх /Англи хэлтэй/',
            'shaardlaga' => '<h3>Шаардлага:&nbsp;</h3>',
        	'begin_date' => '2018/03/01',
        	'end_date' => '2018/03/31'
        ]);
        DB::table('jobs')->insert([
        	'dep_id' => 1,
        	'name'=>'Хаг түүгч',
            'shaardlaga' => '<h3>Шаардлага:&nbsp;</h3>',
        	'begin_date' => '2018/03/01',
        	'end_date' => '2018/03/31'
        ]);
        DB::table('jobs')->insert([
        	'dep_id' => 1,
        	'name'=>'Чанар шалгагч',
            'shaardlaga' => '<h3>Шаардлага:&nbsp;</h3>',
        	'begin_date' => '2018/03/01',
        	'end_date' => '2018/03/31'
        ]);
        DB::table('jobs')->insert([
        	'dep_id' => 1,
        	'name'=>'Кеттель машин баригч',
            'shaardlaga' => '<h3>Шаардлага:&nbsp;</h3>',
        	'begin_date' => '2018/03/01',
        	'end_date' => '2018/03/31'
        ]);
        DB::table('jobs')->insert([
        	'dep_id' => 1,
        	'name'=>'Оёдолчин',
            'shaardlaga' => '<h3>Шаардлага:&nbsp;</h3>',
        	'begin_date' => '2018/03/01',
        	'end_date' => '2018/03/31'
        ]);
    }
}
