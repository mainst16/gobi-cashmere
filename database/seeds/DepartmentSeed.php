<?php

use Illuminate\Database\Seeder;

class DepartmentSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('deps')->insert([
        	'name'=>'Онцлох ажлын байр'
        ]);
        DB::table('deps')->insert([
        	'name'=>'Маркетинг/ Борлуулалт'
        ]);
        DB::table('deps')->insert([
        	'name'=>'Санхүү/ Хангамж'
        ]);
        DB::table('deps')->insert([
        	'name'=>'Мэдээллийн технологи/ Харилцаа холбоо'
        ]);
        DB::table('deps')->insert([
        	'name'=>'Хүний нөөц/ Захиргаа'
        ]);
        DB::table('deps')->insert([
        	'name'=>'Үйлдвэрлэл/ Түгээлт/ Тээвэр'
        ]);
        DB::table('deps')->insert([
        	'name'=>'Үйлчилгээ'
        ]);
    }
}
