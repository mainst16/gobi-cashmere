<?php

use Illuminate\Database\Seeder;

class ProductsSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products')->insert([
        	'pt_id'=>'1',
        	'brand_id'=>'1',
        	'price'=>'1200000',
        	'name'=>'Пальто',
        	'color'=>'02',
        	'style_no'=>'c02',
        	'pics'=>'G1-15CH12-50-1BC(1).jpg',
        	'desc'=>'<p>Mens Cashmere Bomber Style Cardigan in a deep shade of blue - made in Scotland by Johnstons of Elgin.</p>
                    <p>The bomber cardigan is made from 100% cashmere, soft, luxurious and effortlessly stylish.</p>
                    <p>100% Cashmere</p>
                    <p>4 ply</p>
                    <p>Set-in sleeve, shaped shoulder</p>
                    <p>Jacket Detail &amp; Pockets</p>
                    <p>Double Rib Trims</p>
                    <p>Size: XS S M L XL XXL</p>
                    <p>Length: 69cm / 27"</p>
                    <p>Model is 6ft1" / UK40 and wears size S</p>
                    <p><a class="”prod-page-desc-link”" href="#">Hand Wash</a></p>',
            'size'=>'<div id="sizeguidetable">
                            <tr>
                                <td>36</td>
                                <td>38</td>
                                <td>40</td>
                                <td>42</td>
                                <td>44</td>
                                <td>46</td>
                                <td>48</td>
                                <td>50</td>
                                <td>52</td>
                            </tr>
                            <tr>
                                <td>XS</td>
                                <td>S</td>
                                <td></td>
                                <td>M</td>
                                <td></td>
                                <td>L</td>
                                <td>XL</td>
                                <td>XXL</td>
                                <td></td>
                            </tr>
                        </div>
                        <p><span id="nosize"></span>
                            <br>
                            Please note measurements are in inches, and can vary slightly from garment to garment due to our hand finishing processes.
                            <br>
                            <br>
                            If you require any further information, contact <a href="#">www.gobi.mn</a>
                        </p>',
            'clearing'=>'<h4>Look after your fabric</h4>
                    <h4>Lavish a little love on your favourite things</h4>
                    <p>It only takes a little extra care to make sure your Johnstons of Elgin cashmere keeps its trademark softness for many, many years to come.</p>
                    <p>&nbsp;</p>
                    <h4>Wearing your cashmere for the first time</h4>
                    <p>
                        When you first wear your new garment you may find small balls of fibre or "pills" forming on the surface. These are not a fault or indication of inferior quality but are simply loose fibres tangling together where the garment has been rubbing. They can easily be removed by hand or with our <a title="Cashmere De-pilling Comb" href="#">cashmere de-pilling comb</a>. Once removed, your garment will actually consolidate and become softer. You can browse our full collection of care products <a title="Cashmere Care" href="#">here</a>.
                    </p>
                    <p>&nbsp;</p>
                    <h4>Handwashing and dry-cleaning</h4>
                    <p>
                        Cashmere knitwear can be hand washed or dry-cleaned. We recommend hand washing your knitwear following these simple guidelines and dry cleaning woven items such as scarves or blankets.
                    </p>
                    <p>&nbsp;</p>
                    <p>
                        Hand wash in lukewarm water (35°C) using Johnstons Cashmere Shampoo. Gently squeeze the suds through the fabric, taking care not to rub the garment too much as this may cause shrinkage and increase pilling. Rinse the garment in cool clean water, squeezing gently without twisting or wringing. A short light spin in a spin dryer will help remove most of the water.
                    </p>
                    <p>&nbsp;</p>
                    <p>
                        Smooth the garment back into shape, gently coaxing any facings or trims back to the correct size, and dry naturally away from direct heat such as radiators and sunlight. Ideally the garment should be dried flat on a clean towel. When dry, press lightly with a cool iron.
                    </p>
                    <p>&nbsp;</p>
                    <p>
                        Under no circumstances should the garment be tumble-dried.
                    </p>
                    <p>&nbsp;</p>
                    <p>
                        Following these instructions, your garment will preserve its shape and luxurious touch for many years - the hallmarks of the very best cashmere.
                    </p>  '
        ]);

        DB::table('products')->insert([
            'pt_id'=>'5',
            'brand_id'=>'3',
            'price'=>'1200000',
            'name'=>'Фэйшн Пальто / Fashion coat',
            'color'=>'02',
            'style_no'=>'c02',
            'pics'=>'products/May2018/_MG_4472.png',
            'desc'=>'desc',
            'size'=>'size',
            'clearing'=>'clearing'
        ]);

        DB::table('products')->insert([
            'pt_id'=>'5',
            'brand_id'=>'3',
            'price'=>'1200000',
            'name'=>'Фэйшн Пальто / Fashion coat',
            'color'=>'02',
            'style_no'=>'c02',
            'pics'=>'products/May2018/_MG_4800.png',
            'desc'=>'desc',
            'size'=>'size',
            'clearing'=>'clearing'
        ]);

        DB::table('products')->insert([
            'pt_id'=>'5',
            'brand_id'=>'3',
            'price'=>'1200000',
            'name'=>'Фэйшн Пальто / Fashion coat',
            'color'=>'02',
            'style_no'=>'c02',
            'pics'=>'products/May2018/_MG_4877.png',
            'desc'=>'desc',
            'size'=>'size',
            'clearing'=>'clearing'
        ]);

        DB::table('products')->insert([
            'pt_id'=>'5',
            'brand_id'=>'3',
            'price'=>'1200000',
            'name'=>'Фэйшн Пальто / Fashion coat',
            'color'=>'02',
            'style_no'=>'c02',
            'pics'=>'products/May2018/_MG_5151.png',
            'desc'=>'desc',
            'size'=>'size',
            'clearing'=>'clearing'
        ]);

        DB::table('products')->insert([
            'pt_id'=>'5',
            'brand_id'=>'3',
            'price'=>'1200000',
            'name'=>'Фэйшн Пальто / Fashion coat',
            'color'=>'02',
            'style_no'=>'c02',
            'pics'=>'products/May2018/_MG_5303.png',
            'desc'=>'desc',
            'size'=>'size',
            'clearing'=>'clearing'
        ]);

        DB::table('products')->insert([
            'pt_id'=>'5',
            'brand_id'=>'3',
            'price'=>'1200000',
            'name'=>'Фэйшн Пальто / Fashion coat',
            'color'=>'02',
            'style_no'=>'c02',
            'pics'=>'products/May2018/_MG_5367.png',
            'desc'=>'desc',
            'size'=>'size',
            'clearing'=>'clearing'
        ]);

        DB::table('products')->insert([
            'pt_id'=>'5',
            'brand_id'=>'3',
            'price'=>'1200000',
            'name'=>'Фэйшн Пальто / Fashion coat',
            'color'=>'02',
            'style_no'=>'c02',
            'pics'=>'products/May2018/_MG_5453.png',
            'desc'=>'desc',
            'size'=>'size',
            'clearing'=>'clearing'
        ]);

        DB::table('products')->insert([
            'pt_id'=>'5',
            'brand_id'=>'3',
            'price'=>'1200000',
            'name'=>'Фэйшн Пальто / Fashion coat',
            'color'=>'02',
            'style_no'=>'c02',
            'pics'=>'products/May2018/_MG_5553.png',
            'desc'=>'desc',
            'size'=>'size',
            'clearing'=>'clearing'
        ]);

        DB::table('products')->insert([
            'pt_id'=>'5',
            'brand_id'=>'3',
            'price'=>'1200000',
            'name'=>'Фэйшн Пальто / Fashion coat',
            'color'=>'02',
            'style_no'=>'c02',
            'pics'=>'products/May2018/_MG_5633.png',
            'desc'=>'desc',
            'size'=>'size',
            'clearing'=>'clearing'
        ]);

        DB::table('products')->insert([
            'pt_id'=>'5',
            'brand_id'=>'3',
            'price'=>'1200000',
            'name'=>'Фэйшн Пальто / Fashion coat',
            'color'=>'02',
            'style_no'=>'c02',
            'pics'=>'products/May2018/_MG_5660.png',
            'desc'=>'desc',
            'size'=>'size',
            'clearing'=>'clearing'
        ]);

        DB::table('products')->insert([
            'pt_id'=>'5',
            'brand_id'=>'3',
            'price'=>'1200000',
            'name'=>'Фэйшн Пальто / Fashion coat',
            'color'=>'02',
            'style_no'=>'c02',
            'pics'=>'products/May2018/_MG_5769.png',
            'desc'=>'desc',
            'size'=>'size',
            'clearing'=>'clearing'
        ]);

        DB::table('products')->insert([
            'pt_id'=>'5',
            'brand_id'=>'3',
            'price'=>'1200000',
            'name'=>'Фэйшн Пальто / Fashion coat',
            'color'=>'02',
            'style_no'=>'c02',
            'pics'=>'products/May2018/_MG_5881.png',
            'desc'=>'desc',
            'size'=>'size',
            'clearing'=>'clearing'
        ]);

        DB::table('products')->insert([
            'pt_id'=>'5',
            'brand_id'=>'3',
            'price'=>'1200000',
            'name'=>'Фэйшн Пальто / Fashion coat',
            'color'=>'02',
            'style_no'=>'c02',
            'pics'=>'products/May2018/_MG_6238.png',
            'desc'=>'desc',
            'size'=>'size',
            'clearing'=>'clearing'
        ]);

        DB::table('products')->insert([
            'pt_id'=>'5',
            'brand_id'=>'3',
            'price'=>'1200000',
            'name'=>'Фэйшн Пальто / Fashion coat',
            'color'=>'02',
            'style_no'=>'c02',
            'pics'=>'products/May2018/_MG_6429.png',
            'desc'=>'desc',
            'size'=>'size',
            'clearing'=>'clearing'
        ]);

        DB::table('products')->insert([
            'pt_id'=>'5',
            'brand_id'=>'3',
            'price'=>'1200000',
            'name'=>'Фэйшн Пальто / Fashion coat',
            'color'=>'02',
            'style_no'=>'c02',
            'pics'=>'products/May2018/_MG_6501.png',
            'desc'=>'desc',
            'size'=>'size',
            'clearing'=>'clearing'
        ]);

        DB::table('products')->insert([
            'pt_id'=>'5',
            'brand_id'=>'3',
            'price'=>'1200000',
            'name'=>'Фэйшн Пальто / Fashion coat',
            'color'=>'02',
            'style_no'=>'c02',
            'pics'=>'products/May2018/_MG_6506.png',
            'desc'=>'desc',
            'size'=>'size',
            'clearing'=>'clearing'
        ]);

        DB::table('products')->insert([
            'pt_id'=>'5',
            'brand_id'=>'3',
            'price'=>'1200000',
            'name'=>'Фэйшн Пальто / Fashion coat',
            'color'=>'02',
            'style_no'=>'c02',
            'pics'=>'products/May2018/_MG_6547.png',
            'desc'=>'desc',
            'size'=>'size',
            'clearing'=>'clearing'
        ]);

        DB::table('products')->insert([
            'pt_id'=>'5',
            'brand_id'=>'3',
            'price'=>'1200000',
            'name'=>'Фэйшн Пальто / Fashion coat',
            'color'=>'02',
            'style_no'=>'c02',
            'pics'=>'products/May2018/_MG_6624.png',
            'desc'=>'desc',
            'size'=>'size',
            'clearing'=>'clearing'
        ]);

        DB::table('products')->insert([
            'pt_id'=>'5',
            'brand_id'=>'3',
            'price'=>'1200000',
            'name'=>'Фэйшн Пальто / Fashion coat',
            'color'=>'02',
            'style_no'=>'c02',
            'pics'=>'products/May2018/_MG_6647.png',
            'desc'=>'desc',
            'size'=>'size',
            'clearing'=>'clearing'
        ]);

        DB::table('products')->insert([
            'pt_id'=>'5',
            'brand_id'=>'3',
            'price'=>'1200000',
            'name'=>'Фэйшн Пальто / Fashion coat',
            'color'=>'02',
            'style_no'=>'c02',
            'pics'=>'products/May2018/_MG_6677.png',
            'desc'=>'desc',
            'size'=>'size',
            'clearing'=>'clearing'
        ]);

        DB::table('products')->insert([
            'pt_id'=>'5',
            'brand_id'=>'3',
            'price'=>'1200000',
            'name'=>'Фэйшн Пальто / Fashion coat',
            'color'=>'02',
            'style_no'=>'c02',
            'pics'=>'products/May2018/_MG_6743.png',
            'desc'=>'desc',
            'size'=>'size',
            'clearing'=>'clearing'
        ]);

        DB::table('products')->insert([
            'pt_id'=>'5',
            'brand_id'=>'3',
            'price'=>'1200000',
            'name'=>'Фэйшн Пальто / Fashion coat',
            'color'=>'02',
            'style_no'=>'c02',
            'pics'=>'products/May2018/_MG_6832.png',
            'desc'=>'desc',
            'size'=>'size',
            'clearing'=>'clearing'
        ]);

        DB::table('products')->insert([
            'pt_id'=>'5',
            'brand_id'=>'3',
            'price'=>'1200000',
            'name'=>'Фэйшн Пальто / Fashion coat',
            'color'=>'02',
            'style_no'=>'c02',
            'pics'=>'products/May2018/_MG_6917.png',
            'desc'=>'desc',
            'size'=>'size',
            'clearing'=>'clearing'
        ]);

        DB::table('products')->insert([
            'pt_id'=>'5',
            'brand_id'=>'3',
            'price'=>'1200000',
            'name'=>'Фэйшн Пальто / Fashion coat',
            'color'=>'02',
            'style_no'=>'c02',
            'pics'=>'products/May2018/_MG_6997.png',
            'desc'=>'desc',
            'size'=>'size',
            'clearing'=>'clearing'
        ]);

        DB::table('products')->insert([
            'pt_id'=>'5',
            'brand_id'=>'3',
            'price'=>'1200000',
            'name'=>'Фэйшн Пальто / Fashion coat',
            'color'=>'02',
            'style_no'=>'c02',
            'pics'=>'products/May2018/_MG_7012.png',
            'desc'=>'desc',
            'size'=>'size',
            'clearing'=>'clearing'
        ]);

        DB::table('products')->insert([
            'pt_id'=>'5',
            'brand_id'=>'3',
            'price'=>'1200000',
            'name'=>'Фэйшн Пальто / Fashion coat',
            'color'=>'02',
            'style_no'=>'c02',
            'pics'=>'products/May2018/_MG_7060.png',
            'desc'=>'desc',
            'size'=>'size',
            'clearing'=>'clearing'
        ]);

        DB::table('products')->insert([
            'pt_id'=>'5',
            'brand_id'=>'3',
            'price'=>'1200000',
            'name'=>'Фэйшн Пальто / Fashion coat',
            'color'=>'02',
            'style_no'=>'c02',
            'pics'=>'products/May2018/_MG_7074.png',
            'desc'=>'desc',
            'size'=>'size',
            'clearing'=>'clearing'
        ]);

        DB::table('products')->insert([
            'pt_id'=>'5',
            'brand_id'=>'3',
            'price'=>'1200000',
            'name'=>'Фэйшн Пальто / Fashion coat',
            'color'=>'02',
            'style_no'=>'c02',
            'pics'=>'products/May2018/_MG_7181.png',
            'desc'=>'desc',
            'size'=>'size',
            'clearing'=>'clearing'
        ]);

        DB::table('products')->insert([
            'pt_id'=>'5',
            'brand_id'=>'3',
            'price'=>'1200000',
            'name'=>'Фэйшн Пальто / Fashion coat',
            'color'=>'02',
            'style_no'=>'c02',
            'pics'=>'products/May2018/_MG_7235.png',
            'desc'=>'desc',
            'size'=>'size',
            'clearing'=>'clearing'
        ]);

        DB::table('products')->insert([
            'pt_id'=>'5',
            'brand_id'=>'3',
            'price'=>'1200000',
            'name'=>'Фэйшн Пальто / Fashion coat',
            'color'=>'02',
            'style_no'=>'c02',
            'pics'=>'products/May2018/_MG_7295.png',
            'desc'=>'desc',
            'size'=>'size',
            'clearing'=>'clearing'
        ]);

        DB::table('products')->insert([
            'pt_id'=>'5',
            'brand_id'=>'3',
            'price'=>'1200000',
            'name'=>'Фэйшн Пальто / Fashion coat',
            'color'=>'02',
            'style_no'=>'c02',
            'pics'=>'products/May2018/_MG_7347.png',
            'desc'=>'desc',
            'size'=>'size',
            'clearing'=>'clearing'
        ]);

        DB::table('products')->insert([
            'pt_id'=>'5',
            'brand_id'=>'3',
            'price'=>'1200000',
            'name'=>'Фэйшн Пальто / Fashion coat',
            'color'=>'02',
            'style_no'=>'c02',
            'pics'=>'products/May2018/_MG_7409.png',
            'desc'=>'desc',
            'size'=>'size',
            'clearing'=>'clearing'
        ]);

        DB::table('products')->insert([
            'pt_id'=>'5',
            'brand_id'=>'3',
            'price'=>'1200000',
            'name'=>'Фэйшн Пальто / Fashion coat',
            'color'=>'02',
            'style_no'=>'c02',
            'pics'=>'products/May2018/_MG_7463.png',
            'desc'=>'desc',
            'size'=>'size',
            'clearing'=>'clearing'
        ]);

        DB::table('products')->insert([
            'pt_id'=>'5',
            'brand_id'=>'3',
            'price'=>'1200000',
            'name'=>'Фэйшн Пальто / Fashion coat',
            'color'=>'02',
            'style_no'=>'c02',
            'pics'=>'products/May2018/_MG_7565.png',
            'desc'=>'desc',
            'size'=>'size',
            'clearing'=>'clearing'
        ]);

        DB::table('products')->insert([
            'pt_id'=>'1',
            'brand_id'=>'3',
            'price'=>'1200000',
            'name'=>'Фэйшн Пальто / Fashion coat',
            'color'=>'02',
            'style_no'=>'c02',
            'pics'=>'products/May2018/_MG_4690.png',
            'desc'=>'desc',
            'size'=>'size',
            'clearing'=>'clearing'
        ]);
        DB::table('products')->insert([
            'pt_id'=>'1',
            'brand_id'=>'3',
            'price'=>'1200000',
            'name'=>'Фэйшн Пальто / Fashion coat',
            'color'=>'02',
            'style_no'=>'c02',
            'pics'=>'products/May2018/_MG_4832.png',
            'desc'=>'desc',
            'size'=>'size',
            'clearing'=>'clearing'
        ]);
        DB::table('products')->insert([
            'pt_id'=>'1',
            'brand_id'=>'3',
            'price'=>'1200000',
            'name'=>'Фэйшн Пальто / Fashion coat',
            'color'=>'02',
            'style_no'=>'c02',
            'pics'=>'products/May2018/_MG_4953.png',
            'desc'=>'desc',
            'size'=>'size',
            'clearing'=>'clearing'
        ]);
        DB::table('products')->insert([
            'pt_id'=>'1',
            'brand_id'=>'3',
            'price'=>'1200000',
            'name'=>'Фэйшн Пальто / Fashion coat',
            'color'=>'02',
            'style_no'=>'c02',
            'pics'=>'products/May2018/_MG_5280.png',
            'desc'=>'desc',
            'size'=>'size',
            'clearing'=>'clearing'
        ]);
        DB::table('products')->insert([
            'pt_id'=>'1',
            'brand_id'=>'3',
            'price'=>'1200000',
            'name'=>'Фэйшн Пальто / Fashion coat',
            'color'=>'02',
            'style_no'=>'c02',
            'pics'=>'products/May2018/_MG_5850.png',
            'desc'=>'desc',
            'size'=>'size',
            'clearing'=>'clearing'
        ]);
        DB::table('products')->insert([
            'pt_id'=>'1',
            'brand_id'=>'3',
            'price'=>'1200000',
            'name'=>'Фэйшн Пальто / Fashion coat',
            'color'=>'02',
            'style_no'=>'c02',
            'pics'=>'products/May2018/_MG_5965.png',
            'desc'=>'desc',
            'size'=>'size',
            'clearing'=>'clearing'
        ]);
        DB::table('products')->insert([
            'pt_id'=>'1',
            'brand_id'=>'3',
            'price'=>'1200000',
            'name'=>'Фэйшн Пальто / Fashion coat',
            'color'=>'02',
            'style_no'=>'c02',
            'pics'=>'products/May2018/_MG_6026.png',
            'desc'=>'desc',
            'size'=>'size',
            'clearing'=>'clearing'
        ]);
        DB::table('products')->insert([
            'pt_id'=>'1',
            'brand_id'=>'3',
            'price'=>'1200000',
            'name'=>'Фэйшн Пальто / Fashion coat',
            'color'=>'02',
            'style_no'=>'c02',
            'pics'=>'products/May2018/_MG_6032.png',
            'desc'=>'desc',
            'size'=>'size',
            'clearing'=>'clearing'
        ]);
        DB::table('products')->insert([
            'pt_id'=>'1',
            'brand_id'=>'3',
            'price'=>'1200000',
            'name'=>'Фэйшн Пальто / Fashion coat',
            'color'=>'02',
            'style_no'=>'c02',
            'pics'=>'products/May2018/_MG_6115.png',
            'desc'=>'desc',
            'size'=>'size',
            'clearing'=>'clearing'
        ]);
        DB::table('products')->insert([
            'pt_id'=>'1',
            'brand_id'=>'3',
            'price'=>'1200000',
            'name'=>'Фэйшн Пальто / Fashion coat',
            'color'=>'02',
            'style_no'=>'c02',
            'pics'=>'products/May2018/_MG_6208.png',
            'desc'=>'desc',
            'size'=>'size',
            'clearing'=>'clearing'
        ]);
    }
}

